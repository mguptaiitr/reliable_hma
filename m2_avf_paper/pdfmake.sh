if [ $1 == "clean" ] 
then
    echo "cleaning tex"
    echo "rm -rf *log main.aux main.blg main.out main.pdf main.bbl"
    rm -rf *log main.aux main.blg main.out main.pdf main.bbl
elif [$1 == "pdf" ]
then
    echo "cleaning and building building"
    rm -rf *log main.aux main.blg main.out main.pdf main.bbl
    pdflatex main.tex
    bibtex main.aux
    pdflatex main.tex
    pdflatex main.tex
else
    echo "clean or make"
fi
