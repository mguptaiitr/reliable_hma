\section{Experimental Methodology} \label{sec:expMethod}
We used FaultSim \cite{faultsimDave}  and modified Ramulator \cite{ramulator} to evaluate reliability and performance trade-offs. \\

\subsection{Lifetime, Hit Rate, and Reliability} \label{sec:lifeHitRateReliability}
FaultSim is an event-based DRAM fault simulator developed and published by Nair et al. \cite{faultsimDave}. The simulation framework uses DRAM FIT rate data from the work published by Shridharan et al. \cite{dramVilas}. FaultSim's event-based design and real-world failure statistics make it a fast and accurate alternative for our reliability studies. A user provided number of simulations are run. In each simulation, a fault is inserted in a bit, word, column, row, or bank based on their FIT rates, error-correction scheme is applied, and the outcome is recorded as detected, corrected, or uncorrected error. Faults can be transient or permanent, and an error occurs when the failed bit is used in the software. Every DRAM fault does not necessarily result in an error, e.g. a failed DRAM bit accessed and transferred to the cache but it's never used. FaultSim assumes that every DRAM fault results in an error, which is a conservative assumption and provides an upper error-bound for our studies.  The faults are inserted to simulate a time duration of 348 weeks (approximately 7 years). FaultSim reports the probability of detected, corrected, and uncorrected errors. We will be using probability of uncorrected errors to measure the failure probabilities of heterogeneous memory architectures.

FaultSim simulates SECDED and ChipKill error codes. SECDED and ChipKill have different starting reliabilities and aging curves, as shown in the FaultSim paper. We simulate a heterogeneous memory architecture with level one (M1) memory equipped with SECDED and level two (M2) with ChipKill. Where SECDED provides lower complexity and power, and ChipKill provides higher reliability.

On-chip memories may provide a weaker reliability using schemes such as parity-based detection in order to reduce latency and capacity overhead. Hence, assuming SECDED for an HBM is a realistic assumption for today's HMA. Moreover, HBM's eight times higher density, and newer failure modes such as fault through-silicon vias (TSV) also increase its failure probabilities. In absence of field study data on failure rates of HBM, Citadel \cite{citadel} scales FIT rates from the real-world DRAM field study \cite{dramVilas}, and performs a sensitivity study by sweeping FIT rate for TSV failures. In this work, we illustrate the problem arising from different reliability levels of on-chip and off-package memories. Scaling HBM's FIT rate by eight times, and including newer failure modes is only going to increase the gap in the reliability levels and make our argument stronger. Hence, we demonstrate using SECDED and ChipKill-based memories for the reliability evaluation of two level heterogeneous memory architecture system, for which we have the real-world failure data.

%However, to conduct performance-reliability bound study and illustrate the problem arising form different reliability levels of on-chip and off-package memories, scaling FIT rate by eight times, and including newer failure modes is only going to increase the gap in the reliability levels and make our argument stronger. 

%(Edited out) However, to illustrate the argument we make in this paper, which is to incorporate reliability into data placement policy for heterogeneous memories with varying reliability level, scaling FIT rate by eight times, and including newer failure modes is only going to increase the gap in the reliability levels and make our argument stronger. Hence, we demonstrate using SECDED-based and ChipKill-based memories for the reliability evaluation of two level heterogeneous memory architecture system, for which we have the real-world failure data.

%We ran our fault simulations on an AMD A10-7850K machine, and we could complete simulations, for both SECDED and ChipKill within a minute. We ran the number of simulations to match the counts in the FaultSim paper.

Failure probabilities of a heterogeneous memory system in the week $w$, $FP_{HMA}[w]$, can be computed using equations \ref{equ:hitrate} and \ref{equ:fitrate}. Equation \ref{equ:hitrate} defines the hit rate as the ratio of number of accesses to the level 1 memory $M1$ over the total number of access to the memory.  Given the failure probabilities of the individual memories in the week $w$, as $FP_{M2}[w]$  and $FP_{M1}[w]$, the failure probability of the overall system can be computed using equation \ref{equ:fitrate}. Where \kh and \kd are the scaling factors to account for the difference in densities and geometries of $M1$ and $M2$ (e.g. 8 Gb high density on-chip vs. 1 Gb DRAM memory capacity per die, and different number of channels, rank, bank etc.). \kh and \kd will be equal to one for $M1$ and $M2$ with the same geometry and densities. We used \kh and \kd values as one, as discussed in the last paragraph. Hence, our numbers are proportional to that of in the equation \ref{equ:fitrate}. 


\begin{equation}
Hit\,Rate =  \frac{(Number \, of \, M1 \, Access)}{(Total \, Number \, of \,Access)}
\label{equ:hitrate}
\end{equation}


\begin{equation}
\begin{split}
FP_{HMA}[w] =   \mathbf{K_{1}} \times (Hit\,Rate \times FP_{M1}[w]) \\ \; + \,  \mathbf{K_{2}} \times (1 - Hit\,Rate) \times FP_{M2}[w]
\end{split}
\label{equ:fitrate}
\end{equation}

\begin{figure}
  \centering
    \includegraphics[width=0.45\textwidth]{figs/FIT_TIME_HIT.pdf}
    \caption{Failure probability of an HMA as function of hit rate and time in weeks. $\mathbf{FP_{HMA}}$: Failure probability of HMA}
    \label{fig:FIT_TIME_HIT}
\end{figure}

The surface in figure \ref{fig:FIT_TIME_HIT} shows the overall failure probability of an HMA system, $FP_{HMA}$, on the z-axis. We will refer to this surface as the failure surface. The x-axis and y-axis represent hit rate to M1 and time in weeks, respectively. Line 1 on the failure surface shows the variation of failure probability in a fixed week, and line 2 shows the variation of failure probability with a fixed hit rate over week 0 to 348. The failure surface is constructed by moving line 1 and line 2 over week 0 to 348 and varying hit rate from zero to one, respectively. The slope of line 1 increases as the system ages starting from week 0 to week 348, and the slope of line 2 increases with the increase in the hit rate. Failure probability increases with increase in hit rate because at the higher hit rate, the system makes more accesses to the less reliable M1 memory. The increase in failure probability as the weeks progresses is because of the accumulation of faults. The faults accumulated over weeks 0 to n-1 make the system more vulnerable to encounter an uncorrectable error in week n. For a hit rate of one, $FP_{HMA}$ increases at the fastest rate as the system ages, and reaches the worst failure probability value of $0.0372$ in week 348. The minimum failure probability is in the week zero with a hit rate of zero, $2.7\times10^{-5}$. Hence, when left uncontrolled, failure probability can change by a factor of 1377.77x over the system's lifetime. We don't have control over time. However, using the result of our bound study we propose a system that reduces the hit rate to M1 memory as it ages in order to keep the failure probability under acceptable limits, as shown by the aging-aware hit rate policy line in section \ref{sec:discuss}.

%(Edited out) We don't have control over time. However, as the system ages we can adjust the data placement policy to control the hit rate. We propose an adaptive data placement policy that strives for a perfect hit rate in the initial years of the system deployment and adjust it every few weeks as the system ages, keeping the overall failure probability on the reliability-aware hit rate policy line on the failure surface, as show in figure \ref{fig:FIT_TIME_HIT}.


\subsection{Workloads, Hit Rate, and Performance}
%%Insert memory figure here
\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{./figs/mcf.pdf}
\caption{Left y-axis: Performance of mcf, astar, \& cactus vs. hit rate. Right y-axis: FP = Failure Probability in week 0, 180 and 348 vs. hit rate.}
\label{fig:mcf}
\end{figure}

%%Insert hitrate ipc
%\begin{figure}
%\centering
%\includegraphics[width=0.45\textwidth]{./figs/HITRATE_IPC.pdf}
%\caption{Performance vs hit rate for mcf, astar, and cactus benchmarks.}
%\label{fig:mcf}
%\end{figure}

%OLD figure with 3 sub figures
%\begin{figure*}[t]
%\centering
%\includegraphics[height=2.3in,width=7in]{./figs/mcf.pdf}
%\caption{Performance and failure probability for mcf benchmark in week 0, 180 and 348.}
%\label{fig:mcf}
%\end{figure*}

Placing more heavily accessed pages in DDR rather than HBM memory improves reliability at a cost of performance. HBM and DDR3 have comparable latencies of 40ns and 45ns, respectively \cite{HBM_JEDEC, DDR3_JEDEC}. However, HBM provides 2X-8X higher bandwidth compared to DDR memory, depending on the organization. Hence, using one memory over another also has performance implications that varies by workload. In this subsection, we evaluate the effect of hit rate on the performance of three characteristic workloads (low, medium and high bandwidth) using Ramulator \cite{ramulator}.

Ramulator is a DRAM simulator providing cycle-accurate performance models for different memory standards such as DDR3/4, LPDDR3/4, GDDR5 and HBM. In trace-driven mode, the simulator takes an input trace file. The trace file has a number of non-memory instructions, memory address, and request type (read or write) for every memory request. The current version of Ramulator can simulate only one level of memory. We extended Ramulator to simulate two levels of heterogeneous memory architectures. We simulated a 16-core system with HBM as on-chip memory and DDR3 as off package memory. The complete CPU and memory configurations are in table 1. Memory traces were generated using PinPlay \cite{pintool} and cache filtering was done using Moola \cite{moola}.


%To vary hit rate we ran the simulations with a varying HBM capacity, 64MB to 4GB. A larger HBM capacity is expected to result in a higher hit rate, e.g. cactus, see table \ref{tab:workloads}, touches 2.31 GB of total memory. Hence, an HBM size of 2.31 GB or higher should certainly result in a perfect hit rate. However, capacity of HBM is not directly proportional to the hit rate. An HBM of size 64 MB could result in a close to perfect hit rate, if 64MB can fit all the hot pages of a workload. Hence, a small HBM memory could result in higher hit rate provided initial data placement is done based on hot page selection. We chose a stride-based initial data placement policy, and there is no migration in between two levels of memory at runtime. 

We used three workloads from the SPEC benchmark suite \cite{spec} as shown in table \ref{tab:workloads}. We arranged all the SPEC benchmarks in an increasing order of their MPKI (Misses Per Kilo Instructions) and chose mcf, astar, and cactus, one benchmark from high, medium, and low bandwidth. To vary the hit rate we fixed randomly selected pages in HBM and the rest of the pages in DDR. HBM of size 4GB is simulated to fit the entire working set of astar and cactus workloads into on-chip memory and 16GB (HBM) for mcf. We used 16GB DDR3 memory as level 2 off-package memory. 

We observed an increase in performance as the hit rate to HBM memory is increased from zero, as shown in figure \ref{fig:mcf}. The performance peaks at around 60\% hit rate and starts to drop as the hit rate is increased further to 100\%. Hit rate of 100\% is achieved by fixing the entire working set into HBM. With the entire working set in HBM, the bandwidth provided by DDR is not utilized. The maximum performance occurs when the access to the heterogeneous memory system are distributed in a ratio of the bandwidth provided by off-package DDR and on-chip HBM. Chiachen et al. showed similar results \cite{BATMAN}. Hit rate for the peak performance marginally drifts to the right as we move from mcf to astar and further to cactus. The marginal drift of the peak performance point for lower bandwidth workloads can be explained by slightly lower HBM latency. A low bandwidth workload such as cactus can gain performance by lower HBM latency by moving more accesses to HBM over DDR than astar or mcf.  We also observe that mcf's performance is the most sensitive to the hit rate on either side of the peak performance hit rate point. Hit rate of zero degrades mcf's performance by 51.3\% followed by 44.6\% and 36.7\% for medium and low bandwidth benchmarks, respectively. 

%A high bandwidth workload such as mcf attains the peak performance in a heterogeneous memory architecture when the ratio of accesses to HBM and DDR are in the ratio of the bandwidth offered by them.

The goal of this study is to evaluate the performance-reliability trade-off, as shown in figure \ref{fig:mcf}, where the left y-axis shows the normalized performance, and the right y-axis shows the failure probability in week 0, 180, and 348. In week 0, increasing hit rate has very little effect on the failure probability. However, as the system ages and accumulates permanent memory faults, increasing hit rate will adversely effect failure probability. The performance cap of 80\% IPC is shown by the dotted line in the figure. The average reduction in the failure probabilities by running a workload at 80\% of its peak IPC are $5.15\times10^{-4}$, $72.29\times10^{-4}$, and $137.98\times10^{-4}$ in week 0, 180 and 348, respectively. We would like to reiterate the fact that the reliability numbers for HBM are obtained by modelling SECDED, as explained in section \ref{sec:lifeHitRateReliability}. 

%We are illustrating the concept and evaluating the reliability-performance trade-off of a HMA by modelling HBM no more reliable than SECDED.

%The plot is for mcf benchmark, and three sub-graphs are for week 0 (first), 180 (mid) and 348 (final). The x-axis shows the increasing hit rate, the left y-axis on the each sub-graph represents the instruction per cycle (IPC) normalized to its peak IPC, and the right y-axis shows the failure probability. On all the sub-graphs, performance varies with the same rate as the device ages from week 0 to week 348, represented by the line curve. However, the gradient of failure probability increases as the system ages. In week zero, increasing hit rate has very little effect on failure probability. However, as system ages and accumulate permanent memory faults (and transient faults that are encountered within the scrubbing interval) increasing hit rate will adversely effect failure probability. The performance cap of 80\% IPC is shown by the dotted line line the figure. The reduction in the failure probabilities by running a workload at 80\% of its peak IPC are $7.13\times10^{-4}$, $119.5\times10^{-4}$, and $223.32\times10^{-4}$ in week 0, 180 and 348, respectively. We would like to reiterate the fact that the reliability numbers for HBM are obtained by modelling SECDED, as explained in section \ref{sec:lifeHitRateReliability}. We are simply illustrating the concept and evaluating the reliability-performance trade-off of two level memory systems by assuming HBM modelled no more reliable than SECDED. 

Our results show that a lower hit rate will result in a greater reduction in failure probability in the mid and final week, than in the first week. The reduction in the failure probability (or gain in reliability) is 14x in the mid and 26x in the final week, when compared with the first week.
%The performance cap of 80\% IPC can be achieved by limiting hit rate to 0.4.

\begin{table}
\centering
\begin{tabular}{l l l}
\hline 
 Processor & Values\\
 \hline
 \hline
 Number of cores  & 16 \\
 Core Frequency & 3.2GHz \\
 Issue width & 4-wide out-of-order \\
 ROB size & 128 entries\\
 \hline
 Caches & Values \\
 \hline
 \hline
 L1 I-cache (private) & 32KB, 2-way set-associative \\
 L1 D-cache (private) & 16KB, 4-way set-associative \\
 L2 cache (shared) & 16MB, 16-way set-associative \\
 \hline
 HBM Memory & Values \\
 \hline
 \hline
 Capacity & 4GB, 16GB \\
 Bus frequency & 500Mhz (DDR 1.0GHz)\\
 Bus width & 128 bits \\
 Channels & 8 \\
 Rank & 1 Rank per Channel \\
 Banks & 8 Banks per Rank \\
 %tCAS-tRCD-tRP-tRAS  &45-45-45-180 (CPU cycles)\\
 \hline
 DDR3 (x4) memory & Values \\
 \hline
 \hline
 Capacity & 16 GB \\
 Bus frequency & 800 MHz (DDR 1.6 GHz) \\
 Bus width & 64 bits \\
 Channels & 2 \\
 Ranks & 1 Rank per channel\\
 Banks & 8 Banks per Rank \\
 %tCAS-tRCD-tRP-tRAS & 44-44-44-176 (CPU cycles) \\
\end{tabular}
\caption{Ramulator Simulation Configurations.}
\label{tab:ram}
\end{table}

\begin{table}
\centering
\begin{tabular}{l l l l l l}
\hline 
 WL(x16) 		& MT(GB)  & IPC(Max, Min)   & $IPC_d$(\%)  & $T_1$ & $T_2$\\
 \hline
 \hline
 mcf    	 	&  16.02  &  (2.43, 1.17)   & 51.3         & 132 & 176\\
 astar  		&   2.63  &  (7.68, 4.17)   & 44.6         & 120 & 174\\
 cactus 		&   2.31  & (19.64, 11.72 ) & 36.7         & 108 & 187\\
\end{tabular}
\caption{Workload Characteristics. \textbf{WL}:WorkLoads, \textbf{MT}:Memory Touched, \textbf{MPKI}:Misses Per Kilo Instructions,  $\mathbf{IPC_d}$: \% IPC degradation, $\mathbf{T_1}$: Week when failure probability hits 500x of the initial failure probability value, and $\mathbf{T_2}$: Week when the IPC falls below 10\% of the peak IPC.}
\label{tab:workloads}
\end{table}

%\begin{table}
%\centering
%\begin{tabular}{l l l l l l}
%\hline 
% WL(x16) & MT(GB)   & MPKI  	& Max IPC & Min IPC  & $IPC_d$(\%) \\
% \hline
% \hline
% mcf    	 	&  16.02  &  65.03 &  2.43	  &  1.17     & 51.9 \\
% astar  		&   2.63  &  16.79 &  7.68	  &  4.17     & 45.6 \\
% cactus 		&   2.31  &   3.70 & 19.64   & 11.72     & 40.3\\
%\end{tabular}
%\caption{Workload Characteristics. \textbf{WL}:WorkLoads, \textbf{MT}:Memory Touched, \textbf{MPKI}:Misses Per Kilo Instructions,  $\mathbf{IPC_d}$: \% IPC degradation.}
%\label{tab:workloads}
%\end{table}
                                           
%On-chip memories provide a weaker reliability using schemes such as parity-based detection. Hence, assuming SECDED for an HBM is an optimistic assumption for today's HMA. The failure probabilities reported in this study should be lower than in the field for heterogeneous memory architectures. Moreover, HBM's eight times higher density, and newer failure modes such as fault through-silicon vias (TSV) also increase its failure probabilities. In absence of field study data on failure rates of HBM, Citadel \cite{citadel} scales FIT rates from real-world DRAM field study \cite{dramVilas}, and performs a sensitivity study by sweeping FIT rate for TSV failures. However, for the argument we make in this paper, which to incorporate reliability into data placement policy for heterogeneous memories with varying reliability level, scaling FIT rate by eight times, and including newer failure modes is only going to increase the gap in the reliability levels and make our argument stronger. Hence, we focused to SECDED-based and ChipKill-based memories for the reliability evaluation of two level heterogeneous memory architecture system, for which we have real-world failure data.
