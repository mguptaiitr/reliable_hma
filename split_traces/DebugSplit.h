#ifndef __DEBUGSPLIT_H
#define __DEBUGSPLIT_H

#ifndef DEBUG_SPLIT
#define debugsplit(...)
#else
#define debugsplit(...) do { \
          printf("\033[33m[DEBUG SPLIT] %s ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUGSPLIT */


#endif /* __DEBUGSPLIT_H */
