#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include "DebugSplit.h"

using namespace std;

//MGUPTA : New trace format get_mz_16_core_request definition.
bool get_mz_16_core_request(std::ifstream& file, long& bubble_cnt, long& req_addr, long &req_pc, int& req_pid, long& acc_type){
    static long line_num = 0;
    line_num ++;

    string line; 
    getline(file, line);
    if (file.eof() || line.size() == 0) {
        file.clear();
        file.seekg(0);
        line_num = 0;
        return false;
    }

    size_t pos0, pos1, pos2, pos3, pos4;
    pos0  = line.find(" ");
    pos1  = line.find(" ", pos0+1);
    pos2  = line.find(" ", pos1+1);
    pos3  = line.find(" ", pos2+1);
    pos4  = line.find(" ", pos3+1);

    bubble_cnt = stoul(line.substr(0, pos0), NULL, 10);
    req_addr = stoul(line.substr(pos0+1, pos1-pos0), NULL, 10);
    req_pc = stoul(line.substr(pos1+1, pos2-pos1), NULL, 10);
    req_pid = stoul(line.substr(pos2+1, pos3-pos2), NULL, 10);
    acc_type = stoul(line.substr(pos3+1, pos4-pos3), NULL, 10);

    debugsplit("line# %ld --> bubble_cnt %ld req_addr %ld req_pc %ld req_pid %d acc_type %ld", line_num, bubble_cnt, req_addr, req_pc, req_pid, acc_type);

    return true;

}

string extract_benchmark_name(string tFile){
    string bench;
    size_t found1 = tFile.find_last_of("/\\");
    string dFile = tFile.substr(0, found1);
    size_t found2 = dFile.find_last_of("/\\");
    bench=dFile.substr(found2+1, dFile.find_first_of("_"));
    size_t found3 = bench.find("_");
    bench=bench.substr(0, found3);
    cout<<"Benchmark: "<<bench<<"\n";
    return bench;
}



int main(int argc, const char *argv[])
{
    if (argc < 3) {
        printf("Usage: %s input_trace_file output_directory\n"
                "Example: %s /home/mgupta/traces/mcf/cputrace.trc /home/mgupta/splittrace/mcf \n", argv[0], argv[0]);
        return 0;
    }
    
    std::string intracefile = argv[1];
    std::string outdir = argv[2];
    std::ifstream file(intracefile); 
    std::ofstream ofiles[16];    
    
    string bench = extract_benchmark_name(intracefile);
    //Open 16 output trace files 0 to 15
    for(unsigned int i =0; i<16; i++){
        string outfile = outdir+"/"+"cputrace_"+bench+"_"+to_string(i)+".trc";
        ofiles[i].open(outfile);
    }    
 
    //Five values per line in the trace file 
    long bubble_cnt; 
    long req_addr; 
    long req_pc; 
    int req_pid;
    long acc_type;

    while(get_mz_16_core_request(file, bubble_cnt, req_addr, req_pc, req_pid, acc_type)){
        debugsplit("bubble_cnt %ld req_add %ld req_pc %ld req_pid %d acc_type %ld", bubble_cnt, req_addr, req_pc, req_pid, acc_type);
        //Check this format carefully
        ofiles[req_pid]<<bubble_cnt<<" "<<req_addr<<" "<<req_pc<<" "<<req_pid<<" "<<acc_type<<"\n";
    }
    
    file.close();

    //Close 16 output trace files 0 to 15
    for(unsigned int i =0; i<16; i++){
        ofiles[i].close();
    } 
    return 0;
}
