#include "AVF.h"
#include <cassert>

using namespace std;
using namespace ramulator;

size_t AVF::count = 0;

void AVF::updateAVFLine(Request req, long cpu_retired_inst, long cpu_clk){
    debugavf("%s", __FUNCTION__);
    retired = cpu_retired_inst;
    clk = cpu_clk;

    genericAVFLine(req, avf_lines); //For avf_lines
    genericAVFLine(req, checkpoints_avf_lines); //For checkpoints_avf_lines

    //If checkpoint reached.
    //if(retired > (check_point_no+1) * avf_check_point) //Checkpoint based on number of instructions
    if(cpu_clk > (check_point_no+1) * avf_check_point_clk) //Checkpoint based on number of cpu_clks
        newAVFCheckPoint();
}


void AVF::genericAVFLine(Request req, std::map<long, AVFLine>& lines){
    //debugavf("%s", __FUNCTION__);
    debugavf("Request payload: Type(%d) addr(%ld) arrival(%ld) depart(%ld)", req.type, req.addr, req.arrive, req.depart);
    assert((req.depart > req.arrive)&& "departure time less than arrivale time");

    //1. Get lineno.
    //2. Check if line is present in avf_lines
    //3. Check Request::Type
    //3a) Request::Type RD
    //3b) Request::Type WR

    long lineno = align(req.addr);
    auto it = lines.find(lineno);
    if(it == lines.end()) 
        lines.insert(make_pair(lineno, AVFLine()));

    if(req.type == Request::Type::READ){
        assert(req.depart > lines[lineno].last_line_write && "Last write to line is in future. Check code!!");
        lines[lineno].n_rd++;
        lines[lineno].vul_read_cycles += (req.depart - lines[lineno].last_line_write);
        debugavf("genericAVFLine processed READ request: req.dep (%ld) last_line_write (%ld)", req.depart, lines[lineno].last_line_write);
    }
    else if(req.type == Request::Type::WRITE){
        lines[lineno].n_wr++;
        lines[lineno].last_line_write = req.depart; //Assuming ever WR req will result in WR to entire cache line.
        debugavf("genericAVFLine processed WRITE request: req.dep (%ld) last_line_write (%ld)", req.depart, lines[lineno].last_line_write);
    }
    else {
        std::cout<<"Invalid Request \n";
        exit(0);
    } 
    lines[lineno].n_tot++;
}

void AVF::genericAVFPage(std::map<long, AVFLine>& lines, std::map<long, AVFPage>& pages){
    debugavf("%s", __FUNCTION__);

    //Calcluate line avf for every line in the avf_line
    long total_access = 0;
    for(auto& kv : lines){
        long pageno = getPageNo(kv.first);
        AVFLine l = kv.second;
        l.lineavf = (double)(l.vul_read_cycles)/(double)(clk);

        //Compose avf_page from avf_line
        auto it = pages.find(pageno);
        if(it == pages.end()){
            pages.insert(make_pair(pageno, AVFPage()));
        }
        pages[pageno].n_rd += l.n_rd;
        pages[pageno].n_wr += l.n_wr;
        pages[pageno].n_tot += l.n_tot;
        pages[pageno].pageavf += l.lineavf;

        //Using n_tot as page hottness metric.
        pages[pageno].pagehot = pages[pageno].n_tot;

        total_access += l.n_tot;
    }

    //Compute page hottness as (page n_tot)/(total access)
    for (auto& kv : pages){
        AVFPage& page = kv.second;
        page.pagehot = (double)(page.n_tot)/(double)(total_access);
    }
}


void AVF::newAVFCheckPoint(){
    debugavf("%s", __FUNCTION__);

    avfCheck.retired = retired - retiredLast;
    avfCheck.clk = clk - clkLast;                   

    //Save the current value of retired inst and clk to be subtracted from 
    //the next avfCheck.retired and avfCheck.clk
    retiredLast = retired;
    clkLast = clk;    

    avfCheck.ipc = (double)(avfCheck.retired)/(double)(avfCheck.clk);

    //update avf page info for these checkpoint into avfCheck
    genericAVFPage(checkpoints_avf_lines, avfCheck.checkpoints_avf_pages); 

    //Print/Store current checkpoint in a c_file
    printStoreCheckPoints();

    //Reset all checkpoint avf lines for the next checkpoint
    for(auto& kv : checkpoints_avf_lines){
        AVFLine& l = kv.second;
        l.checkPointReset();
    }

    //Increment checkpoint number
    check_point_no++;    

}

void AVF::printStorePageInfo(ofstream& file, std::map<long, AVFPage>& pages){
    file<<"* Page Avf and Hottness Info *\n";
    file<<"PageNo:#Reads:#Writes:#Total:AvfRdWrDis:AbsRdWrDis:PageAVF:PageHotness\n";
    file<<"PageAVF Data Starts\n";
    for (auto& kv : pages){
        long pageno = kv.first;
        AVFPage& p = kv.second;
        file<<pageno<<"|"<<p.n_rd<<"|"<<p.n_wr<<"|"<<p.n_tot<<"|"<<p.avg_wr_rd_dis<<"|"<<p.abs_wr_rd_dis<<"|"<<p.pageavf<<"|"<<p.pagehot<<"\n";
    }
    file<<"PageAVF Data Ends\n";
}

//void AVF::TopHotPageInfo(ofstream& file, std::map<long, AVFPage>& pages){
void AVF::TopPageInfo(ofstream& file, std::map<long, AVFPage>& pages, std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_func){
    //Convert std::map into std::vector
    std::vector<std::pair<long, AVFPage>> pagevec(pages.begin(), pages.end());
    
    //Store top n pages based on cmp_func
    if(pagevec.size() < num_top_pages)
        num_top_pages = pagevec.size();
    std::partial_sort(pagevec.begin(), pagevec.begin() + num_top_pages, pagevec.end(), cmp_func);

    //Write n top hot pages to the file.
    unsigned int i = 0;
    for (auto& pv: pagevec){
        long pageno = pv.first;
        AVFPage &p = pv.second;
        file<<pageno<<"|"<<p.n_rd<<"|"<<p.n_wr<<"|"<<p.n_tot<<"|"<<p.avg_wr_rd_dis<<"|"<<p.abs_wr_rd_dis<<"|"<<p.pageavf<<"|"<<p.pagehot<<"\n";
        if (++i >= num_top_pages)
            break;
    }
}

void AVF::printStoreAVF(){

    string fName = "./avf_output/"+bench+"_page_avf.dat";
    ofstream page_file(fName);

    genericAVFPage(avf_lines, avf_pages);    

    cout<<"--> Writing Final Page Hotness and AVF" <<"\n";
    cout<<"Number of avf object created(Only for debug purposes) : "<<count<<"\n"; 
    cout<<"Number of checkpoints: "<<check_point_no<<"\n";   
    cout<<"Number of lines: "<<avf_lines.size()<<"\n";
    cout<<"Number of pages: "<<avf_pages.size()<<"\n";

    //Meta data to the page_file
    page_file <<"Number of checkpoints: "<<check_point_no<<"\n";
    page_file<<"Number of pages: "<<avf_pages.size()<<"\n";
    page_file<<"Number of lines: "<<avf_lines.size()<<"\n";

    printStorePageInfo(page_file, avf_pages);
    
    page_file<<"* Top N Hot Pages Starts\n";
    TopPageInfo(page_file, avf_pages, cmp_hot_dec);
    page_file<<"* Top N Hot Pages Ends\n";

    page_file<<"* Least N Hot Pages Starts\n";
    TopPageInfo(page_file, avf_pages, cmp_hot_inc);
    page_file<<"* Least N Hot Pages Ends\n";

    page_file<<"* Top N AVF Pages Starts\n";
    TopPageInfo(page_file, avf_pages, cmp_avf_dec);
    page_file<<"* Top N AVF Pages Ends\n";
    
    page_file<<"* Least N AVF Pages Starts\n";
    TopPageInfo(page_file, avf_pages, cmp_avf_inc);
    page_file<<"* Least N AVF Pages Ends\n";



    page_file.close(); 

    //Put the current counters in the last checkpoint  
    newAVFCheckPoint();
}

void AVF::printStoreCheckPoints(){

    c_file<<"* Checkpoint Number: "<<check_point_no<<"\n";
    c_file<<"Retired Instructions: "<<avfCheck.retired<<"\n";
    c_file<<"CPU Clocks: "<<avfCheck.clk<<"\n";
    c_file<<"IPC: "<<avfCheck.ipc<<"\n";

    //printStorePageInfo(c_file, avfCheck.checkpoints_avf_pages);

        
    c_file<<"* Top N Hot Pages Starts\n";
    TopPageInfo(c_file, avfCheck.checkpoints_avf_pages, cmp_hot_dec);
    c_file<<"* Top N Hot Pages Ends\n";

    c_file<<"* Least N Hot Pages Starts\n";
    TopPageInfo(c_file, avfCheck.checkpoints_avf_pages, cmp_hot_inc);
    c_file<<"* Least N Hot Pages Ends\n";

    c_file<<"* Top N AVF Pages Starts\n";
    TopPageInfo(c_file, avfCheck.checkpoints_avf_pages, cmp_avf_dec);
    c_file<<"* Top N AVF Pages Ends\n";
    
    c_file<<"* Least N AVF Pages Starts\n";
    TopPageInfo(c_file, avfCheck.checkpoints_avf_pages, cmp_avf_inc);
    c_file<<"* Least N AVF Pages Ends\n";


}

