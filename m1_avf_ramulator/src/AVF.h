#ifndef __AVF_H
#define __AVF_H

#include "Config.h"
#include "Request.h"
#include "Statistics.h"
#include "DebugAVF.h"
#include <fstream>
#include <algorithm>
#include <cstdio>
#include <cassert>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <queue>

extern string bench;

namespace ramulator
{


    class AVF {
        
        private:
            static size_t count; //DEBUG variable for number of AVF objects created. 
        public:
            long retired = 0, retiredLast = 0;
            long clk = 0, clkLast = 0;
            
            //Checkpoint related variables
            long avf_check_point = 1000000 * 10; //10M retited inst count (intermediate AVF infomation is recorded.)
            long avf_check_point_clk = 1000000 * 100; //Checkpoint at every 100M clock ticks.
            long check_point_no = 0; //Starting checkpoint no.
            ofstream c_file;
            
            int cache_line_size = 1 << 6; //64B             
                        
            //Top pages per checkpoint
            int size_top_pages_GB = 1;
            int page_size_KB = 4;
            int num_top_pages = (size_top_pages_GB * 1024 * 1024)/page_size_KB;
            int num_top_pages_check = 1000;
            //-----------------------
    
            //AVF CACHE Datastrctures ----------------START DEFINITIONS-----------------------
            //MGUPTA 1: Line Structure that holds avf info for the line.
            //We should have N instances of this structure, where N should be total
            // number of lines required to include the complete dataset.
            struct AVFLine {
                //Line Stats -- These counts are the running counts. For checkpoints these value will be cumulative sum upto that checkpoint.
                long n_rd;
                long n_wr;
                long n_tot;
                double avg_wr_rd_dis; //MG_FIX
                double abs_wr_rd_dis; //MG_FIX
                long vul_read_cycles;
                long last_line_write;  //Add all vunerable read delays for this line in read_delay.

                //Line AVF -- lineavf will hold final total line avf at the end or cumulative lineavf upto the current checkpoint.
                double lineavf;

                

                AVFLine():n_rd(0), n_wr(0), n_tot(0), vul_read_cycles(0), last_line_write(0), lineavf(0) {}

                //Reset avf line data for checkpoints
                void checkPointReset(){
                    avg_wr_rd_dis=0;
                    abs_wr_rd_dis=0;
                }
                
                void printdata(){
                    cout<<"AVFLine data: "<<n_rd<<","<<n_wr<<","<<n_tot<<","<<avg_wr_rd_dis<<","<<abs_wr_rd_dis<<","<<vul_read_cycles<<"\n";
                }
            };


            //MGUPTA 2: Page structure that holds avf info for the page.
            //We should have N instances of this structure, where N should be total
            // number of pages to include the complete dataset.
            struct AVFPage{
                long n_rd;
                long n_wr;
                long n_tot;
                double avg_wr_rd_dis; //MG_FIX
                double abs_wr_rd_dis; //MG_FIX
                double pageavf;
                double pagehot;
            };

            //AVF Checkpoint Stats;
            struct AVFCheckPoint {
                //System wide stats
                long retired;
                long clk;
                double ipc;
                
                //AVF, Hottness and page specific stats 
                std::map<long, AVFPage> checkpoints_avf_pages;
                //AVFCheckPoint():n_rd(0), n_wr(0), ipc(0), avg_wr_rd_dis(0), abs_wr_rd_dis(0), pageavf(0), pagehot(0) {}
            
                void checkPointReset(){
                    retired = 0;
                    clk = 0;
                    ipc = 0;
                    checkpoints_avf_pages.clear(); 
                }
            };



            //MGUPTA 3: This map holds all the lines for the page.
            //checkpointno --> AVFCheckPoint //checkpoint 0,1,2,3,4,5,6,....
            std::list<AVFCheckPoint> avf_checkpoints;
            AVFCheckPoint avfCheck; //Reuse single AVFCheckPoint variable.

            //lineadd --> AVFLine Info
            std::map<long, AVFLine> avf_lines;
            std::map<long, AVFLine> checkpoints_avf_lines;

            //pageno --> AVFPage Info (Final Page Info)
            std::map<long, AVFPage> avf_pages;

            /* FUNCTION FOR AVF CALCULATIONS */
            void updateAVFLine(Request req, long cpu_retired_inst, long mem_clk);
            void genericAVFLine(Request req, std::map<long, AVFLine>& lines);
            void newAVFCheckPoint();
            void genericAVFPage(std::map<long, AVFLine>& lines, std::map<long, AVFPage>& pages);

            //Print the stats in a file or stdout. 
            void printStoreAVF();
            void printStoreCheckPoints();            
            void printStorePageInfo(ofstream& file, std::map<long, AVFPage>& pages);            
            
            //Top N pages by hotness or avf based on the compartor function passed.
            void TopPageInfo(ofstream& file, std::map<long, AVFPage>& pages, std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)>);
            
            //Function for decreasing page hotness
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_hot_dec = 
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return l.second.pagehot > r.second.pagehot;};            
            //Function for increasing page hotness
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_hot_inc =
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return l.second.pagehot < r.second.pagehot;};            
            //Function for decreasing page_avf
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_avf_dec =
               [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return l.second.pageavf > r.second.pageavf;};
             //Function for increasing page_avf
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_avf_inc =
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return l.second.pageavf < r.second.pageavf;};


            //MGUPTA -- get pageno: Returns distinct page number for each addr.
            long getPageNo(long addr) {
                return (addr >> 12);
            }

            //Aligns to cacheline and returns distinct cacheline address.
            long align(long addr) {
                return (addr & ~(cache_line_size-1l));
            }

            AVF(){
                debugavf("Constructer Default");
        
                ++count;
                
                //Open file for checkpoint recording
                string fName = "./avf_output/"+bench+"_checkpoints.dat";
                c_file.open(fName);

            }
            
            ~AVF(){
                debugavf("Destructor Default");
                
                //Close the check point recording file
                c_file.close();
             }
    };

} // namespace ramulator

#endif /* __AVF_H */
