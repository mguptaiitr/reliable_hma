#ifndef __DEBUGAVF_H
#define __DEBUGAVF_H

#ifndef DEBUG_AVF
#define debugavf(...)
#else
#define debugavf(...) do { \
          printf("\033[33m[DEBUG AVF] %s ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUGAVF */


#endif /* __DEBUGAVF_H */
