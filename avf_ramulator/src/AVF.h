#ifndef __AVF_H
#define __AVF_H

#include "Config.h"
#include "Request.h"
#include "Statistics.h"
#include <algorithm>
#include <cstdio>
#include <cassert>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <queue>
#include <list>

namespace ramulator
{

    class AVF {
        protected:
            ScalarStat avf_lines; //Total number of lines for which AVF is collected.
        public:
            enum class Level {
                L1,
                L2,
                L3,
                MAX
            } level;
            std::string level_string;

            struct AvfLine {
                long pageno;
                long totwrites;
                long totreads;
                long acc;

                AvfLine():
                    pageno(0), totwrites(0), totreads(0), acc(0) {}


                AvfLine(long pageno, long totwrites, long totreads, long acc):
                    pageno(pageno), totwrites(totwrites), totreads(totreads), acc(acc) {}
            };

    };

} // namespace ramulator

#endif /* __AVF_H */
