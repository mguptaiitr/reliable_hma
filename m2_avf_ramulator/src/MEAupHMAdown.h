#ifndef __MEAUPHMADOWN_H
#define __MEAUPHMADOWN_H
#include "Counters.h"
using namespace std;

namespace ramulator {
    class MEAupHMAdown : public Counters {
        public:
            //MEA variables for counting M1 and M2 pages.
            unsigned int num_mea_counters; //num should be command line configurable.
            long firstM1Page = 0; //page location in m1 memory. start scan from here.
            long nextM1Page = firstM1Page; //candidate page in fast memory replaced.
            map<long, long> meaCnt;

            //HMA variables for only M1 pages
            unsigned int writeRatioFactor = 1;
            unsigned int nMEA = 0;
            unsigned int nHMAtrigger = 2000;
            map<long, pair<long, long>> pgRdWrCnt; //use this map for finding risky pages
            map<long, pair<long, long>> curr_pgRdWrCnt; //count hma statistics here
            long conflict = 0; //number of times MEA and HMA decide to go against each other

            //Constructor Destructors
            MEAupHMAdown(const Config& configs){
                type = Counters::Type::MEAupHMAdown;
                num_mea_counters = atoi(configs["num_mea_counters"].c_str());

                //HMA counter info
                cmp_func = cmp_wrwrrd; //sort comparision function. sorts in decreasing order of wr^2/rd ratio. 
            }
            ~MEAupHMAdown(){   
                printf("MEA and HMA run into conflict: %ld\n", conflict);
            }

            //sorting functions
            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_wrwrrd =
                [](pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r) 
                {return (l.second.second^2/(l.second.first+1)) > (r.second.second^2/(r.second.first+1));};

            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_func;

            //Define pure virtual functions from Counter class
            virtual void updateCounter(Request& req){
                long pageno = getPageID(req);

                /*** MEA counting for pages in M1 and M2 ***/
                auto pit = meaCnt.find(pageno);                                
                //if pit in mea counter map
                if(pit != meaCnt.end()){
                    debugmig("Updating mea counter for pageno (%ld) meaCnt[%ld] (%ld)", pageno, pageno, meaCnt[pageno]);
                    meaCnt[pageno]++; 
                }
                //pit NOT in mea counter map
                //but there is space in map
                else if(meaCnt.size() < num_mea_counters){
                    debugmig("Adding New mea counter for pageno (%ld)", pageno);
                    meaCnt[pageno] = 1;    
                }
                else{
                    debugmig("FULL mea counters decrement count for each page");
                    vector<long> toDelete;
                    //decrement 1 from every page
                    for(auto& kv : meaCnt){
                        kv.second--;
                        if(kv.second == 0){
                            toDelete.push_back(kv.first);
                        }
                    }   
                    //delete pages that have zero counts. 
                    for(auto& pid : toDelete)
                        meaCnt.erase(pid);
                }

                /*** HMA counting for pages in only M1 ***/
                if(getPageMemLevel(pageno) == 1){
                    debugmig("HMA counting for pageno (%ld) in M1", pageno);
                    auto pit_m1 = curr_pgRdWrCnt.find(pageno);
                    //add pageno in the counters if required
                    if(pit_m1 == curr_pgRdWrCnt.end())
                        curr_pgRdWrCnt.insert(make_pair(pageno, make_pair(0, 0)));
                    //Update read count
                    if(req.type == Request::Type::READ)
                        curr_pgRdWrCnt[pageno].first++;
                    //Update write count
                    if(req.type == Request::Type::WRITE)
                        curr_pgRdWrCnt[pageno].second++;
                }//if(getPageMemLevel(pageno) == 2)
            }

            /* Clear counters */
            //clear MEA counters
            virtual void clear(){
                meaCnt.clear();
                if(nMEA == nHMAtrigger)
                    clearHMAcounters();
                debugcnt("Cleared mea counters meaCnt.size() (%zu)", meaCnt.size());  
            }
            //clear HMA counters 
            void clearHMAcounters(){
                //Make pgRdWrCnt the map used for decision of risky pages equals to curr_pgRdWrCnt
                pgRdWrCnt = curr_pgRdWrCnt;
                //clear or cut the current counts by half.
                //cutting into half retians some history but also has one migration issue (refer notes) 
                for(auto& kv : curr_pgRdWrCnt){
                    auto& data = kv.second;
                    //data.first = data.first/2;
                    //data.second = data.second/2;
                    data.first = 0;
                    data.second = 0;

                }
                nMEA = 0;
            }

            /* Generate migration pairs */
            virtual void generateMigrationPairs(){
                debugcnt("Start generating migrationPair and push into migrationPair vector. meaCnt.size(%zu)", meaCnt.size());
                migrationPairs.clear();
                nMEA++;

                //Get HMA counter data to decide risky pages
                //Convert HMA map to vector and sort it in decreasing order of wr^2/rd ratio
                vector<pair<long, pair<long, long>>> fastPages(pgRdWrCnt.begin(), pgRdWrCnt.end());
                partial_sort(fastPages.begin(), fastPages.begin()+fastPages.size(), fastPages.end(), cmp_func);
                debugcnt("Number of fastPages.size(%ld)", fastPages.size());
                
                //migration stats
                long migCnt=0, i=1;

                //Iterate over all pages in MEA counter map
                for(auto& kv : meaCnt){
                    long meaPageNo = kv.first;

                    //Only migrate mea pages in M2 memory. MEA page in M1 stays entact
                    if(getPageMemLevel(meaPageNo) == 2) {
                        //1. Find replacement for meaPageNo in the used M1 pages (fastPages)
                        if(fastPages.size()>0){
                            //Loop over fastPages from the end to find a replacement
                            while(fastPages.size()>i){
                                auto fast_it = fastPages.end() - i;
                                long riskyPageNo = (*fast_it).first;
                                /*
                                long riskyPageRds = (*fast_it).second.first;
                                long riskyPageWrs = (*fast_it).second.second;
                                long riskyAccess = riskyPageRds + riskyPageWrs;
                                */
                                //terminating threshold
                                //if(riskyPageWrs*writeRatioFactor > riskyPageRds){
                                if(i>fastPages.size()/2 || migCnt>meaCnt.size()/2){
                                    debugcnt("Threshold break at i (%zu) and migCnt (%ld)", i, migCnt);
                                    i = fastPages.size();
                                    break;
                                }

                                //if risky page in MEA counters we have conflict
                                if(meaCnt.find(riskyPageNo) != meaCnt.end()){
                                    debugcnt("Conflict i (%zu) meaPageNo (%ld)", i, meaPageNo);
                                    conflict++;
                                    i++;
                                }
                                //create a migrationPair using meaPageNo and riskyPageNo
                                else{
                                    debugcnt("Migrate in used pages meaPageNo (%ld) <--> riskyPageNo (%ld)", meaPageNo, riskyPageNo);
                                    pair<long, long> migPair = make_pair(meaPageNo, riskyPageNo);
                                    pushMigrationPair(migPair, migCnt);
                                    i++; migCnt++;
                                    break; //get to next meaPageNo   
                                }
                            }
                        }

                        //2. Find replacement for meaPageNo in the unused M1 pages
                        if(i>=fastPages.size()){//we have looked thourgh all used fastPages
                            //terminating conidtions while looking in *unallocated* M1 pages
                            if(first_free_m1_page >= max_m1_pages){
                                debugcnt("Filled unallocated M1 pages. migCnt (%ld)", migCnt);
                                return;
                            }

                            //First unsed M1 page found for replacement with meaPageNo
                            debugcnt("Migrate in free pages meaPageNo (%ld) <--> first_free_m1_page (%ld)", meaPageNo, first_free_m1_page);
                            pair<long, long> migPair = make_pair(meaPageNo, first_free_m1_page);
                            pushMigrationPair(migPair, migCnt);
                            incFirstFreeM1Page();
                            migCnt++;
                        }//if(i>=fastPages.size())
                    }
                }//for(auto& kv : meaCnt)
            }
    }; //class Counters
}//namespace ramulator
#endif //__MEAUPHMADOWN_H
