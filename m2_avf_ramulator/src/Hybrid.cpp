#include "Hybrid.h"

using namespace std;

namespace ramulator
{

    //Hybrid Helper Functions
    unsigned int CHECK_TARGET_MEM(long addr, long m1_max_address){
        unsigned int mem = (addr >= m1_max_address) ? 2 : 1;
        return mem;
    }

    bool CHECK_TARGET_M1(long addr){
        unsigned int mem = (( addr & (~( (1ULL<<30) - 1)) ) > 0ULL) ? 2 : 1;
        if(mem == 1)
            return true;
        return false;
    }

    //Hybrid Class Functions
    void Hybrid::set_hybrid_max_pages(long m1_max, long m2_max){
        max_m1_pages = m1_max;
        max_m2_pages = m2_max;
        
        if(ismigrate) counters->set_counters_max_pages(max_m1_pages, max_m2_pages);
        printf("Hybrid Memory :: M1 max pages (%ld) M2 max pages (%ld) \n", max_m1_pages, max_m2_pages);
    }

    void Hybrid::get_m1_static_page_allocation(){
        string line;
        size_t pos0;
        long virpageno;
        long phypageno = 0;
        long m1_page_cap = max_m1_pages;        
        
        getline(m1_map_file, line);
        total_bench_pages = stoul(line);        
        printf("Static page allocation in progress. Total Page in Workload: %ld\n", total_bench_pages);

        //Check initial allocation policy or in M1:M2 capacity ratio.
        if(init_alloc == "capratio") //Fill M1 in capacity ratio. M1/(M1+M2) from #defines M1_NUM and M1_M2_NUM
            m1_page_cap = total_bench_pages * M1_NUM / M1_M2_NUM;
        //m1 capacity is set more than max_m1_pages by capratio calculation bring it back to max_m1_pages.
        if(m1_page_cap > max_m1_pages)
            m1_page_cap = max_m1_pages;
         
        while(!m1_map_file.eof() && (num_m1_pages < m1_page_cap)){
            getline(m1_map_file, line);
            if(line.size() == 0){
                debughyb("pages mapped phypageno (%ld)", phypageno);
                debughyb("num_m1_pages (%ld)", num_m1_pages);
                return;
            }
            num_m1_pages++;
            pos0  = line.find(",");
            virpageno = stoul(line.substr(0, pos0), NULL, 10);
            ptem1.insert(make_pair(virpageno, phypageno++));
            debughyb("M1 Static Page Map virpageno (%ld) --> phypageno (%ld)", virpageno, ptem1[virpageno]);
            assert(num_m1_pages <= max_m1_pages && "FATAL! trying to allocate pages in HBM more than capacity");
        }
        printf("Static page allocation done. num_m1_pages (%ld), m1_page_cap (%ld), max_m1_pages (%ld)\n", num_m1_pages, m1_page_cap, max_m1_pages);
    }

    long Hybrid::translate_address_vir_to_phy(long addr){

        long virpageno = addr>>12;
        long phyaddr;
        //Find virtual page in M1 pte1
        auto it = ptem1.find(virpageno);
        if(it != ptem1.end()){
            phyaddr = (ptem1[virpageno] << 12) | (addr & ((1 << 12) - 1));
            debughyb("Virtual address (%ld) [VPN (%ld)] --> M1 Physical address (%ld) [PPN (%ld)]", addr, virpageno, phyaddr, ptem1[virpageno]);
            return phyaddr;
        }
        //Find virtal page in M2 pte2
        auto it2 = ptem2.find(virpageno);
        if(it2 != ptem2.end()){
            phyaddr = (ptem2[virpageno]  << 12) | (addr & ((1 << 12) - 1));
            debughyb("Virtual address (%ld) [VPN (%ld)] --> M2 Physical address (%ld) [PPN (%ld)]", addr, virpageno, phyaddr, ptem2[virpageno]);
            return phyaddr;
        }
        //Page not allocated yet. Dynamically allocate the page first.
        debughyb("New dynamic page allocation starts for virpageno (%ld)", virpageno);
        long phypageno = dynamic_allocate_page(virpageno);
        debughyb("New dynamic page allocation ends with phypageno (%ld).", phypageno);
        phyaddr = (phypageno << 12) | (addr & ((1 << 12) - 1));
        return phyaddr;
    }

    //returns phypageno.
    long Hybrid::dynamic_allocate_page(long virpageno){
        long phypageno = -1;
        if(static_map_m1){ //static page map enaabled. HBM pages are statically allocated. All new pages goes to DDR.
            ptem2.insert(make_pair(virpageno, max_m1_pages + num_m2_pages++));
            phypageno = ptem2[virpageno];
            debughyb("M2 Page allocated(M1 is static allocated) virpageno(%ld) --> phypageno(%ld)", virpageno, ptem2[virpageno]);
        }
        else{ //Static page map disabled. 1) fillm1 2) capratio 3) onlym2 4) tophot 5) topwrwrrd 
            if(init_alloc == "fillm1"){
                //Fill M1(HBM) completely use mz defualt allocation 2 4KB pages in M1 2 4KB pages in M2 until M1 is full. Rest in M2
                if(virpageno < max_m1_pages){
                    num_m1_pages++;
                    ptem1.insert(make_pair(virpageno, virpageno));
                    if(ismigrate) counters->incFirstFreeM1Page();
                }
                else{
                    num_m2_pages++;
                    ptem2.insert(make_pair(virpageno, virpageno));
                }
                phypageno = virpageno;
                debughyb("M1 Page allocated to fill M1 complete virpageno(%ld) --> phypageno(%ld)", virpageno, phypageno);
            }            
            //Fill M1 and M2 in the ratio of their capacity
            else if (init_alloc == "capratio"){
                phypageno = fill_m1_m2_in_cap_ratio(virpageno);
            }
            //Fill only M2. Super reliability-focused initial placement
            else if (init_alloc == "onlym2"){
                phypageno = allocate_m2_page(virpageno);
            }
            //Top hot initial placement. Do migrations with hot pages pinned to HBM
            //else if (init_alloc == "tophotfillm1" || init_alloc == "tophotcapratio"){
            else if (find(pinning_types.begin(), pinning_types.end(), init_alloc) != pinning_types.end()){
                if(!pin_m1_pages_done) pin_m1_pages();
                //try and find the page page table entry for HBM again after pinning
                auto it = ptem1.find(virpageno);
                if(it != ptem1.end()) phypageno = ptem1[virpageno];
                else phypageno = allocate_m2_page(virpageno); //all unpinned pages start from DDR (M2) memory
            }
            else {
                cout<<"FATAL! Initial allocation policy not set for hybrid memory simulations.\n";
                exit(0);
            }
        }
        assert(phypageno >= 0 && "FATAL! dynamic_allocate_page fails allocate a physical page");
        assert(num_m1_pages <= max_m1_pages && "FATAL! dynamic_allocate_page trying to allocate pages in HBM more than capacity");
        assert(num_m2_pages <= max_m2_pages && "FATAL! dynamic_allocate_page trying to allocate pages in DDR more than capcity");
        return phypageno;
    }
    
    void Hybrid::pin_m1_pages(){
        //set pin_pages
        pin_m1_pages_done = true;
        
        //start pinning pages to respective memory. 
        //current implementation only pins pages to m1 using init_alloc_m1_map_file and fills.
        string line;
        size_t pos0;
        long virpageno;
        long phypageno = 0;
        long m1_page_cap = max_m1_pages; 
        
        getline(init_alloc_m1_map_file, line);
        total_bench_pages = stoul(line);        
        
        //Check initial allocation policy or in M1:M2 capacity ratio.
        if(init_alloc.find("capratio") != string::npos) //Fill M1 in capacity ratio. M1/(M1+M2) from #defines M1_NUM and M1_M2_NUM
            m1_page_cap = total_bench_pages * M1_NUM / M1_M2_NUM;
        //m1 capacity is set more than max_m1_pages by capratio calculation bring it back to max_m1_pages.
        if(m1_page_cap > max_m1_pages)
            m1_page_cap = max_m1_pages;

        while(!init_alloc_m1_map_file.eof() && (num_m1_pages < m1_page_cap)){
            getline(init_alloc_m1_map_file, line);
            if(line.size() == 0){
                debughyb("pages mapped phypageno (%ld)", phypageno);
                debughyb("num_m1_pages (%ld)", num_m1_pages);
                return;
            }
            num_m1_pages++;
            pos0  = line.find(",");
            virpageno = stoul(line.substr(0, pos0), NULL, 10);
            ptem1.insert(make_pair(virpageno, phypageno++));
            
            //BUG FIXED Start. Conner case: During static pinning migration is not involved. 
            //Howerver, with pinning pages during migration require communicating migration engine 
            //FirstFreeM1Page to able to migrate pages into unallocated HBM space 
            if(ismigrate) counters->incFirstFreeM1Page();
            //BUG FIXED End ---------------------------------------------------------------------//

            debughyb("M1 Page Pinning. virpageno (%ld) --> phypageno (%ld)", virpageno, ptem1[virpageno]);
            assert(num_m1_pages <= max_m1_pages && "FATAL! trying to allocate pages in HBM more than capacity");
        }
        printf("Page Pinning DONE. num_m1_pages (%ld), m1_page_cap (%ld), max_m1_pages (%ld)\n", num_m1_pages, m1_page_cap, max_m1_pages);    
    }
    
    //returns phypageno in capcity ratio
    long Hybrid::fill_m1_m2_in_cap_ratio(long virpageno){
        long phypageno = -1;
        if(num_m1_pages == max_m1_pages){ //HBM/M1 memory is full. All new pages goes to DDR
            ptem2.insert(make_pair(virpageno, max_m1_pages + num_m2_pages++));
            phypageno = ptem2[virpageno];
            debughyb("M2 Page allocated(M1 is full) virpageno(%ld) --> phypageno(%ld)", virpageno, phypageno);
        }
        else{                          //HBM has space. Allocate in cap ratio. Use random number generator
            int num = rand() % (M1_M2_NUM);
            if(num < M1_NUM){
                ptem1.insert(make_pair(virpageno, num_m1_pages++));
                phypageno = ptem1[virpageno];
                if(ismigrate) counters->incFirstFreeM1Page();
                debughyb("M1 Page allocated in cap ratio virpageno(%ld) --> phypageno(%ld)", virpageno, phypageno);
            }
            else{
                ptem2.insert(make_pair(virpageno, max_m1_pages + num_m2_pages++));
                phypageno = ptem2[virpageno];
                debughyb("M2 Page allocated in cap ratio virpageno (%ld) --> phypageno (%ld)", virpageno, phypageno);
            }
        }
        assert(phypageno >= 0 && "FATAL! fill_m1_m2_in_cap_ratio fails allocate a physical page");
        return phypageno;
    }

    //returns phypageno in only m2 (ddr memory) memory
    long Hybrid::allocate_m2_page(long virpageno){
        long phypageno = -1;
        ptem2.insert(make_pair(virpageno, max_m1_pages + num_m2_pages++));
        phypageno = ptem2[virpageno];
        debughyb("M2 Page allocated (only M2 pages are allocated) virpageno (%ld) --> phypageno (%ld)", virpageno, phypageno);
        return phypageno;
    }


    void Hybrid::hybrid_set_configs(const Config& configs){
        //This function is called from Hybrid constructors
        init_alloc = configs["init_alloc"];
        ismigrate = configs.is_migration_on();
        mig_interval = configs.get_avf_clk_interval();
        
        remapTable = new RemapTable();
        
        if(ismigrate)
            initMigration(configs);
        
        //if init_alloc is tophot, topwrwrrd, pcbased get the initi_alloc_m1_map
        if(init_alloc!="fillm1" && init_alloc!="onlym2" && init_alloc!="capratio"){
            //get the initial allocation page map file
            string filename = ramulator_mg+"/m2_avf_ramulator/static_page_maps";

            if(init_alloc=="tophotfillm1" || init_alloc=="tophotcapratio")
                filename = filename+"/"+bench_mg+"_DDR3_TopHot.csv";
            else if(init_alloc=="topwrwrrdratiofillm1" || init_alloc=="topwrwrrdratiocapratio")
                filename = filename+"/"+bench_mg+"_DDR3_TopWrWrRdRatio.csv";
            else if(init_alloc=="pctophot")
                filename = filename+"/"+bench_mg+"_DDR3_PCTopHot.csv";
            else if(init_alloc=="pctophotlowrisk")
                filename = filename+"/"+bench_mg+"_DDR3_PCTopHotLowRisk.csv";
            else
                assert(false && "FATAL!! init_alloc assigment does not have it's implementation");                
           
           //open filename using handle init_alloc_m1_map_file
           printf("Initial allocation page pinning (ENABLED) File --> %s\n", filename.c_str());
           init_alloc_m1_map_file.open(filename);
        }

        //Statistics
        numOfMigrations
            .name("total_migrations")
            .desc("The total number of migrations performed.")
            .precision(0)
            ;
        numOfMigrations = 0;
        
        cout<<"Setting Hybrid Memory Configurations\n";
        cout<<"Initial allocation: "<<init_alloc<<"\n";
        cout<<"Percentage pages to M1 (SET): "<<M1_NUM<<"%\n";
        cout<<"Migration: "<<std::boolalpha<<ismigrate<<"\n";
    }

    void Hybrid::tick(){
        interval++; clk++;

        //If interval hits migration interval generate migration pairs, start migration, and clear counters for next interval.
        if(ismigrate && (interval == mig_interval)){
            interval = 0;
            if(pendingMigration){
                //MGUPTACHK: What's the right strategy here. return and apply exisiting migrationPair to swap
                //or generate new migrationPair. Given that all memory traffic has been stopped during all this time
                //and counters have not been updated.
                printf("Entering next interval while previous migration is pending or swap happening \n");
                return;
            }
            migNum++;
            debugmig("Migration Interval Started %d", migNum);
            counters->updateNumM1Pages(num_m1_pages, num_m2_pages);
            counters->generateMigrationPairs();
            start_migration();
            counters->clear();
            debugmig("Migration Interval Ended %d", migNum); 
        }        
    }

    /***************************** MIGRATION SUPPORT FUNCTIONS *****************************************/
    void Hybrid::initMigration(const Config& configs){
        //Migrations only in **allocated** M1 pages. 
        if(configs["counter_type"] == "HMA"){
            counters = new HMA("HMA");
        }
        else if(configs["counter_type"] == "HMAWrRd"){
            counters = new HMA("HMAWrRd");
        }
        else if(configs["counter_type"] == "HMAWrWrRd"){
            counters = new HMA("HMAWrWrRd");
        }
        else if(configs["counter_type"] == "MEA"){
            counters = new MEA(configs);    
        }
        else if(configs["counter_type"] == "MEAWr2Rd1"){
            counters = new MEAWr2Rd1(configs);    
        }

        //Migrations in **unallocated** M1 pages permited.
        else if(configs["counter_type"] == "HMAUnAlloc"){
            counters = new HMAUnAlloc("HMAUnAlloc");
        }
        else if(configs["counter_type"] == "HMAUnAllocThd1"){
            counters = new HMAUnAlloc("HMAUnAllocThd1");
        }
        else if(configs["counter_type"] == "HMAUnAllocThd2"){
            counters = new HMAUnAlloc("HMAUnAllocThd2");
        }
        else if(configs["counter_type"] == "MEAUnAlloc"){
            counters = new MEAUnAlloc(configs);    
        }
        else if(configs["counter_type"] == "MEAupHMAdown"){
            counters = new MEAupHMAdown(configs);
        }
        else if(configs["counter_type"] == "FullCounters"){
            counters = new FullCounters(configs, "FullCounters");
        }
        else if(configs["counter_type"] == "FullCounters75"){
            counters = new FullCounters(configs, "FullCounters75");
        }
 
        else{
            printf("FATAL! Inside Hybrid::initMigration no counter type present in config\n");
            exit(-1);
        }
    }

    void Hybrid::hybrid_set_m1_m2_avf(MemoryBase* mem1, MemoryBase* mem2, AVF* avf){
        m1=mem1; m2=mem2; mavf=avf;
    }


    void Hybrid::start_migration(){
        debugmig("clk(%ld) migNum(%d) migrationPairs.size()(%zu)", clk, migNum, (counters->migrationPairs).size());
        numOfMigrations += counters->migrationPairs.size();
        pendingMigration=true;        
    }

    void Hybrid::perform_swap(){
        debugmig("clk(%ld) migNum(%d) migrationPairs.size()(%zu)", clk, migNum, (counters->migrationPairs).size());
        //printf("clk(%ld) migNum(%d) migrationPairs.size()(%zu)\n", clk, migNum, (counters->migrationPairs).size());
        /*perform_swap notes
        1. Issues the actual memory requests to swap pages in HBM(M1) <---> DDR(M2) memroy. 
        p2. Record a list of swap cycles TM1, TM2, TM1 and apply vulnerabilty in reverse order.         
        2. Updates remapTable to relay address to new page locations.
        3a. Update vulnerable cycles of all the lines in swapped pages. 
                                    OR
        4. Calls swap_completed in the end.
        */

        //1. MGUPTA: We are skipping the step 1 for now. We will just update the remapTable to
        //Evaluate how much gain can we get from dynamic schemes.
        swapHappening=true;

        //Current interval has nothing to migrate. 
        if(counters->migrationPairs.size() == 0) {
            swap_completed();
            return;
        }
        
        //p2. call to update pagewise miglist in AVF class
        mavf->updateAVFMigList(counters->migrationPairs, remapTable, num_m1_pages, num_m2_pages);

        //2. Update remapTable
        //MGUPTACHK: This loop over all migration pairs might kill performance. 
        debugmig("-------- remapTable before migNum (%d) -------", migNum);
        debugmig("------- (%ld) ---------- \n", remapTable->debugPrintRT());
        for(auto& mp : counters->migrationPairs){
            Request& req_1 = mp.first;
            Request& req_2 = mp.second;

            long pid1 = counters->getPageID(req_1);
            long pid2 = counters->getPageID(req_2);

            remapTable->updateTable(pid1, pid2);
        }
        debugmig("-------- remapTable after migNum (%d) ------------- \n", migNum);
        debugmig("-------- (%ld) ------------ \n", remapTable->debugPrintRT());
        
        //3a. Update vulnerable cycles of swapped pages.
        //mavf->updateAVFmigrationPairs(counters->migrationPairs, remapTable);

        //4. Call swap_completed.
        swap_completed();
    }

    void Hybrid::swap_completed(){
        debugmig("clk(%ld) migNum(%d) migrationPairs.size()(%zu)", clk, migNum, counters->migrationPairs.size());
        pendingMigration=false;
        swapHappening=false; 
    }
    
    long Hybrid::remapAddr(long req_addr){
        //RemapTable: MGUPTA remap physical address (req_addr) to remapped address.
        if(ismigrate){
            return remapTable->remapRequestAddr(req_addr);
        }
        else
            return req_addr; //no migration.
    }


    /*
    long Hybrid::lookupRemapTable(long pid){
        return remapTable->lookup(pid);    
    }
    */
    
    /*
    bool Hybrid::updateRemapTable(long pid_1, long pid_2){
        return remapTable->updateTable(pid_1, pid_2);      
    }
    */

    /**** MIGRATION HELPER Functions ****/
    unsigned int Hybrid::pending_requests(){
        return m1->pending_requests() + m2->pending_requests();    
    }

    /**** Hybrid Constructors *****/
    Hybrid::Hybrid(const Config& configs, string filename){
        hybrid_set_configs(configs);
        if(filename != "none"){
            debughyb("Static Page Map M1 Enabled");
            static_map_m1 = true;
            m1_map_file.open(filename);
        }
    }
}
