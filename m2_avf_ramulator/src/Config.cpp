#include "Config.h"

using namespace std;
using namespace ramulator;

Config::Config(const std::string& fname) {
  parse(fname);
}

void Config::parse(const string& fname)
{
    ifstream file(fname);
    assert(file.good() && "Bad config file");
    string line;
    while (getline(file, line)) {
        char delim[] = " \t=";
        vector<string> tokens;

        while (true) {
            size_t start = line.find_first_not_of(delim);
            if (start == string::npos) 
                break;

            size_t end = line.find_first_of(delim, start);
            if (end == string::npos) {
                tokens.push_back(line.substr(start));
                break;
            }

            tokens.push_back(line.substr(start, end - start));
            line = line.substr(end);
        }

        // empty line
        if (!tokens.size())
            continue;

        // comment line
        if (tokens[0][0] == '#')
            continue;

        // parameter line
        assert(tokens.size() == 2 && "Only allow two tokens in one line");

        options[tokens[0]] = tokens[1];

        if (tokens[0] == "channels") {
          channels = atoi(tokens[1].c_str());
        } else if (tokens[0] == "ranks") {
          ranks = atoi(tokens[1].c_str());
        } else if (tokens[0] == "subarrays") {
          subarrays = atoi(tokens[1].c_str());
        } else if (tokens[0] == "cpu_tick") {
          cpu_tick = atoi(tokens[1].c_str());
        } else if (tokens[0] == "mem_tick") {
          mem_tick = atoi(tokens[1].c_str());
        } else if (tokens[0] == "expected_limit_insts") {
          expected_limit_insts = atoi(tokens[1].c_str());
        }
    }
    file.close();
}

void Config::parseCommandLineArgs(int trace_start, int argc, const char **argv){
    printf("Config::parseCommandLineArgs trace_start (%d) to argc(%d) \n", trace_start, argc);
    for (int i = trace_start; i<argc; i=i+2){
        string bypass(argv[i]);
        string token = bypass.substr(2, string::npos);
        options[token] = argv[i+1];
        cout<<"options["<<token<<"]: "<<options[token]<<"\n";
    }
}

void Config::printConfig() {    
    cout <<"----------------------"<<endl;
    cout <<"PRINTING CONFIGURATION"<<endl
         <<"----------------------"<<endl;
    for (auto it = options.begin(); it != options.end(); it++) {
        cout << it->first<<": "<<it->second<<endl;
    }

    cout<<endl<<"Printing config variables"<<endl
              <<"-------------------------"<<endl;
    cout << "channels = "<<channels<<endl;
    cout << "ranks = "<<ranks<<endl;
    cout << "subarrays = "<<subarrays<<endl;
    cout << "cpu_tick = "<<cpu_tick<<endl;
    cout << "mem_tick = "<<mem_tick<<endl;
    cout <<"-------------------------------"<<endl;
    cout <<"FINISHED PRINTING CONFIGURATION"<<endl
         <<"-------------------------------"<<endl<<endl;

}
