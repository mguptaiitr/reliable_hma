#ifndef __HMAUNALLOC_H
#define __HMAUNALLOC_H
#include "Counters.h"

using namespace std;

namespace ramulator {
    class HMAUnAlloc : public Counters {
        public:
            //HMA counting variables
            unsigned int thetaHot = 32;
            map<long, pair<long, long>> pgRdWrCnt;

            //Interval-based statistics
            long mean_m1_hotness = 0, mean_m1_writes = 0, mean_m1_reads = 0;
            long mean_m2_hotness = 0, mean_m2_writes = 0, mean_m2_reads = 0;

            //Put the same above values in a vecotr to dump in the end of simulation
            vector<long> m1_hot, m1_writes, m1_reads;
            vector<long> m2_hot, m2_writes, m2_reads;
            vector<long> mig_cnt;
            vector<double> m1_hot_std, m2_hot_std;

            //File to store interval-based statistics
            string interval_filename;
            ofstream interval_stat_file;

            //Constructor and Destructors
            HMAUnAlloc(string hmatype) {
                if(hmatype=="HMAUnAlloc") {
                    cmp_func = cmp_hot;
                    thd_func = thd_mean;
                    type = Counters::Type::HMAUnAlloc;
                }
                else if(hmatype=="HMAUnAllocThd1") {
                    cmp_func = cmp_hot;
                    thd_func = thd_mean_p_1std;
                    type = Counters::Type::HMAUnAllocThd1;
                }
                else if(hmatype=="HMAUnAllocThd2") {
                    cmp_func = cmp_hot;
                    thd_func = thd_mean_p_2std;
                    type = Counters::Type::HMAUnAllocThd2;
                }
               else { printf("FATAL! HMA type not found!!\n"); exit(-1); }
            }

            ~HMAUnAlloc() {
                assert(m1_hot.size()==m2_hot.size() && "M1 and M2 vectors not of same length. NOT WRITING INTERVAL STATS");
                
                //Open interval stat file 
                interval_filename = avfout_mg+"/"+bench_mg+"_interval_stats.csv";
                interval_stat_file.open(interval_filename);

                //Write to interval stats
                interval_stat_file<<"#Interval, Mean M1 page hotness, Std M1 hotness, Mean M1 Reads, Mean M1 Writes,"
                                               "Mean M2 page hotness, Std M2 hotness, Mean M2 Reads, Mean M2 Writes,"
                                               "#Migrations\n";
                for(unsigned int i=0; i<m1_hot.size(); i++) {
                    interval_stat_file<<i<<","<<m1_hot[i]<<","<<m1_hot_std[i]<<","<<m1_reads[i]<<","<<m1_writes[i]<<","
                                              <<m2_hot[i]<<","<<m2_hot_std[i]<<","<<m2_reads[i]<<","<<m2_writes[i]<<","
                                              <<mig_cnt[i]<<"\n";
                }
                //Close interval stat file
                interval_stat_file.close();
            }

            //Functions
            //sorting functions
            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_hot =
                [](pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r) 
                {return (l.second.first + l.second.second) > (r.second.first + r.second.second);};

            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_wrrd =
                [](pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r) 
                {return (l.second.second/(l.second.first+1)) > (r.second.second/(r.second.first+1));};

            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_wrwrrd =
                [](pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r) 
                {return (l.second.second^2/(l.second.first+1)) > (r.second.second^2/(r.second.first+1));};

            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_func;
            
            //Hotness threshold functions
            function<long(long mean, double std)> thd_mean = [](long mean, double std) {return mean;};
            function<long(long mean, double std)> thd_mean_p_1std = [](long mean, double std) {return (mean + 1 * std);};
            function<long(long mean, double std)> thd_mean_p_2std = [](long mean, double std) {return (mean + 2 * std);};

            function<long(long mean, double std)> thd_func;


            //Define pure virtual functions from Counter class
            virtual void updateCounter(Request& req){
                long pageno = getPageID(req);
                auto pit = pgRdWrCnt.find(pageno);

                //add pageno in the counters if required
                if(pit == pgRdWrCnt.end())
                    pgRdWrCnt.insert(make_pair(pageno, make_pair(0, 0)));
                //Update read count
                if(req.type == Request::Type::READ)
                    pgRdWrCnt[pageno].first++;
                //Update write count
                if(req.type == Request::Type::WRITE)
                    pgRdWrCnt[pageno].second++;
            }

            virtual void clear(){
                //MGUPTACHK: can we just call clear
                //pgRdWrCnt.clear();
                debugcnt("Clearing counters");
                for(auto& kv : pgRdWrCnt){
                    auto& data = kv.second;
                    data.first = 0;
                    data.second = 0;
                }
                //reset mean hotness, read, and write counters
                mean_m1_hotness = 0; mean_m1_writes = 0; mean_m1_reads = 0;
                mean_m2_hotness = 0; mean_m2_writes = 0; mean_m2_reads = 0;
            }

            virtual void generateMigrationPairs(){
                debugcnt("Start generating migrationPair and push into migrationPair vector");
                migrationPairs.clear();

                //map to vector 
                vector<pair<long, pair<long, long>>> counterVec(pgRdWrCnt.begin(), pgRdWrCnt.end());

                //*** Sort countersVec in decreasing order of hotness only. (Performance-focused migrations)
                partial_sort(counterVec.begin(), counterVec.begin()+counterVec.size(), counterVec.end(), cmp_func);

                //Seperate fast and slow pages.
                vector<pair<long, pair<long, long>>> fastPages, slowPages;
                for(auto& cv : counterVec){
                    long pageno = cv.first;
                    if(getPageMemLevel(pageno) == 1)
                        fastPages.push_back(cv);
                    else
                        slowPages.push_back(cv);                    
                }
                //get mean hotness and riskyness for m1 and m2 pages in this interval. 
                getIntervalStats(fastPages, slowPages);

                debugcnt("Size of fastPages(%zu)", fastPages.size());
                debugcnt("Size of slowPages(%zu)", slowPages.size());

                //generate migration pairs and add in request vectors
                long migCnt = 0;
                
                //Find replacement for M2 pages (slowPages) in used M1 pages (fastPages)
                while(fastPages.size() > 0){
                    
                    auto slow_it = slowPages.begin() + migCnt;
                    auto fast_it = fastPages.end() - 1 - migCnt;

                    long slowAccess = (*slow_it).second.first + (*slow_it).second.second;
                    long fastAccess = (*fast_it).second.first + (*fast_it).second.second;

                    //terminating conditions while looking in *used* M1 pages.
                    if(migCnt > fastPages.size()) {
                        debugcnt("Finished looking in used M1 pages. migCnt (%ld)", migCnt);
                        break;
                    } 
                    if(slowAccess < fastAccess) {
                        debugcnt("Rest of the pages in M2 are less hotter than pages in remaining M1");
                        break;
                    }
                    /* Instead of threshold based cutoff we make sure half of the 
                    if(slowAccess <= thetaHot) {
                        debugcnt("All hot pages in M2 above threshold are moved to M1. Migration return from *used page* loop. migCnt (%ld)", migCnt);
                        return;
                    }
                    */
                    //Instead of threshold use mean page hotness (standard deviation can also be used along with it.)
                    if(slowAccess <= thd_func(mean_m2_hotness, m2_hot_std[m2_hot_std.size()-1])) {
                        debugcnt("All hot pages in M2 above mean hotness + standard deviation are migrated. Migration return from *used page* loop. migCnt (%ld)", migCnt);
                        mig_cnt.push_back(migCnt);
                        return;
                    }
 
                    if(migCnt >= slowPages.size()/2){
                        debugcnt("All hot pages migrated to M2. migCnt (%ld)", migCnt);
                        mig_cnt.push_back(migCnt);
                        return;    
                    }

                    pair<long, long> migPair = make_pair((*slow_it).first, (*fast_it).first);
                    pushMigrationPair(migPair, migCnt);
                    debugcnt("Migrating M2 page (%ld) with M1 *allocated page* (%ld)", (*slow_it).first, (*fast_it).first);
                    migCnt++;
                }

                //Find replacement for M2 pages (slowPages) in free M1 pages (first_free_m1_page --- max_m1_pages).
                while(slowPages.size()>0){
                    auto slow_it = slowPages.begin() + migCnt;
                    long slowAccess = (*slow_it).second.first + (*slow_it).second.second;
                    
                    //terminating conditions while looking in *unallocated* M1 pages.
                    if(first_free_m1_page >= max_m1_pages){
                        debugcnt("M1 pages are full. Filled unallocated M1 pages. migCnt (%ld)", migCnt);
                        mig_cnt.push_back(-migCnt);
                        return;                        
                    }
                    /* Threshold based termination
                    if(slowAccess <= thetaHot){
                        debugcnt("All hot pages in M2 above threshold are moved to M1. Migration terutnr from *free page* loop. migCnt (%ld)", migCnt);
                        return;
                    }
                    */
                    //Instead of threshold use mean page hotness (standard deviation can also be used along with it.)
                    if(slowAccess <= thd_func(mean_m2_hotness, m2_hot_std[m2_hot_std.size()-1])) {
                        debugcnt("All hot pages in M2 above mean hotness + standard deviation are migrated. Migration return from *used page* loop. migCnt (%ld)", migCnt);
                        mig_cnt.push_back(migCnt);
                        return;
                    }
 
                    if(migCnt >= slowPages.size()/2){
                        debugcnt("All hot page migrated M2. migCnt (%ld)", migCnt);
                        mig_cnt.push_back(migCnt);
                        return;
                    }
                    //First unused M1 page 
                    pair<long, long> migPair = make_pair((*slow_it).first, first_free_m1_page);
                    pushMigrationPair(migPair, migCnt);
                    debugcnt("Migrating M2 page (%ld) with M1 *free page* (%ld)", (*slow_it).first, first_free_m1_page);
                    
                    incFirstFreeM1Page();
                    migCnt++;
                }
                mig_cnt.push_back(migCnt);
            }

            void getIntervalStats(vector<pair<long, pair<long, long>>> fastPages, vector<pair<long, pair<long, long>>> slowPages){
                vector<long> m1_hot_vec, m2_hot_vec;
                double m1_stdv=0, m2_stdv=0;
                if(fastPages.size()){
                    for(auto& val : fastPages){
                        auto& data = val.second;
                        long page_hotness = data.first + data.second;
                        m1_hot_vec.push_back(page_hotness);
                        mean_m1_hotness += page_hotness;
                        mean_m1_reads += data.first;
                        mean_m1_writes += data.second;
                    }
                    mean_m1_hotness = mean_m1_hotness/(fastPages.size());
                    mean_m1_reads = mean_m1_reads/(fastPages.size());
                    mean_m1_writes = mean_m1_writes/(fastPages.size());
                    m1_stdv = getStandardDeviation(m1_hot_vec, mean_m1_hotness);
                }
                if(slowPages.size()){
                    for(auto& val : slowPages){
                        auto& data = val.second;
                        long page_hotness = data.first + data.second;
                        m2_hot_vec.push_back(page_hotness);
                        mean_m2_hotness += page_hotness;
                        mean_m2_reads += data.first;
                        mean_m2_writes += data.second;
                    }
                    mean_m2_hotness = mean_m2_hotness/(slowPages.size());
                    mean_m2_reads = mean_m2_reads/(slowPages.size());
                    mean_m2_writes = mean_m2_writes/(slowPages.size());
                    m2_stdv = getStandardDeviation(m2_hot_vec, mean_m2_hotness);
                }
                //Push the mean values to be printed at the end of the simulation into a vector
                m1_hot.push_back(mean_m1_hotness); m1_hot_std.push_back(m1_stdv); m1_reads.push_back(mean_m1_reads); m1_writes.push_back(mean_m1_writes); 
                m2_hot.push_back(mean_m2_hotness); m2_hot_std.push_back(m2_stdv); m2_reads.push_back(mean_m2_reads); m2_writes.push_back(mean_m2_writes);
            }


            //Counter common functions implemented here
    }; //class Counters
}//namespace ramulator
#endif //__HMAUNALLOC_H
