#ifndef __REMAP_TABLE_H
#define __REMAP_TABLE_H
#include "Debug.h"
#include <cassert>
#include <map>
#include <iostream>

using namespace std;

namespace ramulator {

    class RemapTable {
        public:
        RemapTable();
        ~RemapTable();

        //Returns the relay address(pid_ra) for the requested pid after migration.
        long lookup(long pid);
        //Returns the content(orig page address) for the requested pid after migration. 
        long getContent(long pid);
        bool updateTable(long pageno, long newpageno);        
        long remapRequestAddr(long addr);
        long debugPrintRT();

        private:
        //remapTable Incoming Request--> (Realy Address, Content)
        map <long, pair<long,long>> remapTable;

    }; //class RemapTable    
}//namespace ramulator

#endif //__REMAP_TABLE_H
