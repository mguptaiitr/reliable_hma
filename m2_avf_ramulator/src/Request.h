#ifndef __REQUEST_H
#define __REQUEST_H

#include <vector>
#include <functional>

using namespace std;

namespace ramulator
{

class Request
{
public:
    bool is_first_command;
    long addr; //holds the address to which actual memory request is sent after hybrid memory offset adjustment.  
    // long addr_row;
    vector<int> addr_vec;
    // specify which core this request sent from, for virtual address translation
    int coreid;
    
    enum class Type
    {
        READ,
        WRITE,
        REFRESH,
        POWERDOWN,
        SELFREFRESH,
        EXTENSION,
        MAX
    } type;

    long arrive = -1;
    long depart;
    function<void(Request&)> callback; // call back with more info

    //MGUPTA : Hybrid + Migration support 
    long lnum; //For debug purposes. This field holds the trace line number that created this request. 
    int target_mem = 1; //M1 == 1 and M2 == 2 Current location of the address. This should be after migration.
    long orig_addr = -1; //Holds the phyaddr before migration. For migration and remapping.
    long addr_ra = -1; //Holds the relayed address after migration. 
    bool isMigration = false; //isMigration is set to true for migration requests.
    //---------------------------------------------------------------------------------------------------------

    //Request Constructors
    Request(long addr, Type type, int coreid = 0)
        : is_first_command(true), addr(addr), coreid(coreid), type(type), callback([](Request& req){}), orig_addr(addr) {}


    //MGUPTA: Adding orig_addr for Migration and Remapping support.
    Request(long addr, long orig_addr, Type type, function<void(Request&)> callback, int coreid = 0)
        : is_first_command(true), addr(addr), coreid(coreid), type(type), callback(callback), orig_addr(orig_addr), addr_ra(addr) {}

    Request(long addr, Type type, function<void(Request&)> callback, int coreid = 0)
        : is_first_command(true), addr(addr), coreid(coreid), type(type), callback(callback), orig_addr(addr) {}

    Request(vector<int>& addr_vec, Type type, function<void(Request&)> callback, int coreid = 0)
        : is_first_command(true), addr_vec(addr_vec), coreid(coreid), type(type), callback(callback) {}

    Request()
        : is_first_command(true), coreid(0) {}

};

} /*namespace ramulator*/

#endif /*__REQUEST_H*/

