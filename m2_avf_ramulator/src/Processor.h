#ifndef __PROCESSOR_H
#define __PROCESSOR_H

#include "Cache.h"
#include "Config.h"
#include "Memory.h"
#include "Request.h"
#include "RemapTable.h"
#include "Counters.h"
#include "Statistics.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <ctype.h>
#include <functional>
#include "Debug.h"
#include "Hybrid.h"
#include "AVF.h"

namespace ramulator 
{

class Trace {
public:
    Trace(const char* trace_fname);
    // trace file format 1:
    // [# of bubbles(non-mem instructions)] [read address(dec or hex)] <optional: write address(evicted cacheline)>
    bool get_unfiltered_request(long& bubble_cnt, long& req_addr, Request::Type& req_type);
    bool get_filtered_request(long& bubble_cnt, long& req_addr, Request::Type& req_type);
    
    //MGUPTA: MZ trace format get_request_new_format in mz's hybrid ramulator. 
    bool get_mz_16_core_request(long& bubble_cnt, long& req_addr, Request::Type& req_type, long& req_lnum);

    // trace file format 2:
    // [address(hex)] [R/W]
    bool get_dramtrace_request(long& req_addr, Request::Type& req_type);
    
    long lines = 0; //Number of lines processed in the trace file
    long n_bubble = 0; //For sanity check. Checking if number of retired inst == sum(bubble_cnt)

private:
    std::ifstream file;
    std::string trace_name;
};


class Window {
public:
    /*
    int ipc = 4;
    int depth = 128;
    */
    //MGUPTA : Simulating 16 Cores
    int ipc = 64; //4*16 
    int depth = 2048; //128*16
 
    Window() : ready_list(depth), addr_list(depth, -1) {}
    bool is_full();
    bool is_empty();
    void insert(bool ready, long addr);
    long retire();
    void set_ready(long addr, int mask);

private:
    int load = 0;
    int head = 0;
    int tail = 0;
    std::vector<bool> ready_list;
    std::vector<long> addr_list;
};


class Core {
public:
    long clk = 0;
    long retired = 0;
    int id = 0;
    function<bool(Request)> send;
    //MGUPTA: send function for hybrid memory
    function<bool(Request)> send_l2; 
    
    Core(const Config& configs, int coreid,
        const char* trace_fname,
        function<bool(Request)> send_next, Cache* llc,
        std::shared_ptr<CacheSystem> cachesys, MemoryBase* memory,
        AVF* mavf);
    
    void tick();
    
    //MGUPTA: Adding hybrid Core construction and hybrid_tick
    Core(const Config& configs, const Config& configs_l2, int coreid,
        const char* trace_fname,
        function<bool(Request)> send_next, function<bool(Request)> send_next_l2,
        Cache* llc,
        std::shared_ptr<CacheSystem> cachesys, MemoryBase* memory, MemoryBase* memory_l2,
        Hybrid* hyb,
        AVF* mavf);
    void hybrid_tick();
    //-----------------------------------------------------------
    void receive(Request& req);
    double calc_ipc();

    //MGUPTA: Adding new calc_ipc for printing ipc clk and retired instructions in the end.
    void calc_ipc_mg();
    //long remapAddr(long addr); //Function moved to Hybrid class
    //------------------------------------------------------------------------------------
    
    bool finished();
    bool has_reached_limit();
    function<void(Request&)> callback;

    bool no_core_caches = true;
    bool no_shared_cache = true;
   
    int l1_size = 1 << 15;
    int l1_assoc = 1 << 3;
    int l1_blocksz = 1 << 6;
    int l1_mshr_num = 16;

    int l2_size = 1 << 18;
    int l2_assoc = 1 << 3;
    int l2_blocksz = 1 << 6;
    int l2_mshr_num = 16;
    std::vector<std::shared_ptr<Cache>> caches;
    Cache* llc;

    ScalarStat record_cycs;
    ScalarStat record_insts;
    long expected_limit_insts;
    // This is set true iff expected number of instructions has been executed or all instructions are executed.
    bool reached_limit = false;;

private:
    Trace trace;
    Window window;

    long bubble_cnt;
    long req_addr = -1; 
    long req_addr_ra = -1; //For migration use req_addr_ra (relayed address)
    Request::Type req_type;
    long req_lnum = -1; //For debug purposes adding a field in req that has req line no in trace file.
    bool more_reqs;
    long last = 0;

    ScalarStat memory_access_cycles;
    ScalarStat cpu_inst;
    
    //MGUPTA: Change memory from reference to ptr
    //MemoryBase& memory;
    MemoryBase* memory;
    MemoryBase* memory_l2;
    Hybrid* hyb;
    RemapTable* remapTable;
    Counters* counters;
    long m1_max_address = -1;
    
    AVF* mavf;
    bool record_mavf = false;
};

class Processor {
public:
    Processor(const Config& configs, 
                vector<const char*> trace_list,
                function<bool(Request)> send, 
                MemoryBase* memory, 
                AVF* mavf);

    void tick();
  
    //MGUPTA : Adding Processor hybrid constructor and hybrid_tick
    Processor(const Config& configs, const Config& configs_l2, 
                vector<const char*> trace_list,
                function<bool(Request)> send, function<bool(Request)> send_l2, 
                MemoryBase* memory, MemoryBase* memory_l2, 
                Hybrid* hyb, 
                AVF* mavf);

    void hybrid_tick();
    //------------------------------------

    void receive(Request& req);
    bool finished();
    bool has_reached_limit();

    std::vector<std::unique_ptr<Core>> cores;
    std::vector<double> ipcs;
    double ipc = 0;

    // When early_exit is true, the simulation exits when the earliest trace finishes.
    bool early_exit;

    bool no_core_caches = true;
    bool no_shared_cache = true;

    int l3_size = 1 << 23;
    int l3_assoc = 1 << 3;
    int l3_blocksz = 1 << 6;
    int mshr_per_bank = 16;

    std::shared_ptr<CacheSystem> cachesys;
    Cache llc;

    ScalarStat cpu_cycles;
    
    //MGUPTA custom ipc calculator in processor class called after all memory request are served
    void calc_ipc_mg();
    //------------------------------------------------------------------------------------------
};

}
#endif /* __PROCESSOR_H */
