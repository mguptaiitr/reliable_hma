#include "RemapTable.h"

using namespace ramulator;
using namespace std;

//Contructor
RemapTable::RemapTable() {}

//Destructor
RemapTable::~RemapTable(){}


//Retuns true if update was successful.
bool RemapTable::updateTable (long pid_1, long pid_2) {
    long content_1, content_2;

    //Check if pageno exist in remapTable
    auto it = remapTable.find(pid_1);
    if(it == remapTable.end()) {
        debugrt("New entry to remap table for pid_1 (%ld)", pid_1);
        remapTable[pid_1] = make_pair(pid_1, pid_1);
        content_1 = remapTable[pid_1].second;
    }
    else{
        content_1 = remapTable[pid_1].second;
    }

    //Check if newpageno exist in remapTable
    it = remapTable.find(pid_2);
    if(it == remapTable.end()){
        debugrt("New entry to remap table for pid_2 (%ld)", pid_2);
        remapTable[pid_2] = make_pair(pid_2, pid_2);
        content_2 = remapTable[pid_2].second;
    }
    else{
        content_2 = remapTable[pid_2].second;
    }

    //Get relay address after reading contents
    //long ra1 = remapTable[pid_1].first;
    //long ra2 = remapTable[pid_2].first;

    //Update realy address ? Should we use pid_1, pid_2 to index or content_1 and content_2. 
    //From the PDF looks like it should be content_1 and contect_2
    //remapTable[pid_1].first = ra2;
    //remapTable[pid_2].first = ra1;

    //Correct code according to the PDF
    remapTable[content_1].first = pid_2;
    remapTable[content_2].first = pid_1;

    //MGUPTA: (Andreas) Update the contencts of the swapped pages
    remapTable[pid_1].second = content_2;
    remapTable[pid_2].second = content_1;

    return true;
}

//Returns the new page id location for the current pid
long RemapTable::lookup (long pid){
    auto it = remapTable.find(pid);
    if(it == remapTable.end()) {
        debugrt("New entry to remap table for pid (%ld)", pid);
        remapTable[pid] = make_pair(pid, pid);
    }
    debugrt("pid (%ld) --remapTable[pid].first--> (%ld)", pid, remapTable[pid].first);
    return remapTable[pid].first;
}

//Returns the content (origaddr) for the current pid
long RemapTable::getContent(long pid){
    auto it = remapTable.find(pid);
    //assert(it != remapTable.end() && "RemapTable->getContent failed bcoz pid not found");
    if (it == remapTable.end()) return -1;
    return remapTable[pid].second;
}

//Remaps the requested address using RemapTable::lookup to new address
//If RemapTable::lookup gets a cache miss function returns -1 
long RemapTable::remapRequestAddr(long addr){
    long pid = addr>>12;
    long pid_ra = lookup(pid);
    
    debugrt("pid (%ld) remapped to pid_ra (%ld)", pid, pid_ra);
    assert(pid_ra >= 0 && "RemapTable cache yet to be implemented. How did we get here?");
    //RemapTable::lookup cache miss
    if(pid_ra == -1) return -1;

    //Similar to vir_to_phy addr is used to extract pid. pid is used to get remapped pid_ra.
    //pid_ra and lower 12 bits of original address are combined to generate remapped address.
    //Lower 12 bits of the trace address is used to locate cacheline inside pages.
    long addr_ra = (pid_ra<<12) | (addr & ((1 << 12) - 1)); 

    return addr_ra;
}

long RemapTable::debugPrintRT(){
    printf("\n----- Remap table [In PID -> (Relay PID, Content)]  ------\n");
    for(auto& kv : remapTable){
        long pid = kv.first;
        pair<long, long>& ra_content = kv.second;
        printf("%ld: [%ld, %ld]\n", pid, ra_content.first, ra_content.second);   
    }
    return 0;    
}
