#ifndef __HMA_H
#define __HMA_H
#include "Counters.h"

using namespace std;

namespace ramulator {
    class HMA : public Counters {
        public:
            //HMA counting variables
            unsigned int thetaHot = 32;
            map<long, pair<long, long>> pgRdWrCnt;
            
            //Constructor and Destructors
            HMA(string hmatype) {
                if(hmatype=="HMA") {
                    cmp_func = cmp_hot;
                    type = Counters::Type::HMA;
                }
                else if(hmatype=="HMAWrRd") { 
                    cmp_func = cmp_wrrd;
                    type = Counters::Type::HMAWrRd;
                }
                else if(hmatype=="HMAWrWrRd") {
                    cmp_func = cmp_wrwrrd;
                    type = Counters::Type::HMAWrWrRd;
                }
                else { printf("HMA type not found!!"); exit(-1); }
            }

            ~HMA() {}

            //Functions
            //sorting functions
            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_hot =
                [](pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r) 
                {return (l.second.first + l.second.second) > (r.second.first + r.second.second);};

            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_wrrd =
                [](pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r) 
                {return (l.second.second/(l.second.first+1)) > (r.second.second/(r.second.first+1));};

            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_wrwrrd =
                [](pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r) 
                {return (l.second.second^2/(l.second.first+1)) > (r.second.second^2/(r.second.first+1));};

            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_func;

            //Define pure virtual functions from Counter class
            virtual void updateCounter(Request& req){
                long pageno = getPageID(req);
                auto pit = pgRdWrCnt.find(pageno);

                //find the pageno in the counters
                if(pit == pgRdWrCnt.end())
                    pgRdWrCnt.insert(make_pair(pageno, make_pair(0, 0)));
                //Update read count
                if(req.type == Request::Type::READ)
                    pgRdWrCnt[pageno].first++;
                //Update write count
                if(req.type == Request::Type::WRITE)
                    pgRdWrCnt[pageno].second++;
            }

            virtual void clear(){
                //MGUPTACHK: can we just call clear
                //pgRdWrCnt.clear();
                debugmig("Clearing counters");
                for(auto& kv : pgRdWrCnt){
                    auto& data = kv.second;
                    data.first = 0;
                    data.second = 0;
                }
            }

            virtual void generateMigrationPairs(){
                debugmig("Start generating migrationPair and push into migrationPair vector");
                migrationPairs.clear();

                //map to vector 
                vector<pair<long, pair<long, long>>> counterVec(pgRdWrCnt.begin(), pgRdWrCnt.end());

                //sort countersVec
                //sort(counterVec.begin(), counterVec.end(), cmp_hot);
                partial_sort(counterVec.begin(), counterVec.begin()+counterVec.size(), counterVec.end(), cmp_func);

                //seperate fast and slow pages.
                vector<pair<long, pair<long, long>>> fastPages, slowPages;
                for(auto& cv : counterVec){
                    long pageno = cv.first;
                    if(getPageMemLevel(pageno) == 1)
                        fastPages.push_back(cv);
                    else
                        slowPages.push_back(cv);                    
                }
                
                 
                //Do not use unllocated M1 pages
                if(fastPages.size()==0){
                    printf("No pages allocated in fast memory yet \n");
                    return; 
                }

                debugmig("Size of fastPages(%zu)", fastPages.size());
                debugmig("Size of slowPages(%zu)", slowPages.size());

                //generate migration pairs and add in request vectors
                long migCnt = 0;

                while(true){
                    auto slow_it = slowPages.begin() + migCnt;
                    auto fast_it = fastPages.end() - 1 - migCnt;

                    long slowAccess = (*slow_it).second.first + (*slow_it).second.second;
                    long fastAccess = (*fast_it).second.first + (*fast_it).second.second;

                    //terminating conditions are same for all variants of HMA   
                    if(migCnt > fastPages.size()) return; //Do not use *unallocate* space.
                    //if(migCnt == max_m1_pages) return; //Use *unallocated* space.
                    if(slowAccess <= thetaHot) return;
                    if(slowAccess < fastAccess) return;

                    pair<long, long> migPair = make_pair((*slow_it).first, (*fast_it).first);
                    pushMigrationPair(migPair, migCnt);

                    migCnt++;
                }
            }


            //Counter common functions implemented here
    }; //class Counters
}//namespace ramulator
#endif //__HMA_H
