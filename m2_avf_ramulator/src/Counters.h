#ifndef __COUNTERS_H
#define __COUNTERS_H
#include "Debug.h"
#include "Request.h"
#include "Config.h"
#include <map>
#include <vector>
#include <algorithm>
#include <functional>
#include <iostream>

extern string avfout_mg;
extern string bench_mg;

using namespace std;

namespace ramulator {
    class Hybrid;

    class Counters {
        public:
            long max_m1_pages, max_m2_pages;
            long num_m1_pages, num_m2_pages;
            long first_free_m1_page = 0;

            //Migration support variables needed for any counting scheme.
            vector<pair<Request, Request>> migrationPairs;

            //Constructor and Destructors
            Counters() {}

            //Virtual constructor is required for base class with virtual functions
            virtual ~Counters() {
                printf("First free M1 page: %ld\n", first_free_m1_page);
            }

            enum class Type {
                HMA, //HMA sorted with decreasing WR+RD (access counts)
                HMAWrRd, //HMA sorted with decreasing WR/RD ratios
                HMAWrWrRd, //HMA sorted with decreasing WR^2/RD ratios
                MEA, 
                MEAWr2Rd1,
                MEAUnAlloc,
                HMAUnAlloc, //HMA sorted with decreasing WR+RD (access counts). Migration cutoff threshold mean hotness
                HMAUnAllocThd1, //HMA sorted with decreasing WR+RD (access counts). Migration cutoff threshold (mean + 1*std)
                HMAUnAllocThd2, //HMA sorted with decreasing WR+RD (access counts). Migration cutoff threshold (mean + 2*std)
                FullCounters, //Full read/write counts for both M1 and M2 with 50-50 hot/cold and low-risk/high-risk
                FullCounters75, //Full read/write counts for both M1 and M2 with 25-75
                MEAupHMAdown,
                MAX
            } type;

            enum mig_mode_t {reading, writing};

            //Pure Virtual functions to handle specific counters
            //Eg. HMA, THM, MEA, HMA_AVF, THM_AVF, MEA+AVF
            virtual void updateCounter(Request& req) = 0;
            virtual void clear() = 0;
            virtual void generateMigrationPairs() = 0;
            //-------------------------------------------------

            //pushes the migration pair (p1,p2) to the migrationPair vector.
            virtual void pushMigrationPair(pair<long, long> migPair, unsigned int migCnt){
                debugmig("migCnt(%d) migPair.first (%ld) <--> migPair.second (%ld)", migCnt, migPair.first, migPair.second);
                //migPageToAddress does what Andreas does in NLevelMemory::translate_pages_to_address and PageCounters::pageToAddress
                long migAddress1 = migPageToAddress(migPair.first);
                long migAddress2 = migPageToAddress(migPair.second);

                //create request to be issued for actual migrations.
                Request req_1 = createPageMigrationReq(migAddress1, migCnt, reading);
                Request req_2 = createPageMigrationReq(migAddress2, migCnt, reading);
                req_1.isMigration = true;
                req_2.isMigration = true;

                migrationPairs.push_back(make_pair(req_1, req_2));                    
            }

            /*Counter common functions implemented here*/

            //getPageID is called from updateCounter and generateMigrationPair
            //The page number returned from here can be 
            //1) original page number, 2) page number with N Level memory adjustment, and 3) page number after migration.
            long getPageID(Request& req){
                //return req.orig_addr>>12;    
                //return req.addr >> 12; //req.addr where actual memory request is sent addr - m1_max_address
                return req.addr_ra >> 12; //req.addr_ra is remapped address after migration. 
            }

            long migPageToAddress(long pageno){
                //Gives some address inside the page = pageno
                return pageno<<12;                
            }

            Request createPageMigrationReq(long addr, unsigned int migCnt, mig_mode_t migMode){
                function<void(Request&)> callback;
                callback = bind(&Counters::swapReturn, this, placeholders::_1, migCnt);

                //set request type
                Request::Type req_type;
                if (migMode == reading)
                    req_type = Request::Type::READ;
                else
                    req_type = Request::Type::WRITE;

                //Create request instance
                Request req(addr, addr, req_type, callback, 0);

                //Andreas calls decode_request. MGUPTACHK: you don't call
                //decode_req(req);

                return req;
            }

            void swapReturn(Request &req, unsigned int migCnt) {
                printf("Migration request # %d served on address (%ld)\n", migCnt, req.addr);                
            }

            void set_counters_max_pages(long mp1, long mp2){max_m1_pages=mp1; max_m2_pages=mp2;};


            unsigned int getPageMemLevel(long pageno){
                return (pageno >= max_m1_pages ? 2 : 1);    
            }

            void updateNumM1Pages(long nM1pages, long nM2pages) { num_m1_pages = nM1pages; num_m2_pages = nM2pages;}

            //For managing unallocated M1 pages
            void incFirstFreeM1Page() { first_free_m1_page++; }
            void decFirstFreeM1Page() { first_free_m1_page--; }


            //Common helper functions for interval-based statistics
            double getStandardDeviation(vector<long>& data, long mean){
                double sum_sq_diff = 0.0;
                for(auto& val : data){
                    double diff = val - mean;
                    sum_sq_diff += diff*diff;
                }
                double std_dev = sqrt(sum_sq_diff/double(data.size()));
                return std_dev; 
            }

    }; //class Counters
}//namespace ramulator
#endif //__COUNTERS_H
