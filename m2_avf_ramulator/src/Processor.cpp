#include "Processor.h"
#include <cassert>

using namespace std;
using namespace ramulator;

Processor::Processor(const Config& configs,
    vector<const char*> trace_list,
    function<bool(Request)> send_memory,
    MemoryBase* memory,
    AVF* mavf)
    : ipcs(trace_list.size(), -1),
    early_exit(configs.is_early_exit()),
    no_core_caches(!configs.has_core_caches()),
    no_shared_cache(!configs.has_l3_cache()),
    cachesys(new CacheSystem(configs, send_memory)),
    llc(l3_size, l3_assoc, l3_blocksz,
         mshr_per_bank * trace_list.size(),
         Cache::Level::L3, cachesys) {

  assert(cachesys != nullptr);
  int tracenum = trace_list.size();
  assert(tracenum == 1 && "More than one tracefile supplied. Check input command and commandline parsing.");
  printf("tracenum: %d\n", tracenum);
  for (int i = 0 ; i < tracenum ; ++i) {
    printf("trace_list[%d]: %s\n", i, trace_list[i]);
  }
  if (no_shared_cache) {
    for (int i = 0 ; i < tracenum ; ++i) {
      cores.emplace_back(new Core(configs, i, 
                                    trace_list[i], 
                                    send_memory, nullptr,
                                    cachesys, memory,
                                    mavf));
    }
  } else {
    for (int i = 0 ; i < tracenum ; ++i) {
      cores.emplace_back(new Core(configs, i, 
                                    trace_list[i],
                                    std::bind(&Cache::send, &llc, std::placeholders::_1), &llc, 
                                    cachesys, memory,
                                    mavf));
    }
  }
  for (int i = 0 ; i < tracenum ; ++i) {
    cores[i]->callback = std::bind(&Processor::receive, this,
        placeholders::_1);
  }

  // regStats
  cpu_cycles.name("cpu_cycles")
            .desc("cpu cycle number")
            .precision(0)
            ;
  cpu_cycles = 0;
}

//MGUPTA : Processor constructor for hybrid memory
Processor::Processor(const Config& configs,
    const Config& configs_l2,
    vector<const char*> trace_list,
    function<bool(Request)> send_memory,
    function<bool(Request)> send_memory_l2,
    MemoryBase* memory,
    MemoryBase* memory_l2,
    Hybrid* hyb,
    AVF* mavf)
    : ipcs(trace_list.size(), -1),
    early_exit(configs.is_early_exit()),
    no_core_caches(!configs.has_core_caches()),
    no_shared_cache(!configs.has_l3_cache()),
    cachesys(new CacheSystem(configs, send_memory, configs_l2, send_memory_l2, memory->get_max_address())),
    llc(l3_size, l3_assoc, l3_blocksz, mshr_per_bank * trace_list.size(), Cache::Level::L3, cachesys) {
    
  debughyb("Hybrid Constructor Processor::Processor");
  assert(send_memory && "send_memory function is empty");
  assert(memory && "memory is a null pointer");
  assert(send_memory_l2 && "send_memory_l2 function is empty");
  assert(memory_l2 && "memory_l2 is a null pointer");
  
  assert(cachesys != nullptr);
  int tracenum = trace_list.size();
  assert(tracenum > 0);
  printf("tracenum: %d\n", tracenum);
  for (int i = 0 ; i < tracenum ; ++i) {
    printf("trace_list[%d]: %s\n", i, trace_list[i]);
  }
  if (no_shared_cache) {
    for (int i = 0 ; i < tracenum ; ++i) {
      debughyb("Creating new core (no_shared_cache) coreid %d", i);
      cores.emplace_back(new Core(configs, configs_l2, i, 
          trace_list[i], 
          send_memory, send_memory_l2, 
          nullptr,
          cachesys, memory, memory_l2,
          hyb,
          mavf));
    }
  } else {
    for (int i = 0 ; i < tracenum ; ++i) {
      cores.emplace_back(new Core(configs, i, trace_list[i],
          std::bind(&Cache::send, &llc, std::placeholders::_1),
          &llc, cachesys, memory,
          mavf));
    }
  }
  for (int i = 0 ; i < tracenum ; ++i) {
    cores[i]->callback = std::bind(&Processor::receive, this,
        placeholders::_1);
  }

  // regStats
  cpu_cycles.name("cpu_cycles")
            .desc("cpu cycle number")
            .precision(0)
            ;
  cpu_cycles = 0;
}



void Processor::tick() {
  cpu_cycles++;
  if (!(no_core_caches && no_shared_cache)) {
    cachesys->tick();
  }
  for (unsigned int i = 0 ; i < cores.size() ; ++i) {
    Core* core = cores[i].get();
    core->tick();
  }
}

//MGUPTA : hybrid_tick() definition 
void Processor::hybrid_tick() {
  cpu_cycles++;
  if (!(no_core_caches && no_shared_cache)) {
    printf("hybrid tick to cache_sys\n");
    cachesys->hybrid_tick();
  }
  for (unsigned int i = 0 ; i < cores.size() ; ++i) {
    Core* core = cores[i].get();
    core->hybrid_tick();
  }
}


void Processor::receive(Request& req) {
  if (!no_shared_cache) {
    llc.callback(req);
  } else if (!cores[0]->no_core_caches) {
    // Assume all cores have caches or don't have caches
    // at the same time.
    for (unsigned int i = 0 ; i < cores.size() ; ++i) {
      Core* core = cores[i].get();
      core->caches[0]->callback(req);
    }
  }
  for (unsigned int i = 0 ; i < cores.size() ; ++i) {
    Core* core = cores[i].get();
    core->receive(req);
  }
}

bool Processor::finished() {
  if (early_exit) {
    for (unsigned int i = 0 ; i < cores.size(); ++i) {
      if (cores[i]->finished()) {
        for (unsigned int j = 0 ; j < cores.size() ; ++j) {
          ipc += cores[j]->calc_ipc();
        }
        return true;
      }
    }
    return false;
  } else {
    for (unsigned int i = 0 ; i < cores.size(); ++i) {
      if (!cores[i]->finished()) {
        return false;
      }
      if (ipcs[i] < 0) {
        ipcs[i] = cores[i]->calc_ipc();
        ipc += ipcs[i];
      }
    }
    return true;
  }
}

bool Processor::has_reached_limit() {
  for (unsigned int i = 0 ; i < cores.size() ; ++i) {
    if (!cores[i]->has_reached_limit()) {
      return false;
    }
  }
  return true;
}

//MGUPTA: Adding Processor custon ipc calculator.
void Processor::calc_ipc_mg(){
  for (unsigned int i = 0 ; i < cores.size(); ++i){
    cores[i]->calc_ipc_mg();
  }
}

//Core constructor for single memory.
Core::Core(const Config& configs, int coreid,
    const char* trace_fname, 
    function<bool(Request)> send_next,
    Cache* llc, std::shared_ptr<CacheSystem> cachesys, 
    MemoryBase* memory,
    AVF* mavf)
    : id(coreid), no_core_caches(!configs.has_core_caches()),
    no_shared_cache(!configs.has_l3_cache()),
    llc(llc), trace(trace_fname), memory(memory), memory_l2(nullptr),
    mavf(mavf)

{
   //MGUPTA: check if avf is on or off
   record_mavf = configs.is_avf_on();

  // Build cache hierarchy
  if (no_core_caches) {
    send = send_next;
  } else {
    // L2 caches[0]
    caches.emplace_back(new Cache(
        l2_size, l2_assoc, l2_blocksz, l2_mshr_num,
        Cache::Level::L2, cachesys));
    // L1 caches[1]
    caches.emplace_back(new Cache(
        l1_size, l1_assoc, l1_blocksz, l1_mshr_num,
        Cache::Level::L1, cachesys));
    send = bind(&Cache::send, caches[1].get(), placeholders::_1);
    if (llc != nullptr) {
      caches[0]->concatlower(llc);
    }
    caches[1]->concatlower(caches[0].get());
  }
  if (no_core_caches) {
    /* Original
    more_reqs = trace.get_filtered_request(
        bubble_cnt, req_addr, req_type);
    */
   //MGUPTA Use MZ get trace function of cahce filter traces
    more_reqs = trace.get_mz_16_core_request(
                        bubble_cnt, req_addr, req_type, req_lnum);
 
    req_addr = memory->page_allocator(req_addr, id);
  } else {
     assert(true && "Executing unfiltered request. Something WRONG!");
     more_reqs = trace.get_unfiltered_request(
        bubble_cnt, req_addr, req_type);
   
    req_addr = memory->page_allocator(req_addr, id);
  }

  // set expected limit instruction for calculating weighted speedup
  expected_limit_insts = configs.get_expected_limit_insts();

  // regStats
  record_cycs.name("record_cycs_core_" + to_string(id))
             .desc("Record cycle number for calculating weighted speedup. (Only valid when expected limit instruction number is non zero in config file.)")
             .precision(0)
             ;

  record_insts.name("record_insts_core_" + to_string(id))
              .desc("Retired instruction number when record cycle number. (Only valid when expected limit instruction number is non zero in config file.)")
              .precision(0)
              ;

  memory_access_cycles.name("memory_access_cycles_core_" + to_string(id))
                      .desc("memory access cycles in memory time domain")
                      .precision(0)
                      ;
  memory_access_cycles = 0;
  cpu_inst.name("cpu_instructions_core_" + to_string(id))
          .desc("cpu instruction number")
          .precision(0)
          ;
  cpu_inst = 0;
}

//MGUPTA: Core constructor for hybrid memory
Core::Core(const Config& configs, const Config& configs_l2, int coreid,
    const char* trace_fname, 
    function<bool(Request)> send_next, function<bool(Request)> send_next_l2,
    Cache* llc, std::shared_ptr<CacheSystem> cachesys, 
    MemoryBase* memory, MemoryBase* memory_l2, Hybrid* hyb,
    AVF* mavf)
    : id(coreid), no_core_caches(!configs.has_core_caches()),
    no_shared_cache(!configs.has_l3_cache()),
    llc(llc), trace(trace_fname), memory(memory), memory_l2(memory_l2), 
    hyb(hyb), mavf(mavf)
{ 
  debughyb("Hybrid Constructor Core::Core coreid %d", coreid);
  //MGUPTA: 1) Check if avf is on or off AVF is turned on/off.
  //        2) Set m1_max_address variable for only hybrid case to find target adress is in M1 or M2.
  //        3) If migration is enabled, set remapTable and counter pointers.
  record_mavf = configs.is_avf_on();
  m1_max_address = memory->get_max_address();
  if (hyb->ismigrate){
      remapTable = hyb->getRemapTablePtr();
      counters = hyb->getCountersPtr();
      assert((remapTable!=nullptr) && "remapTable not set in Core::Core hybrid constructor for migration");
      assert((counters!=nullptr) && "counters not set in Core::Core hybrid constructor for migration");
  }

  // Build cache hierarchy
  if (no_core_caches) {
    send = send_next;
    send_l2 = send_next_l2;
  } else {
    // L2 caches[0]
    caches.emplace_back(new Cache(
        l2_size, l2_assoc, l2_blocksz, l2_mshr_num,
        Cache::Level::L2, cachesys));
    // L1 caches[1]
    caches.emplace_back(new Cache(
        l1_size, l1_assoc, l1_blocksz, l1_mshr_num,
        Cache::Level::L1, cachesys));
    send = bind(&Cache::send, caches[1].get(), placeholders::_1);
    if (llc != nullptr) {
      caches[0]->concatlower(llc);
    }
    caches[1]->concatlower(caches[0].get());
  }
  if (no_core_caches) {
   //MGUPTA Use MZ get trace function of cahce filter traces
    more_reqs = trace.get_mz_16_core_request(
                        bubble_cnt, req_addr, req_type, req_lnum);
   
    req_addr = hyb->translate_address_vir_to_phy(req_addr);
    
    if(CHECK_TARGET_MEM(req_addr, m1_max_address) == 1)
        req_addr = memory->page_allocator(req_addr, id);
    else
        req_addr = memory_l2->page_allocator(req_addr, id);
  } 
  
  else { //For traces with cache request in them.
     assert(true && "Executing unfiltered request. Something WRONG!");
     more_reqs = trace.get_unfiltered_request(
        bubble_cnt, req_addr, req_type);

    if(CHECK_TARGET_MEM(req_addr, m1_max_address) == 1)
        req_addr = memory->page_allocator(req_addr, id);
    else
        req_addr = memory_l2->page_allocator(req_addr, id);
    
  }

  // set expected limit instruction for calculating weighted speedup
  expected_limit_insts = configs.get_expected_limit_insts();

  // regStats
  record_cycs.name("record_cycs_core_" + to_string(id))
             .desc("Record cycle number for calculating weighted speedup. (Only valid when expected limit instruction number is non zero in config file.)")
             .precision(0)
             ;

  record_insts.name("record_insts_core_" + to_string(id))
              .desc("Retired instruction number when record cycle number. (Only valid when expected limit instruction number is non zero in config file.)")
              .precision(0)
              ;

  memory_access_cycles.name("memory_access_cycles_core_" + to_string(id))
                      .desc("memory access cycles in memory time domain")
                      .precision(0)
                      ;
  memory_access_cycles = 0;
  cpu_inst.name("cpu_instructions_core_" + to_string(id))
          .desc("cpu instruction number")
          .precision(0)
          ;
  cpu_inst = 0;
}


void Core::calc_ipc_mg(){
    printf("(a) Lines in trace file: %ld\n", trace.lines);
    printf("(b) Non-memory instructions[SUM(bubble_cnt)]: %ld\n", trace.n_bubble);
    printf("(a) + (b): %ld\n", trace.lines + trace.n_bubble);
    printf("cpu_insts: %ld\n", long(cpu_inst.value()));
    printf("Core[%d] retired = %ld, clk = %ld\n", id, retired, clk);
    printf("Core[%d] IPC = %f\n", id, (double) retired/clk);

   //---------AVF finish-------
   if(record_mavf){
       mavf->printStoreAVF(retired, clk);
   }
   //---------------------------

}

double Core::calc_ipc()
{   
   return (double) retired / clk;
}

//Core tick for single memory.
void Core::tick()
{
    clk++;

    retired += window.retire();
    
    //Every core tick call avf->tick to sync avf clk 
    //and check if avf needs to generate new interval
    if(record_mavf)
        mavf->tick(retired);
    //----------------------------------------------
    
    if (expected_limit_insts == 0 && !more_reqs) return;

    // bubbles (non-memory operations)
    int inserted = 0;
    while (bubble_cnt > 0) {
        if (inserted == window.ipc) return;
        if (window.is_full()) return;

        window.insert(true, -1);
        inserted++;
        bubble_cnt--;
        cpu_inst++;
        if (long(cpu_inst.value()) == expected_limit_insts && !reached_limit) {
          record_cycs = clk;
          record_insts = long(cpu_inst.value());
          memory->record_core(id);
          reached_limit = true;
        }
    }

    if (req_type == Request::Type::READ) {
        // read request
        if (inserted == window.ipc) return;
        if (window.is_full()) return;

        Request req(req_addr, req_type, callback, id);
        if (!send(req)) return;

        window.insert(false, req_addr);
        cpu_inst++;
    }
    else {
        // write request
        assert(req_type == Request::Type::WRITE);
        Request req(req_addr, req_type, callback, id);
        if (!send(req)) return;
        cpu_inst++;
    }
    if (long(cpu_inst.value()) == expected_limit_insts && !reached_limit) {
      record_cycs = clk;
      record_insts = long(cpu_inst.value());
      memory->record_core(id);
      reached_limit = true;
    }

    if (no_core_caches) {
        //MGUPTA  : Mz 16 core get trace function
        more_reqs = trace.get_mz_16_core_request(
        bubble_cnt, req_addr, req_type, req_lnum);
        
      if (req_addr != -1) {
        req_addr = memory->page_allocator(req_addr, id);
      }
    } else {
        
      more_reqs = trace.get_unfiltered_request(
          bubble_cnt, req_addr, req_type);
        
      if (req_addr != -1) {
        req_addr = memory->page_allocator(req_addr, id);
      }
    }
    if (!more_reqs) {
      if (!reached_limit) { // if the length of this trace is shorter than expected length, then record it when the whole trace finishes, and set reached_limit to true.
        record_cycs = clk;
        record_insts = long(cpu_inst.value());
        memory->record_core(id);
        reached_limit = true;
      }
    }
}

//MGUPTA : hybrid_tick in core for hybrid memory
void Core::hybrid_tick()
{
    clk++;
    retired += window.retire();
    
    /******************* Hybrid Memory + AVF Related Code Starts ***************/
    
    //Every core tick call avf->tick to sync avf clk 
    //and check if avf needs to generate new interval
    if(record_mavf)
        mavf->tick(retired);
    //----------------------------------------------
 
    //interval in hybrid tick will proceed during memory drainage and swapping?
    hyb->tick(); 
    
    //Hybrid Memory Migration Handling M1 <--> M2 Memory
    if(hyb->pendingMigration){
        assert(hyb->ismigrate && "ismigrate set to false and we enetered migration in Core::hybrid_tick()");

        //pendingMigration is true: Wait till memory drains and pending_requests are served and swap (previous migration) is not happening.
        if(hyb->pending_requests() && !(hyb->swapHappening)) return;
        
        //We are here when both M1 and M2 memories are drained. Begin swap of migrtionPairs.
        hyb->perform_swap();
    }

    //MGUPTACHK Check if remapTable is updated when remapTable is present in memory. 
    //For initial exploration I have put remapTable entirely on chip.
    // if(hyb->remapTableUpdatesPending()) return;
    /********** Hybrid memory + AVF Related Code Ends **************************/
    
    if (expected_limit_insts == 0 && !more_reqs) return;

    //bubbles (non-memory operations)
    int inserted = 0;
    while (bubble_cnt > 0) {
        if (inserted == window.ipc) return;
        if (window.is_full()) return;

        window.insert(true, -1);
        inserted++;
        bubble_cnt--;
        cpu_inst++;
        if (long(cpu_inst.value()) == expected_limit_insts && !reached_limit) {
          record_cycs = clk;
          record_insts = long(cpu_inst.value());
          memory->record_core(id);
          memory_l2->record_core(id);
          reached_limit = true;
        }
    }
    
    //Counters: MGUPTA update the counters for read Request.
    //req_addr is the requested physical address after translate_vir_to_phy by the processor. 
    //req_addr_ra is the remapped physical location of the data.
    req_addr_ra = hyb->remapAddr(req_addr);
    if(req_addr_ra<0) return; //cache miss in RemapTable

    if (req_type == Request::Type::READ) {
        // read request
        if (inserted == window.ipc) return;
        if (window.is_full()) return;

        //Request is created with req_addr (Physical address after allocation)
        //req.orig_addr holds the address this physical address and req.addr is
        //remapped before sending to counters for updates and memory. 
        Request req(req_addr_ra, req_addr, req_type, callback, id);
        req.lnum = req_lnum; //traceline no for debug purposes
        //if(hyb->ismigrate) counters->updateCounter(req);
        //--------------------------------------------------------------------------------------------------------
    
        req.target_mem = CHECK_TARGET_MEM(req_addr_ra, m1_max_address);
        if(req.target_mem == 1){
            debughyb("send M1 RD line_num (%ld) req_addr (%ld) --> req_addr_ra (%ld)", req_lnum, req_addr, req_addr_ra);
            req.addr = req_addr_ra; //requested address from memory is req.addr which now relayed address
            if (!send(req)) return;
        }
        else {
            debughyb("send_l2 M2 RD line_num (%ld) req_addr (%ld) --> req_addr_ra (%ld)", req_lnum, req_addr, req_addr_ra);
            req.addr = req_addr_ra - m1_max_address; //requested address is relayed address - M1 offset.
            debughyb("M2 adjust: req_addr_ra (%ld) - m1_max_address(%ld) = req.addr (%ld)", req_addr_ra, m1_max_address, req.addr);
            if (!send_l2(req)) return;
        }
        assert(req.orig_addr != -1 && "orignal address not set right for hybrid memory case");
        if(hyb->ismigrate) counters->updateCounter(req);
        //window.insert(false, req_addr);
        window.insert(false, req.orig_addr);
        cpu_inst++;
    }
    else {
        // write request
        assert(req_type == Request::Type::WRITE);

        Request req(req_addr_ra, req_addr, req_type, callback, id);
        req.lnum = req_lnum; //traceline no for debug purposes
        //if(hyb->ismigrate) counters->updateCounter(req);
        //--------------------------------------------------------------------------------------------------------
        
        req.target_mem = CHECK_TARGET_MEM(req_addr_ra, m1_max_address);
        if(req.target_mem == 1){
            debughyb("send M1 WR req_lnum (%ld) req_addr(%ld) --> req_addr_ra (%ld)", req_lnum, req_addr, req_addr_ra);
            req.addr = req_addr_ra; //requested address from memory is req.addr which now relayed address
            if (!send(req)) return;
        }
        else{
            debughyb("send_l2 WR req_lnum (%ld) req_addr (%ld) --> req_addr_ra (%ld)", req_lnum, req_addr, req_addr_ra);
            req.addr = req_addr_ra - m1_max_address;//requested address is relayed address - M1 offset. 
            debughyb("M2 adjust: req_addr_ra (%ld) - m1_max_address (%ld) = req.addr (%ld)", req_addr_ra, m1_max_address, req.addr);
            if(!send_l2(req)) return;
        }        
        if(hyb->ismigrate) counters->updateCounter(req);
        cpu_inst++;
    }

    if (long(cpu_inst.value()) == expected_limit_insts && !reached_limit) {
      record_cycs = clk;
      record_insts = long(cpu_inst.value());
      memory->record_core(id);
      memory_l2->record_core(id);
      reached_limit = true;
    }
    
    //MGUPTA: We are here becuase previous req_addr is bundled into request is send successfully to memory or memory_l2. 
    if (no_core_caches) {
        //MGUPTA  : Mz 16 core get trace function
        more_reqs = trace.get_mz_16_core_request(bubble_cnt, req_addr, req_type, req_lnum);
        
        if (req_addr != -1) {
            req_addr = hyb->translate_address_vir_to_phy(req_addr);
            if(CHECK_TARGET_MEM(req_addr, m1_max_address) == 1)
                req_addr = memory->page_allocator(req_addr, id);
            else
                req_addr = memory_l2->page_allocator(req_addr, id);
        }
    } else {
        assert(true && "calling trace.get_unfiltered_request SOMETHING WRONG"); 
        more_reqs = trace.get_unfiltered_request(bubble_cnt, req_addr, req_type);

        if (req_addr != -1) {
            if(CHECK_TARGET_MEM(req_addr, m1_max_address) == 1)
                req_addr = memory->page_allocator(req_addr, id);
            else
                req_addr = memory_l2->page_allocator(req_addr, id);
        }
    }
    if (!more_reqs) {
      if (!reached_limit) { // if the length of this trace is shorter than expected length, then record it when the whole trace finishes, and set reached_limit to true.
        record_cycs = clk;
        record_insts = long(cpu_inst.value());
        memory->record_core(id);
        memory_l2->record_core(id);
        reached_limit = true;
      }
    }
}


bool Core::finished()
{
    return !more_reqs && window.is_empty();
}

bool Core::has_reached_limit() {
  return reached_limit;
}

void Core::receive(Request& req)
{
    /* Adding orig_add field in Request class which holds the physical address requested before remap table.
    if(req.target_mem == 2){
        debughyb("Core %d memory request is serviced from M2 address: %ld", id, req.addr);
        req.addr = req.addr + m1_max_address;
        debughyb("Request is for address: %ld", req.addr);
    } */

    debughyb("Core::receive line_num (%ld) req.target_mem (%d) req.addr (%ld) req.orig_addr (%ld) req.arrive (%ld) req.depart (%ld)",
        req.lnum, req.target_mem, req.addr, req.orig_addr, req.arrive, req.depart);
    /* window.set_ready() function uses req.orig_addr instead of req.addr */
    // window.set_ready(req.addr, ~(l1_blocksz - 1l));
    window.set_ready(req.orig_addr, ~(l1_blocksz - 1l));
    if (req.arrive != -1 && req.depart > last) {
      memory_access_cycles += (req.depart - max(last, req.arrive));
      last = req.depart;
    }
}

bool Window::is_full()
{
    return load == depth;
}

bool Window::is_empty()
{
    return load == 0;
}


void Window::insert(bool ready, long addr)
{
    assert(load <= depth);

    ready_list.at(head) = ready;
    addr_list.at(head) = addr;

    head = (head + 1) % depth;
    load++;
}


long Window::retire()
{
    assert(load <= depth);

    if (load == 0) return 0;

    int retired = 0;
    while (load > 0 && retired < ipc) {
        if (!ready_list.at(tail))
            break;

        tail = (tail + 1) % depth;
        load--;
        retired++;
    }

    return retired;
}


void Window::set_ready(long addr, int mask)
{
    if (load == 0) return;

    for (int i = 0; i < load; i++) {
        int index = (tail + i) % depth;
        if ((addr_list.at(index) & mask) != (addr & mask))
            continue;
        ready_list.at(index) = true;
    }
}



Trace::Trace(const char* trace_fname) : file(trace_fname), trace_name(trace_fname)
{
    if (!file.good()) {
        std::cerr << "Bad trace file: " << trace_fname << std::endl;
        exit(1);
    }
}

bool Trace::get_unfiltered_request(long& bubble_cnt, long& req_addr, Request::Type& req_type)
{
    cout<<"Call to get_unfiltered_request\n";
    string line;
    getline(file, line);
    if (file.eof()) {
      file.clear();
      file.seekg(0, file.beg);
      return false;
    }
    size_t pos, end;
    bubble_cnt = std::stoul(line, &pos, 10);
    pos = line.find_first_not_of(' ', pos+1);
    req_addr = std::stoul(line.substr(pos), &end, 0);

    pos = line.find_first_not_of(' ', pos+end);

    if (pos == string::npos || line.substr(pos)[0] == 'R')
        req_type = Request::Type::READ;
    else if (line.substr(pos)[0] == 'W')
        req_type = Request::Type::WRITE;
    else assert(false);
    return true;
}

bool Trace::get_filtered_request(long& bubble_cnt, long& req_addr, Request::Type& req_type)
{   
    cout<<"Call to get_filtered_request\n";
    static bool has_write = false;
    static long write_addr;
    static int line_num = 0;
    if (has_write){
        bubble_cnt = 0;
        req_addr = write_addr;
        req_type = Request::Type::WRITE;
        has_write = false;
        return true;
    }
    string line;
    getline(file, line);
    line_num ++;
    if (file.eof() || line.size() == 0) {
        file.clear();
        file.seekg(0, file.beg);
        has_write = false;
        line_num = 0;
        return false;
    }

    size_t pos, end;
    bubble_cnt = std::stoul(line, &pos, 10);

    pos = line.find_first_not_of(' ', pos+1);
    req_addr = stoul(line.substr(pos), &end, 0);
    req_type = Request::Type::READ;

    pos = line.find_first_not_of(' ', pos+end);
    if (pos != string::npos){
        has_write = true;
        write_addr = stoul(line.substr(pos), NULL, 0);
    }
    return true;
}

//MGUPTA : New trace get function definition.
bool Trace::get_mz_16_core_request(long& bubble_cnt, long& req_addr, Request::Type& req_type, long& req_lnum){
    static long line_num = 0;
    long req_pc, req_pid, acc_type, pid_count;
    string line;
    getline(file, line);
    line_num ++;
    /*Original trace termination code*/
    if (file.eof() || line.size() == 0) { 
        file.clear();
        file.seekg(0);
        line_num = 0;
        req_addr = -1; //reseting req_add to -1 so that last line in the trace file is not send to memory twice.
        return false;
    }
   
    lines++; 
    size_t pos0, pos1, pos2, pos3, pos4;
    pos0  = line.find(" ");
    pos1  = line.find(" ", pos0+1);
    pos2  = line.find(" ", pos1+1);
    pos3  = line.find(" ", pos2+1);
    pos4  = line.find(" ", pos3+1);

    bubble_cnt = stoul(line.substr(0, pos0), NULL, 10);
    req_addr = stoul(line.substr(pos0+1, pos1-pos0), NULL, 10);
    req_pc = stoul(line.substr(pos1+1, pos2-pos1), NULL, 10);
    req_pid = stoul(line.substr(pos2+1, pos3-pos2), NULL, 10);
    acc_type = stoul(line.substr(pos3+1, pos4-pos3), NULL, 10);
    pid_count = stoul(line.substr(pos4+1, line.length()-pos4+1), NULL, 10);
    req_lnum = line_num;

    if(acc_type == 2)
        req_type = Request::Type::WRITE;
    else
        req_type = Request::Type::READ;
    
    n_bubble += bubble_cnt;
    debughyb("line_num (%ld) bubble_cnt (%ld) req_addr (%ld) req_pc (%ld) acc_type (%ld) pid_count (%ld)", line_num, bubble_cnt, req_addr, req_pc, acc_type, pid_count);

    return true;

}

bool Trace::get_dramtrace_request(long& req_addr, Request::Type& req_type)
{
    string line;
    getline(file, line);
    if (file.eof()) {
        return false;
    }
    size_t pos;
    req_addr = std::stoul(line, &pos, 16);

    pos = line.find_first_not_of(' ', pos+1);

    if (pos == string::npos || line.substr(pos)[0] == 'R')
        req_type = Request::Type::READ;
    else if (line.substr(pos)[0] == 'W')
        req_type = Request::Type::WRITE;
    else assert(false);
    return true;
}
