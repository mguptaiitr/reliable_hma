#ifndef __DEBUG_H
#define __DEBUG_H

#define YELLOW 33m
#define GREEN  32m

#ifndef DEBUG_AVF
#define debugavf(...)
#else
#define debugavf(...) do { \
          printf("\033[32m[DEBUG AVF] Function %s | ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUGAVF */

#ifndef DEBUG_WRAVF
#define debugwravf(...)
#else
#define debugwravf(...) do { \
          printf("\033[32m[DEBUG WR AVF] Function %s | ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUGWRAVF */

#ifndef DEBUG_CHK
#define debugchk(...)
#else
#define debugchk(...) do { \
          printf("\033[32m[DEBUG CHK] Function %s | ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUGCHK */

#ifndef DEBUG_HYB
#define debughyb(...)
#else
#define debughyb(...) do { \
          printf("\033[33m[DEBUG HYB] Function %s | ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUGHYB */

#ifndef DEBUG_MAP
#define debugmap(...)
#else
#define debugmap(...) do { \
          printf("\033[33m[DEBUG MAP] Function %s | ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUGMAP */

#ifndef DEBUG_HBM
#define debughbm(...)
#else
#define debughbm(...) do { \
          printf("\033[33m[DEBUG HBM] Function %s | ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUGHYB */

#ifndef DEBUG_RT
#define debugrt(...)
#else
#define debugrt(...) do { \
          printf("\033[35m[DEBUG RemapTable] Function %s | ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUGRT */

#ifndef DEBUG_MIG
#define debugmig(...)
#else
#define debugmig(...) do { \
          printf("\033[36m[DEBUG Migrations] Function %s | ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUG_MIG */

#ifndef DEBUG_MEA
#define debugmea(...)
#else
#define debugmea(...) do { \
          printf("\033[36m[DEBUG MEA] Function %s | ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUG_MEA */

#ifndef DEBUG_CNT
#define debugcnt(...)
#else
#define debugcnt(...) do { \
          printf("\033[36m[DEBUG CNT] Function %s | ", __FUNCTION__); \
          printf(__VA_ARGS__); \
          printf("\033[0m\n"); \
      } while (0)
#endif /* DEBUG_CNT */


#endif /* __DEBUG_H */
