#include "Processor.h"
#include "Config.h"
#include "Controller.h"
#include "SpeedyController.h"
#include "Memory.h"
#include "DRAM.h"
#include "Statistics.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>
#include <functional>
#include <map>

/* Standards */
#include "Gem5Wrapper.h"
#include "DDR3.h"
#include "DDR4.h"
#include "DSARP.h"
#include "GDDR5.h"
#include "LPDDR3.h"
#include "LPDDR4.h"
#include "WideIO.h"
#include "WideIO2.h"
#include "HBM.h"
#include "SALP.h"
#include "ALDRAM.h"
#include "TLDRAM.h"

#include "Debug.h"
#include "AVF.h"

using namespace std;
using namespace ramulator;

string bench_mg; //benchmark name
string mode_mg;  //normal or hybrid mode
string avfout_mg="None"; //path of the folder in avf_output
string ramulator_mg;    //path to RELIABLE_HMA

template<typename T>
void run_dramtrace(const Config& configs, Memory<T, Controller>* memory, const char* tracename) {

    /* initialize DRAM trace */
    Trace trace(tracename);

    /* run simulation */
    bool stall = false, end = false;
    int reads = 0, writes = 0, clks = 0;
    long addr = 0;
    Request::Type type = Request::Type::READ;
    map<int, int> latencies;
    auto read_complete = [&latencies](Request& r){latencies[r.depart - r.arrive]++;};

    Request req(addr, type, read_complete);

    while (!end || memory->pending_requests()){
        if (!end && !stall){
            end = !trace.get_dramtrace_request(addr, type);
        }

        if (!end){
            req.addr = addr;
            req.type = type;
            stall = !memory->send(req);
            if (!stall){
                if (type == Request::Type::READ) reads++;
                else if (type == Request::Type::WRITE) writes++;
            }
        }
        memory->tick();
        clks ++;
        Stats::curTick++; // memory clock, global, for Statistics
    }
    // This a workaround for statistics set only initially lost in the end
    memory->finish();
    Stats::statlist.printall();

}
    template <typename T, typename K>
void hybrid_run_cputrace(const Config& configs, const Config& configs_l2, 
        Memory<T, Controller>* memory, Memory<K, Controller>* memory_l2, 
        const std::vector<const char *>& files, Hybrid* hyb,
        AVF* mavf)
{
    //L1 memory tick
    int cpu_tick = configs.get_cpu_tick();
    int mem_tick = configs.get_mem_tick();
    auto send = bind(&Memory<T, Controller>::send, memory, placeholders::_1);

    //L2 memory tick
    int cpu_tick_l2 = configs_l2.get_cpu_tick();
    int mem_tick_l2 = configs_l2.get_mem_tick();
    auto send_l2 = bind(&Memory<K, Controller>::send, memory_l2, placeholders::_1);
    
    //Create proc object by calling Processor hybrid constructor
    Processor proc(configs, configs_l2, files, send, send_l2, memory, memory_l2, hyb, mavf);
    for (long i = 0; ; i++) {
        proc.hybrid_tick();
        Stats::curTick++; // processor clock, global, for Statistics
        if (i % cpu_tick == (cpu_tick - 1))
            for (int j = 0; j < mem_tick; j++)
                memory->tick();
        if (i % cpu_tick_l2 == (cpu_tick_l2 - 1))   // Mz: for l2 memory
            for (int j = 0; j < mem_tick_l2; j++)
                memory_l2->tick();
        if (configs.calc_weighted_speedup()){
            if(proc.has_reached_limit()){
                break;
            }
        }
        if (configs.is_early_exit() && configs_l2.is_early_exit()) {
            if (proc.finished())
                break;
        } else {
            if (proc.finished() && ( (memory->pending_requests() == 0) &&  (memory_l2->pending_requests() == 0) ))
                break;
        }
    }

    //This a workaround for statistics set only initially lost in the end
    proc.calc_ipc_mg();
    memory->finish();
    memory_l2->finish();
    Stats::statlist.printall();
}


    template <typename T>
void run_cputrace(const Config& configs, Memory<T, Controller>* memory, const std::vector<const char *>& files, AVF* mavf)
{
    int cpu_tick = configs.get_cpu_tick();
    int mem_tick = configs.get_mem_tick();
    auto send = bind(&Memory<T, Controller>::send, memory, placeholders::_1);
    Processor proc(configs, files, send, memory, mavf);
    for (long i = 0; ; i++) {
        proc.tick();
        Stats::curTick++; // processor clock, global, for Statistics
        if (i % cpu_tick == (cpu_tick - 1))
            for (int j = 0; j < mem_tick; j++)
                memory->tick();
        if (configs.calc_weighted_speedup()) {
            if (proc.has_reached_limit()) {
                break;
            }
        } else {
            if (configs.is_early_exit()) {
                if (proc.finished())
                    break;
            } else {
                if (proc.finished() && (memory->pending_requests() == 0))
                    break;
            }
        }
    }
    // This a workaround for statistics set only initially lost in the end
    proc.calc_ipc_mg();
    memory->finish();
    Stats::statlist.printall();
}

template<typename T>
void start_run(const Config& configs, T* spec, const vector<const char*>& files) {
    // initiate controller and memory
    int C = configs.get_channels(), R = configs.get_ranks();
    // Check and Set channel, rank number
    spec->set_channel_number(C);
    spec->set_rank_number(R);
    std::vector<Controller<T>*> ctrls;
    for (int c = 0 ; c < C ; c++) {
        DRAM<T>* channel = new DRAM<T>(spec, T::Level::Channel);
        channel->id = c;
        channel->regStats("");
        Controller<T>* ctrl = new Controller<T>(configs, channel);
        ctrls.push_back(ctrl);
    }
    AVF *mavf = new AVF(configs);
    Memory<T, Controller>* memory = new Memory<T, Controller>(configs, ctrls, mavf);

    assert(files.size() != 0);
    if (configs["trace_type"] == "CPU") {
        run_cputrace(configs, memory, files, mavf);
    } else if (configs["trace_type"] == "DRAM") {
        run_dramtrace(configs, memory, files[0]);
    }
}

template<typename T, typename K>
void hybrid_start_run(const Config& configs, const Config& configs_l2, T* spec, K* spec_l2, const vector<const char*>& files, Hybrid *hyb) {
    // initiate controller and memory
    int C = configs.get_channels(), R = configs.get_ranks();
    //Mz: initiate 2nd level controller and memory
    int C_l2 = configs_l2.get_channels(), R_l2 = configs_l2.get_ranks();
    debughyb("Config M1=%s: Ch(%d) Ra(%d) and Config M2=%s: Ch(%d) Ra(%d)", configs["standard"].c_str(), C, R, configs_l2["standard"].c_str(), C_l2, R_l2);    

    // Check and Set channel, rank number
    spec->set_channel_number(C);
    spec->set_rank_number(R);
    std::vector<Controller<T>*> ctrls;
    for (int c = 0 ; c < C ; c++) {
        DRAM<T>* channel = new DRAM<T>(spec, T::Level::Channel);
        channel->id = c;
        channel->regStats("");
        Controller<T>* ctrl = new Controller<T>(configs, channel);
        ctrls.push_back(ctrl);
    }

    //One AVF variable for both memories
    AVF *mavf = new AVF(configs, configs_l2);
    Memory<T, Controller>* memory = new Memory<T, Controller>(configs, ctrls, mavf);

    //Mz: Check and Set l2 channel, rank number
    spec_l2->set_channel_number(C_l2);
    spec_l2->set_rank_number(R_l2);
    std::vector<Controller<K>*> ctrls_l2;
    for (int c = 0 ; c < C_l2 ; c++) {
        DRAM<K>* channel_l2 = new DRAM<K>(spec_l2, K::Level::Channel);
        channel_l2->id = c;
        channel_l2->regStats("");
        Controller<K>* ctrl_l2 = new Controller<K>(configs_l2, channel_l2);
        ctrls_l2.push_back(ctrl_l2);
    }
    Memory<K, Controller>* memory_l2 = new Memory<K, Controller>(configs_l2, ctrls_l2, mavf);
    
    //Set M1 and M2 max pages based on capacity and remap pages if page remapping enabled.
    //Set/Pass m1, m2, and avf memory pointers in hybrid memory class. 
    hyb->set_hybrid_max_pages(memory->get_max_address()>>12, memory_l2->get_max_address()>>12);
    hyb->hybrid_set_m1_m2_avf(memory, memory_l2, mavf);
    //---------------------------------------------------------------------------------------
    if(hyb->static_map_m1){
        hyb->get_m1_static_page_allocation();
    }
    hybrid_run_cputrace(configs, configs_l2, memory, memory_l2, files, hyb, mavf);
}

/*
void extract_benchmark_name(string tFile){
    cout<<"tFile: "<<tFile<<"\n";
    size_t found1 = tFile.find_last_of("/\\");
    cout<<"found1: "<<found1<<"\n";
    string dFile = tFile.substr(0, found1);
    cout<<"dFile: "<<dFile<<"\n";
    size_t found2 = dFile.find_last_of("/\\");
    cout<<"found2: "<<found2<<"\n";
    bench_mg=dFile.substr(found2+1, dFile.find_first_of("_"));
    cout<<"bench_mg "<<bench_mg<<"\n";
    size_t found3 = bench_mg.find("_");
    cout<<"found3: "<<found3<<"\n";
    bench_mg=bench_mg.substr(0, found3);
    cout<<"bench_mg "<<bench_mg<<"\n";
}
*/

/*Above extract_benchmark_name function breaks after pointing traces to home_masters*/
void extract_benchmark_name(string tFile){
    //cout<<"tFile: "<<tFile<<"\n";
    size_t found1 = tFile.find_last_of("/\\");
    //cout<<"found1: "<<found1<<"\n";
    string dFile = tFile.substr(0, found1);
    //cout<<"dFile: "<<dFile<<"\n";
    size_t found2 = dFile.find_last_of("/\\");
    //cout<<"found2: "<<found2<<"\n";
    bench_mg=dFile.substr(found2+1, dFile.find_last_of("_"));
    //cout<<"bench_mg "<<bench_mg<<"\n";
    size_t found3 = bench_mg.find("_");
    //cout<<"found3: "<<found3<<"\n";
    bench_mg=bench_mg.substr(0, found3);
    //cout<<"bench_mg "<<bench_mg<<"\n";
}

int main(int argc, const char *argv[])
{
    if (argc < 2) {
        printf("Usage: %s [normal, hybrid] <configs-file> --mode=cpu [--stats <filename>] <trace-filename1> <trace-filename2>\n"
                "Example1 (Single core normal mode): %s normal configs/DDR3-16GB-x4-config.cfg --mode=cpu --stat STATFILE_LOCATION --avfout AVFOUTPUT_LOCATION ~/nvm2016/cputraces/trace_mz_march_22_2016/trace_16core_32M/test1MB_cpu_trace/cpu_trace.trc\n"

                "Example 2.0 (Single core hybrid mode): %s hybrid configs/HBM-1GB-config.cfg configs/DDR3-16GB-x4-config.cfg --mode=cpu --stat STATFILE_LOCATION  --avfout AVFOUTPUT_LOCATION ~/nvm2016/cputraces/trace_mz_march_22_2016/trace_16core_32M/test1MB_cpu_trace/cpu_trace.trc \n"

                "Example 2.1 (Single core hybrid mode + static page map): %s hybrid configs/HBM-1GB-config.cfg configs/DDR3-16GB-x4-config.cfg --mode=cpu --stat STATFILE_LOCATION --avfout AVFOUTPUT_LOCATION --staticmap ~/nvm2016/reliable_hma/m2_avf_ramulator/static_page_maps/PAGEMAP  ~/nvm2016/cputraces/trace_mz_march_22_2016/trace_16core_32M/test1MB_cpu_trace/cpu_trace.trc \n"

                "Example3 (Multi core hybrid mode): %s hybrid configs/HBM-config.cfg configs/DDR3-x4-config.cfg --mode=cpu ~/nvm2016/cputraces/trace_mz_march_22_2016/split_trace_16core_32M/test1MB_cpu_trace/cputrace_test1MB_0.trc ~/nvm2016/cputraces/trace_mz_march_22_2016/split_trace_16core_32M/test1MB_cpu_trace/cputrace_test1MB_1.trc\n", argv[0], argv[0], argv[0], argv[0], argv[0]);

        return 0;
    }

    string run_mode = argv[1];
    mode_mg = run_mode; //mode is a global variable used find ramulator running mode "normal" or "hybrid"
    if(const char* env_p = getenv("RELIABLE_HMA")){
        ramulator_mg = env_p;
        cout<<"Envirionment variable RELIABLE_HMA = "<<ramulator_mg<<"\n";
    }
    else{        
        cout<<"FATAL!! Envirionment variable RELIABLE_HMA NOT FOUND!! \n";
        exit(0);
    }

    if(run_mode=="normal"){
        Config configs(argv[2]);

        const std::string& standard = configs["standard"];
        assert(standard != "" || "DRAM standard should be specified.");

        const char *trace_type = strstr(argv[3], "=");
        trace_type++;
        if (strcmp(trace_type, "cpu") == 0) {
            configs.add("trace_type", "CPU");
        } else if (strcmp(trace_type, "dram") == 0) {
            configs.add("trace_type", "DRAM");
        } else {
            printf("invalid trace type: %s\n", trace_type);
            assert(false);
        }

        int trace_start = 4;
        string stats_out;
        if (strcmp(argv[4], "--stats") == 0) {
            Stats::statlist.output(argv[5]);
            stats_out = argv[5];
            trace_start = 6;
        } else {
            Stats::statlist.output(standard+".stats");
            stats_out = standard + string(".stats");
        }
        
        //Location of avf analyzer
        if (strcmp(argv[trace_start], "--avfout") == 0) {
            trace_start++;
            printf("AVF output files will be written to folder --> %s\n", argv[trace_start]);
            avfout_mg = argv[trace_start++];
        }
        else{
            printf("FATAL! AVF OUTPUT LOCATION IS REQUIRED\n");
            exit(0);
        }

        std::vector<const char*> files(&argv[trace_start], &argv[trace_start+1]);
        configs.set_core_num(1);

        //parse cmd line arguments to bypass config files.
        //set the arguments from command line in configs.
        //note that avf/migration related configs are only
        //set from M1 configs and not M2 configs_l2.
        configs.parseCommandLineArgs(++trace_start, argc, argv);
  
        //MGUPTA: Extracting benchmark name
        extract_benchmark_name(files[0]);
        cout<<"bench_mg: "<<bench_mg<<"\n";
        //--------------------------
        if (standard == "DDR3") {
            DDR3* ddr3 = new DDR3(configs["org"], configs["speed"]);
            start_run(configs, ddr3, files);
        } else if (standard == "DDR4") {
            DDR4* ddr4 = new DDR4(configs["org"], configs["speed"]);
            start_run(configs, ddr4, files);
        } else if (standard == "SALP-MASA") {
            SALP* salp8 = new SALP(configs["org"], configs["speed"], "SALP-MASA", configs.get_subarrays());
            start_run(configs, salp8, files);
        } else if (standard == "LPDDR3") {
            LPDDR3* lpddr3 = new LPDDR3(configs["org"], configs["speed"]);
            start_run(configs, lpddr3, files);
        } else if (standard == "LPDDR4") {
            // total cap: 2GB, 1/2 of others
            LPDDR4* lpddr4 = new LPDDR4(configs["org"], configs["speed"]);
            start_run(configs, lpddr4, files);
        } else if (standard == "GDDR5") {
            GDDR5* gddr5 = new GDDR5(configs["org"], configs["speed"]);
            start_run(configs, gddr5, files);
        } else if (standard == "HBM") {
            HBM* hbm = new HBM(configs["org"], configs["speed"]);
            start_run(configs, hbm, files);
        } else if (standard == "WideIO") {
            // total cap: 1GB, 1/4 of others
            WideIO* wio = new WideIO(configs["org"], configs["speed"]);
            start_run(configs, wio, files);
        } else if (standard == "WideIO2") {
            // total cap: 2GB, 1/2 of others
            WideIO2* wio2 = new WideIO2(configs["org"], configs["speed"], configs.get_channels());
            wio2->channel_width *= 2;
            start_run(configs, wio2, files);
        }
        // Various refresh mechanisms
        else if (standard == "DSARP") {
            DSARP* dsddr3_dsarp = new DSARP(configs["org"], configs["speed"], DSARP::Type::DSARP, configs.get_subarrays());
            start_run(configs, dsddr3_dsarp, files);
        } else if (standard == "ALDRAM") {
            ALDRAM* aldram = new ALDRAM(configs["org"], configs["speed"]);
            start_run(configs, aldram, files);
        } else if (standard == "TLDRAM") {
            TLDRAM* tldram = new TLDRAM(configs["org"], configs["speed"], configs.get_subarrays());
            start_run(configs, tldram, files);
        }
        printf("Simulation done. Statistics written to %s\n", stats_out.c_str());
    }

    //Hybrid Mode
    else if (run_mode=="hybrid"){
        printf("Starting hybrid mode \n");
        Hybrid *hyb;

        Config configs(argv[2]);
        Config configs_l2(argv[3]);
        debughyb("Configurations config1 --> %s config2 --> %s", argv[2], argv[3]);
        
        const std::string& standard = configs["standard"];
        const std::string& standard_l2 = configs_l2["standard"]; 
        assert(standard != "" || "M1 standard should be specified.");
        assert(standard_l2 != "" || "M2 standard should be specified.");


        const char *trace_type = strstr(argv[4], "=");
        trace_type++;
        if (strcmp(trace_type, "cpu") == 0) {
            configs.add("trace_type", "CPU");
            configs_l2.add("trace_type", "CPU");
        } else {
            printf("invalid trace type: %s\n", trace_type);
            assert(false);
        }

        //Locaiton of stat files
        int trace_start = 5;
        string stats_out;
        if (strcmp(argv[5], "--stats") == 0) {
            debughyb("Statfile name --> %s", argv[6]);
            Stats::statlist.output(argv[6]);
            stats_out = argv[6];
            trace_start = 7;
        }  else {
            Stats::statlist.output(standard+"_"+ standard_l2+".stats");
            stats_out = standard+"_"+ standard_l2 + string(".stats");
        }

        //Location of avf analyzer output
        if (strcmp(argv[trace_start], "--avfout") == 0) {
            trace_start++;
            printf("AVF output files will be written to folder --> %s\n", argv[trace_start]);
            avfout_mg = argv[trace_start++];
        }
        else{
            printf("FATAL! AVF OUTPUT LOCATION IS REQUIRED\n");
            exit(0);
        } 

        //Check if static page map file is supplied
        string static_pm = "none";
        if (strcmp(argv[trace_start], "--staticmap") == 0) {
            trace_start++;
            static_pm = argv[trace_start++];
            printf("Static page map (ENABLED) Mapping File --> %s\n", static_pm.c_str());
        }
        
        /* Original multi-core settings
        std::vector<const char*> files(&argv[trace_start], &argv[argc]);
        configs.set_core_num(argc - trace_start);
        configs_l2.set_core_num(argc - trace_start);
        
        cout<<"trace_start: "<<trace_start<<"\n";
        cout<<"argc: "<<argc<<"\n";
        cout<<"argv[trace_start]: "<<argv[trace_start]<<"\n";
        cout<<"argv[argc]: "<<argv[argc]<<"\n";
        */
        
        //set single core and one trace file. 
        std::vector<const char*> files(&argv[trace_start], &argv[trace_start+1]);
        configs.set_core_num(1);
        configs_l2.set_core_num(1);
        
        debughyb("argc - trace_start                --> %d", argc - trace_start);
        debughyb("Number of cores set for memory M1 --> %d", configs.get_core_num());
        debughyb("Number of cores set for memory M2 --> %d", configs_l2.get_core_num());

        //MGUPTA: Extracting benchmark name
        extract_benchmark_name(files[0]);
        //--------------------------------

        //parse cmd line arguments to bypass config files.
        //set the arguments from command line in configs.
        //note that avf/migration related configs are only
        //set from M1 configs and not M2 configs_l2.
        configs.parseCommandLineArgs(++trace_start, argc, argv);
        
        //Set hybrid variable.
        hyb = new Hybrid(configs, static_pm);

        //Hybrid memory: M1 and M2 memories.
        HBM* hbm = new HBM(configs["org"], configs["speed"]);
        DDR3* ddr3 = new DDR3(configs_l2["org"], configs_l2["speed"]);
        
        //Start hybrid ramulator simulation
        hybrid_start_run(configs, configs_l2, hbm, ddr3, files, hyb);
        
        //Print basic stats after simulation
        delete hyb;
        printf("Simulation done. Statistics written to %s\n", stats_out.c_str());
    }
    else {
        printf("Invaild run-mode\n");
        exit(0);
    }
    return 0;
}
