#ifndef __CONFIG_H
#define __CONFIG_H

#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <iostream>
#include <cassert>

namespace ramulator
{

class Config {

private:
    std::map<std::string, std::string> options;
    int channels;
    int ranks;
    int subarrays;
    int cpu_tick;
    int mem_tick;
    int core_num = 0;
    long expected_limit_insts = 0;

public:
    Config() {}
    Config(const std::string& fname);
    void parse(const std::string& fname);
    std::string operator [] (const std::string& name) const {
      if (options.find(name) != options.end()) {
        return (options.find(name))->second;
      } else {
        printf("ATTENTION!! %s NOT found in configuration\n", name.c_str());
        return "";
      }
    }

    bool contains(const std::string& name) const {
      if (options.find(name) != options.end()) {
        return true;
      } else {
        return false;
      }
    }

    void add (const std::string& name, const std::string& value) {
      if (!contains(name)) {
        options.insert(make_pair(name, value));
      } else {
        printf("ramulator::Config::add options[%s] already set.\n", name.c_str());
      }
    }

    void set_core_num(int _core_num) {core_num = _core_num;}

    int get_channels() const {return channels;}
    int get_subarrays() const {return subarrays;}
    int get_ranks() const {return ranks;}
    int get_cpu_tick() const {return cpu_tick;}
    int get_mem_tick() const {return mem_tick;}
    int get_core_num() const {return core_num;}
    long get_expected_limit_insts() const {return expected_limit_insts;}
    bool has_l3_cache() const {
      if (options.find("cache") != options.end()) {
        const std::string& cache_option = (options.find("cache"))->second;
        return (cache_option == "all") || (cache_option == "L3");
      } else {
        return false;
      }
    }
    bool has_core_caches() const {
      if (options.find("cache") != options.end()) {
        const std::string& cache_option = (options.find("cache"))->second;
        return (cache_option == "all" || cache_option == "L1L2");
      } else {
        return false;
      }
    }
    
    //AVF configs ---------------------------------
    bool is_avf_on() const {
      if (options.find("avf") != options.end()) {
        const std::string& avf_option = (options.find("avf"))->second;
        return (avf_option == "on");
      } else {
        return false;
      }
    }

    bool is_avf_checkpoint_on() const {
      if (options.find("avf_checkpoint") != options.end()) {
        const std::string& avf_checkpoint_option = (options.find("avf_checkpoint"))->second;
        return (avf_checkpoint_option == "on");
      } else {
        return false;
      }
    }

    long get_avf_clk_interval() const {
      if (options.find("avf_interval") != options.end()) {
        const long avf_chk_clk_interval = atol(((options.find("avf_interval"))->second).c_str());
        return avf_chk_clk_interval;
      }
      else{
        printf("AVF checkpoint clk interval not found in the config file\n"); 
        exit(0);
      }
    }
    //AVF configs ------------- END -----------------------

    //Hybrid configs -------------------------------------
    bool is_migration_on() const {
        if (options.find("migration") != options.end()){
            const std::string& migration_option = (options.find("migration"))->second;
            return (migration_option == "on");
        }
        else 
            return false;
    }
    //Hybrid configs -----------END-----------------------

    bool is_early_exit() const {
      // the default value is true
      if (options.find("early_exit") != options.end()) {
        if ((options.find("early_exit"))->second == "off") {
          return false;
        }
        return true;
      }
      return true;
    }
    bool calc_weighted_speedup() const {
      return (expected_limit_insts != 0);
    }
    bool record_cmd_trace() const {
      // the default value is false
      if (options.find("record_cmd_trace") != options.end()) {
        if ((options.find("record_cmd_trace"))->second == "on") {
          return true;
        }
        return false;
      }
      return false;
    }
    bool print_cmd_trace() const {
      // the default value is false
      if (options.find("print_cmd_trace") != options.end()) {
        if ((options.find("print_cmd_trace"))->second == "on") {
          return true;
        }
        return false;
      }
      return false;
    }

    void parseCommandLineArgs(int trace_start, int argc, const char **argv);
    void printConfig();
};


} /* namespace ramulator */

#endif /* _CONFIG_H */

