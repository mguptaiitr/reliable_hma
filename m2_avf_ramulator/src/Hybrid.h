#ifndef __HYBRID_H
#define __HYBRID_H

#include "Debug.h"
#include "AVF.h"
#include "Memory.h"
#include "Config.h"
#include "Counters.h"
#include "HMA.h"
#include "HMAUnAlloc.h"
#include "FullCounters.h"
#include "MEA.h"
#include "MEAUnAlloc.h"
#include "MEAWr2Rd1.h"
#include "MEAupHMAdown.h"
#include "RemapTable.h"
#include "Statistics.h"
#include <cassert>
#include <stdint.h>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <map>

#define M1_M2_NUM 100
#define M1_NUM  11

using namespace std;

namespace ramulator
{

    //unsigned int CHECK_TARGET_MEM(long addr);
    unsigned int CHECK_TARGET_MEM(long addr, long m1_max_address);
    bool CHECK_TARGET_M1(long addr);
    //extern string ramulator_mg;
    //extern string bench_mg;

    //New class to handle M1 and M2 to page mapping (allocations) 
    //The same class can be extended for migration.
    class Hybrid{
        public:
            long clk=0, interval=0, mig_interval=0; //Hybrid memory clock is synced with cpu clk.

            /************ STATIC PAGE ALLOCATION SUPPORT **********/
            bool static_map_m1 = false;
            string init_alloc = "none"; //fillm1 (Mz 50 50 until HBM is full),  capratio (M1 M2 in capacity ratio), onlym2 (start with only m2)
            long total_bench_pages = -1;

            //Page table entries for M1 and M2
            std::map<long, long> ptem1;
            std::map<long, long> ptem2;
            //Number of allocated pages in M1 and M2
            long num_m1_pages = 0;
            long num_m2_pages = 0;
            //Max page capacity of M1 and M2
            long max_m1_pages;
            long max_m2_pages;

            //PAGE ALLOCATION Functions
            void set_hybrid_max_pages(long m1_max, long m2_max);
            void get_m1_static_page_allocation();
            long dynamic_allocate_page(long virpageno);
            long translate_address_vir_to_phy(long addr);
            long fill_m1_m2_in_cap_ratio(long virpageno);            
            long allocate_m2_page(long virpageno);            
            void hybrid_set_configs(const Config& configs);

            //Hybrid tick gets a tick at every core tick.
            void tick(); 

            /****** MIGRATION SUPPORT *******/
            unsigned int migNum=0;
            MemoryBase* m1; MemoryBase* m2; AVF* mavf;
            bool ismigrate=false, swapHappening=false, pendingMigration=false;
            Counters *counters; 
            RemapTable* remapTable;


            //Stat variable for migation counting/logging
            ScalarStat numOfMigrations;

            //MIGRATION SUPPORT Functions
            void initMigration(const Config& configs);
            void hybrid_set_m1_m2_avf(MemoryBase* mem1, MemoryBase* mem2, AVF* avf);
            void start_migration();
            void perform_swap();
            void swap_completed();
            long remapAddr(long addr);
            //long lookupRemapTable(long pid);
            //bool updateRemapTable(long pid_1, long pid_2);

            //MIGRATION HELPER Functions
            unsigned int pending_requests();
            RemapTable* getRemapTablePtr () {return remapTable; }
            Counters* getCountersPtr () {return counters; }
            
            //For Pinning Pages During Migration
            bool pin_m1_pages_done = false;
            void pin_m1_pages();
            vector<string> pinning_types = {"tophotfillm1", "toiphotcapratio", "topwrwrrdratiofillm1", "topwrwrrdratiocapratio", "pctophot", "pctophotlowrisk"};
            //Constructors
            Hybrid(const Config& configs, string filename);

            //Destructor
            ~Hybrid(){
                //print hybrid summary
                printf("Num Pages Mapped to M1 = (%ld) Num Pages Mapped to M2 = (%ld)  Num Pages Mapped to M1+M2 = (%ld) \n", 
                        num_m1_pages, num_m2_pages, num_m1_pages + num_m2_pages);
                printf("Ratio of M1:M2 page allocation (FINAL): (%.2f)\n", float(num_m1_pages)/(float(num_m2_pages)));

                if(m1_map_file)
                    m1_map_file.close();

                if(init_alloc_m1_map_file)
                    init_alloc_m1_map_file.close();

                 //delete objects created by hybrid class
                 delete counters;
                 delete remapTable;
            }

        private:
            std::ifstream m1_map_file;            //page map for pinning pages during static allocation
            std::ifstream init_alloc_m1_map_file; //page map for pinning pages during migration
    };

} //namespace ramulator

#endif

