#ifndef __AVF_H
#define __AVF_H

#include "Config.h"
#include "Request.h"
#include "RemapTable.h"
#include "Statistics.h"
#include "Debug.h"
#include <fstream>
#include <algorithm>
#include <cstdio>
#include <cassert>
#include <functional>
#include <list>
#include <map>
#include <vector>
#include <memory>
#include <queue>

extern string bench_mg;
extern string mode_mg;
extern string avfout_mg;
extern string ramulator_mg;

namespace ramulator
{


    class AVF {

        private:
            static size_t count; //DEBUG variable for number of AVF objects created. 
        public:

            bool record_avf;

            //Memory standard for Level 1 and Level 2 (l2) memories
            string standard;
            string standard_l2;

            //FaultSim Info
            int num_weeks = 30;
            double FP_bch[30] = {0.00138, 0.00246, 0.00375, 0.00484, 0.00583, 0.00688, 0.00804, 0.00934, 0.01058, 0.012, 0.01316, 0.01449, 0.01598, 0.01707, 0.01839, 0.01951, 0.02061, 0.02227, 0.02343, 0.02468, 0.02586, 0.02688, 0.02808, 0.0292, 0.03034, 0.03172, 0.03298, 0.03417, 0.03541, 0.0367};
            double FP_chipkill[30] = {3e-05, 6e-05, 9.8e-05, 0.000132, 0.000154, 0.00018, 0.000199, 0.000221, 0.000245, 0.000264, 0.000284, 0.000306, 0.000323, 0.000353, 0.000373, 0.000386, 0.000405, 0.000418, 0.00043, 0.000443, 0.000455, 0.000465, 0.000469, 0.000477, 0.000485, 0.000488, 0.000494, 0.000496, 0.000498, 0.000498};
            std::vector<double> serM1; 
            std::vector<double> serM2; 

            //Core specfic info
            long retired = 0, retiredLast = 0;
            long clk = 0, clkLast = 0;

            //Checkpoint related variables
            long avf_interval = -1; //Checkpoint at every 10K clock ticks for expermentations
            long interval = 0; //Running interval is set to zero every time clk hits multiple of avf_interval.
            long check_point_no = 0; //Starting checkpoint no.
            bool ischeckpoint = false;
            bool logCheckpointAVF = false; //Set logCheckpointAVF to true to generate interval-based heatmaps
            bool smart_wr = false;

            //cpu and memory ticks for converting memory cyles to cpu cycles
            unsigned int cpu_tick[2];
            unsigned int mem_tick[2];

            //Checkpoint files
            ofstream c_common_file, c_pw_hot_file, c_pw_avf_file;
            string c_common_filename, c_pw_hot_filename, c_pw_avf_filename;

            int cache_line_size = 1 << 6; //64B             
            //Top pages per checkpoint
            int size_top_pages_GB = 1;
            int page_size_KB = 4;
            int num_top_pages_final = (size_top_pages_GB * 1024 * 1024)/page_size_KB;
            int num_top_pages_check = 1000;
            int theta_hot = 8; //HPCA paper theta. Pages with 64 access.
            //-----------------------

            //AVF CACHE Datastrctures ----------------START DEFINITIONS-----------------------
            //MGUPTA 1: Line Structure that holds avf info for the line.
            //We should have N instances of this structure, where N should be total
            // number of lines required to include the complete dataset.
            struct AVFLine {
                //Line Stats -- These counts are the running counts. 
                //For checkpoints these value will be cumulative sum upto that checkpoint.
                long vulMi[2]; //Vulnerable cycles for the line in M1 or M2 memory
                long last_rd_wr_cycle;
                //Two additional variables for 
                bool read_once;
                unsigned int curr_target_mem; //M1 or M2
                AVFLine(){
                    //Initialize variables to zeros.
                    vulMi[0]=0; vulMi[1]=0; last_rd_wr_cycle=0; read_once=false;
                }
            };

            //MGUPTA 2: HOTPage structure holds full hottness counters
            struct HOTPage{
                bool touch;
                long n_rd;
                long n_wr;
                vector<long> miglist;
                long m2m1;
                long m1m2;
                HOTPage(): touch(false), n_rd(0), n_wr(0), m2m1(0), m1m2(0) {}

                void checkPointReset(){
                    touch = false;
                    n_rd  = 0;
                    n_wr  = 0;
                }
            };

            //MGUPTA 3: Page structure that holds avf info for the page.
            //We should have M instances of this structure, where M is the total
            //number of pages to include the complete dataset.
            struct AVFPage{
                HOTPage pagehot;
                long vulMi[2];
                double avfMi[2];
                AVFPage() {
                    vulMi[0]=vulMi[1]=0;
                    avfMi[0]=avfMi[1]=0;    
                }
            };

            //Checkpoint interval based stats--------------------------------
            struct AVFCheckPoint {
                //System wide stats
                long retired;
                long clk;
                double ipc;

                //Hotness info for each page in the checkpoint. 
                //chk_pagehot[pageno] --> HOTPage datastructure for that page
                std::map<long, HOTPage> chk_pagehot;

                void updateRead(long pageno){
                    auto pit = chk_pagehot.find(pageno);
                    if(pit == chk_pagehot.end())
                        chk_pagehot.insert(make_pair(pageno, HOTPage()));
                    chk_pagehot[pageno].touch=true;
                    chk_pagehot[pageno].n_rd++;
                }
                void updateWrite(long pageno){
                    auto pit = chk_pagehot.find(pageno);
                    if(pit == chk_pagehot.end())
                        chk_pagehot.insert(make_pair(pageno, HOTPage()));
                    chk_pagehot[pageno].touch=true;
                    chk_pagehot[pageno].n_wr++;
                }

                void checkPointReset(){
                    retired = 0;
                    clk = 0;
                    ipc = 0;
                    //Reset all pages in HOTPage
                    for(auto& kv : chk_pagehot){
                        HOTPage& p = kv.second;
                        p.checkPointReset();
                    }
                    debugchk("Reseting checkpoint lines");
                }
            };
            //avfCheck and vul_cycles holds the for each checkpoint 
            //page-wise hotness and vulnerability 
            AVFCheckPoint avfCheck; 
            std::map<std::pair<unsigned int, long>, long> vul_cycles;
            //------------------------------------------------------------------ 

            //lineadd --> AVFLine Info
            std::map<long, AVFLine> avf_lines;
            //pageno --> AVFPage Info (Final Page Info)
            std::map<long, AVFPage> avf_pages;

            void tick(long cpu_retired_inst);

            /* FUNCTION FOR AVF CALCULATIONS */
            void updateAVFmigrationPairs(std::vector<std::pair<Request, Request>>& migPairs, RemapTable* remapTable);
            void updateAVFMigList(std::vector<std::pair<Request, Request>>& migPairs, RemapTable* remapTable, long numM1Pages, long numM2Pages);
            void updateAVFLine(Request req);
            void genericAVFLine(Request req, std::map<long, AVFLine>& lines, std::map<long, AVFPage>& pages);
            void updateCheckpointVulCycles(Request req, long lineno, long pageno);
            void newAVFCheckPoint(long cpu_retired_inst);
            void genericAVFPage(std::map<long, AVFLine>& lines, std::map<long, AVFPage>& pages, long clk);
            std::pair<unsigned int, long> find_n_add_chkpt_pageno(unsigned int curr_chkpt, long pageno);

            /* Function for SER calculations */
            std::vector<double> computeSER(std::map<long, AVFPage>& pages, string mem_standard, int mem_target);

            /* FUNCTIONS: Secondary function for code optimization. 
                These functions provide additional functions 
             */
            void getAVFDataStructureMemoryUsage(long mem_max_address, string mem_standard);
            void checkpointSanityCheck();

            //Print the stats in a file or stdout. 
            void printStoreAVF(long cpu_retired_inst, long cpu_clk);
            void printStorePageInfo(ofstream& file, std::map<long, AVFPage>& pages);
            void printStoreCheckPointHotnessData();
            void printStoreCheckPointAVFData();

            //Get lines which only had writes. The lines with only writes must have a final read.
            vector<long> getWriteOnlyLines();

            //Records all different static page maps
            void genStaticPageMaps();

            //Top N pages by hotness or avf based on the compartor function passed.
            void TopPageInfo(ofstream& file, std::map<long, AVFPage>& pages, std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_func, int num_top_pages);

            //Function for decreasing page hotness
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_hot_dec = 
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return (l.second.pagehot.n_rd + l.second.pagehot.n_wr) > (r.second.pagehot.n_rd + r.second.pagehot.n_wr);};            
            //Function for increasing page hotness
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_hot_inc =
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return (l.second.pagehot.n_rd + l.second.pagehot.n_wr) < (r.second.pagehot.n_rd + r.second.pagehot.n_wr);};            
            //Function for decreasing page_avf
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_avf_dec =
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return l.second.avfMi[0] > r.second.avfMi[0];};
            //Function for increasing page_avf
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_avf_inc =
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return l.second.avfMi[0] < r.second.avfMi[0];};            
            //Function for decreasing page writes
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_wr_dec = 
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return l.second.pagehot.n_wr > r.second.pagehot.n_wr;};
            //Function for decreasing page reads
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_rd_dec = 
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return l.second.pagehot.n_rd > r.second.pagehot.n_rd;};
            //Function for decreasing page (WR+1)/(RD+1) ratio
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_wr_rd_dec = 
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return (l.second.pagehot.n_wr+1)/(l.second.pagehot.n_rd+1) > (r.second.pagehot.n_wr+1)/(r.second.pagehot.n_rd+1);};            
            //Function for decreasing page (wr^2)/(rd+1)
            std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_wr_wr_rd_dec = 
                [](std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r) { return (l.second.pagehot.n_wr * l.second.pagehot.n_wr)/(l.second.pagehot.n_rd+1) > (r.second.pagehot.n_wr * r.second.pagehot.n_wr)/(r.second.pagehot.n_rd+1);};

            //MGUPTA -- get pageno: Returns distinct page number for each addr.
            long getPageNo(long addr) {
                return (addr >> 12);
            }

            //Aligns to cacheline and returns distinct cacheline address.
            long align(long addr) {
                return (addr & ~(cache_line_size-1l));
            }

            void openCheckpointFile(){
                //Open file for checkpoint common info summary
                assert(avfout_mg!="None" && "avfout not set");
                c_common_filename = avfout_mg+"/"+bench_mg+"_CheckPointSummary.csv";
                c_common_file.open(c_common_filename);
                c_common_file<<"Checkpoint number, retired inst, cpu clks, ipc, n_total, n_rd, n_wr, n_pages, wr_per_page\n";

                if(logCheckpointAVF){
                    //Open file for recording per page per checkpoint hotness
                    c_pw_hot_filename = avfout_mg+"/"+bench_mg+"_CheckPointPageWiseHOTNESS.csv";
                    c_pw_hot_file.open(c_pw_hot_filename);

                    //Open file for recording per page per checkpoint avf
                    c_pw_avf_filename = avfout_mg+"/"+bench_mg+"_CheckPointPageWiseAVF.csv";
                    c_pw_avf_file.open(c_pw_avf_filename);
                }
                //Info printf
                printf("CHCHEKPOINT RELATED FILES\n");
                printf("CHECKPOINTING SUMMARY FILE --> %s\n", c_common_filename.c_str());
                if(logCheckpointAVF){
                    printf("HEATMAP PAGEWISE HOTNESS FILE --> %s\n", c_pw_hot_filename.c_str());
                    printf("HEATMAP PAGEWISE AVF FILE --> %s\n", c_pw_avf_filename.c_str());
                }
            }

            void avfSetConfigCommon(const Config& configs){              
                //NOTE: For the hybrid case record_avf, checkpointing related variables are set by only M1 configs
                record_avf = configs.is_avf_on();
                ischeckpoint = configs.is_avf_checkpoint_on();
                smart_wr = (configs["smart_wr"] == "true");

                //Info prints
                if(record_avf)
                    printf("AVF ON || Simulation mode (%s)\n", mode_mg.c_str());
                else
                    printf("AVF OFF || Simulation mode (%s)\n", mode_mg.c_str());

                if(record_avf && ischeckpoint){
                    avf_interval = configs.get_avf_clk_interval();
                    openCheckpointFile();
                    printf("Interval length avf_interval %ld\n", avf_interval);
                }
                
                if(smart_wr)
                    printf("Smart write (AVF Reduction) simulation is set true \n");   
            }

            AVF(const Config& configs){
                debugavf("AVF Constructer single memory");
                ++count;

                //avf_set_config
                //Setting varibales using configs.
                standard = configs["standard"];
                cpu_tick[0] = configs.get_cpu_tick();
                mem_tick[0] = configs.get_mem_tick();

                //sets common avf variable (normal/hybrid)) prints info and computes avf size usage
                avfSetConfigCommon(configs);                                
            }

            AVF(const Config& configs, const Config& configs_l2){
                debugavf("AVF Constructer hybrid memory");
                ++count;

                //avf_set_config
                //Setting variables using configs and configs_l2.
                standard = configs["standard"];
                cpu_tick[0] = configs.get_cpu_tick();
                mem_tick[0] = configs.get_mem_tick();

                standard_l2 = configs_l2["standard"];
                cpu_tick[1] = configs_l2.get_cpu_tick();
                mem_tick[1] = configs_l2.get_mem_tick();

                //sets common avf variable (normal/hybrid)) prints info and computes avf size usage
                avfSetConfigCommon(configs);
            }

            ~AVF(){
                debugavf("Destructor Default");
            }
    };

} // namespace ramulator

#endif /* __AVF_H */
