#include "AVF.h"
#include <cassert>

using namespace std;
using namespace ramulator;

size_t AVF::count = 0;

void AVF::tick(long cpu_retired_inst){
    clk++; interval++;
    if(ischeckpoint && (interval == avf_interval)){
        interval = 0;
        newAVFCheckPoint(cpu_retired_inst); 
    }
}


void AVF::updateAVFmigrationPairs(std::vector<std::pair<Request, Request>>& migPairs, RemapTable* remapTable){
    debugmig("Updating vulnerability of lines in migration pairs clk(%ld) migPairs.size(%zu)", clk, migPairs.size());
    vector<long> m2Pages;
    vector<long> m1Pages;

    for(auto& mp : migPairs){
        //Extract M1 page and M2 page from migration pair. 
        //First request is from slow memory and second from fast memory.
        Request& reqM2 = mp.first;
        Request& reqM1 = mp.second;
        //migPair holds a page pair (slowPage, fastPage) before migration --> (100, 10) look pdf
        long pidM2 = getPageNo(reqM2.addr);
        long pidM1 = getPageNo(reqM1.addr);        
        //getContent of pages. These are the content after migration.
        //content at pidM2 and pidM1 was in memory M1 and M2 before migration. 
        long pidM1con = remapTable->getContent(pidM2);
        long pidM2con = remapTable->getContent(pidM1);
        //push the pages in m2Pages or m1Pages vector based on the page location before migration.
        debugmig("PageID (%ld) : Push PageContent (%ld) to m2Pages list", pidM2, pidM2con);
        debugmig("PageID (%ld) : Push PageContent (%ld) to m1Pages list", pidM1, pidM1con);
        m2Pages.push_back(pidM2con); 
        avf_pages[pidM2con].pagehot.m2m1++;
        m1Pages.push_back(pidM1con); 
        avf_pages[pidM1con].pagehot.m1m2++;
    }

    //iterate over all lines in avf_lines
    for(auto& kv : avf_lines){
        long lineno = kv.first;
        long pageno = getPageNo(lineno);
        auto it2 = find(m2Pages.begin(), m2Pages.end(), pageno);
        auto it1 = find(m1Pages.begin(), m1Pages.end(), pageno);
        if(it2 != m2Pages.end()){
            //line was in M2 before being migrated. 
            //avf_pages[pageno].pagehot.m2m1++;
            //Request req(lineno, Request::Type::READ, 0);
            //req.arrive = clk - 1;
            //req.depart = clk;
            //req.target_mem = 2;
            debugmig("M2->M1 Migration AVF update at clk (%ld) for lineno (%ld) pageno (%ld) m2m1 (%ld)", clk,lineno,pageno,avf_pages[pageno].pagehot.m2m1);
            //genericAVFLine(req, avf_lines, avf_pages);
            avf_lines[lineno].vulMi[1] += (clk - avf_lines[lineno].last_rd_wr_cycle);
            avf_lines[lineno].last_rd_wr_cycle = clk; //Fix Vilas pointed in the meeting. For not counting vul cycles for consequtive reads twice.
        }
        else if(it1 != m1Pages.end()){
            //line was in M1 before being migrated.
            //avf_pages[pageno].pagehot.m1m2++;
            //Request req(lineno, Request::Type::READ, 0);
            //req.arrive = clk - 1;
            //req.depart = clk;
            //req.target_mem = 1;
            debugmig("M1->M2 Migration AVF update at clk (%ld) for lineno (%ld) pageno (%ld) m2m1 (%ld)", clk,lineno,pageno,avf_pages[pageno].pagehot.m1m2);
            //genericAVFLine(req, avf_lines, avf_pages);
            avf_lines[lineno].vulMi[0] += (clk - avf_lines[lineno].last_rd_wr_cycle);
            avf_lines[lineno].last_rd_wr_cycle = clk; //Fix Vilas pointed in the meeting. For not counting vul cycles for consequtive reads twice.

        }
    } //for(auto& kv : avf_lines)

}

void AVF::updateAVFMigList(std::vector<std::pair<Request, Request>>& migPairs, RemapTable* remapTable, long numM1Pages, long numM2Pages){
    debugmig("Updating miglist of pages in migration pairs clk(%ld) migPairs.size(%zu)", clk, migPairs.size());
    for(auto& mp : migPairs){
        //Extract M1 page and M2 page from migration pair. 
        //First request is from slow memory and second from fast memory.
        Request& reqM2 = mp.first;
        Request& reqM1 = mp.second;
        //migPair holds a page pair (slowPage, fastPage) before migration --> (100, 10) look pdf
        long pidM2 = getPageNo(reqM2.addr);
        long pidM1 = getPageNo(reqM1.addr);        
        //getContent of pages. These are the content before migration.
        //pidM2con resides M2 and pidM1con resides in M1 up till the current clk period.
        //Push the only allocated pages to respective miglist and clk tick they get migrated..
        
        long pidM2con = remapTable->getContent(pidM2);
        if(pidM2con >=0 && pidM2con < numM2Pages){
            avf_pages[pidM2con].pagehot.miglist.push_back(clk);
            avf_pages[pidM2con].pagehot.m2m1++;
            debugmig("PageID (%ld) PageContent (%ld) miglist.size() (%zu) mig_clk (%ld)", pidM2, pidM2con, avf_pages[pidM2con].pagehot.miglist.size(), clk);
        }

        long pidM1con = remapTable->getContent(pidM1);
        if(pidM1con >= 0 && pidM1con < numM1Pages){       
            avf_pages[pidM1con].pagehot.miglist.push_back(clk);
            avf_pages[pidM1con].pagehot.m1m2++;
            debugmig("PageID (%ld) PageContent (%ld) miglist.size() (%zu) mig_clk (%ld)", pidM1, pidM1con, avf_pages[pidM1con].pagehot.miglist.size(), clk);
        }
    }
}

void AVF::updateAVFLine(Request req){
    debugavf("Request info: line_num (%ld)", req.lnum);
    unsigned int Mi = req.target_mem - 1; //Mi is 0 for M1 and Mi is 1 for M2. 
    
    //Converting mem_ticks to cpu_ticks. 
    //Checkpoints are created based on cpu_ticks
    req.arrive = long((double)req.arrive * ((double)cpu_tick[Mi]/(double)mem_tick[Mi]));
    req.depart = long((double)req.depart * ((double)cpu_tick[Mi]/(double)mem_tick[Mi]));    
    //---------------------------------------------

    genericAVFLine(req, avf_lines, avf_pages); //For avf_lines

    //if(ischeckpoint)
    //genericAVFLine(req, avfCheck.checkpoints_avf_lines, avfCheck.checkpoints_avf_pages); //For checkpoints_avf_lines
}

void AVF::genericAVFLine(Request req, std::map<long, AVFLine>& lines, std::map<long, AVFPage>& pages){
    debugavf("Request info: line_num (%ld) target_mem (%d) req.type(%d) addr(%ld) orig_addr(%ld) arrival(%ld) depart(%ld)", req.lnum, req.target_mem, req.type, req.addr, req.orig_addr, req.arrive, req.depart);
    assert((req.depart > req.arrive) && "FATAL! departure time less than arrivale time");

    //1. Get lineno.
    //2. Check if line is present in lines
    //3  Check if page is preset in pages
    //4. Check Request::Type
    //4a) Request::Type RD
    //4b) Request::Type WR

    //** NOTE ** Make sure the addr used for the case of migration is consistent. 
    //It should be Page_phyaddr = req.orig_addr. req.addr will be the address after remapping.
    //If we use remapped address to update line/page vulnerability, we will update different field
    //after every remapping/migration. We want to track vulnerbility of data. 
    
    //1. Get lineno/pageno. target_mem for the request. The memory the line is currently located in.
    long lineno = align(req.orig_addr);
    long pageno = getPageNo(req.orig_addr);
    unsigned int Mi = req.target_mem - 1;
    assert(lineno>=0 && "FATAL! lineno is negative");
    assert(pageno>=0 && "FATAL! pageno is negative");

    //2 & 3. Add line and page in avf_line and avf_page if not present
    auto it = lines.find(lineno);
    if(it == lines.end())
        lines.insert(make_pair(lineno, AVFLine()));
    auto pit = pages.find(pageno);
    if(pit == pages.end())
        pages.insert(make_pair(pageno, AVFPage()));

    /*START smart_wr AVF reduction*/
    if(smart_wr){
        debugavf("Smart wr check");
        //If last_rd_wr_cycle is more than two interval old set it to the 
        //clk - 2*avf_interval
        if(clk - lines[lineno].last_rd_wr_cycle > 2 * avf_interval) {
            lines[lineno].last_rd_wr_cycle = clk - 2 * avf_interval;
        }                
    }
    /*END smart_wr AVF reduction*/
    
    //Store the current location of this memory line. Its used to generate read request for lines that have just writes.
    lines[lineno].curr_target_mem = req.target_mem;    

    debugavf("processing pageno (%ld) line (%ld) last_rd_wr_cycle (%ld)", pageno, lineno, lines[lineno].last_rd_wr_cycle);
    if(req.type == Request::Type::READ){
        
        //Handle checkpointing/Interval based based AVF
        if(ischeckpoint){
            avfCheck.updateRead(pageno);
            if(logCheckpointAVF)
                updateCheckpointVulCycles(req, lineno, pageno);
        }
        
        //Apply vulnerability lineno starting from req.depart to lines[lineno].last_rd_wr_cycle
        long vend = req.depart;
        for(std::vector<long>::reverse_iterator it = pages[pageno].pagehot.miglist.rbegin(); 
                                                it!= pages[pageno].pagehot.miglist.rend(); 
                                                ++it){
            long t_clk = *it;
            if(t_clk > lines[lineno].last_rd_wr_cycle){
                lines[lineno].vulMi[Mi] += (vend - t_clk);
                debugavf("READ request (Split): vulMi[%d] (%ld) vend (%ld) t_clk (%ld)", Mi, lines[lineno].vulMi[Mi], vend, t_clk);
                vend = t_clk;
                Mi = Mi ^ 1;
            }
            else{
                break;
            }
        }//for(std::vector<long>::...
        
        pages[pageno].pagehot.touch=true;
        pages[pageno].pagehot.n_rd++;
        lines[lineno].read_once = true;
        lines[lineno].vulMi[Mi] += (vend - lines[lineno].last_rd_wr_cycle);
        lines[lineno].last_rd_wr_cycle = req.depart; //Fix Vilas pointed in the meeting. For not counting vul cycles for consequtive reads twice.
        debugavf("READ request (Final): vulMi[%d] (%ld) req.dep (%ld) last_rd_wr_cycle (%ld)", Mi, lines[lineno].vulMi[Mi], req.depart, lines[lineno].last_rd_wr_cycle);
    }
    else if(req.type == Request::Type::WRITE){
        if(ischeckpoint){
            avfCheck.updateWrite(pageno);
        } 
        pages[pageno].pagehot.touch=true;
        pages[pageno].pagehot.n_wr++;
        lines[lineno].last_rd_wr_cycle = req.depart; //WR resets the vulnerability.
        debugavf("WRITE request: vulMi[%d] (%ld) req.dep (%ld) last_rd_wr_cycle (%ld)", Mi, lines[lineno].vulMi[Mi], req.depart, lines[lineno].last_rd_wr_cycle);
    }
    else {
        std::cout<<"Invalid Request \n";
        exit(0);
    } 
}

void AVF::updateCheckpointVulCycles(Request req, long lineno, long pageno){
    unsigned int last_chkpt = (avf_lines[lineno].last_rd_wr_cycle)/(avf_interval);
    unsigned int curr_chkpt = (req.depart)/(avf_interval);
    debugchk("lineno (%ld) | pageno (%ld)", lineno, pageno);
    debugchk("avf_lines[lineno].last_rd_wr_cycle (%ld) | req.depart (%ld) | avf_interval (%ld)", avf_lines[lineno].last_rd_wr_cycle, req.depart, avf_interval);    
    debugchk("last_chkpt (%d) | curr_chkpt (%d) | check_point_no (%ld)", last_chkpt, curr_chkpt, check_point_no);    


    if(last_chkpt == curr_chkpt){
        //update vul_cycles(curr_chkpt, pageno)
        debugchk("last_chkpt EQUAL curr_chkpt");
        auto target = find_n_add_chkpt_pageno(curr_chkpt, pageno);
        vul_cycles[target] += req.depart  - avf_lines[lineno].last_rd_wr_cycle;
        debugchk("vul_cycles update 1 -> vul_cycles[target] (%ld) | req.depart (%ld) | avf_lines[lineno].last_rd_wr_cycle (%ld)", vul_cycles[target], req.depart, avf_lines[lineno].last_rd_wr_cycle);
    }
    else{
        //Update last_chkpt interval
        debugchk("last_chkpt NOT EQUAL curr_chkpt");
        auto target = find_n_add_chkpt_pageno(last_chkpt, pageno);
        vul_cycles[target] += ((last_chkpt+1) * avf_interval) - avf_lines[lineno].last_rd_wr_cycle;
        debugchk("vul_cycles update 2.0 -> vul_cycles[target] (%ld) | (last_chkpt+1) * avf_interval) (%ld) | avf_lines[lineno].last_rd_wr_cycle (%ld)", vul_cycles[target], (last_chkpt+1) * avf_interval, avf_lines[lineno].last_rd_wr_cycle);

        //Update intevening intervals
        for(unsigned int i = last_chkpt+1; i<curr_chkpt; i++){
            auto target = find_n_add_chkpt_pageno(i, pageno);
            vul_cycles[target] += avf_interval;
            debugchk("vul_cycles update 2.1 -> vul_cycles[target] (%ld) | (checkpoint) (%d) | avf_lines[lineno].last_rd_wr_cycle (%ld)", vul_cycles[target], i, avf_lines[lineno].last_rd_wr_cycle);
        }

        //Update current interval
        target = find_n_add_chkpt_pageno(curr_chkpt, pageno);
        //vul_cycles[target] += (curr_chkpt * avf_interval) - avf_lines[lineno].last_rd_wr_cycle; BUG
        vul_cycles[target] += req.depart - (curr_chkpt * avf_interval);
        debugchk("vul_cycles update 2.2 -> vul_cycles[target] (%ld) | (curr_chkpt * avf_interval) (%ld) | avf_lines[lineno].last_rd_wr_cycle (%ld)", vul_cycles[target], curr_chkpt * avf_interval, avf_lines[lineno].last_rd_wr_cycle);
    }
}

std::pair<unsigned int, long> AVF::find_n_add_chkpt_pageno(unsigned int curr_chkpt, long pageno){
    auto it = vul_cycles.find(make_pair(curr_chkpt, pageno));
    if(it == vul_cycles.end())
        vul_cycles.insert(make_pair(make_pair(curr_chkpt, pageno), 0));
    return make_pair(curr_chkpt, pageno);
}


void AVF::genericAVFPage(std::map<long, AVFLine>& lines, std::map<long, AVFPage>& pages, long clk_generic){
    debugavf("Accumulating line avf to get page avf");
    printf("Number of cache_line_size: %d\n", cache_line_size);
    //Calcluate line avf for every line in the avf_line.
    //Compose page avf using avf of every line for that page.
    for(auto& kv : lines){
        long pageno = getPageNo(kv.first);
        AVFLine& l = kv.second;
        pages[pageno].vulMi[0] += l.vulMi[0];
        pages[pageno].vulMi[1] += l.vulMi[1];
    }

    for(auto& kv : pages){
        AVFPage& p = kv.second;
        p.avfMi[0] = (double)p.vulMi[0]/(cache_line_size * (double)clk_generic);
        p.avfMi[1] = (double)p.vulMi[1]/(cache_line_size * (double)clk_generic);
    }
}

vector<long> AVF::getWriteOnlyLines(){
    debugwravf("Creating vector of lines that only had writes in this trace");
    vector<long> wrOnlyLines;
    for(auto& kv : avf_lines){
        long lineno = kv.first;
        AVFLine& l = kv.second;
        if(!l.read_once)
            wrOnlyLines.push_back(lineno);
    }
    return wrOnlyLines;
}


void AVF::newAVFCheckPoint(long cpu_retired_inst){
    debugavf("%s", __FUNCTION__);

    //update retired instructions clk is synced and updated by AVF::tick function
    retired = cpu_retired_inst;
    //clk = cpu_clk;

    //avfCheck.retired and avf.Check.clk are cpu retired instrcutions and cpu clock in the current checkpoint
    avfCheck.retired = retired - retiredLast;
    avfCheck.clk = clk - clkLast;                   

    //Save the current value of retired inst and clk to be subtracted from 
    //the next avfCheck.retired and avfCheck.clk
    retiredLast = retired;
    clkLast = clk;    

    avfCheck.ipc = (double)(avfCheck.retired)/(double)(avfCheck.clk);

    //Print/Store current checkpoint in a c_file
    printStoreCheckPointHotnessData();

    //Reset avfCheck variable
    avfCheck.checkPointReset();

    //Increment checkpoint number
    check_point_no++;    

}

void AVF::printStorePageInfo(ofstream& file, std::map<long, AVFPage>& pages){
    file<<"* Page Avf and Hottness Info *\n";
    file<<"PageNo, #Reads, #Writes, #Total, vulM1, vulM2, vulTotal, AVFM1, AVFM2, AVFTotal, M2toM1, M1toM2, TotalMig\n";

    file<<"PageAVF Data Starts\n";
    double totalAVFM1 = 0.0;
    double totalAVFM2 = 0.0;
    for (auto& kv : pages){
        long pageno = kv.first;
        AVFPage& p = kv.second;
        totalAVFM1 += p.avfMi[0];
        totalAVFM2 += p.avfMi[1];
        file<<pageno<<","<<p.pagehot.n_rd<<","<<p.pagehot.n_wr<<","<<p.pagehot.n_rd+p.pagehot.n_wr<<","
        <<p.vulMi[0]<<","<<p.vulMi[1]<<","<<p.vulMi[0]+p.vulMi[1]<<","
        <<p.avfMi[0]<<","<<p.avfMi[1]<<","<<p.avfMi[0]+p.avfMi[1]<<","
        <<p.pagehot.m2m1<<","<<p.pagehot.m1m2<<","<<p.pagehot.m2m1+p.pagehot.m1m2<<"\n";
    }
    file<<"PageAVF Data Ends\n";
    file<<"Total AVF of "<<standard<<" Memory: "<<totalAVFM1<<"\n";
    printf("Total AVF of %s Memory: %.2f\n", standard.c_str(), totalAVFM1);

    if(mode_mg=="hybrid"){
        file<<"Total AVF of "<<standard_l2<<" Memory: "<<totalAVFM2<<"\n";
        printf("Total AVF of %s Memory: %.2f\n",standard_l2.c_str(), totalAVFM2);
        printf("Total AVF of Hybrid Memory: %.2f\n",totalAVFM1+totalAVFM2);
    }
}

void AVF::TopPageInfo(ofstream& file, 
        std::map<long, AVFPage>& pages, 
        std::function<bool(std::pair<long, AVFPage>& l, std::pair<long, AVFPage>& r)> cmp_func, 
        int num_top_pages){
    
    //Convert std::map into std::vector
    std::vector<std::pair<long, AVFPage>> pagevec(pages.begin(), pages.end());

    //Check 
    if(pagevec.size() < num_top_pages)
        num_top_pages = pagevec.size();

    //Sort top n pages based on cmp_func
    std::partial_sort(pagevec.begin(), pagevec.begin() + num_top_pages, pagevec.end(), cmp_func);

    //Write total number of 4KB pages in trace. 
    file<<avf_pages.size()<<"\n";

    //Write n top hot pages to the file
    unsigned int i = 0;
    for (auto& pv: pagevec){
        long pageno = pv.first;
        AVFPage &p = pv.second;
        file<<pageno<<","<<p.pagehot.n_rd<<","<<p.pagehot.n_wr<<","<<p.pagehot.n_rd+p.pagehot.n_wr<<","<<p.vulMi[0]<<","<<p.avfMi[0]<<"\n";
        if (++i >= num_top_pages)
            break;
    }
}

std::vector<double> AVF::computeSER(std::map<long, AVFPage>& pages, string mem_standard, int mem_target){
    double *FP;
    std::vector<double> ser;
    if(mem_standard == "HBM"){
        //cout<<"Computing SER for HBM Pages \n";
        FP = FP_bch;
    }
    else{
        //cout<<"Computing SER for DDR Pages \n";
        FP = FP_chipkill;
    }
    for(int w=0; w < num_weeks; w++){
        double s = 0;
        for (auto& kv : pages){
            AVFPage& p = kv.second;
            s += FP[w] * p.avfMi[mem_target];
        }
        ser.push_back(s);
    }
    return ser;
}

void AVF::genStaticPageMaps(){
    //Write different page maps in different files.
    assert(mode_mg=="normal" && "Generating static page for hybrid mode");

    string fName = ramulator_mg+"/m2_avf_ramulator/static_page_maps/"+bench_mg+"_"+standard+"_TopHot.csv";
    ofstream top_hot_page_file(fName);
    TopPageInfo(top_hot_page_file, avf_pages, cmp_hot_dec, num_top_pages_final);
    top_hot_page_file.close();

    fName = ramulator_mg+"/m2_avf_ramulator/static_page_maps/"+bench_mg+"_"+standard+"_LeastHot.csv";
    ofstream least_hot_page_file(fName);    
    TopPageInfo(least_hot_page_file, avf_pages, cmp_hot_inc, num_top_pages_final);
    least_hot_page_file.close();

    fName = ramulator_mg+"/m2_avf_ramulator/static_page_maps/"+bench_mg+"_"+standard+"_TopAVF.csv";
    ofstream top_avf_page_file(fName);
    TopPageInfo(top_avf_page_file, avf_pages, cmp_avf_dec, num_top_pages_final);
    top_avf_page_file.close();

    fName = ramulator_mg+"/m2_avf_ramulator/static_page_maps/"+bench_mg+"_"+standard+"_LeastAVF.csv";
    ofstream least_avf_page_file(fName);
    TopPageInfo(least_avf_page_file, avf_pages, cmp_avf_inc, num_top_pages_final);
    least_avf_page_file.close();

    fName = ramulator_mg+"/m2_avf_ramulator/static_page_maps/"+bench_mg+"_"+standard+"_TopWr.csv";
    ofstream top_wr_page_file(fName);
    TopPageInfo(top_wr_page_file, avf_pages, cmp_wr_dec, num_top_pages_final);
    top_wr_page_file.close();

    fName = ramulator_mg+"/m2_avf_ramulator/static_page_maps/"+bench_mg+"_"+standard+"_TopRd.csv";
    ofstream top_rd_page_file(fName);
    TopPageInfo(top_rd_page_file, avf_pages, cmp_rd_dec, num_top_pages_final);
    top_rd_page_file.close();

    fName = ramulator_mg+"/m2_avf_ramulator/static_page_maps/"+bench_mg+"_"+standard+"_TopWrRdRatio.csv";
    ofstream top_wr_rd_page_file(fName);
    TopPageInfo(top_wr_rd_page_file, avf_pages, cmp_wr_rd_dec, num_top_pages_final);
    top_wr_rd_page_file.close();

    fName = ramulator_mg+"/m2_avf_ramulator/static_page_maps/"+bench_mg+"_"+standard+"_TopWrWrRdRatio.csv";
    ofstream top_wr_wr_rd_page_file(fName);
    TopPageInfo(top_wr_wr_rd_page_file, avf_pages, cmp_wr_wr_rd_dec, num_top_pages_final);
    top_wr_wr_rd_page_file.close();

}

void AVF::printStoreAVF(long cpu_inst_retired, long cpu_clk){
    assert(avfout_mg!="None" && "avfout not set");
    long numWrOnlyPages=0;
    
    //Update avf variable for cpu retired instructions and clk
    retired = cpu_inst_retired;
    clk = cpu_clk;
    
    //Get lines with writes
    vector<long> wrOnlyLines = getWriteOnlyLines();

    //Send a final read request to all the lines which have been only written
    for(auto &lineno : wrOnlyLines){
        long pageno = getPageNo(lineno);
        //Create sythetic read request on the write only line.
        Request req(lineno, lineno, Request::Type::READ, NULL, 0);
        req.target_mem = avf_lines[lineno].curr_target_mem;
        req.lnum = -1;
        req.arrive = cpu_clk - 1;
        req.depart = cpu_clk;
        //If page has no reads send the request to update the line avf.
        if(avf_pages[pageno].pagehot.n_rd == 0){
            debugwravf("lineno (%ld) pageno (%ld) req.target_mem (%d) avf_pages[pageno].pagehot.n_rd (%ld) avf_pages[pageno].pagehot.n_wr (%ld)", lineno, pageno, req.target_mem, avf_pages[pageno].pagehot.n_rd, avf_pages[pageno].pagehot.n_wr);
            numWrOnlyPages++;
            genericAVFLine(req, avf_lines, avf_pages);
        }
    }

    //Open page_file
    string fName = avfout_mg+"/"+bench_mg+"_"+"FinalPageWiseHottnessAVF.csv";
    ofstream page_file(fName);
    
    //Compose avf_lines information to avf_pages
    genericAVFPage(avf_lines, avf_pages, clk);    

    cout<<"--> Writing Final Page Hotness and AVF to file: "<<fName<<"\n";
    cout<<"bench_mg: "<<bench_mg<<"\n";
    cout<<"Number of avf object created(Only for debug purposes): "<<count<<"\n"; 
    cout<<"Number of checkpoints: "<<check_point_no<<"\n";   
    cout<<"Number of pages: "<<avf_pages.size()<<"\n";
    cout<<"Number of lines: "<<avf_lines.size()<<"\n";
    cout<<"Number of pages with only writes: "<<numWrOnlyPages<<"\n";
    cout<<"Number of lines with only writes: "<<wrOnlyLines.size()<<"\n";

    //Meta data to the page_file
    page_file<<"Number of checkpoints: "<<check_point_no<<"\n";
    page_file<<"Number of pages: "<<avf_pages.size()<<"\n";
    page_file<<"Number of lines: "<<avf_lines.size()<<"\n";
    
    //Write PageInfo for all pages to the page_file
    printStorePageInfo(page_file, avf_pages);
    
    //Close page_file
    page_file.close(); 

    //For normal mode record static page maps
    if(mode_mg=="normal"){
        printf("Normal mode generating new static page map files with memory %s\n", standard.c_str());
        genStaticPageMaps();
    }

    //Compute SER of these pages
    serM1 = computeSER(avf_pages, standard, 0);
    if(mode_mg == "hybrid")
        serM2 = computeSER(avf_pages, standard_l2, 1);
    printf("SER Week 0 (%s) = %f\n", standard.c_str(), serM1[0]);
    if(mode_mg == "hybrid")
        printf("SER Week 0 (%s) = %f\n", standard_l2.c_str(), serM2[0]);

    //Put the current counters in the last checkpoint. Writing final checkpoint
    if(ischeckpoint){
        newAVFCheckPoint(retired);
        if(logCheckpointAVF){
            printStoreCheckPointAVFData();
            checkpointSanityCheck();
            c_pw_hot_file.close();
            c_pw_avf_file.close();
        }
        c_common_file.close(); //Last checkpoint is written to the file. Close the checkpoint file.
    }
}

void AVF::printStoreCheckPointHotnessData(){
    long total_rd=0, total_wr=0, n_pages=0;

    //Print PageWise Checkpoint hotness data
    if(logCheckpointAVF)
        c_pw_hot_file<<"Checkpoint No, "<<check_point_no<<"\n";
    
    for(auto& kv: avfCheck.chk_pagehot){
        HOTPage& p = kv.second;
        if(p.touch){
            total_rd+=p.n_rd;
            total_wr+=p.n_wr;
            n_pages++;
        }
        if(logCheckpointAVF)
            c_pw_hot_file<<kv.first<<","<<p.n_rd<<","<<p.n_wr<<"\n";
    }

    //Checkpoint summary file
    c_common_file<<check_point_no<<","<<avfCheck.retired<<","<<avfCheck.clk<<","<<avfCheck.ipc<<","
        <<total_rd+total_wr<<","<<total_rd<<","<<total_wr<<","<<n_pages<<"\n";
}

void AVF::printStoreCheckPointAVFData(){
    for(auto& kv: avf_pages){
        long pageno = kv.first;
        c_pw_avf_file<<pageno;
        for(unsigned int chk = 0; chk<check_point_no; chk++){
            auto ind = make_pair(chk, pageno);
            c_pw_avf_file<<","<<vul_cycles[ind];
        }
        c_pw_avf_file<<"\n";
    }
}


//* Functions: Secondary functions for additional code informations.
//  Such as datastrcuture sizes and sanity check of the AVF values.
void AVF::getAVFDataStructureMemoryUsage(long mem_max_address, string mem_standard){
    //FIX data structure size extimation.
    long max_lines = mem_max_address/64;
    long max_pages = mem_max_address>>12;

    long mb_size_avfline = (max_lines * sizeof(AVFLine))/1024/1024;
    long mb_size_avfpage = (max_pages * sizeof(AVFPage))/1024/1024;

    printf("Size of one AVFLine variable (%lu) Bytes\n", sizeof(AVFLine));
    printf("Size of one AVFPage variable (%lu) Bytes\n", sizeof(AVFPage));
    printf("AVFLine Memory Size = %lu MB\n", mb_size_avfline);
    printf("AVFPage Memory Size = %lu MB\n", mb_size_avfpage);
    printf("AVF DataStructure Memory Size = %lu MB\n", mb_size_avfline + mb_size_avfpage);
}

void AVF::checkpointSanityCheck(){
    printf("AVF::checkpointSanityCheck: VUL(page) = SUM(VUL(chk, page))\n");
    long missmatch = 0;
    for(auto& kv: avf_pages){
        long pageno = kv.first;
        long vul_total_M1 = avf_pages[pageno].vulMi[0];
        long vul_total_M2 = avf_pages[pageno].vulMi[1];
        long vul_chk_cycles = 0;
        for(unsigned int chk = 0; chk < check_point_no; chk++){
            auto target = make_pair(chk, pageno);
            vul_chk_cycles += vul_cycles[target];
        }
        if(vul_chk_cycles != vul_total_M1){
            missmatch++;
            printf("Pageno (%ld) MISSMATCH: vul_total_M1 (%ld), vul_total_M2 (%ld), vul_chk_cycles (%ld)\n", pageno, vul_total_M1, vul_total_M2, vul_chk_cycles);
        }
    }
    printf("Number of pages total vul cycles miss matches with interval distribution = %ld\n", missmatch);
}
