#ifndef __MEAWR2RD1_H
#define __MEAWR2RD1_H
#include "Counters.h"
using namespace std;


namespace ramulator {
    class MEAWr2Rd1 : public Counters {
        public:
            unsigned int num_mea_counters; //num should be command line configurable.
            long firstM1Page = 0; //page location in m1 memory. start scan from here.
            long nextM1Page = firstM1Page; //candidate page in fast memory replaced.
            map<long, long> meaCnt;

            //Constructor Destructors
            MEAWr2Rd1(const Config& configs){
                type = Counters::Type::MEAWr2Rd1;
                num_mea_counters = atoi(configs["num_mea_counters"].c_str());
            }
            ~MEAWr2Rd1(){}

            //Functions

            //Define pure virtual functions from Counter class
            virtual void updateCounter(Request& req){
                long pageno = getPageID(req);
                auto pit = meaCnt.find(pageno);

                //if pit in mea counter map
                if(pit != meaCnt.end()){
                    //Increase 1 for read
                    if(req.type == Request::Type::READ)
                        meaCnt[pageno] = meaCnt[pageno] + 1;
                     //Increase 2 for write
                     if(req.type == Request::Type::WRITE)
                        meaCnt[pageno] = meaCnt[pageno] + 2;
                }
                //pit NOT in mea counter map
                //but there is space in map
                else if(meaCnt.size() < num_mea_counters){
                    meaCnt[pageno] = 1;
                }
                else{
                    vector<long> toDelete;
                    //decrement 1 from every page
                    for(auto& kv : meaCnt){
                        kv.second--;
                        if(kv.second == 0){
                            toDelete.push_back(kv.first);
                        }
                    }
                    //delete pages that have zero counts. 
                    for(auto& pid : toDelete)
                        meaCnt.erase(pid);
                }
            }

            virtual void clear(){
                meaCnt.clear();
                debugmig("Cleared mea counters meaCnt.size() (%zu)", meaCnt.size());  
            }

            virtual void generateMigrationPairs(){
                debugmig("Start generating migrationPair and push into migrationPair max");
                migrationPairs.clear();
                
                //numFastPages can be set to allocated m1 pages or to total m1 capacity
                long migCnt = 0;
                long numM1Pages = num_m1_pages;
                long startM1Page = nextM1Page;
                //long numM1Pages = max_m1_pages;
                
                //Iterate over the pages in MEA counter map.
                for(auto& kv : meaCnt){
                    long pageno = kv.first;
                    if(getPageMemLevel(pageno) == 2){//hot page in M2 memory. 
                        
                        //Find a page in M1 memory to replace with this page.
                        while(meaCnt.find(nextM1Page) != meaCnt.end()){
                            //nextPageMig is in hot page map.
                            nextM1Page++;
                            if(nextM1Page == numM1Pages){
                                nextM1Page = firstM1Page;                                
                            }
                            if(nextM1Page == startM1Page)
                                return;
                        }//while(meaCnt.find(nextPageMig) != meaCnt.end())
                        
                        //Pages pageno and nextPageMig should be swapped. 
                        pair<long, long> migPair = make_pair(pageno, nextM1Page);
                        pushMigrationPair(migPair, migCnt);
                        migCnt++;                        
                        nextM1Page++;
                        if(nextM1Page == numM1Pages) nextM1Page = firstM1Page;                            
                    }//if(getPageMemLevel(pageno) == 2)
                }//for(auto& kv : meaCnt)
            }


    }; //class Counters
}//namespace ramulator
#endif //__MEAWR2RD1_H
