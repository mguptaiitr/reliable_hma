#ifndef __FULLCOUNTERS_H
#define __FULLCOUNTERS_H
#include "Counters.h"
using namespace std;

namespace ramulator {
    class FullCounters : public Counters {
        public:
            //FullCounters
            map<long, pair<long, long>> fullCntM1; 
            map<long, pair<long, long>> fullCntM2;

            //Interval-based statistics
            long mean_m1_hotness = 0, mean_m1_writes = 0, mean_m1_reads = 0;
            long mean_m2_hotness = 0, mean_m2_writes = 0, mean_m2_reads = 0;

            //Put the same above values in a vecotr to dump in the end of simulation
            vector<long> m1_hot, m1_writes, m1_reads;
            vector<long> m2_hot, m2_writes, m2_reads;
            vector<long> m1_cold_n_risky, m2_hot_n_healthy, mig_cnt;
            vector<double> m1_hot_std, m2_hot_std;

            //CuttOffs and Thresholds
            double m1HotCutOff = 0.25, m1HealthCutOff = 0.75;
            double m2HotCutOff = 0.50, m2HealthCutOff = 0.50;

            //File to store interval-based statistics
            string interval_filename;
            ofstream interval_stat_file;

            //Constructor Destructors
            FullCounters(const Config& configs, string fcTypes){
                if(fcTypes=="FullCounters"){
                    m1HotCutOff = 0.50; m1HealthCutOff = 0.50;
                    type = Counters::Type::FullCounters;
                }
                if(fcTypes=="FullCounters75"){
                    m1HotCutOff = 0.25; m1HealthCutOff = 0.75;
                    type = Counters::Type::FullCounters75;
                }
            }
            ~FullCounters(){
                assert(m1_hot.size()==m2_hot.size() && "M1 and M2 vectors not of same length. NOT WRITING INTERVAL STATS");
                
                //Open interval stat file 
                interval_filename = avfout_mg+"/"+bench_mg+"_interval_stats.csv";
                interval_stat_file.open(interval_filename);

                //Write to interval stats
                interval_stat_file<<"#Interval, Mean M1 page hotness, Std M1 hotness, Mean M1 Reads, Mean M1 Writes,"
                                               "Mean M2 page hotness, Std M2 hotness, Mean M2 Reads, Mean M2 Writes,"
                                               "M1 Cold & Risky Pages, M2 Hot & Healthy Pages, #Migrations\n";
                for(unsigned int i=0; i<m1_hot.size(); i++) {
                    interval_stat_file<<i<<","<<m1_hot[i]<<","<<m1_hot_std[i]<<","<<m1_reads[i]<<","<<m1_writes[i]<<","
                                              <<m2_hot[i]<<","<<m2_hot_std[i]<<","<<m2_reads[i]<<","<<m2_writes[i]<<","
                                              <<m1_cold_n_risky[i]<<","<<m2_hot_n_healthy[i]<<","<<mig_cnt[i]<<"\n";
                }
                //Close interval stat file
                interval_stat_file.close();
            }

            //Sorting functions
            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_hot =
                [](pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r) 
                {return (l.second.first + l.second.second) > (r.second.first + r.second.second);};
            
            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_wrrd =
                [](pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r) 
                {return (l.second.second/(l.second.first+1)) > (r.second.second/(r.second.first+1));};

            function<bool(pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r)> cmp_wrwrrd =
                [](pair<long, pair<long, long>>& l, pair<long, pair<long, long>>& r) 
                {return (l.second.second^2/(l.second.first+1)) > (r.second.second^2/(r.second.first+1));};

            /* Updated Counter Values */
            virtual void updateCounter(Request& req){
                long pageno = getPageID(req);
                unsigned int memLevel = getPageMemLevel(pageno);
                if(memLevel == 1){ //Update FullCounter for M1 memory
                    debugcnt("Update FullCounter for M1. pageno(%ld)", pageno);
                    updateFC(pageno, req, fullCntM1);       
                }
                else{ //Update FullCounter for M2 memory
                    debugcnt("Update FullCounter for M2. pageno(%ld)", pageno);
                    updateFC(pageno, req, fullCntM2);
                }
            }
            void updateFC(long pageno, Request& req, map<long, pair<long, long>>& fullCnt){
                auto pit = fullCnt.find(pageno);
                if(pit == fullCnt.end())
                    fullCnt.insert(make_pair(pageno, make_pair(0, 0)));
                if(req.type == Request::Type::READ)
                    fullCnt[pageno].first++;
                if(req.type == Request::Type::WRITE)
                    fullCnt[pageno].second++;
            }

            /* Clear counters */
            virtual void clear(){
                debugmig("Clearing counters");
                for(auto&kv : fullCntM1){
                    auto& data = kv.second;
                    data.first = 0;
                    data.second = 0;
                }
                for(auto&kv : fullCntM2){
                    auto& data = kv.second;
                    data.first = 0;
                    data.second = 0;
                }
                //reset mean hotness, read, and write counters
                mean_m1_hotness = 0; mean_m1_writes = 0; mean_m1_reads = 0;
                mean_m2_hotness = 0; mean_m2_writes = 0; mean_m2_reads = 0;
            }

            /* Generate migration pairs */
            virtual void generateMigrationPairs(){
                debugcnt("FullCounter generate migrationPair vector.");
                migrationPairs.clear();

                //get mean hotness and riskyness for m1 and m2 pages in this interval. 
                getIntervalStats();

                //M1 map to vector
                vector<pair<long, pair<long, long>>> m1PagesHot(fullCntM1.begin(), fullCntM1.end());
                vector<pair<long, pair<long, long>>> m1PagesGood(fullCntM1.begin(), fullCntM1.end());
                //M2 map to vector
                vector<pair<long, pair<long, long>>> m2PagesHot(fullCntM2.begin(), fullCntM2.end());
                vector<pair<long, pair<long, long>>> m2PagesGood(fullCntM2.begin(), fullCntM2.end());

                //*** Sort M1 vectors in decreasing order of hotness and health
                partial_sort(m1PagesHot.begin(), m1PagesHot.begin()+m1PagesHot.size(), m1PagesHot.end(), cmp_hot);
                partial_sort(m1PagesGood.begin(), m1PagesGood.begin()+m1PagesGood.size(), m1PagesGood.end(), cmp_wrrd);                 

                //*** Sort M2 vectors in decreasing order of hotness and health
                partial_sort(m2PagesHot.begin(), m2PagesHot.begin() + m2PagesHot.size(), m2PagesHot.end(), cmp_hot);
                partial_sort(m2PagesGood.begin(), m2PagesGood.begin() + m2PagesGood.size(), m2PagesGood.end(), cmp_wrrd);            
                
                //*** Find hot and healthy (low risk) pages in M2 memory
                vector<pair<long, pair<long, long>>> m2PagesHot_n_Healthy; //Holds list of M2 hot and healthy pages.
                map<long, long> m2HealthyPages; //Map of healthy M2 page to make a quick check.
                for(unsigned int i = 0; i < m2PagesGood.size() * m2HealthCutOff; i++) {                    
                    m2HealthyPages[m2PagesGood[i].first]=m2PagesGood[i].second.first+m2PagesGood[i].second.second;
                }
                for(unsigned int i = 0; i < m2PagesHot.size() * m2HotCutOff; i++){
                    long pageno = m2PagesHot[i].first;
                    //Check if hot page = pageno is also healthy
                    auto pit = m2HealthyPages.find(pageno);
                    if(pit != m2HealthyPages.end()){
                        //If M2 hot page is also healthy push it into m2PagesHot_n_Healthy list.
                        m2PagesHot_n_Healthy.push_back(m2PagesHot[i]);                        
                    }
                }
                m2_hot_n_healthy.push_back(m2PagesHot_n_Healthy.size()); 
                
                //*** Find cold and risky pages in M1 memory
                vector<pair<long, pair<long, long>>> m1PagesCold_n_Risky; //Holds list oM1 cold and risky pages.
                map<long, long> m1RiskyPages; //Map of risky M1 pages to make a quick check
                if(m1PagesGood.size()){
                for(unsigned int i = m1PagesGood.size()-1; i > m1PagesGood.size() * m1HealthCutOff; i--){
                    //cout<<"looping: "<<i<<" "<<m1PagesGood.size()<<" "<<m1PagesGood.size()*m1HealthCutOff<<"\n";
                    m1RiskyPages[m1PagesGood[i].first] = m1PagesGood[i].second.first+m1PagesGood[i].second.second; 
                }
                for(unsigned int i = m1PagesHot.size()-1; i > m1PagesHot.size() * m1HotCutOff; i--){
                    long pageno = m1PagesHot[i].first;                    
                    //Check if pageno is in m1RiskyPage
                    auto pit = m1RiskyPages.find(pageno);
                    if(pit != m1RiskyPages.end()){
                        //If M1 cold page is also risky push it into m1PagesCold_n_Risky
                        m1PagesCold_n_Risky.push_back(m1PagesHot[i]);
                    }
                }
                }
                m1_cold_n_risky.push_back(m1PagesCold_n_Risky.size());

                //*** Start migration of M2 hot and healty pages
                //generate migration pairs and add in request vectors
                long migCnt = 0;

                //Find replacement for m2PagesHot_n_Healthy in *used* m1_cold_n_risky.
                while(m1PagesCold_n_Risky.size()>0 && m2PagesHot_n_Healthy.size()>0){
                    //While M1 memory has used M1 pages and m2Pages has hot and healthy pages.             
                    //We have multiple options here: 
                    //a) Replace M2 hot and healthy page with risk-iest M1 page
                    //b) Replace M2 hot and healthy page with cold-est M1 page
                    //c) Replace M2 hot and healthy page with cold and risky M1 pages
                    auto slow_it = m2PagesHot_n_Healthy.begin() + migCnt;   //Hot and Healthy from M2 slow memory
                    //auto fast_it = m1PagesGood.end() -1 - migCnt;        //a) Risk-iest page from M1 fast memory
                    //auto fast_it = m1PagesHot.end() -1 - migCnt;         //b) Slowest page from M1 fast memory
                    auto fast_it = m1PagesCold_n_Risky.begin() + migCnt;           //c) Cold and Risky M1 fast memory
                    
                    long slowAccess = (*slow_it).second.first + (*slow_it).second.second;
                    long fastAccess = (*fast_it).second.first + (*fast_it).second.second;

                    //terminating conidtions while looking in *used* M1 pages.
                    if(migCnt >= m1PagesCold_n_Risky.size()){
                        debugcnt("Finished looking in used M1 pages. migCnt (%ld)", migCnt);
                        break;
                    }                    
                    if(slowAccess < fastAccess) {
                        debugcnt("Rest of the pages in M2 are less hotter than pages in remaining M1");
                        break;
                    }                    
                    if(migCnt >= m2PagesHot_n_Healthy.size()){
                        debugcnt("All hot and healthy pages migrated to M1. migCnt (%ld)", migCnt);
                        mig_cnt.push_back(migCnt);
                        return;
                    }
                    pair<long, long> migPair = make_pair((*slow_it).first, (*fast_it).first);
                    pushMigrationPair(migPair, migCnt);
                    debugcnt("Migrating M2 page (%ld) with M1 *allocated page* (%ld)", (*slow_it).first, (*fast_it).first);
                    migCnt++;
                }

                //Find replacement for m2PagesHot_n_Healthy in *free* M1 pages. 
                while(m2PagesHot_n_Healthy.size()>0){
                    auto slow_it = m2PagesHot_n_Healthy.begin() + migCnt;
                    //long slowAccess = (*slow_it).second.first + (*slow_it).second.second;
                    
                    //terminating conditions while looking in *unallocated* M1 pages.
                    if(first_free_m1_page >= max_m1_pages){
                        debugcnt("M1 pages are full. Filled unallocated M1 pages. migCnt (%ld)", migCnt);
                        mig_cnt.push_back(-migCnt); //-ve of migCnt to check later at what interval M1 filled completely
                        return;                        
                    }
                    /* Threshold based termination
                    if(slowAccess <= mean_m2_hotness){
                        debugcnt("All hot pages in M2 above threshold are moved to M1. Migration terutnr from *free page* loop. migCnt (%ld)", migCnt);
                        mig_cnt.push_back(migCnt);
                        return;
                    }
                    */
                    if(migCnt >= m2PagesHot_n_Healthy.size()){
                        debugcnt("All hot and healthy pages migrated to M1. migCnt (%ld)", migCnt);
                        mig_cnt.push_back(migCnt);
                        return;
                    }
                    //First unused M1 page 
                    pair<long, long> migPair = make_pair((*slow_it).first, first_free_m1_page);
                    pushMigrationPair(migPair, migCnt);
                    debugcnt("Migrating M2 page (%ld) with M1 *free page* (%ld)", (*slow_it).first, first_free_m1_page);
                    
                    incFirstFreeM1Page();
                    migCnt++;                            
                }
                mig_cnt.push_back(migCnt);
            }

            void getIntervalStats(){
                vector<long> m1_hot_vec, m2_hot_vec;
                double m1_stdv=0, m2_stdv=0;                                
                if(fullCntM1.size()){
                    for(auto& kv : fullCntM1){
                        auto& data = kv.second;
                        long page_hotness = data.first + data.second;
                        m1_hot_vec.push_back(page_hotness);
                        mean_m1_hotness +=  page_hotness;
                        mean_m1_reads += data.first;
                        mean_m1_writes += data.second;                        
                    }
                    mean_m1_hotness = mean_m1_hotness/(fullCntM1.size());
                    mean_m1_reads = mean_m1_reads/(fullCntM1.size());
                    mean_m1_writes = mean_m1_writes/(fullCntM1.size());
                    m1_stdv = getStandardDeviation(m1_hot_vec, mean_m1_hotness);
                }
                if(fullCntM2.size()){
                    for(auto& kv : fullCntM2){
                        auto& data = kv.second;
                        long page_hotness = data.first + data.second;
                        m2_hot_vec.push_back(page_hotness);
                        mean_m2_hotness += page_hotness;
                        mean_m2_reads += data.first;
                        mean_m2_writes += data.second;
                    }
                    mean_m2_hotness = mean_m2_hotness/(fullCntM2.size());
                    mean_m2_reads = mean_m2_reads/(fullCntM2.size());
                    mean_m2_writes = mean_m2_writes/(fullCntM2.size());
                    m2_stdv = getStandardDeviation(m2_hot_vec, mean_m2_hotness);
                }
                //Push the mean values to be printed at the end of the simulation into a vector
                m1_hot.push_back(mean_m1_hotness); m1_hot_std.push_back(m1_stdv); m1_reads.push_back(mean_m1_reads); m1_writes.push_back(mean_m1_writes); 
                m2_hot.push_back(mean_m2_hotness); m2_hot_std.push_back(m2_stdv); m2_reads.push_back(mean_m2_reads); m2_writes.push_back(mean_m2_writes);
            }

     }; //Class FullCounters
}//namespace ramulator
#endif //__FULLCOUNTERS_H
