from func_regx import *
from hpca_graphLib import *

#ALLBENCHS = ['mcf', 'lbm', 'mix5', 'mix1', 'milc', 'mix2', 'mix6', 'omnetpp', 'mix4', 'astar', 'xsbench', 'mix3', 'lulesh', 'mix8', 'GemsFDTD', 'zeusmp', 'bwaves', 'mix7', 'xalancbmk', 'cactusADM']
## *Reduced* Benchmark Set for the Paper
#ALLBENCHS = ['lbm', 'mix1', 'milc', 'mix2', 'omnetpp', 'mix2', 'mix4', 'astar', 'xsbench', 'mix3', 'lulesh', 'GemsFDTD', 'zeusmp', 'bwaves', 'mix7', 'xalancbmk', 'cactusADM']
#ALLBENCHS = ['lbm', 'mix1', 'milc', 'mix2', 'omnetpp', 'mix2', 'mix4', 'astar', 'xsbench', 'mix3', 'lulesh', 'GemsFDTD', 'zeusmp', 'bwaves', 'mix7', 'xalancbmk', 'cactusADM']
ALLBENCHS = ['lbm', 'mix1', 'milc', 'mix2', 'omnetpp', 'mix2', 'mix4', 'astar', 'xsbench', 'mix3', 'lulesh', 'zeusmp', 'mix7', 'xalancbmk', 'cactusADM']

#PLOT 
def plot_avfhot_breakdown(args):
    print 'plot_avfhot_breakdown'
    #BENCHS = ALLBENCHS
    BENCHS = ['astar', 'xalancbmk', 'mix1', 'lulesh', 'mix8', 'zeusmp']
    #BENCHS = ['astar']
    PMAP = ['TopHot', 'LeastAVF', 'TopWrRdRatio', 'TopWrWrRdRatio']
    #args.initalloc = 'fillm1'
    #Extract plot data from the expdir folder into pdata 
    pdata = get_pdata_for_all_benchmarks(args, BENCHS, PMAP)
    #Number of data points extracted    
    print 'Number of keys: '+str(len(pdata.keys()))
    #print '*** pdata ***'
    #print pdata
    GLplot_avfhot_breakdown(pdata, BENCHS, PMAP)

def plot_ipc_ser_perf_focused_hma(args):
    print 'plot_ipc_ser_perf_focused_hma'
    #Set extraction variable defaults are picked up as in args. 
    BENCHS = ALLBENCHS
    PMAP = ['TopHot']
    #args.initalloc = 'fillm1'
    #Extract plot data from the expdir folder into pdata 
    pdata = get_pdata_for_all_benchmarks(args, BENCHS, PMAP)
    #Number of data points extracted    
    print 'Number of keys: '+str(len(pdata.keys()))
    GLplot_ipc_ser_perf_focused_hma(pdata, BENCHS)
    #plotting only averages for slides
    #GLplot_ipc_ser_perf_focused_hma_plot_avgs(pdata, BENCHS)

def plot_ipc_ser_for_static_pagemap_hma(args, pm):
    print 'plot_ipc_ser_for_pm_hma'
    #Set extraction variable defaults are picked up as in args. 
    BENCHS = ALLBENCHS
    PMAP = ['TopHot', pm]
    #Extract plot data from the expdir folder into pdata 
    pdata = get_pdata_for_all_benchmarks(args, BENCHS, PMAP)
    #Number of data points extracted    
    print 'Number of keys: '+str(len(pdata.keys()))
    #GLplot_ipc_ser_for_pm_hma(pdata, BENCHS, pm, args)
    #GLplot_ipc_ser_for_pm_hma_normalize_to_only_ddr(pdata, BENCHS, pm, args)
    ##Plotting only averages for thesis slides
    #GLplot_ipc_ser_for_pm_hma_normalize_to_only_ddr_plot_avgs(pdata, BENCHS, pm, args)
    ##HPCA
    GLplot_ipc_ser_for_pm_hma_normalize_to_perf_focused(pdata, BENCHS, pm, args)

def plot_ipc_ser_for_hybrid_counter_hma(args, cnt):
    print 'plot_ipc_ser_for_hybrid_counters'
    #Set extraction variable defaults are picked up as in args. 
    BENCHS = ALLBENCHS
    
    #Hold a pristine copy of commmand line arguments 
    args_copy = args

    #Extract plot data from the expdir folder into pdata_static for static pagemaps 
    #PMAP = ['TopHot', 'TopWrRdRatio', 'TopWrWrRdRatio']
    print '\n\n\n ************** EXTRACTING STATIC PLACEMENT Data **********************'
    PMAP = ['TopHot']
    alloc = args.initalloc
    args.initalloc = 'fillm1'
    pdata_static = get_pdata_for_all_benchmarks(args, BENCHS, PMAP)
    
    #Extract plot data from expdir folder into pdata_mig_perf for best performance-focused migration
    print '\n\n\n ************** EXTRACTING PERFORMANCE FOCUSED MIGRATION Data **********************'
    args.interval = INTERVAL_MAP[PERFORMANCE_COUNTER]
    args.meacounters = MEA_COUNTERS_MAP[PERFORMANCE_COUNTER]
    pdata_mig_perf = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, [PERFORMANCE_COUNTER])
    
    #Extract plot data from expdir folder into pdata_mig for counter cnt
    print '\n\n\n ************** EXTRACTING CURRENT Data and Plotting **********************'
    COUNTERS = [cnt]
    args.initalloc = alloc
    args.interval = INTERVAL_MAP[cnt]
    args.meacounters = MEA_COUNTERS_MAP[cnt]
    pdata_mig = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, COUNTERS)
    #Number of data points extracted    
    #GLplot_ipc_ser_for_pm_hma(pdata, BENCHS, pm, args)
    if cnt == PERFORMANCE_COUNTER:
        GLplot_ipc_ser_hybrid_counter_hma_normalize_to_only_ddr(pdata_static, pdata_mig, pdata_mig_perf, BENCHS, args.counters, args)
    #GLplot_ipc_ser_hybrid_counter_hma_normalize_to_only_ddr_plot_avgs(pdata_static, pdata_mig, pdata_mig_perf, BENCHS, args.counters, args)
    ##HPCA
    else:
        GLplot_ipc_ser_hybrid_counter_hma_normalize_to_perf_focused(pdata_static, pdata_mig, pdata_mig_perf, BENCHS, args.counters, args)

#HPCA
def plot_ipc_ser_for_hybrid_counter_hma_hpca(args, cnt):
    print 'plot_ipc_ser_for_hybrid_counters_HPCA'
    #Set extraction variable defaults are picked up as in args. 
    BENCHS = ALLBENCHS
    
    #Hold a pristine copy of commmand line arguments 
    args_copy = args

    #Extract plot data from the expdir folder into pdata_static for static pagemaps 
    #PMAP = ['TopHot', 'TopWrRdRatio', 'TopWrWrRdRatio']
    print '\n\n\n ************** EXTRACTING STATIC PLACEMENT Data **********************'
    PMAP = ['TopHot']
    alloc = args.initalloc
    args.initalloc = 'fillm1'
    pdata_static = get_pdata_for_all_benchmarks(args, BENCHS, PMAP)
    
    #Extract plot data from expdir folder into pdata_mig_perf for best performance-focused migration
    print '\n\n\n ************** EXTRACTING PERFORMANCE FOCUSED MIGRATION Data **********************'
    args.interval = INTERVAL_MAP[PERFORMANCE_COUNTER]
    args.meacounters = MEA_COUNTERS_MAP[PERFORMANCE_COUNTER]
    pdata_mig_perf = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, [PERFORMANCE_COUNTER])
    
    #Extract plot data from expdir folder into pdata_mig for counter cnt
    print '\n\n\n ************** EXTRACTING CURRENT Data and Plotting **********************'
    COUNTERS = ['FullCounters75']
    args.initalloc = 'topwrwrrdratiocapratio'
    args.interval = INTERVAL_MAP['FullCounters75']
    pdata_mig_1 = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, COUNTERS)
    #Number of data points extracted    
    #GLplot_ipc_ser_for_pm_hma(pdata, BENCHS, pm, args)
    GLplot_ipc_ser_hybrid_counter_hma_normalize_to_only_ddr(pdata_static, pdata_mig_1, pdata_mig_perf, BENCHS, args.counters, args)
    #GLplot_ipc_ser_hybrid_counter_hma_normalize_to_only_ddr_plot_avgs(pdata_static, pdata_mig_1, pdata_mig_perf, BENCHS, args.counters, args)

    COUNTERS = ['MEAupHMAdown']
    args.initalloc = 'topwrwrrdratiocapratio'
    args.interval = INTERVAL_MAP['MEAupHMAdown']
    args.meacounters = 64
    pdata_mig_2 = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, COUNTERS)
    #Number of data points extracted    
    #GLplot_ipc_ser_for_pm_hma(pdata, BENCHS, pm, args)
    GLplot_ipc_ser_hybrid_counter_hma_normalize_to_only_ddr(pdata_static, pdata_mig_2, pdata_mig_perf, BENCHS, args.counters, args)
    #GLplot_ipc_ser_hybrid_counter_hma_normalize_to_only_ddr_plot_avgs(pdata_static, pdata_mig_2, pdata_mig_perf, BENCHS, args.counters, args)

#HPCA 
def plot_program_struct_annotated(args):
    print 'plot_program_structure_annotated'
    BENCHS = ALLBENCHS
    pinType = 'PCTopWrWrRdRatio50FillM1' 
    #Get benchmark annotation generation info 
    pdata = get_pdata_for_all_benchmarks_generation_info(args, pinType, BENCHS)
    print pdata
    GLplot_program_structs_annotated(pdata, pinType, BENCHS)

#HPCA
def plot_avg_ipc_ser_for_balanced_schemes(args):
    print 'plot_avg_ipc_ser_for_balanced_schemes'
    BENCHS = ALLBENCHS
    #Save original args
    argsCopy = args

    #Extract plot data from expdir folder into pdata_mig_perf for best performance-focused migration
    print '\n\n\n ************** EXTRACTING PERFORMANCE FOCUSED MIGRATION Data **********************'
    args.interval = INTERVAL_MAP[PERFORMANCE_COUNTER]
    args.meacounters = MEA_COUNTERS_MAP[PERFORMANCE_COUNTER]
    pdata_mig_perf = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, [PERFORMANCE_COUNTER])
    

    print '\n\n\n ************** EXTRACTING RELIABILITY FOCUSED MIGRATION Data **********************'
    pdata_rel = {}
    args.interval = INTERVAL_MAP['FullCounters75']
    args.meacounters = MEA_COUNTERS_MAP['FullCounters75']
    args.initalloc = 'onlym2'
    pdata_mig_fullcounters = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, ['FullCounters75'])
    pdata_rel.update(pdata_mig_fullcounters)

    args.interval = INTERVAL_MAP['MEAupHMAdown']
    args.meacounters = MEA_COUNTERS_MAP['MEAupHMAdown']
    args.initalloc = 'onlym2'
    pdata_mig_meauphmadown = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, ['MEAupHMAdown'])
    pdata_rel.update(pdata_mig_meauphmadown)
    print len(pdata_rel.keys())

    print '\n\n\n ************** EXTRACTING BALANCED MIGRATION Data **********************'
    pdata_bal = {}
    args.interval = INTERVAL_MAP['FullCounters75']
    args.meacounters = MEA_COUNTERS_MAP['FullCounters75']
    args.initalloc = 'topwrwrrdratiocapratio'
    pdata_mig_fullcounters = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, ['FullCounters75'])
    pdata_bal.update(pdata_mig_fullcounters)

    args.interval = INTERVAL_MAP['MEAupHMAdown']
    args.meacounters = MEA_COUNTERS_MAP['MEAupHMAdown']
    args.initalloc = 'topwrwrrdratiocapratio'
    pdata_mig_meauphmadown = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, ['MEAupHMAdown'])
    pdata_bal.update(pdata_mig_meauphmadown)
    print len(pdata_bal.keys()) 

    print '\n\n\n ************** EXTRACTING BALANCED MIGRATION Data (PCBASED/STRUCTURE ANNOTATION) **********************'
    pdata_pcbased = {}

    GLplot_ipc_ser_hybrid_balanced_schemes_normalize_to_perf_focused_migration(BENCHS, pdata_mig_perf, pdata_rel, pdata_bal, pdata_pcbased)

def plot_interval_sweep(args):
    print 'plot_interval_sweep'
    #Set BENCHS to be included in interval sweep
    BENCHS=['mix1', 'astar', 'zeusmp', 'milc', 'lbm'] #Sweep avialable for benchmarks
    BENCHS=['mix1', 'astar', 'milc'] #Sweep avialable for benchmarks
    PMAP = []
    alloc = args.initalloc
    counters = args.counters
    args.initalloc = 'fillm1'
    args.interval = LEGACY_INTERVAL
    pdata_static = get_pdata_for_all_benchmarks(args, BENCHS, PMAP)
 
    args.counters = counters
    COUNTERS=[args.counters]
    args.initalloc = alloc
    int_data={}
    ipc_lists = []
    for interval in QINTERVALFULL:
        args.interval = interval
        pdata = get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, COUNTERS)
        int_list = []
        for b in BENCHS:
            int_data[(b, interval)] = pdata[(b, args.counters)]
            ipc_n = int_data[b, interval]['ipc_mig']/pdata_static[b, 'normal']['ipc_mig']
            int_list.append(ipc_n)
            print str(b)+','+str(interval)
            print str(int_data[b, interval]['ipc_mig'])
        ipc_lists.append(int_list)
    print ipc_lists
    #print 'int_data.keys(): '+str(int_data.keys())
    GLplot_interval_sweep(ipc_lists, QINTERVALFULL[:-1], BENCHS, args)
    #print int_data

def plot_hot_vs_avf(args, bench):
    print 'plot_hot_vs_avf'
    pgdata, wrratioarr, c1, c2, hotmax, avfmax, hotmean, avfmean,  hotstd, avfstd = get_final_pagewise_normal_data(args, bench)
    topNPages = 1000
    pgdata = sorted(pgdata.items(), key=lambda x:x[1][2], reverse=True)   
    #Plots hotness, write ratio, pie-chart, and histogram in one plot.
    #GLplot_UP_hot_vs_avf_DOWN_write_ratio(pgdata, bench, topNPages)
    GLplot_hot_vs_avf(pgdata, bench, topNPages, c1)
    GLplot_write_ratio(pgdata, bench, topNPages, c2)

def plot_pie_hotavf(args, bench, plot, genmap):
    print 'plot_pie_hotavf'
    SKIP = 0
    pgdata, wrratioarr, c1, c2, hotmax, avfmax, hotmean, avfmean, hotstd, avfstd = get_final_pagewise_normal_data(args, bench)
    pghot = sorted(pgdata.items(), key=lambda x:x[1][2], reverse=True)
    pgavf = sorted(pgdata.items(), key=lambda x:x[1][3], reverse=True)
    pgwrwrrd = sorted(pgdata.items(), key=lambda x:x[1][1]*x[1][1]/(x[1][0]+1), reverse=True)
    totalpages = len(pghot)
    hot_end = len(pghot)*(50-(SKIP/2))/100
    cold_start = len(pghot)/2+(len(pghot)*(SKIP/2)/100)
    vul_end = hot_end
    nonvul_start = cold_start
    print '** Benchmark: '+str(bench)
    
    ''' 
    print 'pgdata'
    for pgno in pgdata.keys():
        data = pgdata[pgno]
        print 'Pageno: '+str(pgno) + ' hotness: ' + str(data[2]) + ' avf: '+str(data[3])
    print 'pghot'
    for pg in pghot:
        print 'Pageno: '+str(pg[0]) + ' hotness: ' + str(pg[1][2]) + ' avf: '+str(pg[1][3])
    print 'pgavf'
    for pg in pgavf:
        print 'Pageno: '+str(pg[0]) + ' hotness: ' + str(pg[1][2]) + ' avf: '+str(pg[1][3]) 
    '''
    
    print 'Total pages: '+str(totalpages)
    print 'Hot end: '+str(hot_end)
    print 'Cold start: '+str(cold_start)
    
    #create hot set
    hotset = []
    for pg in pghot[:hot_end]:
        hotset.append(pg[0])
    #create cold set
    coldset = []
    for pg in pghot[cold_start:]:
        coldset.append(pg[0])

    #create vul set
    vulset = []
    for pg in pgavf[:vul_end]:
        vulset.append(pg[0])
    #create nonvul set
    nonvulset = []
    for pg in pgavf[nonvul_start:]:
        nonvulset.append(pg[0])

    
    '''
    print 'hotset: '+str(hotset)
    print 'coldset: '+str(coldset)
    print 'vulset: '+str(vulset)
    print 'nonvulset: '+str(nonvulset)
    '''

    #intersections
    hotnonvul = set(hotset).intersection(nonvulset)
    coldnonvul = set(coldset).intersection(nonvulset)
    hotvul = set(hotset).intersection(vulset)
    coldvul = set(coldset).intersection(vulset)
    
    #hotnonvul75. 
    #pghot holds all pages in decreasing order of hotness. It is partitioned 50:50 into hotset:coldset
    #pgavf holds all pages in decreasing order of avf. It is partitioned 50:50 into vulset:nonvulset
    #considering first 0.5 of hotset will give us 25% of top hot pages
    #considering last 0.5 of the nonvulset will give us 25% top nonvul pages
    hotnonvul75 = set(hotset[:int(len(hotset)*0.5)]).intersection(nonvulset[int(len(nonvulset)*0.5):])
    
    #topwrwrrdratio
    topwrwrrdratio = zip(*pgwrwrrd)[0]
    
    ''' 
    print 'hotnonvul: '+str(hotnonvul)
    print 'coldnonvul: '+str(coldnonvul)
    print 'hotvul: '+str(hotvul)
    print 'coldvul: '+str(coldvul)
    '''

    fpGB = float(totalpages) * 4096.0 / 1024.0 / 1024.0 /1024.0
    hotnonvulGB = float(len(hotnonvul)) * 4096.0 / 1024.0 / 1024.0 /1024.0
    benchset = {'tpages':float(totalpages), 'nhotnonvul':float(len(hotnonvul)), 'ncoldnonvul':float(len(coldnonvul)), 'nhotvul':float(len(hotvul)), 'ncoldvul':float(len(coldvul)), 'fpGB':fpGB, 'hotnonvulGB':hotnonvulGB}
    benchSetPages = {'tpages':int(totalpages), 'hotnonvul':hotnonvul, 'coldnonvul':coldnonvul, 'hotvul':hotvul, 'coldvul':coldvul, 'hotnonvul75':hotnonvul75, 'topwrwrrdratio':topwrwrrdratio}

    '''
    print 'Total pages: '+str(totalpages) 
    print 'size(hotnonvul): '+str(len(hotnonvul))
    print '% of Hot + Non vul pages: '+str(float(len(hotnonvul))/float(totalpages)*100.0)
    '''
    if plot:
        #GLplot_pie_hotavf(benchset, bench)
        GLplot_scatter_hotavf(pgdata, hotmax, avfmax, hotmean, avfmean, hotstd, avfstd, benchset, bench)
        GLplot_write_ratio_histo(pgdata, wrratioarr, bench)
    
    if genmap:
        generate_tophot_lowavf_static_pagemap(pgdata, hotnonvul, benchset, bench)
    return benchSetPages, pgdata

def generate_tophot_lowavf_static_pagemap(pgdata, hotnonvul, benchset, bench):
    filename = STATIC_PAGE_MAPS + '/'+bench+'_DDR3_TopHotLowAVF.csv'
    print 'opening file: '+filename
    f = open(filename,'w')
    f.write(str(int(benchset['tpages'])))
    for pgno in hotnonvul:
       data = pgdata[pgno]
       f.write('\n'+str(pgno)+','+str(data[0])+','+str(data[1])+','+str(data[2])) 
    f.close()
 
def generate_pcbased_pagemap(bench, benchSetPages, pgdata, pcRdWrMap, pcMap, pinType):
    
    filename = STATIC_PAGE_MAPS + '/'+bench+'_DDR3_'+pinType+'.csv'
    infofilename = STATIC_PAGE_MAPS + '/generation_info/' + bench + '_'+pinType+'.inf'
    
    #setting annotation cap
    m1_page_cap = benchSetPages['tpages'] * M1_NUM / M1_M2_NUM
    if 'FillM1' in pinType:
        m1_page_cap = 262144 #MAX 4KB 1GB pages
    if m1_page_cap > 262144:
        m1_page_cap = 262144 
    
    print 'opening file: '+filename
    f = open(filename,'w')
    f.write(str(int(benchSetPages['tpages'])))
    
    #Sort pcs with decreasing order to their access frequency  
    pcRdWrMap = OrderedDict(sorted(pcRdWrMap.items(), key=lambda x:x[1][2], reverse=True))

    #keep annotating program counters until pages equivalent to capacity ratio are pinned
    num_pc_annotated = 0
    pin_pages = set()
    for pc in pcRdWrMap.keys():
        num_pc_annotated = num_pc_annotated + 1
        filter_pages = apply_page_filter(pcMap[pc], benchSetPages, pinType) #Apply pinning filter based on pinType 
        pin_pages = pin_pages | filter_pages
        if len(pin_pages | filter_pages) >= m1_page_cap:
            break
            
    for pageno in pin_pages:
        #write to the pcbased pagemap file
        # pageno, #reads, #writes, #accesses
        data = pgdata[pageno]
        f.write('\n'+str(pageno)+','+str(data[0])+','+str(data[1])+','+str(data[2]))
 
    #close the generated page map file
    f.close()
    
    #Write generate info
    fi = open(infofilename, 'w')
    fi.write('** Generating pcbased page map with: '+pinType+ '\n')
    fi.write('Total number of program counters issuing memory instructions: '+str(len(pcRdWrMap)) + '\n')
    fi.write('Number of annotated program counters: '+str(num_pc_annotated) + '\n')
    fi.write('Total number of pages: '+str(benchSetPages['tpages']) + '\n')
    fi.write('m1_page_cap: '+str(m1_page_cap) + '\n')
    fi.write('Pinned pages through: '+str(len(pin_pages)) + '\n')
    #close generation info file
    fi.close()
    
    #Also print the above things on std out
    print '** Generating pcbased page map with: '+pinType
    print 'Total number of program counters issuing memory instructions: '+str(len(pcRdWrMap))
    print 'Number of annotated program counters: '+str(num_pc_annotated)
    print 'Total number of pages: '+str(benchSetPages['tpages'])
    print 'm1_page_cap: '+str(m1_page_cap)
    print 'Pinned pages through pc annotation: '+str(len(pin_pages))
     
def apply_page_filter(pcpages, benchSetPages, pinType):
    if pinType == 'PCTopHot':
        return pcpages
    elif pinType == 'PCTopHotLowRisk':
        hotnonvulpages = benchSetPages['hotnonvul']
        filter_pages = pcpages.intersection(hotnonvulpages)
        return filter_pages
    elif pinType == 'PCTopHotLowRisk75':
        hotnonvul75pages = benchSetPages['hotnonvul75']
        filter_pages = pcpages.intersection(hotnonvul75pages)
        return filter_pages
    elif pinType == 'PCTopWrWrRdRatio50':
        topwrwrrdratio = benchSetPages['topwrwrrdratio']
        topwrwrrdratio = topwrwrrdratio[:int(len(topwrwrrdratio)*0.50)]
        filter_pages = pcpages.intersection(topwrwrrdratio)
        return filter_pages
    elif pinType == 'PCTopWrWrRdRatio25':
        topwrwrrdratio = benchSetPages['topwrwrrdratio']
        topwrwrrdratio = topwrwrrdratio[:int(len(topwrwrrdratio)*0.25)]
        filter_pages = pcpages.intersection(topwrwrrdratio)
        return filter_pages
    elif pinType == 'PCTopWrWrRdRatio50FillM1':
        topwrwrrdratio = benchSetPages['topwrwrrdratio']
        topwrwrrdratio = topwrwrrdratio[:int(len(topwrwrrdratio)*0.50)] 
        filter_pages = pcpages.intersection(topwrwrrdratio)
        return filter_pages
    else:
        print 'FATAL: Generation fail. pinType '+str(pinType)+' not valid.'
        sys.exit(0)

def plot_perf_vs_rel_intro(args):
    print 'plot_perf_vs_rel_intro'
    #BENCHS = ['astar', 'cactusADM', 'mix1']
    BENCHS = ['mix1']
    #BENCHS = ['astar']
    #BENCHS = ['cactusADM']
    PMAP = PAGEMAPRANDOM
    args.initalloc = 'fillm1'
    pdata = get_pdata_for_all_benchmarks(args, BENCHS, PMAP)
    print '***   Printing pdata  ****'
    print pdata
    avgdata = {}
    #Add avfdata[avg, 'normal'] entry
    avgdata['avg', 'normal']={}
    avgdata['avg', 'normal']={}
    avgdata['avg', 'normal']['ipc']=0
    avgdata['avg', 'normal']['ser_ddr']=0
    
    for bench in BENCHS:
        avgdata['avg', 'normal']['ipc'] += pdata[(bench, 'normal')]['ipc']
        avgdata['avg', 'normal']['ser_ddr'] += pdata[(bench, 'normal')]['ser_ddr']

    #Average out normal ipc and ser_ddr     
    avgdata['avg', 'normal']['ipc'] = avgdata['avg', 'normal']['ipc']/len(BENCHS)
    avgdata['avg', 'normal']['ser_ddr'] = avgdata['avg', 'normal']['ser_ddr']/len(BENCHS)
    
    keys = ['ipc', 'ser_ddr', 'ser_hbm']
    for bench in BENCHS:
        for pm in PMAP:            
            if ('avg', pm) not in avgdata:
                avgdata['avg', pm] = {}
            for k in keys:
                if k not in avgdata['avg', pm]:
                    avgdata['avg', pm][k] = 0
                avgdata['avg', pm][k] += pdata[bench, pm][k]

    #Average out for all pages ipc, ser_ddr, and ser_hbm
    for pm in PMAP:
        for k in keys:
            avgdata['avg', pm][k] = avgdata['avg', pm][k]/len(BENCHS)

    print '*** Printing avgdata ***'
    print avgdata

    GLplot_perf_vs_rel(avgdata, ['avg'])
    #plot_perf_vs_rel(pdata, BENCHS)

def plot_avf_all_benchs(args):
    print 'plot_avf_all_benchs'
    pdata = get_pdata_normal_allbenchmarks(args, ALLBENCHS)
    GLplot_avf_benchs_inc_order(pdata, ALLBENCHS)

def plot_pvf_static(args):
    print 'plot_pvf_static'
    #BENCHS = ALLBENCHS
    #PAGEMAPS = PAGEMAPFULL
    BENCHS = ['mix1']
    PAGEMAPS = PAGEMAPFULL
    pdata = get_pdata_for_all_benchmarks(args, ALLBENCHS, PAGEMAPH1)
    #print pdata
    plot_bars_static_placement(pdata, ALLBENCHS, PAGEMAPH1)

def plot_bench_pvf_static(args):
    print 'plot_bench_pvf_static'
    #PAGEMAPFULL = ['TopHot', 'LeastHot', 'TopAVF', 'LeastAVF', 'TopHotLowAVF', 'TopWrRdRatio', 'TopWrWrRdRatio']
    PAGEMAPFULL = ['TopHot', 'LeastHot', 'TopAVF', 'LeastAVF', 'TopWrRdRatio', 'TopWrWrRdRatio']
    pdata = get_pdata_for_all_benchmarks(args, [args.bench], PAGEMAPFULL)
    plot_ipc_ser_bench_static_placement(pdata, [args.bench], PAGEMAPFULL)

def get_pdata_normal_allbenchmarks(args, BENCHS):
    pdata= {}
    for bench in BENCHS:
        print 'Get pdata for benchmark: '+bench
        args.mig = 'off'
        args.conuters = 'none'
        cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'normal', 'DDR16GB', 'none', 'none', args, False)
        pdata[(bench, 'normal')] = get_output_data_for_norm_exp(expdir)
    return pdata

#GET FUNCTIONS
#Gets pdata for all benchmarks in BENCHS and in PAGEMAPS (placementTypes)
#pdata = {(bench, placementTypes):{'field':value}}
def get_pdata_for_all_benchmarks(args, BENCHS, PAGEMAP):
    pdata = {}
    args.mig = 'off'
    args.counters = 'none'
    for bench in BENCHS:
        #get normal bench data. normal is with single memory of type DDR16GB
        cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'normal', 'DDR16GB', 'none', 'none', args, False)
        pdata[(bench, 'normal')] = get_output_data_for_norm_exp(expdir)
        for pagemap in PAGEMAP:
            print '------------------------------- Benchmark : '+str(bench) +'* Pagemap *'+str(pagemap)
            cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', 'HBM1GB', 'DDR16GB', pagemap, args, False)
            pdata[(bench, pagemap)] = get_output_data_for_hyb_exp(expdir)
    return pdata

#Gets pdata for all benchmarks in BENCHS with different migrations (counters)
#pdata = {(bench, counter):{'field':value}}
def get_pdata_for_all_benchmarks_counters_migrations(args, BENCHS, COUNTERS):
    pdata = {}
    args.mig = 'on'
    #args.initalloc = 'onlym2'
    staticmap = 'none'
    print 'COUNTERS to extract: '+str(COUNTERS)
    for bench in BENCHS:
       for counter in COUNTERS:
            print '------- *Benchmark* : '+str(bench) +' *Counter* '+ counter +' *initalloc* '+args.initalloc
            args.counters = counter
            cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', 'HBM1GB', 'DDR16GB', staticmap, args, False)
            pdata[(bench, counter)] = get_output_data_for_hyb_exp(expdir)
    return pdata
    
#Get pagewise data for normal rundef 
def get_final_pagewise_normal_data(args, bench):
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'normal', 'DDR16GB', 'none', 'none', args, False)
    filename = expdir + '/'+bench+'_FinalPageWiseHottnessAVF.csv'
    print 'opening file: '+filename
    f = open(filename,'r')
    lines = f.readlines()
    f.close()
    #print lines
    idx1 = lines.index('PageAVF Data Starts\n')
    idx2 = lines.index('PageAVF Data Ends\n')
    pgdata={}
    #create arrays for correlations
    wrratioarr=[]
    wr2ratioarr=[]
    hotarr=[]
    avfarr=[]
    for line in lines[idx1+1:idx2]:
    #for line in lines[idx1+1:idx1+100]:
        line = line.rstrip('\n')
        #print line
        data = line.split(',')
        #print data
        pageno = int(data[0])
        rd = int(data[1])
        wr = int(data[2])
        hot = rd + wr
        wrratio = round(float(wr)/float(hot)*100, 2)
        wr2ratio = round(float(wr*wr)/float(hot)*100, 2)
        avf = float(data[9])
        pgdata[pageno]=(rd, wr, hot, avf, wrratio, wr2ratio)
        wrratioarr.append(wrratio)
        wr2ratioarr.append(wr2ratio)
        hotarr.append(hot)
        avfarr.append(avf)
    #print pgdata
    hotmax = max(hotarr)
    avfmax = max(avfarr)
    hotstd = np.std(hotarr)
    avfstd = np.std(avfarr)
    hotmean = sum(hotarr)/len(hotarr)
    avfmean = sum(avfarr)/len(avfarr)

    #Get correaltion coffecient between hotarr and avfarr
    corr_c1 = np.corrcoef(hotarr, avfarr)
    corr_c2 = np.corrcoef(wrratioarr, avfarr)
    corr_c3 = np.corrcoef(wr2ratioarr, avfarr)
    print 'Correlation coefficient array between hotness and avf: '+ str(corr_c1[0][1])
    
    print 'Correlation coefficient array between avf and wrratio: '+ str(corr_c2[0][1])
    
    print 'Correlation coefficient array between avf and wr2ratio: '+ str(corr_c3[0][1])
 
    return pgdata, wrratioarr, corr_c1, corr_c2, hotmax, avfmax, hotmean, avfmean, hotstd, avfstd
    
    
def get_pdata_for_all_benchmarks_generation_info(args, pinType, BENCHS):
    pdata = {}
    for b in BENCHS:
        infofilename = STATIC_PAGE_MAPS + '/generation_info/' + b + '_'+pinType+'.inf'
        f = open(infofilename, 'r')
        out = f.read()
        f.close()
        print out
        data = {}
        data['total_structs'] = int(get_pattern_from_output(out, gen_num_total_structs_pattern))
        data['annotated_structs'] = int(get_pattern_from_output(out, gen_num_annotated_structs_pattern))
        pdata[b, pinType] = data
    return pdata
