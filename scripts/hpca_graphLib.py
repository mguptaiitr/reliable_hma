#Graph related imports
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
from matplotlib import cm
from matplotlib.patches import Ellipse
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import LinearLocator, FormatStrFormatter, FuncFormatter
from matplotlib import rcParams
#All other imports in func_regx
from func_regx import *

FONT_SIZE = 18
BAR_WIDTH = 3.0
FONT_SIZE_TICKS = 18
LABELFONT = 13
LEGENDFONT = 12
TEXTFONT = 10
MARKER_PATTERNS = [ "x", "o", ".", "*", '+', "/" , "\\" , "|" ]
COLOR_PATTERNS = ['0.2', '0.4', '0.6', '0.8']
GRAD_PATTERNS = ['0.9', '0.7', '0.5', '0.3', '0.1']
COLSPM = {'TopHot':'rosybrown', 'LeastHot':'tan', 'TopAVF':'darksalmon', 'LeastAVF':'sandybrown', 'TopWr':'bisque', 'TopRd':'steelblue', 'TopWrRdRatio':'cornflowerblue', 'TopWrWrRdRatio':'skyblue', 'none':'lightblue'}

# python plot_avf.py -pmipcser -pagemap TopWrWrRdRatio
STATICMAP_COUNTERS_LABEL={('TopHot', 'none'):'Perf-focused', ('LeastAVF', 'none'): 'Reliability-focused', ('TopHotLowAVF', 'none'):'Balanced', ('TopWrRdRatio','none'):'Wr ratio', ('TopWrWrRdRatio','none'):'$Wr^2$ ratio', ('none', 'FullCounters'): 'FullCounters', ('none', 'FullCounters75'): 'FullCounters', ('none', 'HMAUnAlloc'):'Performance-focused mig', ('none', 'HMAUnAllocThd1'):'Perf-focused mig', ('none', 'HMAUnAllocThd2'):'Perf-focused mig', ('none', 'MEAupHMAdown'): 'CrossCounters', ('PCTopWrWrRdRatio50FillM1', 'none'): 'Program Annotation', ('PCTopHotLowRisk', 'none'):'Program Annotation (hot & low-risk)'}

BENCHLABELS = {'lbm':'lbm', 'mix1':'mix1', 'milc':'milc', 'mix2':'mix2', 'omnetpp':'omnetpp', 'mix2':'mix2', 'mix4':'mix4', 'astar':'astar', 'xsbench':'xsbench', 'mix3':'mix3', 'lulesh':'lulesh', 'GemsFDTD':'GemsFDTD', 'zeusmp':'zeusmp', 'bwaves':'bwaves', 'mix7':'mix5', 'xalancbmk':'xalancbmk', 'cactusADM':'cactusADM','avg HG':'avg HG', 'avg mix': 'avg mix', 'avg':'avg'}

#Annotation bbox box style used for graph annotations
bbox_props = dict(boxstyle="round", fc="white", ec="0.5", alpha=0.8)

def autolabel(rects, ax, top, rot, ylim, fsize=10):
 # attach some text labels
 for rect in rects:
   height = rect.get_height()
   if height < ylim:
       ax.text(rect.get_x() + rect.get_width()/2., 1.01*height, top % (height), ha='center', va='bottom', rotation=rot, fontsize=fsize)
   else:
       ax.text(rect.get_x() + rect.get_width()/2., 1.01*ylim, top % (height), ha='center', va='bottom', rotation=rot, fontsize=fsize)



def save_n_crop(filename):
    fullpath = PLOT_OUTPUT_FOLDER+'/'+filename
    print 'Saving graph: '+str(fullpath)
    pyplot.savefig(fullpath, format='pdf', dpi=80, bbox_inches='tight')
    #pyplot.savefig(fullpath, format='pdf', dpi=80)
    #CROP COMMAND
    cmd = 'pdfcrop '+fullpath+' '+fullpath
    print 'PDFCROP: ' + str(cmd)
    status, output = commands.getstatusoutput(cmd)
    #SCP TO DROPBOX ON ONZONE
    hostname = socket.gethostname()
    print 'Hostname: '+str(hostname)
    if hostname != 'ozone':
        cmd = 'scp '+fullpath+' manish@ozone.ucsd.edu:~/Dropbox/From_ozone/avf_figs/'+filename
        print 'SCP from matricks to ozone Dropbox: '+str(cmd)
    else:
        cmd = 'cp '+fullpath+' ~/Dropbox/From_ozone/avf_figs/'+filename
        print 'Copy to Dropbox(Ozone): '+str(cmd)
    status, output = commands.getstatusoutput(cmd)

def kilo(x, pos):
    return '%1.0fK' % (x*1e-3)

#get_dict_field_data function returns array of dicttions.
#each dictionary is 
def get_dict_field_data(benchs, pdata, placetype, fields):
    dictarr = []
    for field in fields:
        fielddic = {}
        for bench in benchs:            
            fielddic[bench] = pdata[(bench, placetype)][field]
        dictarr.append(fielddic)
    return dictarr

def GLplot_write_ratio_histo(pgdata, wrratioarr, bench):
    print 'GLplot_write_ratio_histo'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-4,
        'axes.labelpad': -1,
        'text.fontsize': TEXTFONT-4,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-4,
        'ytick.labelsize': LABELFONT-4,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [3, 3]
    } 
    rcParams.update(params)
    
    bins = np.arange(0, 120, 20)
    fig, ax1 = pyplot.subplots()
    n, bins, patches = pyplot.hist(wrratioarr, bins=bins, alpha=0.75)
    
    #BEAUTIFICATION
    pyplot.xlabel('Write ratio (%)')
    pyplot.ylabel('# of pages')
    formatter = FuncFormatter(kilo)
    ax1.yaxis.set_major_formatter(formatter)
    pyplot.grid(True)
    pyplot.tight_layout() 
    plotname = bench+'_WriteRatioHisto.pdf'
    save_n_crop(plotname)

def GLplot_ProgramCounter_histogram(arr, bench, histoType, nbins=100):
    print 'GLplot_ProgramCounter_histogram'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-4,
        'axes.labelpad': -1,
        'text.fontsize': TEXTFONT-4,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-4,
        'ytick.labelsize': LABELFONT-4,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [3, 3]
    } 
    rcParams.update(params)
    
    fig, ax1 = pyplot.subplots()
    #n, bins, patches = pyplot.hist(arr, bins='auto', alpha=0.75)
    n, bins, patches = pyplot.hist(arr, bins=nbins, alpha=0.75)
    
    #BEAUTIFICATION
    pyplot.xlabel('# of '+histoType)
    pyplot.ylabel('# of PCs')
    #formatter = FuncFormatter(kilo)
    #ax1.yaxis.set_major_formatter(formatter)
    pyplot.xticks(rotation=90)
    pyplot.grid(True)
    pyplot.tight_layout() 
    plotname = bench+'_ProgramCounter_Histo_'+histoType+'.pdf'
    save_n_crop(plotname)



def GLplot_pie_hotavf(bset, bench):
    print 'GLplot_pie_hotavf'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-2,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-1,
        'xtick.labelsize': LABELFONT-4,
        'ytick.labelsize': LABELFONT-4,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [3, 3]
    } 
    rcParams.update(params)
    print bset
    hotnonvul = round(bset['nhotnonvul']/bset['tpages']*100.0, 1)
    hotvul = round(bset['nhotvul']/bset['tpages']*100.0, 1)
    coldnonvul = round(bset['ncoldnonvul']/bset['tpages']*100.0, 1)
    coldvul = round(bset['ncoldvul']/bset['tpages']*100.0, 1)
    rest = 100.0 - hotnonvul - hotvul - coldnonvul - coldvul
    #labels = 'Hot &\nlow AVF', 'Hot &\nhigh AVF', 'Cold &\nlow AVF', 'Cold &\nhigh AVF', 'Rest'
    #sizes =  [hotnonvul, hotvul, coldnonvul, coldvul, rest]
    #colors = ['lightsteelblue', 'plum', 'linen', 'bisque', 'white']
    #explode = [0.1,      0,      0,          0,        0]
    labels = 'Hot &\nlow AVF', 'Hot &\nhigh AVF', 'Cold &\nlow AVF', 'Cold &\nhigh AVF'
    sizes =  [hotnonvul, hotvul, coldnonvul, coldvul]
    colors = ['lightsteelblue', 'plum', 'linen', 'bisque']
    explode = [0.1,      0,      0,          0]
    
    print sizes
    fig, ax1 = pyplot.subplots()
    ax1.pie(sizes, explode=explode, colors=colors, labels=labels, autopct='%1.1f%%', shadow=True, startangle=90)
    ax1.axis('equal')
    
    #BEAUTIFICATION
    plotname = bench+'_PieChart.pdf'
    pyplot.tight_layout() 
    save_n_crop(plotname)

def GLplot_scatter_hotavf(pgdata, hotmax, avfmax, hotmean, avfmean, hotstd, avfstd, bset, bench):
    print 'GLplot_scatter_hotavf'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-2,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-5,
        'xtick.labelsize': LABELFONT-4,
        'ytick.labelsize': LABELFONT-4,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [3, 3]
    } 
    rcParams.update(params)
    hotarr = []
    avfarr = []
    for pgno in pgdata.keys():
        data = pgdata[pgno]
        if (data[2] < (hotmean + 3*hotstd)) and (data[2] > (hotmean - 3*hotstd)):
            hotarr.append(float(data[2]))
            avfarr.append(data[3]*100.0)
    #hotmean = sum(hotarr)/len(hotarr)
    #avfmean = sum(avfarr)/len(avfarr)
    #print 'Mean hotness: '+str(hotmean)
    #print 'Mean avf: '+str(avfmean)
    print 'Filted pages: '+str(len(hotarr))
    print 'hotmax: '+str(hotmax)
    print 'hotmean: '+str(hotmean)
    print 'hotstd: '+str(hotstd)
    #SCATTER PLOT
    fig, ax1 = pyplot.subplots()
    pyplot.scatter(avfarr, hotarr, marker='.', c='0.4', s=10, lw=0, alpha=0.2,  rasterized=True)     
    ax1.axhline(y=hotmean, color='0.2', ls='dashed', lw=2.0, label='Mean hotness') 
    ax1.axvline(x=avfmean*100.0, color='0.2', ls='-.', lw=2.0, label='Mean AVF') 

    #ANNOTATIONS
    text = "Hot & low AVF\n("+str(round(bset['hotnonvulGB'], 2))+' GB)'
    ax1.text(20, hotmean+2*hotstd, text, ha="center", va="center", size=TEXTFONT-3, bbox=bbox_props)
 
    
    #BEAUTIFICATION
    ax1.legend(loc='upper right', fancybox=True, framealpha=0.2)
    ax1.set_xlim([0,100])
    y1,y2 = ax1.get_ylim()
    #ax1.set_ylim([0,6000])
    ax1.set_ylim([0, hotmean + 3*hotstd])
    ax1.set_xlabel('AVF (%)')
    ax1.set_ylabel('HOTNESS (Accesses)')
    #formatter = FuncFormatter(kilo)
    #ax1.yaxis.set_major_formatter(formatter)
    #ax1.set_xticklabels([])
    #ax1.set_yticklabels([])
    plotname = bench+'_Scatter_hotavf.pdf'
    pyplot.tight_layout() 
    save_n_crop(plotname)

def GLplot_UP_hot_vs_avf_DOWN_write_ratio(pgdata, bench, topNPages):
    print 'GLplot_hot_vs_avf'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-4,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-4,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-6,
        'ytick.labelsize': LABELFONT-6,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [6, 3]
    } 
    rcParams.update(params)
    
    #Create data arrays
    pidarr = []
    hotarr = []
    avfarr = []
    wrratio = []
    for data in pgdata[:topNPages]:
        pidarr.append(data[0])
        hotarr.append(data[1][2])
        avfarr.append(data[1][3]*100)
        wrratio.append(float(data[1][1])/float(data[1][2]) * 100.0)

    ind = np.arange(len(pidarr)) 

    #plot
    fig, (ax1, ax2) = pyplot.subplots(2, sharex=True)
    width = 0.80
    
    #lower subplot
    ax2.plot(ind, wrratio, color='b', linewidth=0.4, marker='.', markersize=2) 
    #ax2.scatter(ind, wrratio, color='b', marker='.') 
    ax2.set_xticklabels([])
    ax2.set_ylabel('write ratio (%)')
    #ax2.set_xlabel('top '+str(topnpages)+' pages in decreasing hotness')
    #ax2.plot(ind, wrratio, color='b', linewidth='1.5')
    
    #upper subplot
    #upper left side hotness plot
    ax1.bar(ind, hotarr, width, color='0.4', edgecolor='none', label='page hotness')    
    formatter = funcformatter(kilo)
    ax1.yaxis.set_major_formatter(formatter)
    ax1.set_ylabel('page hotness\n(# of accesses)')
    #ax1.plot(ind, hotarr, color='0.4')
    
    #upper right side avf plot
    ax11 = ax1.twinx()
    ax11.plot(ind+width/2, avfarr, color='r', linewidth=0.4, marker='.', markersize=2, label='page avf') 
    #ax11.scatter(ind+width/2, avfarr, color='r', marker='.', label='page avf') 
    ax11.set_ylabel('page avf (%)')
    
    #BEAUTIFICATION
    #ax1.legend(loc='lower left', fancybox=True, framealpha=0.3)
    #ax11.legend(loc='lower right', fancybox=True, framealpha=0.3)
    ax1.legend(loc='upper left', fancybox=True)
    ax11.legend(loc='upper right', fancybox=True)
    ax11.set_ylim([0,100])
    plotname = bench+'_PageHotnessvsAVF.pdf'
    save_n_crop(plotname)

def GLplot_hot_vs_avf(pgdata, bench, topNPages, c1):
    print 'GLplot_hot_vs_avf'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-4,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-4,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-6,
        'ytick.labelsize': LABELFONT-6,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [5.5, 1.5]
    } 
    rcParams.update(params)
    
    #Create data arrays
    pidarr = []
    hotarr = []
    avfarr = []
    wrratio = []
    for data in pgdata[:topNPages]:
        pidarr.append(data[0])
        hotarr.append(data[1][2])
        avfarr.append(data[1][3]*100)
        wrratio.append(float(data[1][1])/float(data[1][2]) * 100.0)

    ind = np.arange(len(pidarr)) 

    #PLOT
    fig, ax1 = pyplot.subplots()
    width = 0.80
    
    #Upper subplot
    #Upper left side hotness plot
    ax1.bar(ind, hotarr, width, color='0.4', edgecolor='none', label='Page hotness')    
    formatter = FuncFormatter(kilo)
    ax1.yaxis.set_major_formatter(formatter)
    ax1.set_ylabel('Page hotness\n(# of accesses)')
    #ax1.plot(ind, hotarr, color='0.4')
    
    #Upper right side avf plot
    ax11 = ax1.twinx()
    ax11.plot(ind+width/2, avfarr, color='r', linewidth=0.4, marker='.', markersize=2, label='Page AVF') 
    #ax11.scatter(ind+width/2, avfarr, color='r', marker='.', label='Page AVF') 
    ax11.set_ylabel('Page AVF (%)', color='r')
    
    #ANNOTATIONS
    coeffstr = "Correlation coeff="+str(round(c1[0][1], 2))
    ax1.text(500, 20000, coeffstr, ha="center", va="center", size=TEXTFONT-2, bbox=bbox_props)
   
    #BEAUTIFICATION
    #ax1.legend(loc='lower left', fancybox=True, framealpha=0.3)
    #ax11.legend(loc='lower right', fancybox=True, framealpha=0.3)
    ax1.legend(loc='upper left', fancybox=True)
    ax11.legend(loc='upper right', fancybox=True)    
    ax1.set_xticklabels([])
    ax11.set_ylim([0,110])
    plotname = bench+'_PageHotnessvsAVF_1.pdf'
    save_n_crop(plotname)

def GLplot_write_ratio(pgdata, bench, topNPages, c2):
    print 'GLplot_write_ratio'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-4,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-4,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-6,
        'ytick.labelsize': LABELFONT-6,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [5, 1.5]
    } 
    rcParams.update(params)
    
    #Create data arrays
    pidarr = []
    hotarr = []
    avfarr = []
    wrratio = []
    for data in pgdata[:topNPages]:
        pidarr.append(data[0])
        hotarr.append(data[1][2])
        avfarr.append(data[1][3]*100)
        wrratio.append(float(data[1][1])/float(data[1][2]) * 100.0)

    ind = np.arange(len(pidarr)) 

    #PLOT
    fig, ax2 = pyplot.subplots()
    width = 0.80
    
    #Lower subplot
    ax2.plot(ind, wrratio, color='b', linewidth=0.4, marker='.', markersize=2, label='Page WR ratio') 
    #ax2.scatter(ind, wrratio, color='b', marker='.') 
    ax2.set_xticklabels([])
    ax2.set_ylabel('Write ratio (%)')
    #ax2.set_xlabel('Top '+str(topNPages)+' pages in decreasing hotness')
    #ax2.plot(ind, wrratio, color='b', linewidth='1.5')
    
    #Lower right side avf plot
    ax21 = ax2.twinx()
    ax21.plot(ind+width/2, avfarr, color='r', linewidth=0.4, marker='.', markersize=2, label='Page AVF') 
    #ax11.scatter(ind+width/2, avfarr, color='r', marker='.', label='Page AVF') 
    ax21.set_ylabel('Page AVF (%)')
    
    #ANNOTATIONS
    coeffstr = "Correlation coeff="+str(round(c2[0][1], 2))
    ax2.text(550, 90, coeffstr, ha="center", va="center", size=TEXTFONT-2, bbox=bbox_props)
 
    #BEAUTIFICATION
    #ax2.legend(loc='lower left', fancybox=True, framealpha=0.3)
    #ax21.legend(loc='lower right', fancybox=True, framealpha=0.3)
    ax2.legend(loc='upper left', fancybox=True)
    ax21.legend(loc='upper right', fancybox=True)
    ax21.set_ylim([0,110])
    ax2.set_xticklabels([])
    plotname = bench+'_PageHotnessvsAVF_write_ratio.pdf'
    save_n_crop(plotname)

#Plot breakdown of hotness and AVF between HBM and DDR
def GLplot_avfhot_breakdown(pdata, BENCHS, PMAP):
    print 'GLplot_avfhot_breakdown'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-2,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-2,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [8, 3]
    } 
    rcParams.update(params)

    #Get data dictionary
    normSERDDR, normIPC, normAVF = get_dict_field_data(BENCHS, pdata, 'normal', ['ser_ddr', 'ipc', 'avf_ddr'])
    #               0           1           2           3           4           5           6       7         8         9
    fieldarray = ['avf_ddr', 'avf_hbm', 'n_wr_ddr', 'n_rd_ddr', 'n_wr_hbm', 'n_rd_hbm', 'n_ddr', 'n_hbm']  #avf_total,  n_total
    
    #create variables with names in fieldarray
    avf_ddr = 0
    avf_hbm = 1
    n_wr_ddr = 2
    n_rd_ddr = 3
    n_wr_hbm = 4
    n_rd_hbm = 5
    n_ddr = 6
    n_hbm = 7
    avf_total = 8
    n_total = 9

    #Page Placement Data
    PDATA = {}
    for pm in PMAP:
        #PDATA is a list of dictionary with key = placement and value = [{'bench':val}]
        PDATA[pm] = get_dict_field_data(BENCHS, pdata, pm, fieldarray)
        PDATA[pm].append(Counter(PDATA[pm][avf_ddr]) + Counter(PDATA[pm][avf_hbm]))
        PDATA[pm].append(Counter(PDATA[pm][n_ddr]) + Counter(PDATA[pm][n_hbm]))
    
    #Get benchOrder in increasing AVF and normalize values for plotting
    normAVF = OrderedDict(sorted(normAVF.items(), key=itemgetter(1)))
    benchOrder = normAVF.keys()
    
    #Create data that goes on the plot in an OrderedDict
    for pm in PMAP:
        PDATA[pm][avf_ddr] = OrderedDict([k, PDATA[pm][avf_ddr][k]/PDATA[pm][avf_total][k]] for k in benchOrder)
        PDATA[pm][avf_hbm] = OrderedDict([k, PDATA[pm][avf_hbm][k]/PDATA[pm][avf_total][k]] for k in benchOrder)
        PDATA[pm][n_ddr] = OrderedDict([k, float(PDATA[pm][n_ddr][k])/float(PDATA[pm][n_total][k])] for k in benchOrder)
        PDATA[pm][n_hbm] = OrderedDict([k, float(PDATA[pm][n_hbm][k])/float(PDATA[pm][n_total][k])] for k in benchOrder)
    
    #plot
    fig, (ax1, ax2) = pyplot.subplots(2, sharex=True)    
    width = (1-0.15)/len(PMAP)
    ind = np.arange(len(benchOrder))        
    
    i=0
    for pm in PMAP:
        print 'Placement: '+str(pm)
        indi = ind + i*width
        #lower subplot
        ax2.bar(indi, PDATA[pm][n_hbm].values(), width, color='0.4', label='HBM accesses')
        ax2.bar(indi, PDATA[pm][n_ddr].values(), width, color='0.8', bottom=PDATA[pm][n_hbm].values(), label='DDR accesses')
        #upper subplot
        ax1.bar(indi, PDATA[pm][avf_hbm].values(), width, color='rosybrown', label='AVF HBM')
        ax1.bar(indi, PDATA[pm][avf_ddr].values(), width, color='bisque', bottom=PDATA[pm][avf_hbm].values(), label='AVF DDR')
        i = i + 1

    ax2.set_xticks(ind+width*2)
    #ax2.set_xticklabels(benchOrder, rotation=90)
    ax2.set_xticklabels(benchOrder)
    ax2.set_ylabel('Access\nbreakdown')
    ax1.set_ylabel('AVF\nbreakdown')
        
    #BEAUTIFICATION
    #ax1.legend(loc='lower left', fancybox=True, framealpha=0.3)
    #ax11.legend(loc='lower right', fancybox=True, framealpha=0.3)
    #ax1.legend(loc='upper left', fancybox=True)
    #ax11.legend(loc='upper right', fancybox=True)
    #x1,x2 = ax1.get_xlim()
    #pyplot.xlim([x1-0.1, x2-0.9])
    
    plotname ='HotnessAVFBreakDown.pdf'
    save_n_crop(plotname)

#PLOT Introduction graph.
def GLplot_perf_vs_rel(pdata, BENCHS):
    print 'GLplot_perf_vs_rel'
    #rc params
    params = {
        'axes.labelsize': LABELFONT,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-3,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [4.5, 3]
    } 
    rcParams.update(params)
   
    fig, ax = pyplot.subplots()
    #PROCESS DATA x and y array
    for bench in BENCHS:
        ipc_arr = []
        ser_ddr_arr = []
        ser_hbm_arr = []
        ser_arr = []
        rel_arr = [] 
        print 'Benchmark '+bench        
        ipc_arr.append(pdata[(bench, 'normal')]['ipc'])
        ser_ddr_arr.append(pdata[(bench, 'normal')]['ser_ddr'])
        ser_hbm_arr.append(0)
        ser_total = pdata[(bench, 'normal')]['ser_ddr'] + 0
        ser_arr.append(ser_total)
        rel_arr.append(1/ser_total)
        for pm in PAGEMAPRANDOM:
            ipc_arr.append(pdata[(bench, pm)]['ipc'])
            ser_ddr_arr.append(pdata[(bench, pm)]['ser_ddr'])
            ser_hbm_arr.append(pdata[(bench, pm)]['ser_hbm'])
            ser_total = pdata[(bench, pm)]['ser_ddr'] + pdata[(bench, pm)]['ser_hbm']             
            ser_arr.append(ser_total)
            rel_arr.append(1/ser_total)
        
        #pyplot.plot(rel_arr[1:12], ipc_arr[1:12], marker='.', linewidth=1.2)
        pyplot.plot(ipc_arr[1:12], rel_arr[1:12], marker='.', linewidth=1.2)
    print 'rel: '+str(rel_arr)
    print 'ipc: '+str(ipc_arr)
    print 'ser: '+str(ser_arr)
 
    #BEAUTIFICATION
    pyplot.xlabel('Performance (IPC)')
    pyplot.ylabel('Reliability (MTTF)')
    x1,x2 = ax.get_xlim()
    pyplot.xlim([x1, 2.95])
    #Hide first xtick and ytick label
    pyplot.setp(ax.get_yticklabels()[0], visible=False)
    pyplot.setp(ax.get_xticklabels()[0], visible=False)
    ax.tick_params(axis=u'both', which=u'both', length=0)    
    ax.grid(True)
    pyplot.tight_layout() 
    save_n_crop('ReliabilityvsPerf_HMA.pdf')

#PLOT IPC/SER for Performance-focused HMA placement
def GLplot_ipc_ser_perf_focused_hma(pdata, BENCHS):
    print 'GLplot_ipc_ser_perf_focused_hma'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-4,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-4,
        'ytick.labelsize': LABELFONT-4,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [5, 3]
    } 
    rcParams.update(params)

    #Get data dictionary
    normSERDDR, normIPC, normAVF = get_dict_field_data(BENCHS, pdata, 'normal', ['ser_ddr', 'ipc', 'avf_ddr'])
    hotSERDDR, hotSERHBM, hotIPC = get_dict_field_data(BENCHS, pdata, 'TopHot', ['ser_ddr', 'ser_hbm', 'ipc'])
    hotSER = Counter(hotSERDDR) + Counter(hotSERHBM)
    
    #Get benchOrder in increasing AVF or MPKI*
    normAVF = OrderedDict(sorted(normAVF.items(), key=itemgetter(1)))
    mpki = OrderedDict(sorted(MPKI.items(), key=itemgetter(1), reverse=True))
    #benchOrder = normAVF.keys()
    benchOrder = mpki.keys()
    benchOrder = [x for x in benchOrder if x in BENCHS]
    print 'Plotting benchmarks: '+str(benchOrder)
    normSERDDR = OrderedDict([(k, normSERDDR[k]) for k in benchOrder])
    normIPC = OrderedDict([(k, normIPC[k]) for k in benchOrder])
    hotSER = OrderedDict([(k, hotSER[k]) for k in benchOrder])
    hotIPC = OrderedDict([(k, hotIPC[k]) for k in benchOrder])
    
    #Normalize IPC and SER
    avgHGIPC = 0
    avgHGSER = 0
    nHG = 0
    avgMIXIPC = 0
    avgMIXSER = 0
    nMIX = 0
    for b in benchOrder:
        hotIPC[b]=hotIPC[b]/normIPC[b]
        hotSER[b]=hotSER[b]/normSERDDR[b]
        if b in HGBENCHLIST:
            avgHGIPC = avgHGIPC + hotIPC[b]
            avgHGSER = avgHGSER + hotSER[b]
            nHG = nHG + 1 
        if b in MIXBENCHLIST:
            avgMIXIPC = avgMIXIPC + hotIPC[b]
            avgMIXSER = avgMIXSER + hotSER[b]
            nMIX = nMIX + 1
    avgHGIPC = avgHGIPC/nHG
    avgHGSER = avgHGSER/nHG
    avgMIXIPC = avgMIXIPC/nMIX
    avgMIXSER = avgMIXSER/nMIX
    
    #Debug prints
    print 'Benchmarks: '+str(normAVF.keys())
    print 'AVF: ' +str(normAVF.values())    
    print 'SER(Normal): '+str(normSERDDR.values())
    print 'IPC(Normal): '+str(normIPC.values())
    print 'SER(TopHot): '+str(hotSER.values())
    print 'IPC(TopHot): '+str(hotIPC.values())
    print 'len(hotSER.values()): '+str(len(hotSER.values()))
    print 'len(hotIPC.values()): '+str(len(hotIPC.values()))

    #Numbers for the paper
    avgIPC = sum(hotIPC.values())/len(hotIPC.values())
    avgSER = sum(hotSER.values())/len(hotSER.values())
    print 'Average IPC increase: '+str(sum(hotIPC.values())/len(hotIPC.values()))
    print 'Average SER increase: '+str(sum(hotSER.values())/len(hotSER.values()))
    
    #Add averages to the plot
    hotIPC['avg HG'] = avgHGIPC
    hotSER['avg HG'] = avgHGSER
    benchOrder.append('avg HG')
    hotIPC['avg mix'] = avgMIXIPC
    hotSER['avg mix'] = avgMIXSER
    benchOrder.append('avg mix')
    hotIPC['avg'] = avgIPC
    hotSER['avg'] = avgSER
    benchOrder.append('avg')
     
    #Plot the graph    
    fig, ax = pyplot.subplots()
    width = 0.6
    ind = np.arange(len(benchOrder))
    print 'ind: '+str(ind)

    #Left side hotness plot
    ax.axhline(y=1.0, color='0.3', ls='dashed', lw=1.0, label='IPC (Only DDRx)') 
    ax.bar(ind, hotIPC.values(), width, color='0.8', label='IPC ('+STATICMAP_COUNTERS_LABEL['TopHot', 'none']+')') 
    #xticks should be before twinx()
    pyplot.xticks(ind+width/2, benchOrder, rotation=90)
    #pyplot.xticks(ind+width/2, benchOrder, rotation=90)
    xlables = map((lambda x: BENCHLABELS[x]), benchOrder)
    ax.set_xticks(ind+width/2)
    ax.set_xticklabels(xlables, rotation=90)


    ax.set_ylabel('IPC (Normalized to only DDRx)')
    
    #Right side avf plot
    ax1 = ax.twinx()
    ax1.plot(ind+width/2, hotSER.values(), color='r', marker='.', linewidth=0, markersize=8, label='SER (HMA)')         
    ax1.axhline(y=1.0, color='r', ls='dashed', lw=4.0, label='SER (Only DDRx)')
    ax1.set_ylabel('SER (Normalized to only DDRx)', color='r')
   
    #BEAUTIFICATION
    x1,x2 = ax.get_xlim()
    ax.axvline(x=x2-2.8, color='0.3', ls='solid', lw=2.0)
    #pyplot.xlim([-0.5, x2-0.1])
    y1,y2 = ax.get_ylim()
    #ax.set_ylim(y1, 3.0)
    #ax1.set_ylim(0, 800)
     
    #ax.legend(loc='upper left', fancybox=True)
    #ax1.legend(loc='upper right', fancybox=True)
    ax.legend(loc='upper center', bbox_to_anchor=(0.3, 1.25),
                  fancybox=True, shadow=True)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.8, 1.25),
                  fancybox=True, shadow=True)


    ax.tick_params(axis=u'both', which=u'both', length=0)
    pyplot.tight_layout() 
    save_n_crop('IPC_SER_'+'TopHot'+'.pdf')


#PLOT IPC/SER for Performance-focused HMA placement
def GLplot_ipc_ser_perf_focused_hma_plot_avgs(pdata, BENCHS):
    print 'GLplot_ipc_ser_perf_focused_hma'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-3,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-4,
        'ytick.labelsize': LABELFONT-4,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [4, 3]
    } 
    rcParams.update(params)

    #Get data dictionary
    normSERDDR, normIPC, normAVF = get_dict_field_data(BENCHS, pdata, 'normal', ['ser_ddr', 'ipc', 'avf_ddr'])
    hotSERDDR, hotSERHBM, hotIPC = get_dict_field_data(BENCHS, pdata, 'TopHot', ['ser_ddr', 'ser_hbm', 'ipc'])
    hotSER = Counter(hotSERDDR) + Counter(hotSERHBM)
    
    #Get benchOrder in increasing AVF or MPKI*
    normAVF = OrderedDict(sorted(normAVF.items(), key=itemgetter(1)))
    mpki = OrderedDict(sorted(MPKI.items(), key=itemgetter(1), reverse=True))
    #benchOrder = normAVF.keys()
    benchOrder = mpki.keys()
    benchOrder = [x for x in benchOrder if x in BENCHS]
    print 'Plotting benchmarks: '+str(benchOrder)
    normSERDDR = OrderedDict([(k, normSERDDR[k]) for k in benchOrder])
    normIPC = OrderedDict([(k, normIPC[k]) for k in benchOrder])
    hotSER = OrderedDict([(k, hotSER[k]) for k in benchOrder])
    hotIPC = OrderedDict([(k, hotIPC[k]) for k in benchOrder])
    
    #Normalize IPC and SER
    avgHGIPC = 0
    avgHGSER = 0
    nHG = 0
    avgMIXIPC = 0
    avgMIXSER = 0
    nMIX = 0
    for b in benchOrder:
        hotIPC[b]=hotIPC[b]/normIPC[b]
        hotSER[b]=hotSER[b]/normSERDDR[b]
        if b in HGBENCHLIST:
            avgHGIPC = avgHGIPC + hotIPC[b]
            avgHGSER = avgHGSER + hotSER[b]
            nHG = nHG + 1 
        if b in MIXBENCHLIST:
            avgMIXIPC = avgMIXIPC + hotIPC[b]
            avgMIXSER = avgMIXSER + hotSER[b]
            nMIX = nMIX + 1
    avgHGIPC = avgHGIPC/nHG
    avgHGSER = avgHGSER/nHG
    avgMIXIPC = avgMIXIPC/nMIX
    avgMIXSER = avgMIXSER/nMIX
    
    #Debug prints
    print 'Benchmarks: '+str(normAVF.keys())
    print 'AVF: ' +str(normAVF.values())    
    print 'SER(Normal): '+str(normSERDDR.values())
    print 'IPC(Normal): '+str(normIPC.values())
    print 'SER(TopHot): '+str(hotSER.values())
    print 'IPC(TopHot): '+str(hotIPC.values())
    print 'len(hotSER.values()): '+str(len(hotSER.values()))
    print 'len(hotIPC.values()): '+str(len(hotIPC.values()))

    #Numbers for the paper
    avgIPC = sum(hotIPC.values())/len(hotIPC.values())
    avgSER = sum(hotSER.values())/len(hotSER.values())
    print 'Average IPC increase: '+str(sum(hotIPC.values())/len(hotIPC.values()))
    print 'Average SER increase: '+str(sum(hotSER.values())/len(hotSER.values()))
    
    #Add averages to the plot
    hotIPC['avg HG'] = avgHGIPC
    hotSER['avg HG'] = avgHGSER
    benchOrder.append('avg HG')
    hotIPC['avg mix'] = avgMIXIPC
    hotSER['avg mix'] = avgMIXSER
    benchOrder.append('avg mix')
    hotIPC['avg'] = avgIPC
    hotSER['avg'] = avgSER
    benchOrder.append('avg')
    
    #Averages only
    avgBenchs = ['avg HG', 'avg mix', 'avg']
    hotIPC_filt = OrderedDict((b, hotIPC[b]) for b in avgBenchs)
    hotSER_filt = OrderedDict((b, hotSER[b]) for b in avgBenchs)
    
    hotIPC = hotIPC_filt
    hotSER = hotSER_filt
    benchOrder = avgBenchs
     
    #Plot the graph    
    fig, ax = pyplot.subplots()
    width = 0.6
    ind = np.arange(len(benchOrder))
    print 'ind: '+str(ind)

    #Left side hotness plot
    ax.axhline(y=1.0, color='0.3', ls='dashed', lw=1.0, label='IPC (Only DDRx)') 
    ax.bar(ind, hotIPC.values(), width, color='0.8', label='IPC ('+STATICMAP_COUNTERS_LABEL['TopHot', 'none']+')') 
    #xticks should be before twinx()
    pyplot.xticks(ind+width/2, benchOrder, rotation=90)
    #pyplot.xticks(ind+width/2, benchOrder, rotation=90)
    xlables = map((lambda x: BENCHLABELS[x]), benchOrder)
    ax.set_xticks(ind+width/2)
    ax.set_xticklabels(xlables, rotation=90)


    ax.set_ylabel('IPC (Normalized to only DDRx)')
    
    #Right side avf plot
    ax1 = ax.twinx()
    ax1.plot(ind+width/2, hotSER.values(), color='r', marker='.', linewidth=0, markersize=8, label='SER (HMA)')         
    ax1.axhline(y=1.0, color='r', ls='dashed', lw=4.0, label='SER (Only DDRx)')
    ax1.set_ylabel('SER (Normalized to only DDRx)', color='r')
   
    #BEAUTIFICATION
    x1,x2 = ax.get_xlim()
    ax.axvline(x=x2-2.8, color='0.3', ls='solid', lw=2.0)
    #pyplot.xlim([-0.5, x2-0.1])
    y1,y2 = ax.get_ylim()
    #ax.set_ylim(y1, 3.0)
    #ax1.set_ylim(0, 800)
     
    #ax.legend(loc='upper left', fancybox=True)
    #ax1.legend(loc='upper right', fancybox=True)
    ax.legend(loc='upper center', bbox_to_anchor=(0.3, 1.25),
                  fancybox=True, shadow=True)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.8, 1.25),
                  fancybox=True, shadow=True)


    ax.tick_params(axis=u'both', which=u'both', length=0)
    pyplot.tight_layout() 
    save_n_crop('IPC_SER_'+'TopHot'+'.pdf')



#PLOT IPC/SER for pageplacement
def GLplot_ipc_ser_for_pm_hma(pdata, BENCHS, pm, args):
    print 'GLplot_ipc_ser_for_pm_hma'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-3,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [5, 3]
    } 
    rcParams.update(params)

    #Get data dictionary
    normSERDDR, normIPC, normAVF = get_dict_field_data(BENCHS, pdata, 'normal', ['ser_ddr', 'ipc', 'avf_ddr'])
    hotSERDDR, hotSERHBM, hotIPC = get_dict_field_data(BENCHS, pdata, 'TopHot', ['ser_ddr', 'ser_hbm', 'ipc'])
    pmSERDDR, pmSERHBM, pmIPC = get_dict_field_data(BENCHS, pdata, pm, ['ser_ddr', 'ser_hbm', 'ipc'])
    pmSER = Counter(pmSERDDR) + Counter(pmSERHBM)
    
    #Get benchOrder in increasing AVF
    normAVF = OrderedDict(sorted(normAVF.items(), key=itemgetter(1)))    
    mpki = OrderedDict(sorted(MPKI.items(), key=itemgetter(1), reverse=True))
    #benchOrder = normAVF.keys()    
    benchOrder = mpki.keys()    
    print benchOrder    
    normSERDDR = OrderedDict([(k, normSERDDR[k]) for k in benchOrder])
    normIPC = OrderedDict([(k, normIPC[k]) for k in benchOrder])
    pmSER = OrderedDict([(k, pmSER[k]) for k in benchOrder])
    pmIPC = OrderedDict([(k, pmIPC[k]) for k in benchOrder])
    
    #Normalize IPC and SER
    avgHGIPC = 0
    avgHGSER = 0
    nHG = 0
    avgMIXIPC = 0
    avgMIXSER = 0
    nMIX = 0
    for b in benchOrder:
        pmIPC[b]=pmIPC[b]/hotIPC[b]
        pmSER[b]=pmSER[b]/normSERDDR[b]
        if b in HGBENCHLIST:
            avgHGIPC = avgHGIPC + pmIPC[b]
            avgHGSER = avgHGSER + pmSER[b]
            nHG = nHG + 1
        if b in MIXBENCHLIST:
            avgMIXIPC = avgMIXIPC + pmIPC[b]
            avgMIXSER = avgMIXSER + pmSER[b]
            nMIX = nMIX + 1
    avgHGIPC = avgHGIPC/nHG
    avgHGSER = avgHGSER/nHG
    avgMIXIPC = avgMIXIPC/nMIX
    avgMIXSER = avgMIXSER/nMIX
 
    #Debug prints
    print 'Benchmarks: '+str(normAVF.keys())
    print 'AVF: ' +str(normAVF.values())    
    print 'SER(Normal): '+str(normSERDDR.values())
    print 'IPC(TopHot): '+str(hotIPC.values())
    print 'SER('+pm+'): '+str(pmSER.values())
    print 'IPC('+pm+'): '+str(pmIPC.values())
    print 'len(pmSER.values()): '+str(len(pmSER.values()))
    print 'len(pmIPC.values()): '+str(len(pmIPC.values()))

    #Numbers for the paper
    avgIPC = sum(pmIPC.values())/len(pmIPC.values())
    avgSER = sum(pmSER.values())/len(pmSER.values())
    print 'Average IPC increase: '+str(sum(pmIPC.values())/len(pmIPC.values()))
    print 'Average SER increase: '+str(sum(pmSER.values())/len(pmSER.values()))
    
    #Add averages to the plot
    pmIPC['avg HG'] = avgHGIPC
    pmSER['avg HG'] = avgHGSER
    benchOrder.append('avg HG')
    pmIPC['avg mix'] = avgMIXIPC
    pmSER['avg mix'] = avgMIXSER
    benchOrder.append('avg MIX')
    pmIPC['avg'] = avgIPC
    pmSER['avg'] = avgSER
    benchOrder.append('avg')
     
    #Plot the graph    
    fig, ax = pyplot.subplots()
    width = 0.6
    ind = np.arange(len(benchOrder))
    print 'ind: '+str(ind)

    #Left side hotness plot
    ax.axhline(y=1.0, color='0.3', ls='dashed', lw=1.0, label='IPC ('+STATICMAP_COUNTERS_LABEL[('TopHot', 'none')]+')')
    ax.bar(ind, pmIPC.values(), width, color='0.8', label='IPC ('+STATICMAP_COUNTERS_LABEL[(pm, args.counters)]+')') 
    #xticks should be before twinx()
    pyplot.xticks(ind+width/2, benchOrder, rotation=90)
    ax.set_ylabel('IPC (Normalized\nto performance-focused HMA)')
    
    #Right side avf plot
    ax1 = ax.twinx()
    ax1.plot(ind+width/2, pmSER.values(), color='r', marker='.', linewidth=0, markersize=8, label='SER (HMA)')         
    ax1.axhline(y=1.0, color='r', ls='dashed', lw=4.0, label='SER (Only DDRx)')
    ax1.set_ylabel('SER (Normalized\nto only DDRx)', color ='r')
   
    #BEAUTIFICATION
    x1,x2 = ax.get_xlim()
    #pyplot.xlim([-0.5, x2-0.1])
    #y1,y2 = ax.get_ylim()
    #ax.set_ylim(y1, 3.0)
    #ax1.set_ylim(0, 800)
     
    #ax.legend(loc='upper left', fancybox=True)
    #ax1.legend(loc='upper right', fancybox=True)
    ax.legend(loc='upper center', bbox_to_anchor=(0.3, 1.25),
                  fancybox=True, shadow=True)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.8, 1.25),
                  fancybox=True, shadow=True)


    ax.tick_params(axis=u'both', which=u'both', length=0)
    pyplot.tight_layout() 
    save_n_crop('IPC_SER_'+pm+'.pdf')

#HPCA PLOT IPC/SER for static page placement
def GLplot_ipc_ser_for_pm_hma_normalize_to_perf_focused(pdata, BENCHS, pm, args):
    print '-->>> GLplot_ipc_ser_for_pm_hma_normalize_to_perf_focused'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-3,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [5, 3]
    } 
    rcParams.update(params)

    #Get data dictionary
    #normSERDDR, normIPC, normAVF = get_dict_field_data(BENCHS, pdata, 'normal', ['ser_ddr', 'ipc_mig', 'avf_ddr'])
    hotSERDDR, hotSERHBM, hotIPC = get_dict_field_data(BENCHS, pdata, 'TopHot', ['ser_ddr', 'ser_hbm', 'ipc_mig'])
    hotSER = Counter(hotSERDDR) + Counter(hotSERHBM)
    pmSERDDR, pmSERHBM, pmIPC = get_dict_field_data(BENCHS, pdata, pm, ['ser_ddr', 'ser_hbm', 'ipc_mig'])
    pmSER = Counter(pmSERDDR) + Counter(pmSERHBM)

    #Get benchOrder in increasing AVF or MPKI*
    mpki = OrderedDict(sorted(MPKI.items(), key=itemgetter(1), reverse=True))
    benchOrder = mpki.keys()
    benchOrder = [x for x in benchOrder if x in BENCHS] #Filtering benchmark not used for the paper
    print 'Plotting benchmarks: '+str(benchOrder)  
    hotIPC = OrderedDict([(k, hotIPC[k]) for k in benchOrder])
    hotSER = OrderedDict([(k, hotSER[k]) for k in benchOrder]) 
    pmIPC = OrderedDict([(k, pmIPC[k]) for k in benchOrder])
    pmSER = OrderedDict([(k, pmSER[k]) for k in benchOrder])
    
    #Remove remapTable overhead
    if pm == 'PCTopWrWrRdRatio50FillM1':
        print '>>>>>> Removing RemapTable Overhead <<<<<<'
    for b in benchOrder:
        pmIPC[b] = pmIPC[b] - 0.01 * pmIPC[b]

  
    #Debug absolute values of IPC and SER prints
    print 'IPC(TopHot): '+str(hotIPC.values())
    print 'SER(TopHot): '+str(hotSER.values())
    print 'SER('+pm+'): '+str(pmSER.values())
    print 'IPC('+pm+'): '+str(pmIPC.values())
    print 'len(pmIPC.values()): '+str(len(pmIPC.values()))
    print 'len(pmSER.values()): '+str(len(pmSER.values()))
   
    #Normalize IPC and SER and hotIPC for stacking on top of IPC of the static placement
    avgHGIPC = 0
    avgHGhotIPC = 0 
    avgHGSER = 0
    nHG = 0
    avgMIXIPC = 0
    avgMIXhotIPC = 0
    avgMIXSER = 0
    nMIX = 0
    for b in benchOrder:
        pmIPC[b]=pmIPC[b]/hotIPC[b]
        pmSER[b]=pmSER[b]/hotSER[b]
        if b in HGBENCHLIST:
            avgHGIPC = avgHGIPC + pmIPC[b]
            avgHGSER = avgHGSER + pmSER[b]
            nHG = nHG + 1
        if b in MIXBENCHLIST:
            avgMIXIPC = avgMIXIPC + pmIPC[b]
            avgMIXSER = avgMIXSER + pmSER[b]
            nMIX = nMIX + 1
    avgHGIPC = avgHGIPC/nHG
    avgHGSER = avgHGSER/nHG
    avgMIXIPC = avgMIXIPC/nMIX
    avgMIXSER = avgMIXSER/nMIX
    
 
    #Numbers for the paper
    avgIPC = sum(pmIPC.values())/len(pmIPC.values())
    avgSER = sum(pmSER.values())/len(pmSER.values())
    print 'Average IPC for the static placement: '+str(avgIPC)
    print 'Average SER for the static placement: '+str(avgSER)
    print 'IPC loss (%): '+str(100-avgIPC*100)
    print 'SER reduction: '+str(1/avgSER)
    
    #Add averages to the plot
    pmIPC['avg HG'] = avgHGIPC
    pmSER['avg HG'] = avgHGSER
    benchOrder.append('avg HG')
    pmIPC['avg mix'] = avgMIXIPC
    pmSER['avg mix'] = avgMIXSER
    benchOrder.append('avg mix')
    pmIPC['avg'] = avgIPC
    pmSER['avg'] = avgSER
    benchOrder.append('avg')
    
    #Plot the graph    
    fig, ax = pyplot.subplots()
    width = 0.6
    ind = np.arange(len(benchOrder))
    print 'ind: '+str(ind)
   
    #Left side hotness plot
    ax.axhline(y=1.0, color='0.3', ls='dashed', lw=2.0, label='IPC (Perf-focused)')
    ax.bar(ind, pmIPC.values(), width, color='0.8', label='IPC ('+STATICMAP_COUNTERS_LABEL[(pm, args.counters)]+')') 
    xlables = map((lambda x: BENCHLABELS[x]), benchOrder)
    ax.set_xticks(ind+width/2)
    ax.set_xticklabels(xlables, rotation=90)

    ax.set_ylabel('IPC (Normalized\nto perf-focused)')
    
    #Right side avf plot
    ax1 = ax.twinx()
    ax1.plot(ind+width/2, [1]*len(pmSER), color='r', marker='.', linewidth=0, markersize=6, markeredgewidth=0.0, label='SER (Perf-focused)')         
    ax1.plot(ind+width/2, pmSER.values(), color='r', marker='v', linewidth=0, markersize=6, markeredgewidth=0.0, label='SER ('+STATICMAP_COUNTERS_LABEL[(pm, args.counters)]+')')         
    #ax1.axhline(y=1.0, color='r', ls='dashed', lw=2.0, label='SER (Perf-focused)')
    ax1.set_ylabel('SER (Normalized\nto perf-focused)', color ='r')
    ax1.spines['right'].set_color('red')
    ax1.yaxis.label.set_color('red')
 
    #BEAUTIFICATION
    x1,x2 = ax.get_xlim()
    ax.axvline(x=x2-2.8, color='0.3', ls='solid', lw=2.0)
    #pyplot.xlim([-0.5, x2-0.1])
    #y1,y2 = ax.get_ylim()
    #ax.set_ylim(0, 1.5)
    ax1.set_ylim(0, 2.0)
     
    #ax.legend(loc='upper left', fancybox=True)
    #ax1.legend(loc='upper right', fancybox=True)
    ax.legend(loc='upper center', bbox_to_anchor=(0.2, 1.30),
                  fancybox=True, shadow=True)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.75, 1.30),
                  fancybox=True, shadow=True)

    ax.tick_params(axis=u'both', which=u'both', length=0)
    pyplot.tight_layout() 
    save_n_crop('IPC_SER_SMAP'+pm+'_CNT'+args.counters+'_ALLOC'+args.initalloc+'.pdf')

#PLOT IPC/SER for migration with a specific counter mechanism
def GLplot_ipc_ser_hybrid_counter_hma_normalize_to_only_ddr(pdata_static, pdata_mig, pdata_mig_perf,  BENCHS, cnt, args):
    print 'GLplot_ipc_ser_for_hybrid_counter_hma_normalize_to_only_ddr'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-3,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [5, 3]
    } 
    rcParams.update(params)

    #Get data dictionary
    normSERDDR, normIPC, normAVF = get_dict_field_data(BENCHS, pdata_static, 'normal', ['ser_ddr', 'ipc_mig', 'avf_ddr'])
    cntSERDDR, cntSERHBM, cntIPC = get_dict_field_data(BENCHS, pdata_mig, cnt, ['ser_ddr', 'ser_hbm', 'ipc_mig'])
    perfSERDDR, perfSERHBM, perfIPC = get_dict_field_data(BENCHS, pdata_mig_perf, PERFORMANCE_COUNTER, ['ser_ddr', 'ser_hbm', 'ipc_mig'])
    cntSER = Counter(cntSERDDR) + Counter(cntSERHBM)
    
    #Get benchOrder in increasing AVF or MPKI*
    normAVF = OrderedDict(sorted(normAVF.items(), key=itemgetter(1)))    
    mpki = OrderedDict(sorted(MPKI.items(), key=itemgetter(1), reverse=True))
    #benchOrder = normAVF.keys()    
    benchOrder = mpki.keys()
    benchOrder = [x for x in benchOrder if x in BENCHS]
    print 'Plotting benchmarks: '+str(benchOrder)  
    normSERDDR = OrderedDict([(k, normSERDDR[k]) for k in benchOrder])
    normIPC = OrderedDict([(k, normIPC[k]) for k in benchOrder])
    cntSER = OrderedDict([(k, cntSER[k]) for k in benchOrder])
    cntIPC = OrderedDict([(k, cntIPC[k]) for k in benchOrder])
    perfIPC = OrderedDict([(k, perfIPC[k]) for k in benchOrder])

    #Normalize IPC and SER
    avgHGIPC = 0
    avgHGSER = 0
    avgPerfHGIPC = 0
    nHG = 0
    avgMIXIPC = 0
    avgMIXSER = 0
    avgPerfMIXIPC = 0
    nMIX = 0
    for b in benchOrder:
        cntIPC[b]=cntIPC[b]/normIPC[b]
        cntSER[b]=cntSER[b]/normSERDDR[b]
        perfIPC[b]=perfIPC[b]/normIPC[b]
        if b in HGBENCHLIST:
            avgHGIPC = avgHGIPC + cntIPC[b]
            avgHGSER = avgHGSER + cntSER[b]
            avgPerfHGIPC = avgPerfHGIPC + perfIPC[b]
            nHG=nHG+1
        if b in MIXBENCHLIST:
            avgMIXIPC = avgMIXIPC + cntIPC[b]
            avgMIXSER = avgMIXSER + cntSER[b]
            avgPerfMIXIPC = avgPerfMIXIPC + perfIPC[b]
            nMIX=nMIX+1
    #avg values for the counter=cnt
    avgHGIPC = avgHGIPC/nHG
    avgHGSER = avgHGSER/nHG
    avgMIXIPC = avgMIXIPC/nMIX
    avgMIXSER = avgMIXSER/nMIX
    #avg values for the counter=PERFORMANCE_COUNTER
    avgPerfHGIPC = avgPerfHGIPC/nHG
    avgPerfMIXIPC = avgPerfMIXIPC/nMIX


    #Debug prints
    print 'Benchmarks: '+str(normAVF.keys())
    print 'AVF: ' +str(normAVF.values())    
    print 'SER(Normal): '+str(normSERDDR.values())
    print 'SER('+cnt+'): '+str(cntSER.values())
    print 'IPC('+cnt+'): '+str(cntIPC.values())
    print 'len(cntSER.values()): '+str(len(cntSER.values()))
    print 'len(cntIPC.values()): '+str(len(cntIPC.values()))

    #Numbers for the paper
    avgIPC = sum(cntIPC.values())/len(cntIPC.values())
    avgSER = sum(cntSER.values())/len(cntSER.values())
    avgPerfIPC = sum(perfIPC.values())/len(perfIPC.values())
    print 'Average IPC increase: '+str(sum(cntIPC.values())/len(cntIPC.values()))
    print 'Average IPC increase (performance migration): '+str(sum(perfIPC.values())/len(perfIPC.values()))
    print 'Average SER increase: '+str(sum(cntSER.values())/len(cntSER.values()))
    
    #Add averages to the plot
    cntIPC['avg HG'] = avgHGIPC
    perfIPC['avg HG'] = avgPerfHGIPC
    cntSER['avg HG'] = avgHGSER
    benchOrder.append('avg HG')
    cntIPC['avg mix'] = avgMIXIPC
    perfIPC['avg mix'] = avgPerfMIXIPC
    cntSER['avg mix'] = avgMIXSER
    benchOrder.append('avg mix')
    cntIPC['avg'] = avgIPC
    perfIPC['avg'] = avgPerfIPC
    cntSER['avg'] = avgSER
    benchOrder.append('avg')
 
    #Get delta in peformance with static page placement and TopHot
    deltaIPC=OrderedDict({})
    for b in benchOrder:
        deltaIPC[b] = perfIPC[b] - cntIPC[b]
        if deltaIPC[b] < 0:
            deltaIPC[b] = 0          
    print 'perfIPC.values(): '+str(perfIPC.values())
    print 'deltaIPC.values(): '+str(deltaIPC.values())
     
    #Plot the graph    
    fig, ax = pyplot.subplots()
    width = 0.6
    ind = np.arange(len(benchOrder))
    print 'ind: '+str(ind)

    #Left side hotness plot
    ax.axhline(y=1.0, color='0.3', ls='dashed', lw=1.0, label='IPC (Only DDR)')
    ax.bar(ind, cntIPC.values(), width, color='0.8', label='IPC ('+STATICMAP_COUNTERS_LABEL[('none', args.counters)]+')')
    #Stack the gap between this and TopHot performance
    if(cnt != PERFORMANCE_COUNTER):
        ax.bar(ind, deltaIPC.values(), width, color='0.5', hatch='//////', bottom=cntIPC.values(), label='IPC (loss)')
   
    #xticks should be before twinx()
    #pyplot.xticks(ind+width/2, benchOrder, rotation=90)
    xlables = map((lambda x: BENCHLABELS[x]), benchOrder)
    ax.set_xticks(ind+width/2)
    ax.set_xticklabels(xlables, rotation=90)
 
    ax.set_ylabel('IPC (Normalized)')
    
    #Right side avf plot
    ax1 = ax.twinx()
    ax1.plot(ind+width/2, cntSER.values(), color='r', marker='.', linewidth=0, markersize=8, label='SER ('+STATICMAP_COUNTERS_LABEL[('none', args.counters)]+')')
    ax1.axhline(y=1.0, color='r', ls='dashed', lw=2.0, label='SER (Only DDRx)')
    ax1.set_ylabel('SER (Normalized)', color ='r')
   
    #BEAUTIFICATION
    x1,x2 = ax.get_xlim()
    ax.axvline(x=x2-2.8, color='0.3', ls='solid', lw=2.0)
    #pyplot.xlim([-0.5, x2-0.1])
    #y1,y2 = ax.get_ylim()
    #ax.set_ylim(0, 2.5)
    ax1.set_ylim(0, 600)
     
    #ax.legend(loc='upper left', fancybox=True)
    #ax1.legend(loc='upper right', fancybox=True)
    if(cnt != PERFORMANCE_COUNTER):
        ax.legend(loc='upper center', bbox_to_anchor=(0.24, 1.34),
                  fancybox=True, shadow=True)
    else:
        ax.legend(loc='upper center', bbox_to_anchor=(0.24, 1.30),
                  fancybox=True, shadow=True)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.80, 1.30),
                  fancybox=True, shadow=True)

    ax.tick_params(axis=u'both', which=u'both', length=0)
    pyplot.tight_layout() 
    save_n_crop('IPC_SER_SMAP_none'+'_CNT'+args.counters+'_ALLOC'+args.initalloc+'.pdf')



#PLOT IPC/SER for migration with a specific counter mechanism
def GLplot_ipc_ser_hybrid_counter_hma_normalize_to_perf_focused(pdata_static, pdata_mig, pdata_mig_perf,  BENCHS, cnt, args):
    print 'GLplot_ipc_ser_for_hybrid_counter_hma_normalize_to_perf_focused'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-3,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [5, 3]
    } 
    rcParams.update(params)

    #Get data dictionary
    #normSERDDR, normIPC, normAVF = get_dict_field_data(BENCHS, pdata_static, 'normal', ['ser_ddr', 'ipc_mig', 'avf_ddr'])
    cntSERDDR, cntSERHBM, cntIPC, cntNMIGS, cntNINTERVALS = get_dict_field_data(BENCHS, pdata_mig, cnt, ['ser_ddr', 'ser_hbm', 'ipc_mig', 'n_mig', 'n_interval'])
    cntSER = Counter(cntSERDDR) + Counter(cntSERHBM)
    perfSERDDR, perfSERHBM, perfIPC = get_dict_field_data(BENCHS, pdata_mig_perf, PERFORMANCE_COUNTER, ['ser_ddr', 'ser_hbm', 'ipc_mig'])
    perfSER = Counter(perfSERDDR) + Counter(perfSERHBM)
     
    #Get benchOrder in increasing AVF or MPKI*
    mpki = OrderedDict(sorted(MPKI.items(), key=itemgetter(1), reverse=True))
    benchOrder = mpki.keys()
    benchOrder = [x for x in benchOrder if x in BENCHS]
    print 'Plotting benchmarks: '+str(benchOrder)  
    cntSER = OrderedDict([(k, cntSER[k]) for k in benchOrder])
    cntIPC = OrderedDict([(k, cntIPC[k]) for k in benchOrder])
    perfSER = OrderedDict([(k, perfSER[k]) for k in benchOrder])
    perfIPC = OrderedDict([(k, perfIPC[k]) for k in benchOrder])
    
    #Remove remapTable overhead
    if cnt == 'MEAupHMAdown':
        print '>>>>>> Removing RemapTable Overhead <<<<<<'
    for b in benchOrder:
        cntIPC[b] = cntIPC[b] - 0.028 * cntIPC[b]

    #Normalize IPC and SER
    avgHGIPC = 0
    avgHGSER = 0
    nHG = 0
    avgMIXIPC = 0
    avgMIXSER = 0
    nMIX = 0
    for b in benchOrder:
        cntIPC[b]=cntIPC[b]/perfIPC[b]
        cntSER[b]=cntSER[b]/perfSER[b]
        if b in HGBENCHLIST:
            avgHGIPC = avgHGIPC + cntIPC[b]
            avgHGSER = avgHGSER + cntSER[b]
            nHG=nHG+1
        if b in MIXBENCHLIST:
            avgMIXIPC = avgMIXIPC + cntIPC[b]
            avgMIXSER = avgMIXSER + cntSER[b]
            nMIX=nMIX+1
    #avg values for the counter=cnt
    avgHGIPC = avgHGIPC/nHG
    avgHGSER = avgHGSER/nHG
    avgMIXIPC = avgMIXIPC/nMIX
    avgMIXSER = avgMIXSER/nMIX
    
    #Average Migrations
    avg_total_migs = sum(cntNMIGS.values())/len(benchOrder)
    avg_total_interval = sum(cntNINTERVALS.values())/len(benchOrder)
    avg_migs_per_interval = avg_total_migs/avg_total_interval
    print 'Average migrations: '+str(avg_total_migs)
    print 'Average intervals: '+str(avg_total_interval)
    print 'Migrartions per interval: '+str(avg_migs_per_interval)

    #Debug prints
    print 'Benchmarks: '+str(mpki.keys())
    print 'SER('+cnt+'): '+str(cntSER.values())
    print 'IPC('+cnt+'): '+str(cntIPC.values())
    print 'len(cntSER.values()): '+str(len(cntSER.values()))
    print 'len(cntIPC.values()): '+str(len(cntIPC.values()))

    #Numbers for the paper
    avgIPC = sum(cntIPC.values())/len(cntIPC.values())
    avgSER = sum(cntSER.values())/len(cntSER.values())
    avgPerfIPC = sum(perfIPC.values())/len(perfIPC.values())
    print 'Average IPC for the dynamic scheme: '+str(avgIPC)
    print 'Average SER for the dynamic scheme: '+str(avgSER)
    print 'IPC loss (%): '+str(100-avgIPC*100)
    print 'SER reduction: '+str(1/avgSER)
    
    #Add averages to the plot
    cntIPC['avg HG'] = avgHGIPC
    cntSER['avg HG'] = avgHGSER
    benchOrder.append('avg HG')
    cntIPC['avg mix'] = avgMIXIPC
    cntSER['avg mix'] = avgMIXSER
    benchOrder.append('avg mix')
    cntIPC['avg'] = avgIPC
    cntSER['avg'] = avgSER
    benchOrder.append('avg')
 
    #Plot the graph    
    fig, ax = pyplot.subplots()
    width = 0.6
    ind = np.arange(len(benchOrder))
    print 'ind: '+str(ind)

    #Left side hotness plot
    ax.axhline(y=1.0, color='0.3', ls='dashed', lw=1.0, label='IPC (Perf-focused)')
    ax.bar(ind, cntIPC.values(), width, color='0.8', label='IPC ('+STATICMAP_COUNTERS_LABEL[('none', args.counters)]+')')
  
    #xticks should be before twinx()
    #pyplot.xticks(ind+width/2, benchOrder, rotation=90)
    xlables = map((lambda x: BENCHLABELS[x]), benchOrder)
    ax.set_xticks(ind+width/2)
    ax.set_xticklabels(xlables, rotation=90)
 
    ax.set_ylabel('IPC (Normalized\nto perf-focused)')
    
    #Right side avf plot
    ax1 = ax.twinx()
    ax1.plot(ind+width/2, [1]*len(cntSER), color='r', marker='.', linewidth=0, markersize=6, markeredgewidth=0.0, label='SER (Perf-focused)')         
    ax1.plot(ind+width/2, cntSER.values(), color='r', marker='v', linewidth=0, markersize=6, markeredgewidth=0.0, label='SER ('+STATICMAP_COUNTERS_LABEL[('none', args.counters)]+')')
    ax1.set_ylabel('SER (Normalized\nto perf-focused)', color ='r')
   
    #BEAUTIFICATION
    x1,x2 = ax.get_xlim()
    ax.axvline(x=x2-2.8, color='0.3', ls='solid', lw=2.0)
    #pyplot.xlim([-0.5, x2-0.1])
    #y1,y2 = ax.get_ylim()
    #ax.set_ylim(0, 2.5)
    ax1.set_ylim(0, 2.0)    
    ax1.spines['right'].set_color('red')
    ax1.yaxis.label.set_color('red')
 
    #ax.legend(loc='upper left', fancybox=True)
    #ax1.legend(loc='upper right', fancybox=True)
    if(cnt != PERFORMANCE_COUNTER):
        ax.legend(loc='upper center', bbox_to_anchor=(0.20, 1.30),
                  fancybox=True, shadow=True)
    else:
        ax.legend(loc='upper center', bbox_to_anchor=(0.20, 1.28),
                  fancybox=True, shadow=True)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.80, 1.30),
                  fancybox=True, shadow=True)
    
    ax.tick_params(axis=u'both', which=u'both', length=0)
    pyplot.tight_layout() 
    save_n_crop('IPC_SER_SMAP_none'+'_CNT'+args.counters+'_ALLOC'+args.initalloc+'.pdf')

def GLplot_ipc_ser_hybrid_counter_hma_normalize_to_only_ddr_plot_avgs(pdata_static, pdata_mig, pdata_mig_perf,  BENCHS, cnt, args):
    print 'GLplot_ipc_ser_for_hybrid_counter_hma_normalize_to_only_ddr'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-2,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [4, 3]
    } 
    rcParams.update(params)

    #Get data dictionary
    normSERDDR, normIPC, normAVF = get_dict_field_data(BENCHS, pdata_static, 'normal', ['ser_ddr', 'ipc_mig', 'avf_ddr'])
    cntSERDDR, cntSERHBM, cntIPC = get_dict_field_data(BENCHS, pdata_mig, cnt, ['ser_ddr', 'ser_hbm', 'ipc_mig'])
    perfSERDDR, perfSERHBM, perfIPC = get_dict_field_data(BENCHS, pdata_mig_perf, PERFORMANCE_COUNTER, ['ser_ddr', 'ser_hbm', 'ipc_mig'])
    cntSER = Counter(cntSERDDR) + Counter(cntSERHBM)
    
    #Get benchOrder in increasing AVF or MPKI*
    normAVF = OrderedDict(sorted(normAVF.items(), key=itemgetter(1)))    
    mpki = OrderedDict(sorted(MPKI.items(), key=itemgetter(1), reverse=True))
    #benchOrder = normAVF.keys()    
    benchOrder = mpki.keys()
    benchOrder = [x for x in benchOrder if x in BENCHS]
    print 'Plotting benchmarks: '+str(benchOrder)  
    normSERDDR = OrderedDict([(k, normSERDDR[k]) for k in benchOrder])
    normIPC = OrderedDict([(k, normIPC[k]) for k in benchOrder])
    cntSER = OrderedDict([(k, cntSER[k]) for k in benchOrder])
    cntIPC = OrderedDict([(k, cntIPC[k]) for k in benchOrder])
    perfIPC = OrderedDict([(k, perfIPC[k]) for k in benchOrder])

    #Normalize IPC and SER
    avgHGIPC = 0
    avgHGSER = 0
    avgPerfHGIPC = 0
    nHG = 0
    avgMIXIPC = 0
    avgMIXSER = 0
    avgPerfMIXIPC = 0
    nMIX = 0
    for b in benchOrder:
        cntIPC[b]=cntIPC[b]/normIPC[b]
        cntSER[b]=cntSER[b]/normSERDDR[b]
        perfIPC[b]=perfIPC[b]/normIPC[b]
        if b in HGBENCHLIST:
            avgHGIPC = avgHGIPC + cntIPC[b]
            avgHGSER = avgHGSER + cntSER[b]
            avgPerfHGIPC = avgPerfHGIPC + perfIPC[b]
            nHG=nHG+1
        if b in MIXBENCHLIST:
            avgMIXIPC = avgMIXIPC + cntIPC[b]
            avgMIXSER = avgMIXSER + cntSER[b]
            avgPerfMIXIPC = avgPerfMIXIPC + perfIPC[b]
            nMIX=nMIX+1
    #avg values for the counter=cnt
    avgHGIPC = avgHGIPC/nHG
    avgHGSER = avgHGSER/nHG
    avgMIXIPC = avgMIXIPC/nMIX
    avgMIXSER = avgMIXSER/nMIX
    #avg values for the counter=PERFORMANCE_COUNTER
    avgPerfHGIPC = avgPerfHGIPC/nHG
    avgPerfMIXIPC = avgPerfMIXIPC/nMIX


    #Debug prints
    print 'Benchmarks: '+str(normAVF.keys())
    print 'AVF: ' +str(normAVF.values())    
    print 'SER(Normal): '+str(normSERDDR.values())
    print 'SER('+cnt+'): '+str(cntSER.values())
    print 'IPC('+cnt+'): '+str(cntIPC.values())
    print 'len(cntSER.values()): '+str(len(cntSER.values()))
    print 'len(cntIPC.values()): '+str(len(cntIPC.values()))

    #Numbers for the paper
    avgIPC = sum(cntIPC.values())/len(cntIPC.values())
    avgSER = sum(cntSER.values())/len(cntSER.values())
    avgPerfIPC = sum(perfIPC.values())/len(perfIPC.values())
    print 'Average IPC increase: '+str(sum(cntIPC.values())/len(cntIPC.values()))
    print 'Average IPC increase (performance migration): '+str(sum(perfIPC.values())/len(perfIPC.values()))
    print 'Average SER increase: '+str(sum(cntSER.values())/len(cntSER.values()))
    
    #Add averages to the plot
    cntIPC['avg HG'] = avgHGIPC
    perfIPC['avg HG'] = avgPerfHGIPC
    cntSER['avg HG'] = avgHGSER
    benchOrder.append('avg HG')
    cntIPC['avg mix'] = avgMIXIPC
    perfIPC['avg mix'] = avgPerfMIXIPC
    cntSER['avg mix'] = avgMIXSER
    benchOrder.append('avg mix')
    cntIPC['avg'] = avgIPC
    perfIPC['avg'] = avgPerfIPC
    cntSER['avg'] = avgSER
    benchOrder.append('avg')
 
    #Get delta in peformance with static page placement and TopHot
    deltaIPC=OrderedDict({})
    for b in benchOrder:
        deltaIPC[b] = perfIPC[b] - cntIPC[b]
        if deltaIPC[b] < 0:
            deltaIPC[b] = 0          
    print 'perfIPC.values(): '+str(perfIPC.values())
    print 'deltaIPC.values(): '+str(deltaIPC.values())
    
    avgBenchs = ['avg HG', 'avg mix', 'avg']
    cntIPC_filt = OrderedDict((b, cntIPC[b]) for b in avgBenchs)
    cntSER_filt = OrderedDict((b, cntSER[b]) for b in avgBenchs)
    deltaIPC_filt = OrderedDict((b, deltaIPC[b]) for b in avgBenchs)
    
    cntIPC = cntIPC_filt
    cntSER = cntSER_filt
    deltaIPC = deltaIPC_filt
    benchOrder = avgBenchs
 
     
    #Plot the graph    
    fig, ax = pyplot.subplots()
    width = 0.6
    ind = np.arange(len(benchOrder))
    print 'ind: '+str(ind)

    #Left side hotness plot
    ax.axhline(y=1.0, color='0.3', ls='dashed', lw=1.0, label='IPC (Only DDR)')
    ax.bar(ind, cntIPC.values(), width, color='0.8', label='IPC ('+STATICMAP_COUNTERS_LABEL[('none', args.counters)]+')')
    #Stack the gap between this and TopHot performance
    if(cnt != PERFORMANCE_COUNTER):
        ax.bar(ind, deltaIPC.values(), width, color='0.5', hatch='//////', bottom=cntIPC.values(), label='IPC (loss)')
   
    #xticks should be before twinx()
    #pyplot.xticks(ind+width/2, benchOrder, rotation=90)
    xlables = map((lambda x: BENCHLABELS[x]), benchOrder)
    ax.set_xticks(ind+width/2)
    ax.set_xticklabels(xlables, rotation=90)
 
    ax.set_ylabel('IPC (Normalized)')
    
    #Right side avf plot
    ax1 = ax.twinx()
    ax1.plot(ind+width/2, cntSER.values(), color='r', marker='.', linewidth=0, markersize=14, label='SER ('+STATICMAP_COUNTERS_LABEL[('none', args.counters)]+')')
    ax1.axhline(y=1.0, color='r', ls='dashed', lw=2.0, label='SER (Only DDRx)')
    ax1.set_ylabel('SER (Normalized)', color ='r')
   
    #BEAUTIFICATION
    x1,x2 = ax.get_xlim()
    ax.axvline(x=x2-2.8, color='0.3', ls='solid', lw=2.0)
    #pyplot.xlim([-0.5, x2-0.1])
    #y1,y2 = ax.get_ylim()
    #ax.set_ylim(0, 2.5)
    ax1.set_ylim(0, 600)
     
    #ax.legend(loc='upper left', fancybox=True)
    #ax1.legend(loc='upper right', fancybox=True)
    if(cnt != PERFORMANCE_COUNTER):
        ax.legend(loc='upper center', bbox_to_anchor=(0.15, 1.34),
                  fancybox=True, shadow=True)
    else:
        ax.legend(loc='upper center', bbox_to_anchor=(0.10, 1.30),
                  fancybox=True, shadow=True)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.80, 1.30),
                  fancybox=True, shadow=True)

    ax.tick_params(axis=u'both', which=u'both', length=0)
    pyplot.tight_layout() 
    save_n_crop('IPC_SER_SMAP_none'+'_CNT'+args.counters+'_ALLOC'+args.initalloc+'.pdf')

def normalize_and_return_ipc_ser(cntIPC, cntSER, normIPC, normSER, benchOrder):
    #Normalize IPC and SER in cntIPC and cntSER w.r.t. IPC and SER in normIPC and normSER
    avgHGIPC = 0
    avgHGSER = 0
    nHG = 0
    avgMIXIPC = 0
    avgMIXSER = 0
    nMIX = 0
    for b in benchOrder:
        cntIPC[b]=cntIPC[b]/normIPC[b]
        cntSER[b]=cntSER[b]/normSER[b]
        if b in HGBENCHLIST:
            avgHGIPC = avgHGIPC + cntIPC[b]
            avgHGSER = avgHGSER + cntSER[b]
            nHG=nHG+1
        if b in MIXBENCHLIST:
            avgMIXIPC = avgMIXIPC + cntIPC[b]
            avgMIXSER = avgMIXSER + cntSER[b]
            nMIX=nMIX+1
    #avg values for the counter=cnt
    avgHGIPC = avgHGIPC/nHG
    avgHGSER = avgHGSER/nHG
    avgMIXIPC = avgMIXIPC/nMIX
    avgMIXSER = avgMIXSER/nMIX
    return cntIPC, cntSER


#plot average results normalized performance focused migrations for balanced scheme
def GLplot_ipc_ser_hybrid_balanced_schemes_normalize_to_perf_focused_migration(BENCHS, pdata_mig_perf, pdata_rel, pdata_bal, pdata_pcbased):
    print 'GLplot_ipc_ser_for_hybrid_balanced_schemes_normalize_to_perf_focused_migration'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-2,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [4, 3]
    } 
    rcParams.update(params)

    #Use performance-focused migration to normalize both IPC and SER
    perfSERDDR, perfSERHBM, perfIPC = get_dict_field_data(BENCHS, pdata_mig_perf, PERFORMANCE_COUNTER, ['ser_ddr', 'ser_hbm', 'ipc_mig'])
    perfSER = Counter(perfSERDDR) + Counter(perfSERHBM)
    print 'perfSER: '+str(perfSER)
    
    ''' 
    #Get benchOrder in increasing AVF or MPKI*
    mpki = OrderedDict(sorted(MPKI.items(), key=itemgetter(1), reverse=True))
    benchOrder = mpki.keys()
    benchOrder = [x for x in benchOrder if x in BENCHS]
    print 'Plotting benchmarks: '+str(benchOrder)  
    perfIPC = OrderedDict([(k, perfIPC[k]) for k in benchOrder])
    perfSER = OrderedDict([k, perfIPC[k]]) for k in benchOrder])
    '''

    IPCarr = []
    SERarr = [] 
    #Average dynmaic reliability focused numbers
    COUNTERS = ['FullCounters75', 'MEAupHMAdown']
    print 'Average numbers for dynamic reliability migrations'
    for counter in COUNTERS:
        cntSERDDR, cntSERHBM, cntIPC = get_dict_field_data(BENCHS, pdata_rel, counter, ['ser_ddr', 'ser_hbm', 'ipc_mig'])
        cntSER = Counter(cntSERDDR) + Counter(cntSERHBM) 
        #print 'cntSER: '+str(cntSER)
        cntIPC, cntSER = normalize_and_return_ipc_ser(cntIPC, cntSER, perfIPC, perfSER, BENCHS)
        #print 'normalized cntSER: '+str(cntSER)
        #Numbers for the paper
        avgIPC = sum(cntIPC.values())/len(cntIPC.values())
        avgSER = sum(cntSER.values())/len(cntSER.values())
        print 'Average IPC increase: '+str(avgIPC)
        print 'Average SER increase: '+str(avgSER)
        
        IPCarr.append(avgIPC)
        SERarr.append(avgSER)
 
    print 'Average numbers for balanced migrations'
    for counter in COUNTERS:
        cntSERDDR, cntSERHBM, cntIPC = get_dict_field_data(BENCHS, pdata_bal, counter, ['ser_ddr', 'ser_hbm', 'ipc_mig'])
        cntSER = Counter(cntSERDDR) + Counter(cntSERHBM) 
        #print 'cntSER: '+str(cntSER)
        cntIPC, cntSER = normalize_and_return_ipc_ser(cntIPC, cntSER, perfIPC, perfSER, BENCHS)
        #print 'normalized cntSER: '+str(cntSER)
        #Numbers for the paper
        avgIPC = sum(cntIPC.values())/len(cntIPC.values())
        avgSER = sum(cntSER.values())/len(cntSER.values())
        print 'Average IPC increase: '+str(avgIPC)
        print 'Average SER increase: '+str(avgSER)
        
        IPCarr.append(avgIPC)
        SERarr.append(avgSER)
    
    ''' 
    #Plot the graph 
    fig, ax = pyplot.subplots()
    width = 0.6
    ind = np.arange(len(benchOrder))
    print 'ind: '+str(ind)

    #Left side hotness plot
    ax.axhline(y=1.0, color='0.3', ls='dashed', lw=1.0, label='IPC (Perf-focused migration)')
    ax.bar(ind, cntIPC.values(), width, color='0.8', label='IPC ('+STATICMAP_COUNTERS_LABEL[('none', args.counters)]+')')
   
    #xticks should be before twinx()
    #pyplot.xticks(ind+width/2, benchOrder, rotation=90)
    xlables = map((lambda x: BENCHLABELS[x]), benchOrder)
    ax.set_xticks(ind+width/2)
    ax.set_xticklabels(xlables, rotation=90)
 
    ax.set_ylabel('IPC (Normalized\nto perf-focused migration)')
    
    #Right side avf plot
    ax1 = ax.twinx()
    ax1.plot(ind+width/2, cntSER.values(), color='r', marker='.', linewidth=0, markersize=14, label='SER ('+STATICMAP_COUNTERS_LABEL[('none', args.counters)]+')')
    ax1.axhline(y=1.0, color='r', ls='dashed', lw=2.0, label='SER (Only DDRx)')
    ax1.set_ylabel('SER (Normalized)', color ='r')
   
    #BEAUTIFICATION
    x1,x2 = ax.get_xlim()
    ax.axvline(x=x2-2.8, color='0.3', ls='solid', lw=2.0)
    #pyplot.xlim([-0.5, x2-0.1])
    #y1,y2 = ax.get_ylim()
    #ax.set_ylim(0, 2.5)
    ax1.set_ylim(0, 600)
     
    #ax.legend(loc='upper left', fancybox=True)
    #ax1.legend(loc='upper right', fancybox=True)
    if(cnt != PERFORMANCE_COUNTER):
        ax.legend(loc='upper center', bbox_to_anchor=(0.15, 1.34),
                  fancybox=True, shadow=True)
    else:
        ax.legend(loc='upper center', bbox_to_anchor=(0.10, 1.30),
                  fancybox=True, shadow=True)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.80, 1.30),
                  fancybox=True, shadow=True)

    ax.tick_params(axis=u'both', which=u'both', length=0)
    pyplot.tight_layout() 
    save_n_crop('IPC_SER_SMAP_none'+'_CNT'+args.counters+'_ALLOC'+args.initalloc+'.pdf')
    '''


#PLOT INTERVAL SWEEP GRAPH
def GLplot_interval_sweep(ipc_lists, INTERVALS, BENCHS, args):
    print 'GLplot_interval_sweep'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-3,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [5, 3]
    } 
    rcParams.update(params)
    
    N = len(BENCHS)
    ind = np.arange(N)
    width = (1-0.05)/len(INTERVALS)
    i = 0 
    fig, ax = pyplot.subplots()
    for interval in INTERVALS:
        indi = ind + i*width
        interval_label = round(interval/(3.2*1000000000)*1000,2)
        interval_label = str(interval_label)+'ms'
        ax.bar(indi, ipc_lists[i], width, color=GRAD_PATTERNS[i], label=interval_label)
        i = i + 1
    ax.axhline(y=1.0, color='0.2', ls='dashed', lw=2.0) 
    
    #BEAUTIFICATION
    pyplot.ylabel('IPC (Normalized to Only DDR)')
    #x1,x2 = ax.get_xlim()
    ax.legend(loc='upper right', fancybox=True, shadow=True, ncol = 1)
    ax.set_xticks(ind + width*i/2)
    xlables = map((lambda x: BENCHLABELS[x]), BENCHS)
    ax.set_xticklabels(xlables)
    #pyplot.xlim([-0.5, x2-0.2])
    pyplot.grid(b=True, which='major', axis='y')
    ax.tick_params(axis=u'both', which=u'both', length=0)    
    pyplot.tight_layout() 
    save_n_crop('IntervalSweep.pdf')



#PLOT AVF ALL BENCHMARKS
def GLplot_avf_benchs_inc_order(pdata, BENCHS):
    print 'plot_avf_benchs_inc_order'
    #rc params
    params = {
        'axes.labelsize': LABELFONT,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-3,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [4.5, 3]
    } 
    rcParams.update(params)

    normAVF, ipc = get_dict_field_data(BENCHS, pdata, 'normal', ['avf_ddr', 'ipc'])
    normAVF = OrderedDict(sorted(normAVF.items(), key=itemgetter(1)))
    
    for k in normAVF.keys():
        normAVF[k] = normAVF[k]*100.0

    fig, ax = pyplot.subplots()
    pyplot.plot(normAVF.values(), marker='.', linewidth=1.2)
    
    #BEAUTIFICATION
    pyplot.ylabel('AVF(%)')
    xlables = map((lambda x: BENCHLABELS[x]), normAVF.keys())
    pyplot.xticks(np.arange(len(normAVF.keys())+0.2), xlables, rotation='vertical')
    #pyplot.xticks(arange(len(benchvec)+0.2), benchvec, rotation='vertical')
    x1,x2 = ax.get_xlim()
    pyplot.xlim([-0.5, x2-0.2])
    pyplot.grid(b=True, which='major', axis='y')
    ax.tick_params(axis=u'both', which=u'both', length=0)    
    pyplot.tight_layout() 
    save_n_crop('AllBenchOnlyDDRxAVF.pdf')

#PLOT STATIC IPC and SER
def plot_bars_static_placement(pdata, BENCHS, PAGEMAP):
 print 'plot_bars_static_placement(pdata)'
 #rc params
 params = {
   'axes.labelsize': LABELFONT,
   'axes.labelpad': 1,
   'text.fontsize': TEXTFONT-2,
   'legend.fontsize': LEGENDFONT-3,
   'xtick.labelsize': LABELFONT,
   'ytick.labelsize': LABELFONT,
   'xtick.major.pad': 2,
   'ytick.major.pad': 2,
   'figure.figsize': [9, 4]
 } 
 rcParams.update(params)
 ipcvec = {}
 servec = {}
 print pdata

 for bench in BENCHS:
     print '** Benchmark: '+bench
     print 'Normal IPC: '+str(pdata[bench, 'normal']['ipc'])
     print 'Normal SER: '+str(pdata[bench, 'normal']['ser_ddr'])
     normipc = (pdata[bench, 'normal']['ipc'])
     normser = (pdata[bench, 'normal']['ser_ddr'])
     for pagemap in PAGEMAP:
       print pagemap
       ipc = pdata[bench, pagemap]['ipc']/normipc
       ser = (pdata[bench, pagemap]['ser_hbm'] + pdata[bench, pagemap]['ser_ddr'])/normser
       if pagemap not in ipcvec:
           ipcvec[pagemap]=[]
           servec[pagemap]=[]
       ipcvec[pagemap].append(ipc)
       servec[pagemap].append(ser)

     print 'IPCs: '+str(ipcvec)
     print 'SERs: '+str(servec)

     
 #Start plotting
 N = len(BENCHS)
 ind = np.arange(N)
 width = (1-0.2)/len(PAGEMAP)

 fig, ax = pyplot.subplots()
 ax1 = ax.twinx()
  
 #plot IPC
 reacts={}
 i = 0
 for pm in PAGEMAP:
     indi = ind + i*width
     reacts[pm]=ax.bar(indi, ipcvec[pm], width, color=COLSPM[pm], label=pm)
     i=i+1
 
 #plot SER
 i = 0
 for pm in PAGEMAP:
     indi = ind + i*width+0.05
     ax1.scatter(indi, servec[pm], c='red')
     i=i+1
      
 #x- and y-axis labels and legends
 #x1, x2 = ax.get_xlim()
 #pyplot.xlim([x1-0.2, x2-0.5])
 ax.set_ylabel('IPC Normalized')
 ax1.set_ylabel('SER Normalized')
 ax.set_xticks(ind+0.3)
 xlabels = BENCHS
 ax.set_xticklabels(xlabels, rotation=90)
 ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=9, mode="expand", borderaxespad=0.)
 #ax.legend(loc='upper center', ncol=9)
 ax.axis('tight')
 ax1.axis('tight')
 
 pyplot.grid(b=True, which='major', axis='y')
 #ax.tick_params(axis=u'both', which=u'both', length=0)    
 pyplot.tight_layout() 
 save_n_crop('pvf_static_placement.pdf')

#PLOT STATIC IPC and SER
def plot_ipc_ser_bench_static_placement(pdata, BENCHS, PAGEMAP):
 print 'plot_bars_static_placement(pdata)'
 #rc params
 params = {
   'axes.labelsize': LABELFONT,
   'axes.labelpad': 1,
   'text.fontsize': TEXTFONT-2,
   'legend.fontsize': LEGENDFONT-3,
   'xtick.labelsize': LABELFONT,
   'ytick.labelsize': LABELFONT,
   'xtick.major.pad': 2,
   'ytick.major.pad': 2,
   'figure.figsize': [5, 4]
 } 
 rcParams.update(params)
 bench = BENCHS[0]

 #Get data dictionary
 normSERDDR, normIPC, normAVF = get_dict_field_data(BENCHS, pdata, 'normal', ['ser_ddr', 'ipc_mig', 'avf_ddr'])
 #In loop extract all static IPC SER numbers
 pmSER_arr = [normSERDDR[bench]]
 pmIPC_arr = [normIPC[bench]]

 for PM in PAGEMAP:
    pmSERDDR, pmSERHBM, pmIPC = get_dict_field_data(BENCHS, pdata, PM, ['ser_ddr', 'ser_hbm', 'ipc_mig'])
    pmSER = Counter(pmSERDDR) + Counter(pmSERHBM)
    pmSER_arr.append(pmSER[bench])
    pmIPC_arr.append(pmIPC[bench])
 
 print pmSER_arr
 print pmIPC_arr       
 #Plot the graph    
 fig, ax = pyplot.subplots()
 width = 0.5
 ind = np.arange(len(pmIPC_arr))
 print 'ind: '+str(ind)
    
 #Left side hotness plot
 #ax.axhline(y=1.0, color='0.3', ls='dashed', lw=1.0, label='IPC (Only DDR)')
 ax.bar(ind, pmIPC_arr, width, color='0.8') 
 #xlabels
 xlabels = ['OnlyDDR'] + PAGEMAP
 ax.set_xticks(ind+width/2)
 ax.set_xticklabels(xlabels, rotation=90)
 ax.set_ylabel('IPC')
    
 #Right side avf plot
 ax1 = ax.twinx()
 ax1.plot(ind+width/2, pmSER_arr, color='r', marker='.', linewidth=0, markersize=12)   
 ax1.set_ylabel('SER', color ='r')
   
 #BEAUTIFICATION
 x1,x2 = ax.get_xlim()
 #ax.axvline(x=x2-2.8, color='0.3', ls='solid', lw=2.0)
 #pyplot.xlim([-0.5, x2-0.1])
 #y1,y2 = ax.get_ylim()
 #ax.set_ylim(0, 8)
 #ax1.set_ylim(0, 600)
     
 #ax.legend(loc='upper left', fancybox=True)
 #ax1.legend(loc='upper right', fancybox=True)
 #ax.legend(loc='upper center', bbox_to_anchor=(0.3, 1.35), fancybox=True, shadow=True)
 #ax1.legend(loc='upper center', bbox_to_anchor=(0.8, 1.25), fancybox=True, shadow=True)

 #ax.tick_params(axis=u'both', which=u'both', length=0)
 pyplot.tight_layout() 
 save_n_crop(bench+'all_static_placements.pdf')


#Structure Annotated
def GLplot_program_structs_annotated(pdata, pinType, BENCHS):
    print '-->>> GLplot_ipc_ser_for_pm_hma_normalize_to_perf_focused'
    #rc params
    params = {
        'axes.labelsize': LABELFONT-3,
        'axes.labelpad': 1,
        'text.fontsize': TEXTFONT-2,
        'legend.fontsize': LEGENDFONT-4,
        'xtick.labelsize': LABELFONT-2,
        'ytick.labelsize': LABELFONT-2,
        'xtick.major.pad': 2,
        'ytick.major.pad': 2,
        'figure.figsize': [5, 3]
    } 
    rcParams.update(params)

    #Get data dictionary. Get total and annotated structures 
    tStructs, aStructs = get_dict_field_data(BENCHS, pdata, pinType, ['total_structs', 'annotated_structs'])
    
    #Get benchOrder in increasing AVF or MPKI*
    #mpki = OrderedDict(sorted(MPKI.items(), key=itemgetter(1), reverse=True))
    #benchOrder = mpki.keys()
    nStructs = OrderedDict(sorted(aStructs.items(), key=itemgetter(1)))
    benchOrder = nStructs.keys()
    benchOrder = [x for x in benchOrder if x in BENCHS] #Filtering benchmark not used for the paper
    print 'Plotting benchmarks: '+str(benchOrder)  
    tStructs = OrderedDict([(k, tStructs[k]) for k in benchOrder])
    aStructs = OrderedDict([(k, aStructs[k]) for k in benchOrder]) 
    
    #Debug Prints
    print tStructs
    print aStructs
     
    #Plot the graph    
    fig, ax = pyplot.subplots()
    width = 0.6
    ind = np.arange(len(benchOrder))
    print 'ind: '+str(ind)
   
    #Left side hotness plot
    reacts = ax.bar(ind, aStructs.values(), width, color='0.8') 
    xlables = map((lambda x: BENCHLABELS[x]), benchOrder)
    ax.set_xticks(ind+width/2)
    ax.set_xticklabels(xlables, rotation=90)

    ax.set_ylabel('# annotated structures')
    
    #BEAUTIFICATION
    x1,x2 = ax.get_xlim()
    pyplot.xlim([-0.5, x2-0.1])
    #y1,y2 = ax.get_ylim(
    ylim = 10
    ax.set_ylim(0, ylim)
    autolabel(reacts, ax, '%d', 'horizontal', ylim)
    ax.yaxis.grid(b=True)
     
    ax.tick_params(axis=u'both', which=u'both', length=0)
    pyplot.tight_layout() 
    save_n_crop('AnnotatedStructures'+pinType+'.pdf')
