import os, commands, sys, socket
import operator, time, re, shutil
import pickle, argparse, datetime
import numpy as np
import logging
from logging import debug
from random import shuffle
from string import Template
from collections import OrderedDict, Counter
from operator import itemgetter
import itertools


#Directories
TRACE_PATH = os.environ['TRACE_PATH']
RELIABLE_HMA = os.environ['RELIABLE_HMA']

##DERIVED
AVF_RAMULATOR=RELIABLE_HMA+'/avf_ramulator'
AVF_OUTPUT=AVF_RAMULATOR+'/avf_output'

#M1 AVF RAMULATOR PATH
MAVF_RAMULATOR=RELIABLE_HMA+'/m1_avf_ramulator'
MAVF_OUTPUT=MAVF_RAMULATOR+'/avf_output'

#M2 AVF RAMULATOR PATH
M2AVF_RAMULATOR=RELIABLE_HMA+'/m2_avf_ramulator'
M2AVF_OUTPUT=M2AVF_RAMULATOR+'/avf_output'
STATIC_PAGE_MAPS=M2AVF_RAMULATOR+'/static_page_maps'
PLOT_OUTPUT_FOLDER=RELIABLE_HMA+'/graphs'

#BENCHSMARK LISTS
BENCHMARKLIST=['astar', 'lbm',  'mix2', 'mix7', 'test1GB', 'testNPage', 'testMig', 'bwaves', 'lulesh',  'mix3',  'mix8', 'test1KB', 'xalancbmk', 'cactusADM128MB', 'mcf', 'mix4',  'omnetpp', 'test1MB', 'xsbench', 'cactusADM', 'milc', 'mix5', 'test100Lines',  'test1Page',  'zeusmp', 'GemsFDTD', 'mix1',  'mix6',  'test10Lines',  'test64MB']
HGBENCHLIST=['astar', 'lbm',  'bwaves', 'lulesh',  'xalancbmk', 'omnetpp', 'xsbench', 'cactusADM', 'milc', 'zeusmp', 'GemsFDTD']
MIXBENCHLIST=['mix1', 'mix2', 'mix3',  'mix4', 'mix5',  'mix6', 'mix7',  'mix8']

#BENCHMARKS MPKI
MPKI = {'mcf':65, 'lbm':44.2, 'mix5':42.2, 'mix1':29.3, 'milc':23.1, 'mix2':20.4, 'mix6':19.6, 'omnetpp':18.9, 'mix4':18.2, 'astar':16.8, 'xsbench':11.03, 'mix3':10.9, 'lulesh':10.7, 'mix8':10.3, 'GemsFDTD':9.5, 'zeusmp':8.1, 'bwaves':6.9, 'mix7':4.8, 'xalancbmk':4.5, 'cactusADM':3.7} 

#STATIC_PAGEMAPS LISTS
#PAGEMAPRANDOM=['Random2','Random4','Random6','Random8','Random10','Random12','Random14','Random16','Random18','Random20','Random22','Random24','Random26','Random28','Random30','Random32','Random34','Random36','Random38','Random40','Random42','Random44','Random46','Random48','Random50','Random52','Random54','Random56','Random58','Random60','Random62','Random64','Random66','Random68','Random70','Random72','Random74','Random76','Random78','Random80','Random82','Random84','Random86','Random88','Random90','Random92','Random94','Random96','Random98','Random100']
PAGEMAPRANDOM=['Random4','Random8','Random10','Random14','Random16','Random20','Random24','Random26','Random30','Random34','Random40','Random44','Random48','Random50','Random54','Random56','Random58','Random60']
#PAGEMAPRANDOM=['Random50','Random54']
#PAGEMAPRANDOM=['Random10','Random20','Random30','Random40','Random50','Random60','Random70','Random80','Random90','Random100']
PAGEMAPHOTFULL = ['TopHot5', 'TopHot15', 'TopHot20', 'TopHot25', 'TopHot30', 'TopHot35', 'TopHot40', 'TopHot45', 'TopHot50', 'TopHot55', 'TopHot60', 'TopHot65', 'TopHot70', 'TopHot75', 'TopHot80', 'TopHot85', 'TopHot90', 'TopHot95', 'TopHot']
PAGEMAPFULL = ['TopHot', 'LeastHot', 'TopAVF', 'LeastAVF', 'TopWr', 'TopRd', 'TopWrRdRatio', 'TopWrWrRdRatio', 'TopHotLowAVF', 'none']
PAGEMAPTEMP = ['TopHotLowAVF']
PAGEMAPH1 = ['TopHot', 'LeastHot', 'TopAVF', 'LeastAVF']
PAGEMAPH2 = ['TopWr', 'TopWrRdRatio', 'TopWrWrRdRatio', 'none'] #Add top 'TopRd'

#PAGEMAPOPTIONS
PMOPTIONS = {'pmfull':PAGEMAPFULL, 'pmh1':PAGEMAPH1, 'pmh2':PAGEMAPH2, 'pmhot':PAGEMAPHOTFULL, 'pmrand':PAGEMAPRANDOM, 'pmtemp':PAGEMAPTEMP}

#COUNTERS/MIGRATION LISTS
#COUNTERFULL = ['MEAWr2Rd1'] #HMA migration, HMA migration WR/RD ratio, HMA migration WR^2/RD ratio. 
COUNTERFULL = ['HMA', 'HMAWrRd', 'HMAWrWrRd', 'MEA', 'MEAWr2Rd1', 'HMAUnAlloc', 'MEAupHMAdown'] #HMA migration, HMA migration WR/RD ratio, HMA migration WR^2/RD ratio. 
COUNTERTEMP = ['HMAUnAlloc', 'MEAupHMAdown'] #HMA migration, HMA migration WR/RD ratio, HMA migration WR^2/RD ratio. 
#COUNTEROPTIONS/MIGOPTIONS select one migration based on input
CNTOPTIONS = {'cntfull':COUNTERFULL, 'cnttemp':COUNTERTEMP}

#PERFORMANCE AND RELIABILITY COUNTERS USED FOR THE PAPER
PERFORMANCE_COUNTER = 'HMAUnAllocThd1'
RELIABILITY_COUNTER = 'FullCounters'

#INTERVAL LIST
INTERVALFULL = [100000000, 50000000, 10000000, 1000000]
QINTERVALFULL = [3200000, 32000000, 320000000, 3200000000, 6400000000]
#INTERVALFULL = [100000000, 50000000]

#INTERVAL OPTIONS
INTOPTIONS = {'intfull':INTERVALFULL}

#INTERVAL MAP (INTERVAL_MAP[counter] = interval_length)
LEGACY_INTERVAL = 10000000
#BEST MIGRATION INTERVAL FOR THE BELOW COUNTERS
MEA_INTERVAL = 320000
HMA_INTERVAL = 320000000
FULLCOUNTER_INTERVAL = 320000000
#CROSS_INTERVAL = 320000
CROSS_INTERVAL = 160000

#INTERVAL_MAP = {'MEA':MEA_INTERVAL ,'HMA':HMA_INTERVAL, 'MEAupHMAdown':CROSS_INTERVAL}
INTERVAL_MAP = {'MEA':LEGACY_INTERVAL, 'MEAWr2Rd1':LEGACY_INTERVAL, 'MEAUnAlloc':CROSS_INTERVAL, 'HMA':LEGACY_INTERVAL, 'HMAWrRd':HMA_INTERVAL, 'HMAWrWrRd':HMA_INTERVAL, 'HMAUnAlloc':HMA_INTERVAL, 'HMAUnAllocThd1':HMA_INTERVAL, 'HMAUnAllocThd2':HMA_INTERVAL, 'MEAupHMAdown':CROSS_INTERVAL, 'FullCounters':FULLCOUNTER_INTERVAL, 'FullCounters75':FULLCOUNTER_INTERVAL}

#NUM MEA COUNTERS
LEGACY_MEA_COUNTERS = 128
MEA_COUNTERS_MAP={'MEAupHMAdown':64, 'MEA':LEGACY_MEA_COUNTERS, 'MEAUnAlloc':64, 'HMA':LEGACY_MEA_COUNTERS, 'HMAUnAlloc': LEGACY_MEA_COUNTERS, 'HMAUnAllocThd1': LEGACY_MEA_COUNTERS, 'HMAUnAllocThd2':LEGACY_MEA_COUNTERS, 'FullCounters':LEGACY_MEA_COUNTERS, 'FullCounters75':LEGACY_MEA_COUNTERS}

#FIT VALUES CHOOSE CAREFULLY
FITHBM = 0.01707
FITDDR = 0.000323

#MIGRATION OVERHEAD
#PAGE_MIG_CYCLES = 500
PAGE_MIG_CYCLES = 300

#FOR PCBASED PAGEMAP GENERATIONS IN CAPACITY RATIO
M1_M2_NUM = 100
M1_NUM = 11

#REGEX std.out PATTERNS
interval_pattern=re.compile('Interval length avf_interval (\d+)')
n_interval_pattern=re.compile('Number of checkpoints: (\d+)')
ser_ddr_pattern=re.compile('SER Week 0 \(DDR3\) = (\d+.\d+)')
ser_hbm_pattern=re.compile('SER Week 0 \(HBM\) = (\d+.\d+)')
hitrate_pattern=re.compile('HIT RATE = (\d+.\d+)')
retired_inst_pattern=re.compile('Core\[0\] retired = (\d+)')
clk_pattern=re.compile(', clk = (\d+)')
ipc_pattern=re.compile('Core\[0\] IPC = (\d+.\d+)')
total_avf_hbm_pattern=re.compile('Total AVF of HBM Memory: ([+-]?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?)')
total_avf_ddr_pattern=re.compile('Total AVF of DDR3 Memory: ([+-]?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?)')
no_pages_pattern=re.compile('Number of pages: (\d+)')
pages_m1_pattern=re.compile('Num Pages Mapped to M1 = \((\d+)\)')
pages_m2_pattern=re.compile('Num Pages Mapped to M2 = \((\d+)\)')
pages_m1_m2_pattern=re.compile('Num Pages Mapped to M1\+M2 = \((\d+)\)')
total_pages_hbm_pattern=re.compile('Number of 4KB Pages\(HBM\) = (\d+)')
total_pages_ddr_pattern=re.compile('Number of 4KB Pages\(DDR3\) = (\d+)')
num_mea_counters_pattern=re.compile('options\[num_mea_counters\]: (\d+)')

#REGEX ramulator.out PATTERNS
n_mig_pattern=re.compile('(\d+)                                      # The total number of migrations performed.')
num_ddr_incoming_req_pattern=re.compile('(\d+)                                      # Number of incoming requests to DDR3')
num_ddr_read_req_pattern=re.compile('(\d+)                                      # Number of incoming read requests to DDR3 per core')
num_ddr_write_req_pattern=re.compile('(\d+)                                      # Number of incoming write requests to DDR3 per core')
num_hbm_incoming_req_pattern=re.compile('(\d+)                                      # Number of incoming requests to HBM')
num_hbm_read_req_pattern=re.compile('(\d+)                                      # Number of incoming read requests to HBM per core')
num_hbm_write_req_pattern=re.compile('(\d+)                                      # Number of incoming write requests to HBM per core')

#REGEX generation info PATTERS
gen_num_total_structs_pattern = re.compile('Total number of program counters issuing memory instructions: (\d+)')
gen_num_annotated_structs_pattern = re.compile('Number of annotated program counters: (\d+)')
gen_num_total_pages_pattern = re.compile('Total number of pages: (\d+)')
gen_m1_cap_pattern = re.compile('m1_page_cap: (\d+)')
gen_pinned_pages_pattern = re.compile('Pinned pages through: (\d+)')

#RAMULATOR CALLING TEMPLATES
RAMULATOR_NORMAL = Template(""" $RAMULATORBIN normal $CONFIGFILE --mode=cpu --stats $STATFILE --avfout $AVFOUTDIR $CPUTRACEFILE  --avf $AVF --avf_checkpoint $AVFCHECKPOINT --avf_interval $AVFINTERVAL --migration $MIGRATION --counter_type $COUNTERTYPE --smart_wr $SMARTWR --num_mea_counters $MEACOUNTERS --init_alloc $INITALLOC""")
RAMULATOR_HYBRID = Template(""" $RAMULATORBIN hybrid $CONFIG1 $CONFIG2 --mode=cpu --stats $STATFILE --avfout $AVFOUTDIR $CPUTRACEFILE  --avf $AVF --avf_checkpoint $AVFCHECKPOINT --avf_interval $AVFINTERVAL --migration $MIGRATION --counter_type $COUNTERTYPE --smart_wr $SMARTWR --num_mea_counters $MEACOUNTERS --init_alloc $INITALLOC""")
RAMULATOR_HYBRID_STATIC_PAGE_MAP = Template(""" $RAMULATORBIN hybrid $CONFIG1 $CONFIG2 --mode=cpu --stats $STATFILE --avfout $AVFOUTDIR --staticmap $PAGEMAP $CPUTRACEFILE  --avf $AVF --avf_checkpoint $AVFCHECKPOINT --avf_interval $AVFINTERVAL --migration $MIGRATION --counter_type $COUNTERTYPE --smart_wr $SMARTWR --num_mea_counters $MEACOUNTERS --init_alloc $INITALLOC""")

#MEMORY CONFIGS DICTIONARIES
MEMCONFIG={'HBM1GB':'configs/HBM-1GB-config.cfg',
           'HBM2GB':'configs/HBM-2GB-config.cfg',
           'HBM4GB':'configs/HBM-4GB-config.cfg',
           'HBM16GB':'configs/HBM-16GB-config.cfg', 
           'HBM1GBavfoff':'configs/HBM-1GB-avfoff-config.cfg', 
           'HBM2GBavfoff':'configs/HBM-2GB-avfoff-config.cfg', 
           'HBM4GBavfoff':'configs/HBM-4GB-avfoff-config.cfg', 
           'HBM16GBavfoff':'configs/HBM-16GB-avfoff-config.cfg', 
           'DDR16GB':'configs/DDR3-16GB-x4-config.cfg',
           'DDR16GBtest':'configs/DDR3-16GB-x4-test.cfg',
           'DDR16GBavfoff':'configs/DDR3-16GB-x4-avfoff-config.cfg'}

#Python global datastrcuture for avf 3d plots
g_avfdata={}
g_chks=[]
g_pages=set([])
g_x=[]
g_y=[]
g_z=[]

#FUNCTIONS
def get_pattern_from_output(output, pattern):
	match = re.search(pattern, output)
	if match:
		return match.group(1)
	else:
		return False

def get_avf_data(avf_data, i1, i2):
    data = {}
    for i in range(i1+1,i2):
        print avf_data[i]
        line = avf_data[i].split(':')
        data[int(line[0])] = (float(line[1]), float(line[2]))
    return data


def get_mem_avf_data(avf_data, i1, i2):
    data = {}
    for i in range(i1+1,i2):
        #print avf_data[i]
        line = avf_data[i].split('|')
        data[int(line[0])] = (int(line[1]),int(line[2]),int(line[3]),int(line[4]),int(line[5]),float(line[6]), float(line[7]))
    return data

#RUN TRACE FUNCTIONS
def create_avf_outputdir_ret_cmd(bench, mode, mem1, mem2, pagemap, args, createfolder):
    if not os.path.exists(M2AVF_OUTPUT):
        os.makedirs(M2AVF_OUTPUT)

    #extract cagrs and place them in python variables
    avf = args.avf
    interval = args.interval
    mig = args.mig.lower()
    counters = args.counters
    smartwr = args.smartwr
    meacounters = args.meacounters
    initalloc = args.initalloc
    
    #create args mode folder if the modedir not present
    if mode=='normal':
        if mem1 == 'none':
            mem1 = 'DDR16GB'
        interval = LEGACY_INTERVAL
        meacounters = 128 
        expparms = mode+'_'+mem1+'_'+pagemap.upper()+'_avf'+avf.upper()+'_int'+str(interval)+'_mig'+mig.upper()+'_cnt'+counters.upper()+'_meacnt'+str(meacounters)+'_initallocNONE'
    else:
        if mem1 == 'none':
            mem1 = 'HBM1GB'
        if mem2 == 'none':
            mem2 = 'DDR16GB'
        expparms = mode+'_'+mem1+'_'+mem2+'_'+pagemap+'_avf'+avf.upper()+'_int'+str(interval)+'_mig'+mig.upper()+'_cnt'+counters.upper()+'_meacnt'+str(meacounters)+'_initalloc'+initalloc.upper()

    #expdir folder that holds the experiment output.
    expdir = M2AVF_OUTPUT+'/'+expparms+'/'+bench
    
    #check benchmark and memory configs are valid
    if bench not in BENCHMARKLIST:
        print bench+" not found in "+str(BENCHMARKLIST)
        sys.exit(0)
    if mem1 not in MEMCONFIG:
        print mem1+" not found in "+str(MEMCONFIG.keys())
        sys.exit(0)
    if mode == 'hybrid' and mem2 not in MEMCONFIG:
        print mem2+" not found in "+str(MEMCONFIG.keys())
        sys.exit(0)

    #ramulator binary, statfiles, and trace file paths
    binpath = M2AVF_RAMULATOR+'/ramulator'
    if not os.path.exists(binpath):
        print 'RAMULATOR BINARY NOT FOUND'
        sys.exit(0)
    statsfile = expdir+'/'+'ramulator.stats'
    trace = TRACE_PATH+'/'+bench+'_cpu_trace/cpu_trace.trc'

    #create command string
    cmd='none'
    if mode=='normal':
        cmd = RAMULATOR_NORMAL.safe_substitute(RAMULATORBIN=binpath, CONFIGFILE=M2AVF_RAMULATOR+'/'+MEMCONFIG[mem1], STATFILE=statsfile, AVFOUTDIR=expdir, CPUTRACEFILE=trace, AVF=avf, AVFCHECKPOINT=avf, AVFINTERVAL=str(interval), MIGRATION=mig, COUNTERTYPE=counters, SMARTWR=smartwr, MEACOUNTERS=meacounters, INITALLOC=initalloc)
    elif mode=='hybrid' and pagemap=='none':
        cmd = RAMULATOR_HYBRID.safe_substitute(RAMULATORBIN=binpath, CONFIG1=M2AVF_RAMULATOR+'/'+MEMCONFIG[mem1], CONFIG2=M2AVF_RAMULATOR+'/'+MEMCONFIG[mem2], STATFILE=statsfile, AVFOUTDIR=expdir, CPUTRACEFILE=trace, AVF=avf, AVFCHECKPOINT=avf, AVFINTERVAL=str(interval), MIGRATION=mig, COUNTERTYPE=counters, SMARTWR=smartwr, MEACOUNTERS=meacounters, INITALLOC=initalloc)
    elif mode=='hybrid' and pagemap!='none':
        staticpagemap=STATIC_PAGE_MAPS+'/'+bench+'_DDR3_'+pagemap+'.csv'
        if not os.path.exists(staticpagemap):
            print 'NOT FOUND Static Map = '+str(staticpagemap)
            staticpagemap = 'STATIC PAGE MAP NOT FOUND'
            sys.exit()
        cmd = RAMULATOR_HYBRID_STATIC_PAGE_MAP.safe_substitute(RAMULATORBIN=binpath, CONFIG1=M2AVF_RAMULATOR+'/'+MEMCONFIG[mem1], CONFIG2=M2AVF_RAMULATOR+'/'+MEMCONFIG[mem2],  STATFILE=statsfile, AVFOUTDIR=expdir, PAGEMAP=staticpagemap, CPUTRACEFILE=trace, AVF=avf, AVFCHECKPOINT=avf, AVFINTERVAL=str(interval), MIGRATION='off', COUNTERTYPE='none', SMARTWR=smartwr, MEACOUNTERS=meacounters, INITALLOC=initalloc)
    if cmd == 'none':
        print 'cmd is : ' + cmd
        print 'cmd not set properly in function create_avf_outputdir_ret_cmd'
        sys.exit()
    
    #create directory for the benchmark and expdir inside it. If they don't exist and create is true. 
    if createfolder:
        print 'Starting new simulation.'
        modedir = M2AVF_OUTPUT+'/'+expparms
        if not os.path.exists(modedir):
            print 'New experiment mode.'
            os.makedirs(modedir)
        if not os.path.exists(expdir):
            os.makedirs(expdir)
    else:
        print 'Reading completed simulation results.'
        if not os.path.exists(expdir):
            print str(expdir)+' DOES NOT EXIST.'
            sys.exit(0)
    return cmd, expdir

def launch_single_thread(cmd_n_dir):
    os.chdir(M2AVF_RAMULATOR)
    cmd = cmd_n_dir[0]
    expdir = cmd_n_dir[1]
    
    if 'STATIC PAGE MAP NOT FOUND' in cmd:
        return '!!!!!!!!!!!!! FAILED !!!!!!!!!!!!!!'+cmd+'\n'

    print M2AVF_RAMULATOR + '$' + cmd
    
    run = raw_input("Run single ramulator simulation (y/n): ")
    if run != 'y':
        sys.exit(0)


    status, output = commands.getstatusoutput(cmd)
    print '** launch_single_thread completed **'
    print output

    #Write the stdout in a file
    stdoutfile = expdir+'/'+'std.out'
    f = open(stdoutfile, 'w')
    f.write('Command: '+cmd)
    f.write(output)
    f.close()
    return 'DONE '+cmd+'\n'


def get_output_data_for_norm_exp(expdir):
    #stdout
    stdoutfile = expdir+'/std.out'
    f = open(stdoutfile, 'r')
    out = f.read()
    f.close()
    
    #ramulator stats
    ramstatfile = expdir+'/ramulator.stats'
    fr = open(ramstatfile, 'r')
    stats = fr.read()
    fr.close()
    
    #dictionary data strcuture that holds all the data
    data={}

    #use patterns to get stdout data
    data['interval'] = int(get_pattern_from_output(out, interval_pattern))
    data['n_interval'] = int(get_pattern_from_output(out, n_interval_pattern))
    data['ipc'] = round(float(get_pattern_from_output(out, ipc_pattern)),4)
    data['total_avf_ddr'] = float(get_pattern_from_output(out, total_avf_ddr_pattern))
    data['ser_ddr_std'] = round(float(get_pattern_from_output(out, ser_ddr_pattern)),2)
    data['clk'] = int(get_pattern_from_output(out, clk_pattern))
    data['retired_inst'] = int(get_pattern_from_output(out, retired_inst_pattern))
    data['pages_m1'] = int(get_pattern_from_output(out, no_pages_pattern))
    data['total_pages_m1'] = int(get_pattern_from_output(out, total_pages_ddr_pattern))
    
    #use pattern to get ramulator stat data
    data['n_ddr'] = int(get_pattern_from_output(stats, num_ddr_incoming_req_pattern))
    data['n_rd_ddr'] = int(get_pattern_from_output(stats, num_ddr_read_req_pattern))
    data['n_wr_ddr'] = int(get_pattern_from_output(stats, num_ddr_write_req_pattern))

    #Compute AVF and SER
    data['avf_ddr'] = data['total_avf_ddr']/data['total_pages_m1']
    data['ser_ddr'] = FITDDR * data['avf_ddr']

    #MIGRATION overhead for the normal case
    data['ipc_mig'] = data['ipc']

    print '******* Expdir: '+str(expdir)
    print 'Output Numbers (normal mode): '+str(data)
    return data

def get_output_data_for_hyb_exp(expdir):
    #stdout
    stdoutfile = expdir+'/std.out'
    f = open(stdoutfile, 'r')
    out = f.read()
    f.close()

    #ramulator stats
    ramstatfile = expdir+'/ramulator.stats'
    fr = open(ramstatfile, 'r')
    stats = fr.read()
    fr.close()

    data={}
    
    #print out
    data['interval'] = int(get_pattern_from_output(out, interval_pattern))
    data['n_interval'] = int(get_pattern_from_output(out, n_interval_pattern))
    data['ipc'] = round(float(get_pattern_from_output(out, ipc_pattern)), 2)
    data['total_avf_hbm'] = float(get_pattern_from_output(out, total_avf_hbm_pattern))
    data['total_avf_ddr'] = float(get_pattern_from_output(out, total_avf_ddr_pattern))
    data['ser_hbm_std'] = round(float(get_pattern_from_output(out, ser_hbm_pattern)),2)
    data['ser_ddr_std'] = round(float(get_pattern_from_output(out, ser_ddr_pattern)),2)
    data['clk'] = int(get_pattern_from_output(out, clk_pattern))
    data['retired_inst'] = int(get_pattern_from_output(out, retired_inst_pattern))
    data['pages_m1'] = int(get_pattern_from_output(out, pages_m1_pattern))
    data['pages_m2'] = int(get_pattern_from_output(out, pages_m2_pattern))
    data['pages_m1_m2'] = int(get_pattern_from_output(out, pages_m1_m2_pattern))
    data['total_pages_m1'] = int(get_pattern_from_output(out, total_pages_hbm_pattern))
    data['total_pages_m2'] = int(get_pattern_from_output(out, total_pages_ddr_pattern))
 
    #use pattern to get ramulator stat data
    data['n_mig'] = int(get_pattern_from_output(stats, n_mig_pattern))

    data['n_ddr'] = int(get_pattern_from_output(stats, num_ddr_incoming_req_pattern))
    data['n_rd_ddr'] = int(get_pattern_from_output(stats, num_ddr_read_req_pattern))
    data['n_wr_ddr'] = int(get_pattern_from_output(stats, num_ddr_write_req_pattern))

    data['n_hbm'] = int(get_pattern_from_output(stats, num_hbm_incoming_req_pattern))
    data['n_rd_hbm'] = int(get_pattern_from_output(stats, num_hbm_read_req_pattern))
    data['n_wr_hbm'] = int(get_pattern_from_output(stats, num_hbm_write_req_pattern))

    #Compute AVF_HBM AVF_DDR and SER_HBM SER_DDR
    data['avf_hbm'] = data['total_avf_hbm']/data['total_pages_m1']
    data['avf_ddr'] = data['total_avf_ddr']/data['total_pages_m2']

    data['ser_hbm'] = FITHBM * data['avf_hbm']
    data['ser_ddr'] = FITDDR * data['avf_ddr']

    #MIGRATION OVERHEAD
    clk_mig = data['clk'] + data['n_mig'] * PAGE_MIG_CYCLES
    data['ipc_mig'] = round(float(data['retired_inst'])/float(clk_mig),2)

    print '******* Expdir: '+str(expdir)
    print 'Output Numbers (hybrid mode): '+str(data)
    return data

#Functions to write data to the csv files
def write_normal_data_to_csv_file(f, data, args):
    f.write('Only DDR 16GB'+','+str(data['interval'])+','+str(data['n_interval'])+','+str(0)\
        +','+str(data['ipc'])+','+str(data['ipc_mig'])+','+str(0+data['avf_ddr'])+','+str(0+data['ser_ddr'])\
        +','+str(1.00)+','+str(1.00)\
        +','+str(0)+','+str(data['avf_ddr'])+','+str(0)+','+str(data['ser_ddr'])\
        +','+str(0)+','+str(0)+','+str(0)\
        +','+str(data['n_ddr'])+','+str(data['n_rd_ddr'])+','+str(data['n_wr_ddr'])\
        +','+str(0.0)+','+str(0 + data['n_ddr'])+','+str(data['retired_inst'])+','+str(data['clk'])\
        +','+str(data['pages_m1'])+','+str(0)+','+str(data['pages_m1'])\
        +','+str(data['total_pages_m1'])+','+str(0)\
        +','+str(0)+','+str(data['total_avf_ddr'])+','+str(0 + data['total_avf_ddr'])\
        +','+str(0)+','+str(data['ser_ddr_std'])+','+str(0 + data['ser_ddr_std'])+'\n')

def write_hybrid_data_to_csv_file(f, data, args, data_n):
    #normalized ipc and ser
    ipc_n = round(data['ipc_mig']/data_n['ipc_mig'], 2)
    ser_n = round((data['ser_hbm']+data['ser_ddr'])/(data_n['ser_ddr']), 2)
     
    f.write(args.placetype+','+str(data['interval'])+','+str(data['n_interval'])+','+str(data['n_mig'])\
        +','+str(data['ipc'])+','+str(data['ipc_mig'])+','+str(data['avf_hbm'] + data['avf_ddr'])+','+str(data['ser_hbm']+data['ser_ddr'])\
        +','+str(ipc_n)+','+str(ser_n)\
        +','+str(data['avf_hbm'])+','+str(data['avf_ddr'])+','+str(data['ser_hbm'])+','+str(data['ser_ddr'])\
        +','+str(data['n_hbm'])+','+str(data['n_rd_hbm'])+','+str(data['n_wr_hbm'])\
        +','+str(data['n_ddr'])+','+str(data['n_rd_ddr'])+','+str(data['n_wr_ddr'])\
        +','+str(float(data['n_hbm'])/float(data['n_hbm']+data['n_ddr']))+','+str(data['n_hbm']+data['n_ddr'])+','+str(data['retired_inst'])+','+str(data['clk'])\
        +','+str(data['pages_m1'])+','+str(data['pages_m2'])+','+str(data['pages_m1_m2'])
        +','+str(data['total_pages_m1'])+','+str(data['total_pages_m2'])\
        +','+str(data['total_avf_hbm'])+','+str(data['total_avf_ddr'])+','+str(data['total_avf_hbm'] + data['total_avf_ddr'])\
        +','+str(data['ser_hbm_std'])+','+str(data['ser_ddr_std'])+','+str(data['ser_hbm_std']+data['ser_ddr_std'])+'\n')
