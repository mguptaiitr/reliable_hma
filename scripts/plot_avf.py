################################## COMMAND HELP #############################
######### AVF variation of all benchmarks ##########
# python plot_avf.py -avfallbenchs
####### STATIC PAGEMAP IPC and SER for different static pagemap ######
# python plot_avf.py -perfipcser 
# python plot_avf.py -pmipcser -pagemap LeastAVF
# python plot_avf.py -pmipcser -pagemap TopHotLowAVF
# python plot_avf.py -pmipcser -pagemap TopWrRdRatio
# python plot_avf.py -pmipcser -pagemap TopWrWrRdRatio
# python plot_avf.py -pmipcser -pagemap PCTopWrWrRdRatio50FillM1
###### MIGRATION for PERFORMANCE IPC and SER #####
# python plot_avf.py -cntipcser -counter HMAUnAllocThd1 -initalloc fillm1
# python plot_avf.py -cntipcser -counter HMAUnAlloc -initalloc onlym2
# python plot_avf.py -cntipcser -counter HMAUnAlloc -initalloc fillm1
##### MIGRATION for RELIABILITY (FullCounters) IPC and SER #####
# python plot_avf.py -cntipcser -counter FullCounters -initalloc onlym2
# python plot_avf.py -cntipcser -counter FullCounters -initalloc capratio
# python plot_avf.py -cntipcser -counter FullCounters -initalloc fillm1
##### MIGRATION for RELIABILITY (CrossCounters) IPC and SER ####
# python plot_avf.py -cntipcser -counter MEAupHMAdown -initalloc onlym2
#### INTERVAL SWEEP GRAPH ####
# python plot_avf.py -intervalsweep -counter HMAUnAllocThd1 -initalloc onlym2
#### MIGRATION + PINNING IPC and SER (Note: meacounters is specified for cntipcser plots to extract the data from the right folder name) ####
## Perf-Migration + Perf-Pinning ##
# python plot_avf.py -cntipcser -counter HMAUnAllocThd1 -initalloc tophotcapratio
## Reliability-Migration + Reliability-Pinning ##
# python plot_avf.py -cntipcser -counter FullCounters -initalloc topwrwrrdratiocapratio
# python plot_avf.py -cntipcser -counter FullCounters75 -initalloc topwrwrrdratiocapratio
## Reliability-Migration + Perf-Pinning
# python plot_avf.py -cntipcser -counter FullCounters -initalloc tophotcapratio
# python plot_avf.py -cntipcser -counter FullCounters75 -initalloc tophotcapratio
## HPCA Dynamic Migration Graph
# python plot_avf.py -cntipcser -counter FullCounters75 -initalloc topwrwrrdratiocapratio
# python plot_avf.py -cntipcser -counter MEAupHMAdown -initalloc topwrwrrdratiocapratio
##################################
from func_regx import *
from helper_plot_avf import *

parser = argparse.ArgumentParser()

#Plot types
parser.add_argument("-avfallbenchs", dest='avfallbenchs', action='store_true', default=False, help="Plot avf of all benchmark with only DDR in an increasing order.")
parser.add_argument("-perfrelintro", dest='perfrelintro', action='store_true', default=False, help="Plot performance vs. reliablity for performance-focoused data placement workloads.")
parser.add_argument("-perfipcser", dest='perfipcser', action='store_true', default=False, help="IPC and SER of all benchmarks with performance-focused static placement on HMA.")
parser.add_argument("-pmipcser", dest='pmipcser', action='store_true', default=False, help="IPC and SER of all benchmarks with a static page placement.")
parser.add_argument("-cntipcser", dest='cntipcser', action='store_true', default=False, help="IPC and SER of all benchmarks with a dynamic counters.")
parser.add_argument("-cntipcserhpca", dest='cntipcserhpca', action='store_true', default=False, help="IPC and SER of all benchmarks with a dynamic counters with good initial allocation.")
parser.add_argument("-cntpinipcseravg", dest='cntpinipcseravg', action='store_true', default=False, help="Plot average IPC SER for dynamic migration and balanced (pinning) schems.")
parser.add_argument("-avfhotbreakdown", dest='avfhotbreakdown', action='store_true', default=False, help="Plots the breakdown of AVF and Hotness between HBM and DDR.")
parser.add_argument("-pvfstatic", dest='pvfstatic', action='store_true', default=False, help="Plots all static placement results")
parser.add_argument("-benchpvfstatic", dest='benchpvfstatic', action='store_true', default=False, help="Plots one benchmark static placement results")
parser.add_argument("-pvfmig", dest='pvfmig', action='store_true', default=False, help="Plot all migration results")
parser.add_argument("-hotvsavf", dest='hotvsavf', action='store_true', default=False, help="Plots AVF of top N hot pages")
parser.add_argument("-piebenchplot", dest='piebenchplot', action='store_true', default=False, help="Pie chart of hot and vulnerable pages")
parser.add_argument("-gentophotlowavfpmap", dest='gentophotlowavfpmap', action='store_true', default=False, help="Generate top hot low avf page map")
parser.add_argument("-rdwrhisto", dest='rdwrhisto', action='store_true', default=False, help="Historgram of Wr/Rd ratio of pages")
parser.add_argument("-intervalsweep", dest='intervalsweep', action='store_true', default=False, help="Plot Interval Sweep Results for Selected Workloads")
parser.add_argument("-pcstruct", dest='pcstruct', action='store_true', default=False, help="Plots number of program structure annotated")

#Benchmark
parser.add_argument("-bench", default='none', required=False, help="Name of benchmark (astar, cactusADM, ..)")

#pagemap for ploting ipcserpmhma
parser.add_argument("-pagemap", default='none', help="Specify page map to plot SER and IPC for the particular static placement e.g.. LeastAVF, TopAVF, TopWrRd.")

#M1 and M2 memory types
parser.add_argument("-m1", default='HBM1GB', help="M1 default is HBM for normal mode HBM1GB, HBM16GB")
parser.add_argument("-m2", default='DDR16GB', help="M2 default is DDR for normal mode DDR16GB")
#PAGE MAP
#parser.add_argument("-pmlist", default='pmfull', help="PageMap list pmfull, pmh1, pmh2, pmtophot. Extracts simulation information about static page mappings.")
#Counter list
#parser.add_argument("-cntlist", default='cntfull', help="Counter list cntall. Extract simulation information about migrations.")
#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-interval", default=LEGACY_INTERVAL, help="AVF/Migration Interval length 10000000, 100000")
parser.add_argument("-mig", default='off', help="Migration ON/OFF")
parser.add_argument("-counters", default='none', help="Migration type HMA, HMAWrRd, HMAWrWrRd, MEA, ..")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")
parser.add_argument("-meacounters", default=LEGACY_MEA_COUNTERS, help="Number of MEA counters. Maximum number of MEA-based migrations per interval")
parser.add_argument("-initalloc", default='fillm1', help="Specify initial allocation. fillm1 = Fill M1/M2 50 50 until M1 is full. caprato = Fill M1 and M2 in capacity ratio. onlym2 = Fills only M2. M1 only gets migrated pages.")

args = parser.parse_args()

if args.perfrelintro:
    plot_perf_vs_rel_intro(args)

if args.avfallbenchs:
    plot_avf_all_benchs(args)

if args.pvfstatic:
    plot_pvf_static(args)

if args.benchpvfstatic:
    if args.bench == 'none':
        print 'FATAL! Benchmark needed for this plot'
        sys.exit(0)
    plot_bench_pvf_static(args)

if args.pvfmig:
    plot_pvf_migrtion(args)

if args.hotvsavf:
    if args.bench == 'none':
        print 'FATAL! Benchmark needed for this plot'
        sys.exit(0)
    plot_hot_vs_avf(args, args.bench)

if args.piebenchplot:
    if args.bench == 'none':
        print 'FATAL! Benchmark needed for this plot'
        sys.exit(0)
    plot_pie_hotavf(args, args.bench, True, False)

if args.gentophotlowavfpmap:
    benchsetdata = {}
    #BENCHS = ['test1MB']
    BENCHS=['mcf', 'astar', 'lbm',  'mix2', 'bwaves', 'lulesh',  'mix3',  'mix8', 'xalancbmk', 'cactusADM128MB', 'mix4',  'omnetpp', 'test1MB', 'xsbench', 'cactusADM', 'milc', 'mix5',  'zeusmp', 'GemsFDTD', 'mix1',  'mix6', 'mix7']
    #BENCHS=['astar', 'lbm',  'mix2', 'bwaves', 'lulesh',  'mix3',  'mix8', 'xalancbmk']
    for bench in BENCHS:
        benchsetdata[bench], pgdata = plot_pie_hotavf(args, bench, False, True)
        print '*** Benchmark : ' + str(bench)
        print 'benchsetdata : ' + str(benchsetdata[bench])

if args.perfipcser:
    plot_ipc_ser_perf_focused_hma(args)

if args.pmipcser:
    if args.pagemap == 'none':
        print 'FATAL! -pageamp required for this plot'
        sys.exit(0)
    plot_ipc_ser_for_static_pagemap_hma(args, args.pagemap)

if args.cntipcser:
    if args.counters == 'none':
        print 'FATAL! -counters required for this plot'
        sys.exit(0)
    plot_ipc_ser_for_hybrid_counter_hma(args, args.counters)

#HPCA
if args.cntipcserhpca:
    plot_ipc_ser_for_hybrid_counter_hma_hpca(args, args.counters)

#HPCA
if args.cntpinipcseravg:
    plot_avg_ipc_ser_for_balanced_schemes(args)

if args.avfhotbreakdown:
    plot_avfhot_breakdown(args)
    
if args.intervalsweep:
    if args.counters == 'none':
        print 'FATAL! -counters required for this plot'
        sys.exit(0)
    plot_interval_sweep(args) 

if args.pcstruct:
    plot_program_struct_annotated(args)
