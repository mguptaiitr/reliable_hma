## Command help ##
# python run_static_pagemap_multi.py -bench test1MB -m1 HBM1GB -m2 DDR16GB
# python run_static_pagemap_multi.py -bench test1MB -m1 HBM1GB -m2 DDR16GB -pmlist pmfull
# python run_static_pagemap_multi.py -bench test1MB -m1 HBM1GB -m2 DDR16GB -pmlist pmhot

import commands, time, shutil, operator
import sys, os, subprocess, re, datetime 
import multiprocessing
import subprocess
import argparse
from func_regx import *

PAGEMAPS = []

parser = argparse.ArgumentParser()
parser.add_argument("-bench", required=True, help="Run benchmark give specific benchmark or all" )
parser.add_argument("-pmlist", default='pmfull', help="pmfull, h1, h2" )

#M1 and M2 memory types
parser.add_argument("-m1", default='HBM1GB', help="M1 default is HBM for normal mode HBM1GB, HBM16GB")
parser.add_argument("-m2", default='DDR16GB', help="M2 default is DDR for normal mode DDR16GB")

#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-interval", default=10000000, help="AVF/Migration Interval length")
parser.add_argument("-mig", default='off', help="Migration ON/OFF")
parser.add_argument("-counters", default='none', help="Migration type HMA, HMAWrRd, HMAWrWrRd, MEA, ..")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")
parser.add_argument("-meacounters", default='64', help="Number of MEA counters. Maximum number of MEA-based migrations per interval")
parser.add_argument("-initalloc", default='fillm1', help="Specify initial allocation. fillm1, capratio, onlym2. fillm1 = Fill M1/M2 50 50 until M1 is full. caprato = Fill M1 and M2 in capacity ratio. onlym2 = Fills only M2. M1 only gets migrated pages.")
args = parser.parse_args()

bench = args.bench
mem1 = args.m1
mem2 = args.m2
pm = args.pmlist

if pm in PMOPTIONS.keys():
    PAGEMAPS = PMOPTIONS[pm]
else:
    print pm+' not found in PMOPTION.keys() ='+str(PMOPTIONS.keys())
    sys.exit()

cmdlist=[]
#Create all commands
i=0
for pm in PAGEMAPS:
 i=i+1
 cmd_n_dir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, pm, args, True)
 print '** Command '+str(i)+': '+cmd_n_dir[0]
 cmdlist.append(cmd_n_dir)

num_cpu = multiprocessing.cpu_count()
print '** Number of commands: ' + str(len(cmdlist))
print '** Number of cpu: ' + str(num_cpu)
run = raw_input('Launch MultiThread Runs (y/n): ')
#run = 'y'
if run != 'y':
    sys.exit(0)

pool = multiprocessing.Pool(num_cpu-2)
results = []
r = pool.map_async(launch_single_thread, cmdlist, callback=results.append)
r.wait()
print '***** ALL DONE ALL '+ str(len(cmdlist)) +' Commands *****'
