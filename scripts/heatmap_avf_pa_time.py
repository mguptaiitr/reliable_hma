####### COMMANDS HELP ############
# 1) python process_plot_trace.py -bench test1MB -m1 HBM1GB -m2 DDR16GB -pmlist pmfull
# 2) python process_plot_trace.py -bench test1MB -m1 HBM1GB -m2 DDR16GB -pmlist pmhot

from func_regx import *
from matplotlib import cm
import matplotlib.colors as colors
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot, rcParams
from pylab import meshgrid,cm,imshow,contour,clabel,colorbar,axis,title,show,savefig
import seaborn as sns
import numpy as np

np.set_printoptions(threshold='nan')
sns.set()

#logging.basicConfig(level=logging.DEBUG, format='%(levelname)s - %(message)s')

parser = argparse.ArgumentParser()
parser.add_argument("-bench", required=True, help="Run benchmark")

#M1 and M2 memory types
parser.add_argument("-m1", default='DDR16GB', help="M1 default is HBM for normal mode HBM1GB, HBM16GB")

#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-interval", default=10000000, help="AVF/Migration Interval length")
parser.add_argument("-mig", default='off', help="Migration ON/OFF")
parser.add_argument("-counters", default='none', help="Migration type HMA, HMAWrRd, HMAWrWrRd, MEA, ..")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")

args = parser.parse_args()
bench = args.bench
mem1 = args.m1

#Inputs
print 'bench: '+str(bench)
print 'mem1: '+str(mem1)

#Read the checkpoint file
cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'normal', mem1, 'none', 'none', args)
c_pw_avf_file = expdir+'/'+bench+'_CheckPointPageWiseAVF.csv'
c_pw_hot_file = expdir+'/'+bench+'_CheckPointPageWiseHOTNESS.csv'

if not os.path.exists(c_pw_avf_file):
    print str(c_pw_avf_file) + ' not in path'

if not os.path.exists(c_pw_hot_file):
    print str(c_pw_hot_file) + ' not in path'

#Get total number of distinct pages
stdoutfile = expdir+'/std.out'
f = open(stdoutfile, 'r')
out = f.read()
f.close()
total_pages = int(get_pattern_from_output(out, no_pages_pattern))
print 'Total number of pages: '+str(total_pages)


f = open(c_pw_avf_file,'r')
now1= datetime.datetime.now()
linesavf = f.read().splitlines()
now2= datetime.datetime.now()
print 'Time readfile (sec): '+str(round((now2-now1).total_seconds(), 2))
f.close()

f1 = open(c_pw_hot_file,'r')
now1 = datetime.datetime.now()
lineshot = f1.read().splitlines()
now2 = datetime.datetime.now()
print 'Time readfile (sec): '+str(round((now2-now1).total_seconds(), 2))
f1.close()


now1 = datetime.datetime.now()
chkind = [i for i, x in enumerate(lineshot) if "Checkpoint No" in x]
endind = []
for ind,i in zip(chkind[:-1], range(len(chkind[:-1]))):
    print ind
    print chkind[i+1]
    endind.append(chkind[i+1])
endind.append(len(lineshot))
now2= datetime.datetime.now()
print 'Time enumerating (sec): '+str(round((now2-now1).total_seconds(), 2))


print 'Checkpoint No at: '+str(chkind)
print 'Total number of checkpoints:'+str(len(chkind))

#plot a checkpoint
pagemap={}
pnum=0
AVF   = np.zeros(shape=(total_pages, len(chkind)), dtype=np.int)
READ  = np.zeros(shape=(total_pages, len(chkind)), dtype=np.int)
WRITE = np.zeros(shape=(total_pages, len(chkind)), dtype=np.int)
HOT   = np.zeros(shape=(total_pages, len(chkind)), dtype=np.int)

print 'Size of the AVF datatstrcutre is: '+str(sys.getsizeof(AVF))
print 'Size of the HOTNESS datatstrcutre is: '+str(sys.getsizeof(HOT))

#Read hot pw data
for ind, indend in zip(chkind,endind):
    chk=int(lineshot[ind].split(',')[1])
    print 'Checkpoint processing HOTNESS: '+str(chk)
    now1= datetime.datetime.now()
    for line in lineshot[ind+1:indend]:
        data = line.split(',')
        pageno = int(data[0])
        read = int(data[1])
        write = int(data[2])
        hot = read+write

        #pageno not in pagemap.keys() is an expensive operation O(n) key in list
        if pageno not in pagemap:            #O(1) operation key in map
            pagemap[pageno]=pnum
            debug(pagemap)
            pnum = pnum+1            
        debug( 'Writing at at: '+str(chk)+','+str(pagemap[pageno]))
        READ[pagemap[pageno], chk] = read
        WRITE[pagemap[pageno], chk] = write
        HOT[pagemap[pageno], chk] = hot
    now2= datetime.datetime.now()
    print 'Time to process hotness one checkpoint (sec): '+str(round((now2-now1).total_seconds(), 2))

#Read avf pw data
now1= datetime.datetime.now()
for line in linesavf:
    debug(line)
    data = line.split(',')
    pageno = int(data[0])
    i = 0
    for d in data[1:]:
        AVF[pagemap[pageno], i]=int(d)
        i = i + 1

now2= datetime.datetime.now()
print 'Time to process avfdata (sec): '+str(round((now2-now1).total_seconds(), 2))
print 'Processed pages in pagewise avf file: '+str(len(linesavf))

debug(AVF)
AVFmax = np.max(AVF)
AVFmin = np.min(AVF)

print 'Shape/size of AVF heatmap:' + str(AVF.shape)
print 'max AVF: '+str(AVFmax)
print 'min AVF: '+str(AVFmin)
print 'Checkpoints in pagewise hotness file: '+str(len(chkind))
print 'Checkpoints in pagewise avf file: '+str(len(linesavf[0].split(',')) - 1)

#max min and mean
debug('mean AVF of all pages accross checkpoints: '+str(AVF.mean(1)))


#plotting heatmap
params = {
    'axes.labelsize': 11,
    'axes.labelpad': 0,
    'figure.figsize': [10, 8]
    } 

rcParams.update(params)

fig, (ax1, ax2, ax3, ax4) = pyplot.subplots(1, 4, sharey=True)

'''
cmap=cm.get_cmap('binary', 10)
im = pyplot.imshow(AVF, cmap=cmap, aspect='auto', interpolation='none')
pyplot.colorbar(im) # adding the colobar on the right
'''

im = sns.heatmap(AVF, yticklabels=False, rasterized=True, ax=ax1)
im = sns.heatmap(HOT, yticklabels=False, rasterized=True, ax=ax2)
im = sns.heatmap(READ, yticklabels=False, rasterized=True, ax=ax3)
im = sns.heatmap(WRITE, yticklabels=False, rasterized=True, ax=ax4)

ax1.set_xlabel('Checkpoints(time)')
ax2.set_xlabel('Checkpoints(time)')
ax3.set_xlabel('Checkpoints(time)')
ax4.set_xlabel('Checkpoints(time)')

ax1.set_ylabel('Pages(Memory)')

pyplot.title('Heat maps for benchmark '+bench)
ax1.set_title('AVF')
ax2.set_title('HOTNESS')
ax3.set_title('READ')
ax4.set_title('WRITE')

#Save heatmaps
plotname=bench+'_heatmap.pdf'
plotname = 'heatmaps/'+plotname
print 'Saving heatmap to : '+str(plotname)
fig.savefig(plotname, format='pdf', dpi=300)

