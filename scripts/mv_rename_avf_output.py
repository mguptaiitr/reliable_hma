from func_regx import *

BENCHS=['mcf', 'astar', 'lbm',  'mix2', 'mix7', 'bwaves', 'lulesh',  'mix3',  'mix8', 'xalancbmk', 'mcf', 'mix4',  'omnetpp', 'xsbench', 'cactusADM', 'milc', 'mix5', 'zeusmp', 'GemsFDTD', 'mix1',  'mix6']

#BENCHS = ['cactusADM']

BENCHEXPDIRS = os.listdir(M2AVF_OUTPUT)

for bench in BENCHS:
    benchdir = M2AVF_OUTPUT+'/'+bench
    expdirs = os.listdir(benchdir)
    for oldExpDir in expdirs:
        params = oldExpDir.split('_')
        #create newExpDir
        newExpDir = params[0]
        print '********'
        #print params[1:] 
        for param in params[1:]:
            if 'cnt' in param:
                newExpDir = newExpDir +'_cnt'+param[3:].upper()
            elif 'film1FULL' in param:
                newExpDir = newExpDir +'_initallocFILLM1'
            elif 'film1RATIO' in param:
                newExpDir = newExpDir +'_initallocCAPRATIO'
            elif 'fillm1NONE' in param:
                newExpDir = newExpDir +'_initallocNONE'
            else:
                newExpDir = newExpDir +'_'+param
            #print 'Param: '+param
            #print 'newExpDir: '+newExpDir
        print 'Old expdir: '+str(oldExpDir)
        print 'New expdir: '+str(newExpDir)
        cmd = 'mv '+benchdir+'/'+oldExpDir +' '+benchdir+'/'+newExpDir
        print 'MOVING oldExpDir to newExpDir: '+str(cmd)
        status, output = commands.getstatusoutput(cmd)
