from func_regx import *

##### AVIALABLE NODES ON SGE CLUSTER
## ALL NODES
#MATRIXNODE_LIST = ['matricks', 'matrix0-00', 'matrix0-01', 'matrix0-02', 'matrix0-03', 'matrix0-04', 'matrix0-05', 'matrix0-06', 'matrix0-07', 'matrix0-08', 'matrix0-09', 'matrix0-10', 'matrix0-11', 'matrix0-12', 'matrix0-14', 'matrix0-15', 'matrix0-16', 'matrix0-17', 'matrix0-18', 'matrix0-19', 'matrix0-21']
MATRIXNODE_LIST = ['matricks', 'matrix0-00', 'matrix0-01', 'matrix0-03', 'matrix0-04', 'matrix0-05', 'matrix0-06', 'matrix0-07', 'matrix0-08', 'matrix0-09', 'matrix0-10', 'matrix0-11', 'matrix0-12', 'matrix0-14', 'matrix0-15', 'matrix0-16', 'matrix0-17', 'matrix0-18', 'matrix0-19', 'matrix0-21']

######### BENCHMARKS on QSUB jobscript will run on the below benchamrks
#### *ALL* 20 BENCHMARKS
#QSUBBENCHS=['mcf', 'astar', 'lbm',  'mix2', 'bwaves', 'lulesh',  'mix3',  'mix8', 'xalancbmk', 'mix4',  'omnetpp', 'xsbench', 'cactusADM', 'milc', 'mix5',  'zeusmp', 'GemsFDTD', 'mix1',  'mix6', 'mix7']
#### *REDUCED* BENCHMARK SET 16 BENCHMARKS FOR THE PAPER (EXCLUDED mcf, mix5, mix6, mix8)
#QSUBBENCHS=['astar', 'lbm',  'mix2', 'bwaves', 'lulesh', 'mix3', 'xalancbmk', 'mix4',  'omnetpp', 'xsbench', 'cactusADM', 'milc',  'zeusmp', 'GemsFDTD', 'mix1', 'mix7']
#QSUBBENCHS=['astar',  'mix2', 'bwaves', 'lulesh', 'mix3', 'xalancbmk', 'mix4',  'omnetpp', 'cactusADM', 'milc',  'zeusmp', 'GemsFDTD', 'mix1', 'mix7']
#PCBASED failed traces
QSUBBENCHS=['lbm', 'xsbench']


#### *Omitted* BENCHMARKS from MICRO PAPER
#QSUBBENCHS=['mix5', 'mix6', 'mix8']

#QSUBBENCHS=['lbm',  'mix2', 'bwaves', 'lulesh',  'mix3']
#QSUBBENCHS=['mix8', 'xalancbmk', 'mix4',  'omnetpp']
#QSUBBENCHS=['xsbench', 'milc', 'mix5',  'zeusmp']
#QSUBBENCHS=['GemsFDTD', 'mix7']
#MIGRATION EXPERIMENTS FOR
#QSUBBENCHS=['mix1', 'astar', 'cactusADM']
#QSUBBENCHS=['lbm',  'zeusmp', 'mix7', 'mix6']
#QSUBBENCHS=['mix2', 'mix4', 'lulesh']
#QSUBBENCHS=['milc']
#FOR TEST
#QSUBBENCHS=['cactusADM', 'astar']
#QSUBBENCHS=['cactusADM128MB', 'test64MB', 'astar', 'cactusADM']
#QSUBBENCHS=['cactusADM128MB', 'test64MB']

### PAGEMAP STATIC on QSUB 
QPAGEMAPFULL = ['TopHot', 'LeastHot', 'TopAVF', 'LeastAVF', 'TopWr', 'TopRd', 'TopWrRdRatio', 'TopWrWrRdRatio', 'TopHotLowAVF', 'none']
QPAGEMAPTEMP = ['PCTopHotLowRisk', 'PCTopWrWrRdRatio50FillM1']
## PAGEMAP OPTIONS
QPMOPTIONS = {'pmfull':QPAGEMAPFULL, 'pmtemp':QPAGEMAPTEMP}

### COUNTERS on QSUB 
QCOUNTERFULL = ['HMA', 'HMAWrRd', 'HMAWrWrRd', 'MEA', 'MEAWr2Rd1', 'HMAUnAlloc', 'MEAupHMAdown']
QCOUNTERTEMP = ['FullCounters75', 'MEAupHMAdown']
QCOUNTERREL = ['FullCounters', 'FullCounters75', 'MEAupHMAdown']
QCOUNTERPERF = ['HMAUnAllocThd1']

## COUNTER OPTIONS
QCNTOPTIONS = {'cntfull':QCOUNTERFULL, 'cnttemp':QCOUNTERTEMP, 'cntrel':QCOUNTERREL, 'cntperf':QCOUNTERPERF}

#INTERVAL LIST
QINTERVALFULL = [3200000, 32000000, 320000000, 3200000000, 6400000000]

#INTERVAL OPTIONS
QINTOPTIONS = {'intfull':QINTERVALFULL}


#QSUB_CMD = Template("""qsub -V -b y -o $STDOUT -e $ERROUT -cwd $RAMULATORCMD""") 
QSUB_CMD = Template("""qsub -V -o $STDOUT -e $ERROUT -cwd $RAMULATORSH""") 

def launch_qsub_cmd(cmd_n_dir):
    #os.chdir(M2AVF_RAMULATOR)
    os.chdir(cmd_n_dir[1])
    cmd = cmd_n_dir[0]
    expdir = cmd_n_dir[1]
    
    if 'STATIC PAGE MAP NOT FOUND' in cmd:
        return '!!!!!!!!!!!!! FAILED !!!!!!!!!!!!!!'+cmd+'\n'
    
    stdoutfile = expdir+'/'+'std.out'
    erroutfile = expdir+'/'+'err.out'
    
    #Remove stdoutfile and erroutfile if they exists.
    #qsub appends -o and -e files.
    if os.path.exists(stdoutfile):
        os.remove(stdoutfile) 
    if os.path.exists(erroutfile):
        os.remove(erroutfile)
    
    #Create jobscript in expdir folder
    jobscript=expdir+'/jobscript.sh'
    f = open(jobscript, 'w')
    #Write header of the jobscript
    f.write('#$ -N ramulator\n')
    f.write('#$ -S /bin/bash\n')
    #For restricting nodes
    avialable_nodes = MATRIXNODE_LIST[0]+'*'
    for nodes in MATRIXNODE_LIST[1:]:
        avialable_nodes = avialable_nodes + '|' + nodes+'*'
    #f.write('#$ -l hostname='+avialable_nodes+'\n')
    f.write('#$ -cwd\n')
    f.write('#$ -j y\n')
    f.write('\n')
    #Write command
    f.write(cmd)
    f.close()

    qsubcmd = QSUB_CMD.safe_substitute(STDOUT=stdoutfile, ERROUT=erroutfile, RAMULATORSH=jobscript)
    
    print 'QSUB COMMAND '+M2AVF_RAMULATOR+'$ ----> ' + qsubcmd
    status, output = commands.getstatusoutput(qsubcmd)
    print 'QSUB OUTPUT$ ' + output
    return


def launch_qsub_parse_cmd(cmd_n_bench):
    #os.chdir(M2AVF_RAMULATOR)
    os.chdir(RELIABLE_HMA+'/scripts')
    cmd = cmd_n_bench[0]
    bench = cmd_n_bench[1]
    expdir = STATIC_PAGE_MAPS+'/generation_info'
    
    stdoutfile = expdir+'/'+bench+'_std.out'
    erroutfile = expdir+'/'+bench+'_err.out'
    
    #Remove stdoutfile and erroutfile if they exists.
    #qsub appends -o and -e files.
    if os.path.exists(stdoutfile):
        os.remove(stdoutfile) 
    if os.path.exists(erroutfile):
        os.remove(erroutfile)
    
    #Create jobscript in expdir folder
    jobscript=expdir+'/'+bench+'_jobscript.sh'
    f = open(jobscript, 'w')
    #Write header of the jobscript
    f.write('#$ -N python_parse_trace\n')
    f.write('#$ -S /bin/bash\n')
    #For restricting nodes
    avialable_nodes = MATRIXNODE_LIST[0]+'*'
    for nodes in MATRIXNODE_LIST[1:]:
        avialable_nodes = avialable_nodes + '|' + nodes+'*'
    #f.write('#$ -l hostname='+avialable_nodes+'\n')
    f.write('#$ -cwd\n')
    f.write('#$ -j y\n')
    f.write('\n')
    #Write command
    f.write(cmd)
    f.close()

    qsubcmd = QSUB_CMD.safe_substitute(STDOUT=stdoutfile, ERROUT=erroutfile, RAMULATORSH=jobscript)
    
    print 'QSUB COMMAND '+STATIC_PAGE_MAPS+'$ ----> ' + qsubcmd
    status, output = commands.getstatusoutput(qsubcmd)
    print 'QSUB OUTPUT$ ' + output
    return
