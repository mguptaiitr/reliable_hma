python $RELIABLE_HMA/scripts/run_trace.py -bench cactusADM128MB -normal -m1 DDR16GB
python $RELIABLE_HMA/scripts/run_static_pagemap_multi.py -bench cactusADM128MB
python $RELIABLE_HMA/scripts/run_migration_multi.py -bench cactusADM128MB
python $RELIABLE_HMA/scripts/process_plot_trace.py -bench cactusADM128MB
