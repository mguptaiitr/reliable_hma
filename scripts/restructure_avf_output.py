from func_regx import *

BENCHS=['mcf', 'astar', 'lbm',  'mix2', 'mix7', 'bwaves', 'lulesh',  'mix3',  'mix8', 'xalancbmk', 'mix4',  'omnetpp', 'xsbench', 'cactusADM', 'milc', 'mix5', 'zeusmp', 'GemsFDTD', 'mix1',  'mix6', 'test1MB', 'cactusADM128MB']

BENCHEXPDIRS = os.listdir(M2AVF_OUTPUT)
print BENCHEXPDIRS
BENCHEXPDIRS = [b for b in BENCHEXPDIRS if b in BENCHS]
print 'Restructuring benchmarks: '+str(BENCHEXPDIRS)

for bench in BENCHEXPDIRS:
    bfullpath = M2AVF_OUTPUT+'/'+bench
    print '**** Benchmark Full Path: '+bfullpath
    expdirs = os.listdir(bfullpath)
    for edir in expdirs:
        print '*** Experiment Directory: '+edir
        #stdout file in edir
        oldExpDir = bfullpath+'/'+edir
        stdoutfile = oldExpDir+'/std.out'
        if os.path.exists(stdoutfile):
            f = open(stdoutfile, 'r')
            out = f.read()
            f.close()
            num_mea = get_pattern_from_output(out, num_mea_counters_pattern)
            if num_mea:
                print 'Num mea counters: '+str(num_mea)
                dashAt = [i for i, ch in enumerate(edir) if ch == '_']
                lastDash = dashAt[-1]
                print dashAt 
                cntStart = dashAt[-2]
                cnt = edir[cntStart+4:lastDash]
                print 'counter: '+cnt
                if cnt not in ['MEA', 'MEAUNALLOC', 'MEAWR2RD1', 'MEAUPHMADOWN']:
                    num_mea = LEGACY_MEA_COUNTERS
                newModeDir = edir[:lastDash]+'_meacnt'+str(num_mea)+edir[lastDash:]
                print 'New mode directory: '+newModeDir
                #If newModeDir is not present create it in M2AVF_OUTPUT
                fullModeDirPath = M2AVF_OUTPUT+'/'+newModeDir
                
                if not os.path.exists(fullModeDirPath):
                    os.makedirs(fullModeDirPath)
                '''
                                    
                '''
                #make benchmark direcotry inside fullModeDirPath
                benchExpDir = fullModeDirPath+'/'+bench
                os.makedirs(benchExpDir)
                #move everything from oldexp dir to new bench
                cmd = 'mv '+oldExpDir+'/*.* '+benchExpDir
                print 'Moving data: '+cmd
                status, output = commands.getstatusoutput(cmd)
                print output
    #remove benchdir from M2AVF_OUTPUT
    cmd = 'rm -rf '+bfullpath
    print 'Removing old data director: '+cmd
    status, output = commands.getstatusoutput(cmd)
    print output
