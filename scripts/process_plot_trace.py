####### COMMANDS HELP ############
# 1) python process_plot_trace.py -bench test1MB -m1 HBM1GB -m2 DDR16GB -pmlist pmfull
# 2) python process_plot_trace.py -bench test1MB -m1 HBM1GB -m2 DDR16GB -pmlist pmhot

from func_regx import *

parser = argparse.ArgumentParser()
parser.add_argument("-bench", required=True, help="Run benchmark")

#M1 and M2 memory types
parser.add_argument("-m1", default='HBM1GB', help="M1 default is HBM for normal mode HBM1GB, HBM16GB")
parser.add_argument("-m2", default='DDR16GB', help="M2 default is DDR for normal mode DDR16GB")

#PAGE MAP
parser.add_argument("-pmlist", default='pmfull', help="PageMap list pmfull, pmh1, pmh2, pmtophot. Extracts simulation information about static page mappings.")

#Counter list
parser.add_argument("-cntlist", default='cnttemp', help="Counter list cntall. Extract simulation information about migrations.")

#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-interval", default=LEGACY_INTERVAL, help="AVF/Migration Interval length 10000000, 100000")
parser.add_argument("-mig", default='on', help="Migration ON/OFF")
parser.add_argument("-counters", default='none', help="Migration type HMA, HMAWrRd, HMAWrWrRd, MEA, ..")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")
parser.add_argument("-meacounters", default=LEGACY_MEA_COUNTERS, help="Number of MEA counters. Maximum number of MEA-based migrations per interval")
parser.add_argument("-initalloc", default='fillm1', help="Specify initial allocation. fillm1 = Fill M1/M2 50 50 until M1 is full. caprato = Fill M1 and M2 in capacity ratio. onlym2 = Fills only M2. M1 only gets migrated pages.")
args = parser.parse_args()

bench = args.bench
mem1 = args.m1
mem2 = args.m2
pmlist = args.pmlist
cntlist = args.cntlist

if pmlist in PMOPTIONS.keys():
    PAGEMAPS = PMOPTIONS[pmlist]
else:
    print pmlist+' not found in PMOPTION.keys() ='+str(PMOPTIONS.keys())
    sys.exit()

if cntlist in CNTOPTIONS.keys():
    COUNTERS = CNTOPTIONS[cntlist]
else:
    print cntlist+' not found in CNTOPTION.keys() ='+str(CNTOPTIONS.keys())
    sys.exit()

if bench not in BENCHMARKLIST:
    print bench +" not in "+str(BENCHMARKLIST)
    sys.exit()

#Inputs
print 'bench: '+str(bench)
print 'mem1: '+str(mem1)
print 'mem2: '+str(mem2)
print 'PAGEMAP LIST: '+str(PAGEMAPS)
print 'COUNTER LIST: '+str(COUNTERS)

benchsum = M2AVF_OUTPUT+'/'+bench+'_static_n_migration'+'.csv'
f = open(benchsum,'w')
f.write('Page Placement, Interval Length, #Intervals, #Migrations,\
            IPC, IPC*, AVF, SER,\
            IPCn, SERn,\
	        AVF_HBM, AVF_DDR, SER_HBM(Week 0), SER_DDR(Week 0),\
	        n_HBM, n_rd_HBM, n_wr_HBM, n_DDR, n_rd_DDR, n_wr_DDR,\
	        Hit Rate, Sanity Check(n_hbm+n_ddr), Retired Instructions, clk,\
            M1 pages, M2 pages, M1+M2 pages,\
            M1 tot_pages, M2 tot_pages,\
            tot_AVF_HBM, tot_AVF_DDR, tot_AVF,\
            SER_HBM_std, SER_DDR_std, SER_std\n')

#Normal data
args.mig = 'off'
args.counters = 'none'
args.meacounters = LEGACY_MEA_COUNTERS
cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'normal', 'DDR16GB', 'none', 'none', args, False)
print '--> Only DDR 16GB (All pages in DDR) in directory = ' + str(expdir)
#print os.listdir(expdir)
data = get_output_data_for_norm_exp(expdir)
write_normal_data_to_csv_file(f, data, args)
f.write('\n')
#Hold only ddr data for normalization
data_norm = data

#Static Hybrid data pagemap init allocation fill m1 complete
f.write('Static Placement, Initial Allocation: FILL M1 FULL\n')
args.mig = 'off'
args.counters = 'none'
args.meacounters = LEGACY_MEA_COUNTERS
args.initalloc = 'fillm1'
for pagemap in PAGEMAPS:
    args.placetype = pagemap
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, pagemap, args, False)
    print '--> Hybrid mode with static page maps = ('+pagemap+') in directory = ' + str(expdir)
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')

#Static Hybrid data pagemap with capacity ratio
f.write('Static Placement, Initial Allocation: CAP RATIO\n')
args.mig = 'off'
args.counters = 'none'
args.meacounters = LEGACY_MEA_COUNTERS
args.initalloc = 'capratio'
for pagemap in PAGEMAPS:
    args.placetype = pagemap
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, pagemap, args, False)
    print '--> Hybrid mode with static page maps = ('+pagemap+') in directory = ' + str(expdir)
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')

'''
#Migrations with Migrations FILL M1 (M1 <--> M2)
f.write('Migration, Initial Allocation: FILL M1 FULL\n')
args.mig = 'on'
args.initalloc = 'fillm1'
for counter in COUNTERS:
    args.counters = counter
    args.interval = INTERVAL_MAP[counter]
    args.placetype = counter
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
    print '--> Hybrid mode with migration = ('+counter+') in directory = ' + str(expdir)    
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')
'''

'''
#Migrations with CAPRATIO (M1 <--> M2)
f.write('Migration, Initial Allocation: CAPRATIO\n')
args.mig = 'on'
args.initalloc = 'capratio'
for counter in COUNTERS:
    args.counters = counter
    args.interval = INTERVAL_MAP[counter]
    args.placetype = counter
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
    print '--> Hybrid mode with migration = ('+counter+') in directory = ' + str(expdir)    
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')
'''

#HMAUnAlloc Performance-focused Migrations with ONLYM2 (M1 <--> M2)
f.write('HMAUnAlloc Performance-focused Migration, Initial Allocation: ONLYM2\n')
args.mig = 'on'
args.initalloc = 'onlym2'
args.counters = 'HMAUnAlloc'
args.meacounters = MEA_COUNTERS_MAP[args.counters]
args.interval = INTERVAL_MAP[args.counters]
args.placetype = args.counters
cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
print '--> Hybrid mode with migration = ('+args.counters+') in directory = ' + str(expdir)    
data = get_output_data_for_hyb_exp(expdir)
write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')

#HMAUnAlloc Performance-focused Migrations with CAPRATIO (M1 <--> M2)
f.write('HMAUnAlloc Performance-focused Migration, Initial Allocation: CAPRATIO\n')
args.mig = 'on'
args.initalloc = 'capratio'
args.counters = 'HMAUnAlloc'
args.meacounters = MEA_COUNTERS_MAP[args.counters]
args.interval = INTERVAL_MAP[args.counters]
args.placetype = args.counters
cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
print '--> Hybrid mode with migration = ('+args.counters+') in directory = ' + str(expdir)    
data = get_output_data_for_hyb_exp(expdir)
write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')

#HMAUnAlloc Performance-focused Migrations with FILLM1 (M1 <--> M2)
f.write('HMAUnAlloc Performance-focused Migration, Initial Allocation: FILLM1\n')
args.mig = 'on'
args.initalloc = 'fillm1'
args.counters = 'HMAUnAlloc'
args.meacounters = MEA_COUNTERS_MAP[args.counters]
args.interval = INTERVAL_MAP[args.counters]
args.placetype = args.counters
cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
print '--> Hybrid mode with migration = ('+args.counters+') in directory = ' + str(expdir)    
data = get_output_data_for_hyb_exp(expdir)
write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')


#FullCounters Migrations Interval Sweep IntialAllocation ONLYM2 (M1 <--> M2)
f.write('FullCounters Migration Interval Sweep, Initial Allocation: ONLYM2\n')
args.mig = 'on'
args.initalloc = 'onlym2'
args.counters = 'FullCounters'
args.meacounters = MEA_COUNTERS_MAP[args.counters]
for interval in QINTERVALFULL:
    args.interval = interval
    args.placetype = str(interval)
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
    print '--> Hybrid mode with migration interval =('+str(interval)+') in directory = ' + str(expdir)    
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')

#FullCounters75 Migrations Interval Sweep IntialAllocation ONLYM2 (M1 <--> M2)
f.write('FullCounters75 Migration Interval Sweep, Initial Allocation: ONLYM2\n')
args.mig = 'on'
args.initalloc = 'onlym2'
args.counters = 'FullCounters75'
args.meacounters = MEA_COUNTERS_MAP[args.counters]
for interval in QINTERVALFULL:
    args.interval = interval
    args.placetype = str(interval)
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
    print '--> Hybrid mode with migration interval =('+str(interval)+') in directory = ' + str(expdir)    
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')

#Migrations with ONLYM2 (M1 <--> M2)
f.write('Migration, MEAupHMAdown Initial Allocation: ONLYM2\n')
args.mig = 'on'
args.initalloc = 'onlym2'
args.counters = 'MEAupHMAdown'
args.interval = INTERVAL_MAP[args.counters]
args.placetype = args.counters
args.meacounters = MEA_COUNTERS_MAP[args.counters]
cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
print '--> Hybrid mode with migration = ('+args.counters+') in directory = ' + str(expdir)    
data = get_output_data_for_hyb_exp(expdir)
write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')



################### Pinning Pages Experiments ################################
#Perf-Migrations + Perf-Pinning + HMAUnAlloc
INITALLOC_PINNING = ['tophotcapratio', 'tophotfillm1']
#INITALLOC_PINNING = ['tophotcapratio']  #For test workloads such as cactus128MB, test64MB fillm1 doesn't work
args.mig = 'on'
args.counters = 'HMAUnAlloc'
args.interval = INTERVAL_MAP[args.counters]
args.placetype = args.counters
for initalloc in INITALLOC_PINNING:
    f.write('Perf-Migration + Perf-Pinning, HMAUnAlloc  Initial Allocation '+initalloc+'\n')
    args.initalloc = initalloc
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
    print '--> Hybrid mode with migration = ('+args.counters+') in directory = ' + str(expdir)    
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')

#Perf-Migrations + Perf-pinning + HMAUnAllocThd1
args.mig = 'on'
args.counters = 'HMAUnAllocThd1'
args.interval = INTERVAL_MAP[args.counters]
args.placetype = args.counters
for initalloc in INITALLOC_PINNING:
    f.write('Perf-Migration + Perf-Pinning, HMAUnAllocThd1  Initial Allocation '+initalloc+'\n')
    args.initalloc = initalloc
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
    print '--> Hybrid mode with migration = ('+args.counters+') in directory = ' + str(expdir)    
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')

#Reliability-Migration + Reliability-Pinning + FullCounters
INITALLOC_PINNING = ['topwrwrrdratiocapratio', 'topwrwrrdratiofillm1']
#INITALLOC_PINNING = ['topwrwrrdratiocapratio'] #For test workload such as cactus128MB, test64MB fillm1 doesn't work
args.mig = 'on'
args.counters = 'FullCounters'
args.interval = INTERVAL_MAP[args.counters]
args.placetype = args.counters
for initalloc in INITALLOC_PINNING:
    f.write('Reliability-Migration + Reliability-Pinning, FullCounters  Initial Allocation '+initalloc+'\n')
    args.initalloc = initalloc
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
    print '--> Hybrid mode with migration = ('+args.counters+') in directory = ' + str(expdir)    
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')

#Reliability-Migration + Reliability-Pinning + FullCounters75
args.mig = 'on'
args.counters = 'FullCounters75'
args.interval = INTERVAL_MAP[args.counters]
args.placetype = args.counters
for initalloc in INITALLOC_PINNING:
    f.write('Reliability-Migration + Reliability-Pinning, FullCounters75  Initial Allocation '+initalloc+'\n')
    args.initalloc = initalloc
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
    print '--> Hybrid mode with migration = ('+args.counters+') in directory = ' + str(expdir)    
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')


'''
##### Performance-focused Migration Interval Sweep. Only for a few benchmarks  
#HMAUnAlloc Migrations Interval Sweep IntialAllocation ONLYM2 (M1 <--> M2)
f.write('HMAUnAlloc Migration Interval Sweep, Initial Allocation: ONLYM2\n')
args.mig = 'on'
args.initalloc = 'onlym2'
args.counters = 'HMAUnAlloc'
for interval in QINTERVALFULL:
    args.interval = interval
    args.placetype = str(interval)
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
    print '--> Hybrid mode with migration interval =('+str(interval)+') in directory = ' + str(expdir)    
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')
'''

'''
#HMAUnAlloc Migrations Interval Sweep IntialAllocation CAPRATIO (M1 <--> M2)
f.write('HMAUnAlloc Migration Interval Sweep, Initial Allocation: CAPRATIO\n')
args.mig = 'on'
args.initalloc = 'capratio'
args.counters = 'HMAUnAlloc'
for interval in QINTERVALFULL:
    args.interval = interval
    args.placetype = str(interval)
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, False)
    print '--> Hybrid mode with migration interval =('+str(interval)+') in directory = ' + str(expdir)    
    data = get_output_data_for_hyb_exp(expdir)
    write_hybrid_data_to_csv_file(f, data, args, data_norm)
f.write('\n')
'''

#CLOSE STTAT FILE
f.close()
