####### COMMANDS HELP ############
# Split page maps from 0 to 100
# 1) python run_trace.py -bench test1MB -pm Random

from func_regx import *
parser = argparse.ArgumentParser()
parser.add_argument("-bench", required=True, help="Run benchmark")

#PAGE MAP
parser.add_argument("-pm", default='none', help="Page map options [TopHot, LeastAVF, TopWr, TopWrRdRatio, TopWrWrRdRatio]")
args = parser.parse_args()

bench = args.bench
pagemap =args.pm

#Inputs
print 'bench: '+str(bench)
print 'pagemap: '+str(pagemap)


staticpagemap=STATIC_PAGE_MAPS+'/'+bench+'_DDR3_'+pagemap+'.csv'
if not os.path.exists(staticpagemap):
    print 'Static PageMap not found '+staticpagemap
    sys.exit()

f = open(staticpagemap, 'r')
lines = f.readlines()
f.close()
totalpages = len(lines)
start = 0
end = 102
incperc = 2
NEWPMRANGE = np.arange(start, end, incperc)

print 'Total pages in the original pagemap: '+str(totalpages)

mstr = ''

print '**************** Splitting page maps in intervals of 5%,10%,15%,... ******************'
for perc in NEWPMRANGE:
    pages = (int)((totalpages*perc)/100)
    print 'creating pagemap of '+str(perc)+'% with number of pages='+str(pages)
    newpagemap=STATIC_PAGE_MAPS+'/'+bench+'_DDR3_'+pagemap+str(perc)+'.csv'
    mstr = mstr + "'" + pagemap + str(perc) + "',"
    print 'new file :'+newpagemap
    fn = open(newpagemap, 'w')
    for line in lines[:pages]:
        fn.write(line)
    fn.close()
print mstr
