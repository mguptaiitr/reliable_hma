## Command help ##
# GENERATE QSUB COMMANDS ##
from sge import *

parser = argparse.ArgumentParser()
parser.add_argument("-pmlist", default='pmfull', help="pmfull, h1, h2" )
#M1 and M2 memory types
parser.add_argument("-m1", default='HBM1GB', help="M1 default is HBM1GB")
parser.add_argument("-m2", default='DDR16GB', help="M2 default is DDR16GB")
#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-interval", default=10000000, help="AVF/Migration Interval length")
parser.add_argument("-mig", default='off', help="Migration ON/OFF")
parser.add_argument("-counters", default='none', help="Counters type HMA, MEA, HMARdWr")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")
parser.add_argument("-meacounters", default=LEGACY_MEA_COUNTERS, help="Number of MEA counters. Maximum number of MEA-based migrations per interval")
parser.add_argument("-initalloc", default='fillm1', help="Specify initial allocation. fillm1 = Fill M1/M2 50 50 until M1 is full. caprato = Fill M1 and M2 in capacity ratio. onlym2 = Fills only M2. M1 only gets migrated pages.")
args = parser.parse_args()

mem1 = args.m1
mem2 = args.m2
pm = args.pmlist

if pm in QPMOPTIONS.keys():
    PAGEMAPS = QPMOPTIONS[pm]
else:
    print pm+' not found in PMOPTION.keys() ='+str(PMOPTIONS.keys())
    sys.exit()

#Create all commands
i=0
cmdlist = []

for bench in QSUBBENCHS:
 for pm in PAGEMAPS:
  i=i+1
  cmd_n_dir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, pm, args, True)
  print '** Command '+str(i)+': '+cmd_n_dir[0]
  cmdlist.append(cmd_n_dir)

#Check if generated qsub commands to be launched
print 'BENCHMAKRS: '+str(QSUBBENCHS)
print 'PAGEMAPS: '+str(PAGEMAPS)
run = raw_input("Issue "+str(len(cmdlist)) +" qsub commands (y/n): ")
if run != 'y':
 sys.exit(0)

#launch all qsub commands
for cmd in cmdlist:
 launch_qsub_cmd(cmd)
