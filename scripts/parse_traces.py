#from func_regx import *
from helper_plot_avf import *
#from graphLib import *

parser = argparse.ArgumentParser()

#Benchmark
parser.add_argument("-bench", default='none', required=False, help="Name of benchmark (astar, cactusADM, ..)")

#pagemap for ploting ipcserpmhma
parser.add_argument("-pagemap", default='none', help="Specify page map to plot SER and IPC for the particular static placement e.g.. LeastAVF, TopAVF, TopWrRd.")

#M1 and M2 memory types
parser.add_argument("-m1", default='HBM1GB', help="M1 default is HBM for normal mode HBM1GB, HBM16GB")
parser.add_argument("-m2", default='DDR16GB', help="M2 default is DDR for normal mode DDR16GB")
#PAGE MAP
#parser.add_argument("-pmlist", default='pmfull', help="PageMap list pmfull, pmh1, pmh2, pmtophot. Extracts simulation information about static page mappings.")
#Counter list
#parser.add_argument("-cntlist", default='cntfull', help="Counter list cntall. Extract simulation information about migrations.")
#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-interval", default=LEGACY_INTERVAL, help="AVF/Migration Interval length 10000000, 100000")
parser.add_argument("-mig", default='off', help="Migration ON/OFF")
parser.add_argument("-counters", default='none', help="Migration type HMA, HMAWrRd, HMAWrWrRd, MEA, ..")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")
parser.add_argument("-meacounters", default=LEGACY_MEA_COUNTERS, help="Number of MEA counters. Maximum number of MEA-based migrations per interval")
parser.add_argument("-initalloc", default='fillm1', help="Specify initial allocation. fillm1 = Fill M1/M2 50 50 until M1 is full. caprato = Fill M1 and M2 in capacity ratio. onlym2 = Fills only M2. M1 only gets migrated pages.")

args = parser.parse_args()

bench = args.bench

#return if no benchmark specified
if bench not in BENCHMARKLIST:
    print 'WRONG!! Benchmark '+bench+' not found!'
    sys.exit()

#Input tracefile
trace = TRACE_PATH+'/'+args.bench+'_cpu_trace/cpu_trace.trc'
print 'Parsing benchmark trace file: '+trace

#maintain three maps
pgMap = {} # pageno : [PC]
pcMap = {} # PC : [pageno]
pcRdWrMap = {} #PC : (#Rd, #Wr, #Rd + #Wr)

#parse trace file and fill three map structures above
f = open(trace, 'r')
nlines = 0
prev_pid = -1
prev_pc = -1
prev_acc = -1

#for line in itertools.islice(f, 1, None, 16):
for line in f:
    nlines = nlines + 1
    data = line.split(' ')
    bubble = int(data[0])
    req_addr = int(data[1])
    req_pc = int(data[2])
    req_pid = int(data[3])
    acc_type = int(data[4])
    req_pageno = req_addr>>12
    #print line.rstrip('\n')
    #print 'line no: '+str(nlines)
    #print 'pageno: '+str(req_pageno)
    
    #update all three maps
    if req_pageno not in pgMap:
        pgMap[req_pageno] = set()
    pgMap[req_pageno].add(req_pc)
    
    if req_pc not in pcMap:
        pcMap[req_pc] = set()
    pcMap[req_pc].add(req_pageno)
    
    if req_pc not in pcRdWrMap:
        pcRdWrMap[req_pc]=[0,0,0]
    if acc_type == 2:
        pcRdWrMap[req_pc][1] = pcRdWrMap[req_pc][1] + 1 #increment write
        pcRdWrMap[req_pc][2] = pcRdWrMap[req_pc][2] + 1 #increment access
    elif acc_type == 1:
        pcRdWrMap[req_pc][0] = pcRdWrMap[req_pc][0] + 1 #increment read
        pcRdWrMap[req_pc][2] = pcRdWrMap[req_pc][2] + 1 #increment access
        if req_pid == prev_pid and req_pc == prev_pc and prev_acc == 2:
            pcRdWrMap[req_pc][1] = pcRdWrMap[req_pc][1] - 1 #decrement write
            pcRdWrMap[req_pc][2] = pcRdWrMap[req_pc][2] - 1 #decrement access
    prev_pc = req_pc
    prev_acc = acc_type
    prev_pid = req_pid


#Process data extract useful information using three maps
benchSetPages, pgdata = plot_pie_hotavf(args, args.bench, False, False)

#Plot program counter histograms of reads, writes, and access
readarr, writearr, accarr = zip(*pcRdWrMap.values())
#GLplot_ProgramCounter_histogram(readarr, bench, 'Read')
#GLplot_ProgramCounter_histogram(writearr, bench, 'Write')
GLplot_ProgramCounter_histogram(accarr, bench, 'Access', 30)

#Generate PC-based page maps 
#PC-based pagemap files
generate_pcbased_pagemap(bench, benchSetPages, pgdata, pcRdWrMap, pcMap, 'PCTopHot')
generate_pcbased_pagemap(bench, benchSetPages, pgdata, pcRdWrMap, pcMap, 'PCTopHotLowRisk')
generate_pcbased_pagemap(bench, benchSetPages, pgdata, pcRdWrMap, pcMap, 'PCTopHotLowRisk75')
generate_pcbased_pagemap(bench, benchSetPages, pgdata, pcRdWrMap, pcMap, 'PCTopWrWrRdRatio50')
generate_pcbased_pagemap(bench, benchSetPages, pgdata, pcRdWrMap, pcMap, 'PCTopWrWrRdRatio25')
generate_pcbased_pagemap(bench, benchSetPages, pgdata, pcRdWrMap, pcMap, 'PCTopWrWrRdRatio50FillM1')
