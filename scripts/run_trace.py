########################################## COMMANDS HELP ########################################
### Normal Mode ###
# python run_trace.py -bench test1MB -normal -m1 DDR16GB
# python run_trace.py -bench test1MB -normal -m1 HBM16GB (16GB HBM without increasing channels.)
### Hybrid Mode ###
# python run_trace.py -bench test1MB -hybrid -m1 HBM1GB -m2 DDR16GB
#Hybrid Mode with static page mapping
# python run_trace.py -bench test1MB -hybrid -m1 HBM1GB -m2 DDR16GB -pm TopHot 
# python run_trace.py -bench test1MB -hybrid -m1 HBM1GB -m2 DDR16GB -pm LeastAVF 
### Hybrid Mode with migrations ###
# python run_trace.py -bench test1MB -hybrid -m1 HBM1GB -m2 DDR16GB -interval 100000 -mig on -counters HMA 
# python run_trace.py -bench test1MB -hybrid -m1 HBM1GB -m2 DDR16GB -interval 100000 -mig on -counters MEA -meacounters 16
### Hybrid Mode with migiration and initial allocation ###
# python run_trace.py -bench test1MB -hybrid -m1 HBM1GB -m2 DDR16GB -interval 100000 -mig on -counters HMA -initalloc onlym2
### Hybrid Mode with migrations to unallocated m1 memory and initial allocation (starting allocation) as onlym2 ###
# python run_trace.py -bench test1MB -hybrid -m1 HBM1GB -m2 DDR16GB -interval 100000 -mig on -counters HMAUnAlloc -initalloc onlym2
### Hybrid Mode with migrations to unallocated m1 memory and initial allocation as fill m1 as much as possible ###
# python run_trace.py -bench test1MB -hybrid -m1 HBM1GB -m2 DDR16GB -interval 100000 -mig on -counters HMAUnAlloc -initalloc fillm1
################################################################################################

from func_regx import *
parser = argparse.ArgumentParser()
parser.add_argument("-bench", required=True, help="Name of benchmark (astar, cactusADM, ..)")

#MODE
mode = parser.add_mutually_exclusive_group(required=True)
mode.add_argument('-normal', action="store_true")
mode.add_argument('-hybrid', action="store_true")

#M1 and M2 memory types
parser.add_argument("-m1", default='none', help="M1 default is DDR16GB for normal mode and HBM1GB for hybrid mode")
parser.add_argument("-m2", default='none', help="M2 default is DDR for normal mode DDR16GB")

#PAGE MAP
parser.add_argument("-pm", default='none', help="Static Page map options "+str(PAGEMAPFULL))

#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-interval", default=10000000, help="AVF/Migration Interval length")
parser.add_argument("-mig", default='off', help="Migration ON/OFF")
parser.add_argument("-counters", default='none', help="Migration type HMA, HMAWrRd, HMAWrWrRd, MEA, MEAWr2Rd1, MEAupHMAdown, HMAUnAlloc, FullCounters, FullCounters75")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")
parser.add_argument("-meacounters", default='64', help="Number of MEA counters. Maximum number of MEA-based migrations per interval")
parser.add_argument("-initalloc", default='fillm1', help="Specify initial allocation. fillm1, capratio, onlym2. fillm1 = Fill M1/M2 50 50 until M1 is full. caprato = Fill M1 and M2 in capacity ratio. onlym2 = Fills only M2. M1 only gets migrated pages.")

args = parser.parse_args()

bench = args.bench
normal = args.normal
hybrid = args.hybrid
mem1 = args.m1
mem2 = args.m2
pagemap = args.pm


#Inputs
print 'bench: '+str(bench)
print 'normal: '+str(normal)
print 'hybrid: '+str(hybrid)
print 'mem1: '+str(mem1)
print 'mem2: '+str(mem2)
print 'pagemap: '+str(pagemap)

if normal:
    mode='normal'
else:
    mode='hybrid'

#Create the command string and get the directory to put the results for the command
cmd, expdir = create_avf_outputdir_ret_cmd(bench, mode, mem1, mem2, pagemap, args, True)

print '**************** Ramulator Simulation ******************'
cmd_n_dir = [cmd, expdir]

now1= datetime.datetime.now()
print 'Start Time: '+str(now1.strftime("%H:%M Start Date %m-%d-%y"))

#launch simulation
launch_single_thread(cmd_n_dir)

now2 = datetime.datetime.now()
print 'End Time: '+str(now2.strftime("%H:%M End Date %m-%d-%y"))
delta = now2 - now1
print 'Time taken by the ramulator simulation (minutes): '+str(round(delta.total_seconds()/60.0, 2))
