## Command help ##
# 1) python run_trace_multi.py -bench test1MB -m1 HBM1GB -m2 DDR16GB
# 2) python run_trace_multi.py -bench test1MB -m1 HBM1GB -m2 DDR16GB -pmlist pmfull
# 3) python run_trace_multi.py -bench test1MB -m1 HBM1GB -m2 DDR16GB -pmlist pmhot

import commands, time, shutil, operator
import sys, os, subprocess, re, datetime 
import multiprocessing
import subprocess
import argparse
from func_regx import *

#FILL BENCHMARKS to RUN in BENCHS
#BENCHS = ['GemsFDTD', 'lulesh', 'omnetpp', 'milc']
#BENCHS = ['xalancbmk', 'zeusmp', 'xsbench']
BENCHS = ['astar', 'cactusADM', 'xalancbmk']

cmdlist=[]
#Create all commands
i=0
for bench in BENCHS:
 i=i+1
 cmd_n_dir = create_avf_outputdir_ret_cmd(bench, 'normal', 'DDR16GB', 'none','none')
 print '** Command '+str(i)+': '+cmd_n_dir[0]
 cmdlist.append(cmd_n_dir)

num_cpu = multiprocessing.cpu_count()
print '** Number of commands: ' + str(len(cmdlist))
print '** Number of cpu: ' + str(num_cpu)
run = raw_input('Launch MultiThread Runs (y/n): ')
if run != 'y':
    sys.exit(0)

pool = multiprocessing.Pool(num_cpu-2)
results = []
r = pool.map_async(launch_single_thread, cmdlist, callback=results.append)
r.wait()
print '***** ALL DONE ALL '+ str(len(cmdlist)) +' Commands *****'
