## Command help ##
# GENERATE QSUB COMMANDS ##
from sge import *

parser = argparse.ArgumentParser()
#M1 and M2 memory types
parser.add_argument("-m1", default='DDR16GB', help="M1 default is DDR16GB")
parser.add_argument("-m2", default='none', help="M2 default none")

#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-interval", default=10000000, help="AVF/Migration Interval length")
parser.add_argument("-mig", default='off', help="Migration ON/OFF")
parser.add_argument("-counters", default='none', help="Counters type HMA, MEA, HMARdWr")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")
parser.add_argument("-meacounters", default=LEGACY_MEA_COUNTERS, help="Number of MEA counters. Maximum number of MEA-based migrations per interval")
parser.add_argument("-initalloc", default='fillm1', help="Specify initial allocation. fillm1 = Fill M1/M2 50 50 until M1 is full. caprato = Fill M1 and M2 in capacity ratio. onlym2 = Fills only M2. M1 only gets migrated pages.")
args = parser.parse_args()

mem1 = args.m1
mem2 = args.m2


#Create all commands
i=0
cmdlist = []

for bench in QSUBBENCHS:
 i=i+1
 cmd_n_dir = create_avf_outputdir_ret_cmd(bench, 'normal', mem1, mem2, 'none', args, True)
 print '** Command '+str(i)+': '+cmd_n_dir[0]
 print ' Directory: '+cmd_n_dir[1]
 cmdlist.append(cmd_n_dir)

#Check if generated qsub commands to be launched
print 'BENCHMARKS: '+str(QSUBBENCHS)
run = raw_input("Issue "+str(len(cmdlist)) + " qsub commands (y/n): ")
if run != 'y':
 sys.exit(0)

#launch all qsub commands
for cmd in cmdlist:
 launch_qsub_cmd(cmd)
