## Command help ##
# GENERATE QSUB  ##
#python qsub_bench_migration.py -cntlist cnttemp -initalloc topwrwrrdratiocapratio -meacounters 64

from sge import *

parser = argparse.ArgumentParser()
#Migration types
parser.add_argument("-cntlist", default='cnttemp', help="List of counter types to launch in this batch e.g. cntfull, cnttemp, cntrel, cntperf" )
#M1 and M2 memory types
parser.add_argument("-m1", default='HBM1GB', help="M1 default is HBM for normal mode HBM1GB, HBM16GB")
parser.add_argument("-m2", default='DDR16GB', help="M2 default is DDR for normal mode DDR16GB")
#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-interval", default=10000000, help="AVF/Migration Interval length")
parser.add_argument("-mig", default='on', help="Migration ON/OFF")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")
parser.add_argument("-meacounters", default=LEGACY_MEA_COUNTERS, help="Number of MEA counters. Maximum number of MEA-based migrations per interval")
parser.add_argument("-initalloc", default='onlym2', help="Specify initial allocation. fillm1 = Fill M1/M2 50 50 until M1 is full. caprato = Fill M1 and M2 in capacity ratio. onlym2 = Fills only M2. M1 only gets migrated pages. tophotfillm1 = pin top hot pages to M1 until M1 is full. tophotcapratio = pint top hot pages to M1 in capacity ratio of memory")
args = parser.parse_args()

mem1 = args.m1
mem2 = args.m2
cntlist = args.cntlist

if cntlist in QCNTOPTIONS.keys():
    COUNTERS = QCNTOPTIONS[cntlist]
else:
    print cntlist+' not found in CNTOPTIONS.keys() ='+str(QCNTOPTIONS.keys())
    sys.exit()

#Create all commands
i=0
cmdlist=[]
for bench in QSUBBENCHS:
 for counter in COUNTERS:
  i=i+1
  args.counters = counter
  args.interval = INTERVAL_MAP[counter]
  cmd_n_dir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args, True)
  print '** Command '+str(i)+': '+cmd_n_dir[0]
  cmdlist.append(cmd_n_dir)

#Check if generated qsub commands to be launched
print 'BENCHMAKRS: '+str(QSUBBENCHS)
print 'COUNTERS: '+str(COUNTERS)
run = raw_input("Issue "+str(len(cmdlist)) +" qsub commands (y/n): ")
if run != 'y':
 sys.exit(0)

#launch all qsub commands
for cmd in cmdlist:
 launch_qsub_cmd(cmd)
