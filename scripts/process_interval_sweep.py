####### COMMANDS HELP ############
# 1) python process_interval_sweep.py -bench test1MB 
# 2) python process_interval_sweep.py -bench test1MB -intlist intfull -cntlist cntfull

from func_regx import *

parser = argparse.ArgumentParser()
parser.add_argument("-bench", required=True, help="Run benchmark")

#M1 and M2 memory types
parser.add_argument("-m1", default='HBM1GB', help="M1 default is HBM for normal mode HBM1GB, HBM16GB")
parser.add_argument("-m2", default='DDR16GB', help="M2 default is DDR for normal mode DDR16GB")

#Interval list
parser.add_argument("-intlist", default='intfull', help="Interval sweep list")

#Page list
parser.add_argument("-pmlist", default='pmfull', help="PageMap list pmfull, pmh1, pmh2, pmtophot.")

#Counter list
parser.add_argument("-cntlist", default='cntfull', help="Counter list cntall. Extract simulation information about migrations.")

#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-mig", default='on', help="Migration ON/OFF")
parser.add_argument("-interval", default=10000000, help="Setting args.interval for normal and static placement extraction")
parser.add_argument("-counters", default='none', help="Migration type HMA, HMAWrRd, HMAWrWrRd, MEA, ..")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")
parser.add_argument("-meacounters", default='128', help="Number of MEA counters. Maximum number of MEA-based migrations per interval")
parser.add_argument("-fillm1complete", default='false', help="true = Fill M1 complete, false = Fill M1 and M2 in their capacity ratio")
args = parser.parse_args()

bench = args.bench
mem1 = args.m1
mem2 = args.m2
pmlist = args.pmlist
intlist = args.intlist
cntlist = args.cntlist

if intlist in INTOPTIONS.keys():
    INTERVALS = INTOPTIONS[intlist]
else:
    print intlist+' not found in INTOPTION.keys() ='+str(INTOPTIONS.keys())
    sys.exit()

if pmlist in PMOPTIONS.keys():
    PAGEMAPS = PMOPTIONS[pmlist]
else:
    print pmlist+' not found in PMOPTIONS.keys() ='+str(PMOPTIONS.keys())

if cntlist in CNTOPTIONS.keys():
    COUNTERS = CNTOPTIONS[cntlist]
else:
    print cntlist+' not found in CNTOPTION.keys() ='+str(CNTOPTIONS.keys())
    sys.exit()

if bench not in BENCHMARKLIST:
    print bench +" not in "+str(BENCHMARKLIST)
    sys.exit()

#INPUTS
print 'bench: '+str(bench)
print 'mem1: '+str(mem1)
print 'mem2: '+str(mem2)
print 'INTERVAL LIST: '+str(INTERVALS)
print 'PAGEMAP LIST: '+str(PAGEMAPS)
print 'COUNTER LIST: '+str(COUNTERS)

benchsum = M2AVF_OUTPUT+'/'+bench+'/'+bench+'_interval_sweep'+'.csv'
f = open(benchsum,'w')
f.write('Page Placement, Interval Length, #Intervals, #Migrations, IPC, AVF, SER, \
	AVF_HBM, AVF_DDR, SER_HBM(Week 0), SER_DDR(Week 0), \
	n_HBM, n_rd_HBM, n_wr_HBM, n_DDR, n_rd_DDR, n_wr_DDR, \
	Hit Rate, Sanity Check(n_hbm+n_ddr), Retired Instructions, clk,\
    M1 pages, M2 pages, M1+M2 pages\n')

#Normal data
args.mig = 'off'
args.counters = 'none'
cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'normal', 'DDR16GB', 'none', 'none', args)
print '-> Only DDR 16GB (All pages in DDR) in directory = ' + str(expdir)
print os.listdir(expdir)
data = get_output_data_for_norm_exp(expdir)
f.write('Only DDR 16GB'+','+str(data['interval'])+','+str(data['n_interval'])+','+str(0)+','+str(data['ipc'])+','+str(0+data['avf_ddr'])+','+str(0+data['ser_ddr'])\
        +','+str(0)+','+str(data['avf_ddr'])+','+str(0)+','+str(data['ser_ddr'])\
        +','+str(0)+','+str(0)+','+str(0)\
        +','+str(data['n_ddr'])+','+str(data['n_rd_ddr'])+','+str(data['n_wr_ddr'])\
        +','+str(0.0)+','+str(0 + data['n_ddr'])+','+str(data['retired_inst'])+','+str(data['clk'])\
        +','+str(data['pages_m1'])+','+str(0)+','+str(data['pages_m1'])+'\n')
f.write('\n')

#Static Hybrid data
f.write('Static Page Placement for HMA\n')
args.mig = 'off'
args.counters = 'none'
for pagemap in PAGEMAPS:
    cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, pagemap, args)
    print '-> Hybrid mode with static page maps = ('+pagemap+') in directory = ' + str(expdir)
    print os.listdir(expdir)
    data = get_output_data_for_hyb_exp(expdir)
    f.write(pagemap+','+str(data['interval'])+','+str(data['n_interval'])+','+str(data['n_mig'])+','+str(data['ipc'])+','+str(data['avf_hbm'] + data['avf_ddr'])+','+str(data['ser_hbm']+data['ser_ddr'])\
        +','+str(data['avf_hbm'])+','+str(data['avf_ddr'])+','+str(data['ser_hbm'])+','+str(data['ser_ddr'])\
        +','+str(data['n_hbm'])+','+str(data['n_rd_hbm'])+','+str(data['n_wr_hbm'])\
        +','+str(data['n_ddr'])+','+str(data['n_rd_ddr'])+','+str(data['n_wr_ddr'])\
        +','+str(float(data['n_hbm'])/float(data['n_hbm']+data['n_ddr']))+','+str(data['n_hbm']+data['n_ddr'])+','+str(data['retired_inst'])+','+str(data['clk'])\
        +','+str(data['pages_m1'])+','+str(data['pages_m2'])+','+str(data['pages_m1_m2'])+'\n')
f.write('\n')

#Migration Hybrid data
f.write('Migration for HMA\n')
args.mig = 'on'
for counter in COUNTERS:
    args.counters = counter
    for interval in INTERVALS:
        args.interval = interval
        cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'hybrid', mem1, mem2, 'none', args)
        print '-> Hybrid mode with migration = ('+counter+') and interval = ('+str(interval)+') in directory = ' + str(expdir)
        print os.listdir(expdir)
        data = get_output_data_for_hyb_exp(expdir)
        f.write(counter+','+str(data['interval'])+','+str(data['n_interval'])+','+str(data['n_mig'])+','+str(data['ipc'])+','+str(data['avf_hbm'] + data['avf_ddr'])+','+str(data['ser_hbm']+data['ser_ddr'])\
            +','+str(data['avf_hbm'])+','+str(data['avf_ddr'])+','+str(data['ser_hbm'])+','+str(data['ser_ddr'])\
            +','+str(data['n_hbm'])+','+str(data['n_rd_hbm'])+','+str(data['n_wr_hbm'])\
            +','+str(data['n_ddr'])+','+str(data['n_rd_ddr'])+','+str(data['n_wr_ddr'])\
            +','+str(float(data['n_hbm'])/float(data['n_hbm']+data['n_ddr']))+','+str(data['n_hbm']+data['n_ddr'])+','+str(data['retired_inst'])+','+str(data['clk'])\
            +','+str(data['pages_m1'])+','+str(data['pages_m2'])+','+str(data['pages_m1_m2'])+'\n')
    f.write('\n')
f.close()
