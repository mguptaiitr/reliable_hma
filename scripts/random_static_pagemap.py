####### COMMANDS HELP ############
# 1) python run_trace.py -bench test1MB
from func_regx import *
parser = argparse.ArgumentParser()

parser.add_argument("-bench", required=True, help="Run benchmark")

#M1 and M2 memory types
parser.add_argument("-m1", default='DDR16GB', help="M1 default is DDR16GB for normal mode and HBM1GB for hybrid mode")
parser.add_argument("-m2", default='none', help="M2 default is DDR for normal mode DDR16GB")

#PAGE MAP
parser.add_argument("-pm", default='none', help="Static Page map options "+str(PAGEMAPFULL))

#Command line arguments. Overwriting config file data.
parser.add_argument("-avf", default='on', help="AVF ON/OFF")
parser.add_argument("-interval", default=10000000, help="AVF/Migration Interval length")
parser.add_argument("-mig", default='off', help="Migration ON/OFF")
parser.add_argument("-counters", default='none', help="Migration type HMA, HMAWrRd, HMAWrWrRd, MEA, MEAWr2Rd1,..")
parser.add_argument("-smartwr", default='false', help="smart wr true/false")
parser.add_argument("-meacounters", default='128', help="Number of MEA counters. Maximum number of MEA-based migrations per interval")
parser.add_argument("-fillm1complete", default='false', help="true = Fill M1 complete, false = Fill M1 and M2 in their capacity ratio")

args = parser.parse_args()

bench = args.bench
mem1 = args.m1
mem2 = args.m2
pagemap = args.pm

#Inputs
print 'bench: '+str(bench)
print 'mem1: '+str(mem1)
print 'mem2: '+str(mem2)
print 'pagemap: '+str(pagemap)

cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'normal', 'DDR16GB', 'none', pagemap, args)
staticpagemap = expdir+'/'+bench+'_FinalPageWiseHottnessAVF.csv'
print 'Randomizing static page map: '+str(staticpagemap)

if not os.path.exists(staticpagemap):
    print 'NOT FOUND '+staticpagemap
    print 'Contenct of '+expdir+'/.. :'+str(os.listdir(expdir+'/..'))
    sys.exit()

f = open(staticpagemap, 'r')
lines = f.readlines()
f.close()

idx1 = lines.index('PageAVF Data Starts\n')
idx2 = lines.index('PageAVF Data Ends\n')
print 'First page: '+str(lines[idx1+1])
print 'Last page: '+str(lines[idx2])
lines = lines[idx1+1:idx2]

totalpages = len(lines)
print 'Total pages in the original pagemap: '+str(totalpages)

x = [i for i in range(totalpages)]
shuffle(x)
#print 'Randomized Pages:'+ str(x)

perc_pattern = re.compile('Random(\d+)')
for randPageMap in PAGEMAPRANDOM:
    newpagemap=STATIC_PAGE_MAPS+'/'+bench+'_DDR3_'+randPageMap+'.csv'
    perc = int(get_pattern_from_output(randPageMap, perc_pattern))
    num = int(totalpages*perc/100)
    print 'Generating pagemap with pages: '+str(num) +' perc: '+str(perc)
    fn = open(newpagemap, 'w')
    fn.write(str(totalpages)+'\n')
    for i in x[:num]:
        fn.write(lines[i])
    fn.close()
