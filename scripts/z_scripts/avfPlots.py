from graphLib import *
from func_regx import *

parser = argparse.ArgumentParser()
parser.add_argument("-bench", help="Run benchmark (all, mcf, cactus, mix5, )", required=True)
parser.add_argument("-cache", help="Run benchmark (L1_L2, 32KB_1MB)", default='32KB_1MB')
args = parser.parse_args()


#Read AVF Output Files
AVF_BENCH_FILE=AVF_OUTPUT+args.bench+'_'+args.cache+'.avf'


print 'Opening AVF Output File'
print AVF_BENCH_FILE

f = open(AVF_BENCH_FILE, 'r')
avf_data = f.read().splitlines()
f.close()

ind1 = avf_data.index('Page Hotness and AVF at cache level 1')
ind2 = avf_data.index('Page Hotness and AVF at cache level 0')
ind3 = avf_data.index('Simulation done. Statistics written to DDR3.stats')

print avf_data[ind1]
print avf_data[ind2]
print avf_data[ind3]

avf_l1_data = get_avf_data(avf_data, ind2, ind3)
avf_l1_data  = OrderedDict(sorted(avf_l1_data.items(), key=lambda x: x[1][1], reverse=True))


avf_l2_data = get_avf_data(avf_data, ind1, ind2)
avf_l2_data  = OrderedDict(sorted(avf_l2_data.items(), key=lambda x: x[1][1], reverse=True))

#print avf_l1_data.items()[:100]
#print avf_l1_data

#plot_avf_hottness(avf_l1_data, args.bench)
plot_avf_hottness(avf_l2_data, args.bench)
