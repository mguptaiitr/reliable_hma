####### COMMANDS HELP ############
# 1) python process_plot_trace.py -bench test1MB -m1 HBM1GB -m2 DDR16GB -pmlist pmfull
# 2) python process_plot_trace.py -bench test1MB -m1 HBM1GB -m2 DDR16GB -pmlist pmhot

from func_regx import *
from graphLib import *

parser = argparse.ArgumentParser()
parser.add_argument("-bench", required=True, help="Run benchmark")

#M1 and M2 memory types
parser.add_argument("-m1", default='DDR16GB', help="M1 default is HBM for normal mode HBM1GB, HBM16GB")

args = parser.parse_args()
bench = args.bench
mem1 = args.m1

#Inputs
print 'bench: '+str(bench)
print 'mem1: '+str(mem1)

#Read the checkpoint file
cmd, expdir = create_avf_outputdir_ret_cmd(bench, 'normal', 'DDR16GB', 'none', 'none')
chk_all_page_file = expdir+'/'+bench+'_DDR3_CheckPointPageWiseAVF.csv'
if not os.path.exists(chk_all_page_file):
    print str(chk_all_page_file) + 'Not in path'

f = open(chk_all_page_file,'r')
lines = f.read().splitlines()
f.close()

#process data from file into python datastrcutures
chkno = -1
for line in lines:
    if 'Checkpoint No' in line:
        chkno = int(line.split(',')[1])
        g_chks.append(chkno)
        print 'New checkpoint no '+str(chkno)
        continue
    pa = int(line.split(',')[0])
    avf = float(line.split(',')[1])
    g_pages.add(pa) 
    g_avfdata[(chkno, pa)]=avf

g_pages = sorted(g_pages)

print 'Number of lines in the file: ' +str(len(lines))
print 'Number checkpoints: '+str(g_chks)
print 'Number of pages: '+str(len(g_pages))
#print 'Checkpoints: '+str(g_chks)
#print 'Pages: '+str(g_pages)

plotname=expdir+'/'+bench+'_avf_time_page_3d.pdf'
plot_avf_chk_pages(plotname)
#print avfdata
