from graphLib import *
from func_regx import *

parser = argparse.ArgumentParser()
parser.add_argument("-bench", help="Run benchmark (all, mcf, cactus, mix5, )", required=True)
args = parser.parse_args()

#Read AVF Output Files
AVF_BENCH_PAGE_FILE = MAVF_OUTPUT+args.bench+'_page_avf'+'.dat'
AVF_BENCH_CHECKPOINT_FILE = MAVF_OUTPUT+args.bench+'_checkpoints'+'.dat'

print 'Opening AVF Output File'
print AVF_BENCH_PAGE_FILE

f = open(AVF_BENCH_PAGE_FILE, 'r')
avf_page_data = f.read().splitlines()
f.close()

allpage1 = avf_page_data.index('PageAVF Data Starts')
allpage2 = avf_page_data.index('PageAVF Data Ends')

tophot1 = avf_page_data.index('* Top N Hot Pages Starts')
tophot2 = avf_page_data.index('* Top N Hot Pages Ends')
leasthot1 = avf_page_data.index('* Least N Hot Pages Starts')
leasthot2 = avf_page_data.index('* Least N Hot Pages Ends')

topavf1 = avf_page_data.index('* Top N AVF Pages Starts')
topavf2 = avf_page_data.index('* Top N AVF Pages Ends')
leastavf1 = avf_page_data.index('* Least N AVF Pages Starts')
leastavf2 = avf_page_data.index('* Least N AVF Pages Ends')

#Test Prints
print avf_page_data[allpage1]
print avf_page_data[allpage2]

print avf_page_data[tophot1]
print avf_page_data[tophot2]

print avf_page_data[leasthot1]
print avf_page_data[leasthot1]

avf_mem = get_mem_avf_data(avf_page_data, tophot1, tophot2)
avf_mem = OrderedDict(sorted(avf_mem.items(), key=lambda x: x[1][6], reverse=True))

sub_plot_mavf_hottness(avf_mem, args.bench)

#print avf_mem

#sort avf data beased on page hotness.
#avf_l1_data  = OrderedDict(sorted(avf_l1_data.items(), key=lambda x: x[1][1], reverse=True))

#avf_l2_data = get_avf_data(avf_data, ind1, ind2)
#avf_l2_data  = OrderedDict(sorted(avf_l2_data.items(), key=lambda x: x[1][1], reverse=True))

#print avf_l1_data.items()[:100]
#print avf_l1_data

#plot_avf_hottness(avf_l1_data, args.bench)
#plot_avf_hottness(avf_l2_data, args.bench)
